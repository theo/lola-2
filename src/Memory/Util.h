/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Utility functions and other things to implement stuff from newer C++ standards with strict
compiler requirements
*/

#ifndef SRC_MEMORY_UTIL_H
#define SRC_MEMORY_UTIL_H

#include <cstddef>
#include <memory>
#include <ranges>
#include <type_traits>

namespace lola
{

/**
 * @brief GSL narrow_cast -> searchable way for narrowing static_casts
 *
 * @tparam T
 * @tparam U
 * @param u
 * @return constexpr T
 */
template <typename T, typename U>
constexpr T narrow_cast(U&& u) noexcept
{
    return static_cast<T>(std::forward<U>(u));
}

/**
 * @brief Simple helper to generate an iota view bounded to a given size. deduces type from argument
 * and initializes counter properly with static_cast<Size>(0) (just 0 would default to int)
 *
 * @tparam Size Type of counter variable
 * @param size bound
 * @return decltype(auto) iota view
 */
template <std::integral Size>
decltype(auto) counter(Size size)
{
    return std::views::iota(static_cast<Size>(0), size);
}

namespace memory
{

/**
 * @brief Implementation for std::make_unique_for_overwrite, since clang only supports this after
 * clang 16
 *
 * @tparam T
 */
template <typename T>
requires(!std::is_array_v<T>)
auto make_unique() -> std::unique_ptr<T>
{
    return std::unique_ptr<T>(new T);
}
/**
 * @brief Overload for array types
 *
 * @tparam T
 * @param n
 * @return std::unique_ptr<T>
 */
template <typename T>
requires std::is_unbounded_array_v<T>
auto make_unique(std::size_t n) -> std::unique_ptr<T>
{
    return std::unique_ptr<T>(new std::remove_extent_t<T>[n]);
}

/**
 * @brief Construction of arrays with known bound disallowed (use std::array...)
 *
 * @tparam T
 * @tparam Args
 * @return requires
 */
template <typename T, typename... Args>
requires std::is_bounded_array_v<T>
void make_unique(Args&&...) = delete;
}  // namespace memory
}  // namespace lola

#endif /* SRC_MEMORY_UTIL_H */
