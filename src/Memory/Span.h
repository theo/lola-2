/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Definitions of multidimensional views for 1D-Arrays. Basically std::mdspan for 2D-row
matrices, 3D arrays of such matrices and 4D arrays of such 3D arrays.
*/

#ifndef SRC_MEMORY_SPAN_H
#define SRC_MEMORY_SPAN_H

#include <concepts>
#include <cassert>
#include <cstddef>
#include <span>
#include <type_traits>

namespace lola
{
namespace memory
{
namespace detail
{

// forward declaration so that we can specialize the empty extents case
template <typename IndexType, std::size_t... Sizes>
class extents;

// specialization for a 0-dimensional extent, comparison basically always returns true, subextents
// is always *this
template <typename IndexType>
class extents<IndexType>
{
public:
    using index_type = IndexType;
    using size_type = std::make_unsigned_t<index_type>;
    using rank_type = std::size_t;

    constexpr extents() noexcept = default;
    // TODO: the recursive constructors for higher dimensions with extents from a std::span call
    // this with a 0-sized span. Is this undefined behavior?
    template <typename SizeType, std::size_t N>
    constexpr extents(std::span<SizeType, N>) noexcept
    {
    }
    template <typename SizeType, std::size_t N>
    constexpr extents(std::array<SizeType, N>) noexcept
    {
    }

    template <std::size_t... other_sizes>
    friend constexpr auto
    operator==(const extents&, const extents<IndexType, other_sizes...>&) noexcept -> bool
    {
        if constexpr (sizeof...(other_sizes) != 0)
        {
            return false;
        }
        return true;
    }

    template <IndexType I>
    constexpr auto subextents() const noexcept
    {
        return *this;
    }

    static constexpr auto rank() noexcept -> rank_type { return 0; }
    static constexpr auto rank_dynamic() noexcept -> rank_type { return 0; }
};

/**
 * @brief Our (almost standard conforming) implementation of std::extents. Uses a recursive template
 * to store dynamic extents. The following things differ from the standard:
 * - Constructor extents(const extents<OtherIndexType, OtherExtents...>&) is missing (TODO: could be
 * done)
 *
 * - extent(...) and static_extent(...) member function take the index by template parameter (TODO:
 * could probably be changed)
 *
 * - Making extent(...) and static_extent(...) take runtime index values results in
 * O(sizeof...(dims)+1) accessing performance instead of O(1), because of recursive descend through
 * the subobjects (TODO: can be done, but only after changing the way the storage is implemented,
 * probably like llvm-18 does it)
 *
 * - subextents<I>() template member function added to copy a subobject, allowing easy creation of
 * extents<b,c> from extents<a,b,c> or similar. This benefits from the recursive template
 * implementation.
 *
 * @tparam IndexType
 * @tparam dim
 * @tparam dims
 */
template <typename IndexType, std::size_t dim, std::size_t... dims>
class extents<IndexType, dim, dims...> : extents<IndexType, dims...>
{
public:
    using index_type = IndexType;
    using size_type = std::make_unsigned_t<index_type>;
    using rank_type = std::size_t;

private:
    static constexpr rank_type rank_internal = sizeof...(dims) + 1;
    static constexpr rank_type rank_dynamic_internal = ((dims == std::dynamic_extent) + ... + 0);

public:
    static constexpr auto rank() noexcept -> rank_type { return rank_internal; }
    static constexpr auto rank_dynamic() noexcept -> rank_type { return rank_dynamic_internal; }

    constexpr extents() = default;

    template <typename SizeType, typename... SizeTypes>
    constexpr explicit extents(SizeType size, SizeTypes... sizes) noexcept requires(
        (std::is_convertible_v<SizeTypes, index_type> && ...
         && std::is_convertible_v<SizeType, index_type>)
        && (std::is_nothrow_constructible_v<index_type, SizeTypes> && ...
            && std::is_nothrow_constructible_v<index_type, SizeType>)
        && (sizeof...(SizeTypes) + 1 == rank() || sizeof...(SizeTypes) + 1 == rank_dynamic())
    )  // subobject initialization through immediately evaluated lambda to use if constexpr for
       // return expression
        : extents<index_type, dims...>(
              [&]()
              {
                  if constexpr (sizeof...(SizeTypes) + 1 == rank())
                  {
                      // if the user passes values for all extents, only pass remaining
                      return extents<index_type, dims...>(static_cast<index_type>(sizes)...);
                  }
                  else
                  {
                      // if the user passes values for just dynamic extents, pass all
                      return extents<index_type, dims...>(
                          static_cast<index_type>(size), static_cast<index_type>(sizes)...
                      );
                  }
              }()
          )
    {
    }

    template <typename SizeType, std::size_t N>
    constexpr explicit(N != rank_dynamic()) extents(std::span<SizeType, N> data) noexcept requires(
        std::is_convertible_v<const SizeType&, index_type>
        && std::is_nothrow_constructible_v<index_type, const SizeType&>
        && (N == rank_dynamic() || N == rank())
    )
        : extents<index_type, dims...>(
              [data]()
              {
                  if constexpr (N == 1)
                  {
                      // for 1-dimensional case, the subobject is the empty 0-dimensional base
                      return extents<index_type>{};
                  }
                  // similar to above, pass just remaining or all values
                  else if constexpr (N == rank())
                  {
                      return extents<index_type, dims...>(data.template last<N - 1>());
                  }
                  else
                  {
                      return extents<index_type, dims...>(data);
                  }
              }()
          )
    {
    }

    template <typename SizeType, std::size_t N>
    constexpr explicit(N != rank_dynamic()) extents(const std::array<SizeType, N>& data) noexcept
        requires(
            std::is_convertible_v<const SizeType&, index_type>
            && std::is_nothrow_constructible_v<index_type, const SizeType&>
            && (N == rank_dynamic() || N == rank())
        )  // delegate to span-based constructor
        : extents(std::span(data))
    {
    }

    template <index_type I>
    static constexpr auto static_extent() noexcept -> std::size_t requires(I < rank())
    {
        if constexpr (I > 0)
        {
            return extents<index_type, dims...>::template static_extent<I - 1>();
        }
        else
        {
            return dim;
        }
    }

    template <index_type I>
    constexpr auto extent() const noexcept -> index_type requires(I < rank())
    {
        if constexpr (I > 0)
        {
            return static_cast<extents<index_type, dims...>>(*this).template extent<I - 1>();
        }
        else
        {
            return dim;
        }
    }

    template <index_type I>
    constexpr auto subextents() const noexcept requires(I <= rank())
    {
        if constexpr (I > 0)
        {
            return static_cast<extents<index_type, dims...>>(*this).template subextents<I - 1>();
        }
        else
        {
            return *this;
        }
    }

    template <std::size_t... other_sizes>
    friend constexpr auto operator==(
        const extents& lhs, const extents<index_type, other_sizes...>& rhs
    ) noexcept -> bool
    {
        if constexpr (rank() != sizeof...(other_sizes))
        {
            return false;
        }
        else
        {
            return (lhs.template extent<0>() == rhs.template extent<0>())
                && (lhs.template subextents<1>() == rhs.template subextents<1>());
        }
    }
};

// specialization for dynamic extent. basically the same as above, with some changes
template <typename IndexType, std::size_t... dims>
class extents<IndexType, std::dynamic_extent, dims...> : extents<IndexType, dims...>
{
public:
    using index_type = IndexType;
    using size_type = std::make_unsigned_t<index_type>;
    using rank_type = std::size_t;

private:
    static constexpr rank_type rank_internal = sizeof...(dims) + 1;
    static constexpr rank_type rank_dynamic_internal = ((dims == std::dynamic_extent) + ... + 1);
    // need to store runtime size
    index_type size;

public:
    static constexpr auto rank() noexcept -> rank_type { return rank_internal; }
    static constexpr auto rank_dynamic() noexcept -> rank_type { return rank_dynamic_internal; }

    constexpr extents() = default;

    template <typename SizeType, typename... SizeTypes>
    constexpr explicit extents(SizeType size, SizeTypes... sizes) noexcept
        requires((std::is_convertible_v<SizeTypes, index_type> && ...
                  && std::is_convertible_v<SizeType, index_type>)
                 && (std::is_nothrow_constructible_v<index_type, SizeTypes> && ...
                     && std::is_nothrow_constructible_v<index_type, SizeType>)
                 && (sizeof...(SizeTypes) + 1 == rank()
                     || sizeof...(SizeTypes) + 1 == rank_dynamic()))
        // no need to check, we always pass just the remainder to the subobject
        : extents<IndexType, dims...>(static_cast<index_type>(sizes)...),
          size{static_cast<index_type>(size)}
    {
    }

    template <typename SizeType, std::size_t N>
    constexpr explicit(N != rank_dynamic()) extents(std::span<SizeType, N> data) noexcept
        requires(std::is_convertible_v<const SizeType&, index_type>
                 && std::is_nothrow_constructible_v<index_type, const SizeType&>
                 && (N == rank_dynamic() || N == rank()))
        : extents<IndexType, dims...>(data.template last<N - 1>()),
          size{static_cast<index_type>(data.front())}
    {
    }

    template <typename SizeType, std::size_t N>
    constexpr explicit(N != rank_dynamic()) extents(const std::array<SizeType, N>& data) noexcept
        requires(
            std::is_convertible_v<const SizeType&, index_type>
            && std::is_nothrow_constructible_v<index_type, const SizeType&>
            && (N == rank_dynamic() || N == rank())
        )
        : extents(std::span(data))
    {
    }

    template <size_t I>
    static constexpr auto static_extent() noexcept -> std::size_t requires(I < rank())
    {
        if constexpr (I > 0)
        {
            return extents<index_type, dims...>::template static_extent<I - 1>();
        }
        else
        {
            return std::dynamic_extent;
        }
    }

    template <size_t I>
    constexpr auto extent() const noexcept -> index_type requires(I < rank())
    {
        if constexpr (I > 0)
        {
            return static_cast<extents<index_type, dims...>>(*this).template extent<I - 1>();
        }
        else
        {
            return size;
        }
    }

    template <index_type I>
    constexpr auto subextents() const noexcept requires(I <= rank())
    {
        if constexpr (I > 0)
        {
            return static_cast<extents<index_type, dims...>>(*this).template subextents<I - 1>();
        }
        else
        {
            return *this;
        }
    }

    template <std::size_t... other_sizes>
    friend constexpr auto operator==(
        const extents& lhs, const extents<index_type, other_sizes...>& rhs
    ) noexcept -> bool
    {
        if constexpr (rank() != sizeof...(other_sizes))
        {
            return false;
        }
        else
        {
            return (lhs.template extent<0>() == rhs.template extent<0>())
                && (lhs.template subextents<1>() == rhs.template subextents<1>());
        }
    }
};

// helper for std::dextents
template <typename IndexType, std::size_t rank, typename Extents = extents<IndexType>>
struct make_dextents;

template <typename IndexType, std::size_t rank, std::size_t... dims>
struct make_dextents<IndexType, rank, extents<IndexType, dims...>>
{
    using type = typename make_dextents<
        IndexType, rank - 1, extents<IndexType, std::dynamic_extent, dims...>>::type;
};

template <typename IndexType, std::size_t... dims>
struct make_dextents<IndexType, 0, extents<IndexType, dims...>>
{
    using type = extents<IndexType, dims...>;
};

// C++23 alias
template <typename IndexType, std::size_t N>
using dextents = typename make_dextents<IndexType, N>::type;

// C++26 alias
template <std::size_t N, typename IndexType = std::size_t>
using dims = dextents<IndexType, N>;

// helper to check if a type is an extents specialization -> needed for mdspan
template <typename T>
struct is_extents : std::false_type
{
};

template <typename IndexType, std::size_t... dims>
struct is_extents<extents<IndexType, dims...>> : std::true_type
{
};

// concepts to describe iterator requirements for a std::span constructor with an Iterator
// we don't model AccessPolicies with our mdspan, therefore the data_handle_type is simply a pointer
// its requirements can therefore equal the ones from std::span:
// - contiguous iterator
// - conversion from std::iter_reference_t<It> to element_type is at most a qualification conversion
template <typename It, typename T>
concept compatible_iterator = std::contiguous_iterator<It>
    && std::is_convertible_v<std::remove_reference_t<std::iter_reference_t<It>>, T>;

}  // namespace detail

/**
 * @brief A class to view an underlying 1D-Array as a multidimensional array. A custom
 * implementation of std::mdspan with fixed LayoutPolicy and AccessorPolicy (both default values of
 * std::mdspan).
 *
 * @tparam T The data type of a single element in the underlying array
 * @tparam Extents Specialization of detail::extents to describe static and dynamic extents
 */
template <typename T, typename Extents>
class SpanMD
{
    // requirement for mdspan
    static_assert(
        detail::is_extents<Extents>::value,
        "Extents template parameter must be a specialization of detail::extents"
    );

    /**
     * @brief Helper class to describe the storage requirements for a SpanMD iterator -> iterator
     * over 1D views stores a std::span, while the rest store a SpanMD and stride. This distinction
     * is useful, since std::span stores the stride already and we want to use the STL whenever
     * possible.
     *
     * @tparam __T
     * @tparam __Extents
     */
    template <typename __T, typename __Extents>
    class iterator_storage
    {
    public:
        using value_type = SpanMD<__T, __Extents>;
        using reference = value_type;
        using stride_type = typename value_type::index_type;
        using difference_type = typename value_type::difference_type;
        using iterator_category = std::random_access_iterator_tag;
        using iterator_concept = std::random_access_iterator_tag;

        constexpr iterator_storage() noexcept = default;
        constexpr iterator_storage(value_type i, stride_type s) noexcept : i{std::move(i)}, s{s} {}
        constexpr void advance(difference_type n = 1) noexcept
        {
            i = value_type{i.data_handle() + n * static_cast<difference_type>(s), i.extents()};
        }
        constexpr auto base() const noexcept -> const typename value_type::pointer
        {
            return i.data_handle();
        }
        constexpr auto stride() const noexcept -> stride_type { return s; }

        value_type i;
        stride_type s;
    };

    // specialization for iterating over 1D views
    template <typename __T, typename IndexType>
    class iterator_storage<__T, detail::extents<IndexType>>
    {
    public:
        using iterator_type = __T*;
        using value_type = typename std::iterator_traits<iterator_type>::value_type;
        using reference = typename std::iterator_traits<iterator_type>::reference;
        using stride_type = std::size_t;
        using difference_type = typename std::iterator_traits<iterator_type>::difference_type;
        using iterator_category = typename std::iterator_traits<iterator_type>::iterator_category;
        using iterator_concept = std::contiguous_iterator_tag;

        constexpr iterator_storage() noexcept = default;
        constexpr iterator_storage(iterator_type i) noexcept : i{std::move(i)} {}
        constexpr void advance(difference_type n = 1) noexcept { i += n; }
        constexpr auto base() const noexcept -> iterator_type { return i; }
        constexpr auto stride() const noexcept -> stride_type { return stride_type{1}; }

        iterator_type i;
    };

    /**
     * @brief Iterator class. We can iterate over n-dimensional views, where dereferencing returns
     * an (n-1)-dimensional view. The latter does not exist in memory however, therefore the
     * iterator/SpanMD acts like a view factory (see std::ranges::iota_view) and owns a copy of the
     * (n-1)-dimensional view. Dereferencing does not return a reference, meaning this is not
     * compatible with C++17 LegacyIterators (except maybe LegacyInputIterator), only C++20
     * iterators
     *
     * @tparam __T
     * @tparam __Extents
     */
    template <typename __T, typename __Extents>
    class strided_iterator
    {
        friend class SpanMD;

    public:
        using storage_type = iterator_storage<__T, __Extents>;
        using value_type = typename storage_type::value_type;
        using reference = typename storage_type::reference;
        using difference_type = typename storage_type::difference_type;
        using stride_type = typename storage_type::stride_type;
        using iterator_category = typename storage_type::iterator_category;
        using iterator_concept = typename storage_type::iterator_concept;

    private:
        storage_type i;

    public:
        // required by std::forward_iterator
        constexpr strided_iterator() noexcept = default;

        // required by std::input_or_output_iterator
        constexpr auto operator*() const noexcept -> reference
        {
            if constexpr (std::is_lvalue_reference_v<reference>)
            {
                return *(this->i.i);
            }
            else
            {
                return this->i.i;
            }
        }

        // required by contiguous iterator, only valid when we iterate over real values
        constexpr auto operator->() const noexcept
            -> __T* requires(std::is_lvalue_reference_v<reference>) {
                return std::to_address(this->i.i);
            }

        // required by std::input_or_output_iterator
        constexpr auto operator++() noexcept -> strided_iterator&
        {
            this->i.advance();
            return *this;
        }

        // required by std::forward_iterator (atleast the return value, the expression itself
        // required by input_or_output_iterator)
        constexpr auto operator++(int) noexcept -> strided_iterator
        {
            strided_iterator tmp(*this);
            ++(*this);
            return tmp;
        }

        // required by std::bidirectional_iterator
        constexpr auto operator--() noexcept -> strided_iterator&
        {
            this->i.advance(-1);
            return *this;
        }

        // required by std::bidirectional_iterator
        constexpr auto operator--(int) noexcept -> strided_iterator
        {
            strided_iterator tmp(*this);
            --(*this);
            return tmp;
        }

        // required by std::random_access_iterator
        constexpr auto operator+=(difference_type n) noexcept -> strided_iterator&
        {
            this->i.advance(n);
            return *this;
        }

        // required by std::random_access_iterator
        constexpr auto operator-=(difference_type n) noexcept -> strided_iterator&
        {
            this->i.advance(-n);
            return *this;
        }

        // required by std::random_access_iterator
        constexpr auto operator[](difference_type n) const noexcept -> reference
        {
            if constexpr (std::is_lvalue_reference_v<reference>)
            {
                return this->i.i[n];
            }
            else
            {
                strided_iterator tmp(*this);
                tmp.i.advance(n);
                return tmp;
            }
        }

        // required by std::random_access_iterator
        friend constexpr auto operator+(strided_iterator x, difference_type n) noexcept
            -> strided_iterator
        {
            x += n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator+(difference_type n, strided_iterator x) noexcept
            -> strided_iterator
        {
            x += n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator-(strided_iterator x, difference_type n) noexcept
            -> strided_iterator
        {
            x -= n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator-(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> difference_type
        {
            assert(x.i.stride() == y.i.stride() && "Differently strided iterators are compared?");
            return (x.i.base() - y.i.base()) / static_cast<difference_type>(x.i.stride());
        }

        // required by std::forward_iterator
        friend constexpr auto operator==(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> bool
        {
            return x.i.base() == y.i.base();
        }

        // required by std::random_access_iterator
        friend constexpr auto operator<(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> bool
        {
            return x.i.base() < y.i.base();
        }

        // required by std::random_access_iterator
        friend constexpr auto operator>(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> bool
        {
            return y.i.base() < x.i.base();
        }

        // required by std::random_access_iterator
        friend constexpr auto operator<=(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> bool
        {
            return !(y.i.base() < x.i.base());
        }

        // required by std::random_access_iterator
        friend constexpr auto operator>=(
            const strided_iterator& x, const strided_iterator& y
        ) noexcept -> bool
        {
            return !(x.i.base() < y.i.base());
        }

        constexpr decltype(auto) base() const noexcept { return i.base(); }
        constexpr auto stride() const noexcept -> stride_type { return i.stride(); }

    private:
        // helper for consuming containers
        constexpr explicit strided_iterator(storage_type i) noexcept : i{std::move(i)} {}
    };

    /**
     * @brief Sentinel class for the iterator. C++20 iterators don't require that ::end() methods
     * return an iterator, just a sentinel, which is comparable to the iterator. We can use this,
     * since the comparison is along the underlying pointers, so the end iterators don't need to
     * store extents or strides. This breaks compatibility with non-ranges algorithms (except
     * range-based for loop)
     *
     * The sentinel also encodes the extents in the type. This allows the requirement, that a
     * sentinel can only be compared with an iterator with the same dimensionality. Does not
     * guarantee that the pair is always a valid range.
     *
     */
    template <typename __T, typename __Extents>
    class strided_sentinel
    {
        using pointer = __T*;

    private:
        pointer sentinel;

    public:
        // required by std::sentinel_for
        constexpr strided_sentinel() = default;
        constexpr explicit strided_sentinel(pointer sentinel) noexcept : sentinel{sentinel} {}

        // required by std::sentinel_for
        friend constexpr auto operator==(
            const strided_iterator<__T, __Extents>& x, const strided_sentinel& y
        ) noexcept -> bool
        {
            return x.base() == y.sentinel;
        }

        // required by std::sized_sentinel_for
        friend constexpr auto operator-(
            const strided_iterator<__T, __Extents>& x, const strided_sentinel& y
        ) noexcept -> typename strided_iterator<__T, __Extents>::difference_type
        {
            return (x.base() - y.sentinel)
                / static_cast<typename strided_iterator<__T, __Extents>::difference_type>(x.stride()
                );
        }

        // required by std::sized_sentinel_for
        friend constexpr auto operator-(
            const strided_sentinel& x, const strided_iterator<__T, __Extents>& y
        ) noexcept -> typename strided_iterator<__T, __Extents>::difference_type
        {
            return -(y - x);
        }
    };

public:
    // typedefs, from standard and just the ones we implement
    using extents_type = Extents;
    template <std::size_t N>
    using subextents_type = decltype(std::declval<extents_type>().template subextents<N>());
    using element_type = T;
    using value_type = std::remove_cv_t<T>;
    using index_type = typename Extents::index_type;
    using size_type = typename Extents::size_type;
    using rank_type = typename Extents::rank_type;
    using difference_type = std::ptrdiff_t;
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;
    template <std::size_t N>
    using iterator = strided_iterator<element_type, subextents_type<N + 1>>;
    template <std::size_t N>
    using const_iterator = strided_iterator<const element_type, subextents_type<N + 1>>;
    template <std::size_t N>
    using sentinel = strided_sentinel<element_type, subextents_type<N + 1>>;
    template <std::size_t N>
    using const_sentinel = strided_sentinel<const element_type, subextents_type<N + 1>>;

private:
    /**
     * @brief Helper struct to determine the return type of operator[]. In the case where we access
     * a single value, return type deduction does not deduce data[i] as a reference type (since the
     * standard says the built-in operator[] for T* / T[] return T), so we need to specify the
     * return type. The type depends on how many dimensions are not accessed. Special cases:
     * - Access 1D subview -> return std::span
     * - Access 0D subview (single value) -> return reference
     *
     * @tparam remainder
     */
    template <std::size_t remainder>
    struct access_return_type
    {
        using type = std::conditional_t<
            remainder == 0, reference,
            SpanMD<element_type, subextents_type<extents_type::rank() - remainder>>>;
    };

public:
    // constructors, as described by cppreference

    constexpr SpanMD() requires(extents_type::rank_dynamic() > 0)
    = default;

    template <detail::compatible_iterator<element_type> It, typename... Counts>
    constexpr explicit SpanMD(It p, Counts... counts) noexcept
        requires((std::is_convertible_v<Counts, index_type> && ...)
                 && (std::is_nothrow_constructible_v<index_type, Counts> && ...)
                 && (sizeof...(Counts) == extents_type::rank()
                     || sizeof...(Counts) == extents_type::rank_dynamic()))
        : data{std::to_address(p)}, dim{static_cast<index_type>(std::move(counts))...}
    {
    }

    template <detail::compatible_iterator<element_type> It, typename SizeType, std::size_t N>
    constexpr explicit(extents_type::rank_dynamic() != N)
        SpanMD(It p, std::span<SizeType, N> exts) noexcept
        requires(std::is_convertible_v<const SizeType&, index_type>
                 && std::is_nothrow_constructible_v<index_type, const SizeType&>
                 && (N == extents_type::rank() || N == extents_type::rank_dynamic()))
        : data{std::to_address(p)}, dim{exts}
    {
    }

    template <detail::compatible_iterator<element_type> It, typename SizeType, std::size_t N>
    constexpr explicit(extents_type::rank_dynamic() != N)
        SpanMD(It p, const std::array<SizeType, N>& exts) noexcept
        requires(std::is_convertible_v<const SizeType&, index_type>
                 && std::is_nothrow_constructible_v<index_type, const SizeType&>
                 && (N == extents_type::rank() || N == extents_type::rank_dynamic()))
        : data{std::to_address(p)}, dim{exts}
    {
    }

    template <detail::compatible_iterator<element_type> It>
    constexpr SpanMD(It p, const extents_type& ext) noexcept : data{std::to_address(p)}, dim{ext}
    {
    }

    // access operators, differ from cppreference since multi-parameter operator[] is C++23. As a
    // workaround we take by array -> all converted to std::span, so this is the first overload

    // further, we return submdspans for incomplete md-indices

    template <typename SizeType, std::size_t N>
    constexpr auto operator[](std::span<SizeType, N> indices) const noexcept -> decltype(auto)
        requires(N <= extents_type::rank())
    {
        const auto index = get_index(std::make_index_sequence<N>{}, indices);
        return subscript_impl<N>(index);
    }

    template <typename... Sizes>
    constexpr auto operator[](Sizes... indices) const noexcept ->
        typename access_return_type<extents_type::rank() - sizeof...(Sizes)>::type
        requires(sizeof...(Sizes) <= extents_type::rank())
    {
        const auto index = get_index(std::make_index_sequence<sizeof...(Sizes)>{}, indices...);
        return subscript_impl<sizeof...(Sizes)>(index);
    }

    // allow c style arrays for mdspan[{1,2,3}] access

    template <typename SizeType, std::size_t N>
    constexpr decltype(auto) operator[](const SizeType (&indices)[N]) const noexcept
        requires(N <= extents_type::rank())
    {
        return operator[](std::span(indices));
    }

    template <typename SizeType, std::size_t N>
    constexpr decltype(auto) operator[](const std::array<SizeType, N>& indices) const noexcept
        requires(N <= extents_type::rank())
    {
        return operator[](std::span(indices));
    }

    constexpr size_type size() const noexcept
    {
        // size is product over all extents -> access requires index sequence -> immediately
        // evaluated lambda saves extra _impl function
        return [&]<std::size_t... Idxs>(std::index_sequence<Idxs...>)
        {
            return (dim.template extent<Idxs>() * ... * 1);
        }(std::make_index_sequence<extents_type::rank()>{});
    }

    constexpr bool empty() const noexcept
    {
        // span is empty when size is 0 -> happens when an extent is 0
        return [&]<std::size_t... Idxs>(std::index_sequence<Idxs...>)
        {
            return (extents_type::rank() > 0)
                && ((dim.template extent<Idxs>() == 0) || ... || false);
        }(std::make_index_sequence<extents_type::rank()>{});
    }

    template <rank_type I>
    constexpr index_type stride() const noexcept
    {
        // stride I -> for the remaining extents (rank - I - 1), the product of them (Idxs + I + 1
        // for offset)
        return [&]<std::size_t... Idxs>(std::index_sequence<Idxs...>)
        {
            return (dim.template extent<Idxs + I + 1>() * ... * static_cast<index_type>(1));
        }(std::make_index_sequence<extents_type::rank() - I - 1>{});
    }

    constexpr const extents_type& extents() const noexcept { return dim; }
    constexpr pointer data_handle() const noexcept { return data; }

    template <typename... SizeType>
    constexpr decltype(auto) begin(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        return begin_impl<iterator>(std::forward<SizeType>(indices)...);
    }

    template <typename... SizeType>
    constexpr decltype(auto) cbegin(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        return begin_impl<const_iterator>(std::forward<SizeType>(indices)...);
    }

    template <typename... SizeType>
    constexpr decltype(auto) end(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        return end_impl<sentinel>(std::forward<SizeType>(indices)...);
    }

    template <typename... SizeType>
    constexpr decltype(auto) cend(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        return end_impl<const_sentinel>(std::forward<SizeType>(indices)...);
    }

private:
    template <template <std::size_t> typename Iter, typename... SizeType>
    constexpr auto begin_impl(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        constexpr auto N = sizeof...(SizeType);
        const auto index = get_index(
            std::make_index_sequence<N>{}, std::forward<SizeType>(indices)...
        );

        using subiterator = Iter<N>;

        if constexpr (const auto remaining = extents_type::rank() - N - 1; remaining == 0)
        {
            return subiterator{std::addressof(data[index])};
        }
        else
        {
            return subiterator{
                {{std::addressof(data[index]), dim.template subextents<N + 1>()}, stride<N>()}
            };
        }
    }

    template <template <std::size_t> typename Sen, typename... SizeType>
    constexpr auto end_impl(SizeType&&... indices) const noexcept
        requires(sizeof...(SizeType) < extents_type::rank())
    {
        constexpr auto N = sizeof...(SizeType);
        const auto index = get_index(
                               std::make_index_sequence<N>{}, std::forward<SizeType>(indices)...
                           )
            + stride<N>() * dim.template extent<N>();  // add number of elements for "past the end"
        return Sen<N>(std::addressof(data[index]));
    }

    template <typename SizeType, std::size_t N, std::size_t... Idxs>
    constexpr index_type get_index(
        std::index_sequence<Idxs...>, std::span<SizeType, N> indices
    ) const noexcept
    {
        // multi index -> 1D index: multiply each value by stride and sum up
        // assertion is added through comma operator
        return (
            (assert(
                 static_cast<index_type>(indices[Idxs]) < dim.template extent<Idxs>()
                 && "SpanMD: operator[] out of bounds access"
             ),
             (static_cast<index_type>(indices[Idxs]) * stride<Idxs>()))
            + ... + 0
        );
    }

    template <typename... SizeType, std::size_t... Idxs>
    constexpr index_type get_index(
        std::index_sequence<Idxs...>, SizeType&&... indices
    ) const noexcept
    {
        // multi index -> 1D index: multiply each value by stride and sum up
        // assertion is added through comma operator
        return (
            (assert(
                 indices < dim.template extent<Idxs>() && "SpanMD: operator[] out of bounds access"
             ),
             (static_cast<index_type>(indices) * stride<Idxs>()))
            + ... + 0
        );
    }

    template <index_type Dims>
    constexpr auto subscript_impl(index_type index) const noexcept ->
        typename access_return_type<extents_type::rank() - Dims>::type
        requires(Dims <= extents_type::rank())
    {
        if constexpr (const auto remaining = extents_type::rank() - Dims; remaining == 0)
        {
            return data[index];
        }
        else
        {
            return SpanMD<element_type, subextents_type<Dims>>{
                std::addressof(data[index]), dim.template subextents<Dims>()
            };
        }
    }

    pointer data;
    [[no_unique_address]] extents_type dim;
};

// deduction guides, just the relevant ones from cppreference

template <typename CArray>
requires(std::is_array_v<CArray> && std::rank_v<CArray> == 1)
SpanMD(CArray&) -> SpanMD<
    std::remove_all_extents_t<CArray>, detail::extents<std::size_t, std::extent_v<CArray, 0>>>;

template <typename Pointer>
requires(std::is_pointer_v<std::remove_reference_t<Pointer>>)
SpanMD(Pointer&&) -> SpanMD<
    std::remove_pointer_t<std::remove_reference_t<Pointer>>, detail::extents<std::size_t>>;

template <typename ElementType, typename... Integrals>
requires((std::is_convertible_v<Integrals, std::size_t> && ...) && sizeof...(Integrals) > 0)
explicit SpanMD(ElementType*, Integrals...)
    -> SpanMD<ElementType, detail::dextents<std::size_t, sizeof...(Integrals)>>;

template <typename ElementType, typename OtherIndexType, std::size_t N>

SpanMD(ElementType*, std::span<OtherIndexType, N>)
    -> SpanMD<ElementType, detail::dextents<std::size_t, N>>;
template <typename ElementType, typename OtherIndexType, std::size_t N>

SpanMD(ElementType*, const std::array<OtherIndexType, N>&)
    -> SpanMD<ElementType, detail::dextents<std::size_t, N>>;
template <typename ElementType, typename IndexType, std::size_t... ExtentsPack>

SpanMD(ElementType*, const detail::extents<IndexType, ExtentsPack...>&)
    -> SpanMD<ElementType, detail::extents<IndexType, ExtentsPack...>>;

}  // namespace memory

}  // namespace lola

template <typename T, typename Extents>
inline constexpr bool std::ranges::enable_borrowed_range<lola::memory::SpanMD<T, Extents>> = true;

template <typename T, typename Extents>
inline constexpr bool std::ranges::enable_view<lola::memory::SpanMD<T, Extents>> = true;

#endif /* SRC_MEMORY_SPAN_H */
