/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * @file static_vector.h
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definition of std::vector-like container without automatic resizing
 * @version 0.1
 * @date 2024-05-20
 *
 * @copyright Copyright (c) 2024 - present
 *
 */

#ifndef SRC_MEMORY_STATIC_VECTOR_H
#define SRC_MEMORY_STATIC_VECTOR_H

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <type_traits>

namespace lola
{
namespace memory
{
namespace detail
{
/**
 * @brief Base class handling allocation of underlying storage. Makes some exception handling
 * easier, but is mainly used later to optimize for implicit_lifetime types
 * TODO: add specializations for implicit_lifetime types, where size can equal capacity all the time
 *
 * @tparam T
 * @tparam Alloc
 */
template <typename T, typename Alloc>
struct static_vector_base
{
    using allocator_type = Alloc;
    // make sure our allocator allocates T's
    using value_alloc_type =
        typename std::allocator_traits<allocator_type>::template rebind_alloc<T>;
    using pointer = typename std::allocator_traits<value_alloc_type>::pointer;
    using size_type = typename std::allocator_traits<value_alloc_type>::size_type;

    // internal data type to decouple from allocator, useful for resizing, where we reuse the
    // allocator
    struct static_vector_data
    {
        // use pointer{} instead of nullptr because pointer is not required to be a *real* pointer
        pointer start = pointer{};
        pointer finish = pointer{};
        pointer end_of_allocation = pointer{};

        constexpr static_vector_data() noexcept = default;
        constexpr static_vector_data(static_vector_data&& x) noexcept
            : start{std::move(x.start)}, finish{std::move(x.finish)},
              end_of_allocation{std::move(x.end_of_allocation)}
        {
            x.start = x.finish = x.end_of_allocation = pointer{};
        }
        // defined as function instead of copy constructor/assignment operator to leave this a
        // move-only type
        constexpr void copy_data(const static_vector_data& x) noexcept
        {
            start = x.start;
            finish = x.finish;
            end_of_allocation = x.end_of_allocation;
        }

        constexpr void swap_data(static_vector_data& x) noexcept
        {
            static_vector_data tmp;
            tmp.copy_data(*this);
            copy_data(x);
            x.copy_data(tmp);
        }
    };

    constexpr static_vector_base(
    ) noexcept(std::is_nothrow_default_constructible_v<value_alloc_type>)
        requires(std::is_default_constructible_v<value_alloc_type>)
        : alloc{}
    {
    }

    constexpr explicit static_vector_base(static_vector_base&& x) noexcept = default;

    constexpr explicit static_vector_base(const allocator_type& other_alloc) noexcept
        : alloc(other_alloc)
    {
    }

    constexpr explicit static_vector_base(size_type n) : storage{}, alloc{} { create_storage(n); }
    constexpr static_vector_base(size_type n, const allocator_type& other_alloc)
        : storage{}, alloc(other_alloc)
    {
        create_storage(n);
    }

    constexpr ~static_vector_base() noexcept
    {
        deallocate(
            storage.start, static_cast<size_type>(storage.end_of_allocation - storage.start)
        );
    }

    constexpr auto get_value_allocator() noexcept -> value_alloc_type& { return alloc; }
    constexpr auto get_value_allocator() const noexcept -> const value_alloc_type& { return alloc; }

    constexpr auto get_allocator() const noexcept -> allocator_type
    {
        return allocator_type(alloc);
    }

    constexpr auto allocate(size_type n) -> pointer
    {
        if (n > std::allocator_traits<value_alloc_type>::max_size(alloc)) [[unlikely]]
        {
            throw std::length_error("Requested array size too big");
        }
        if (n != 0)
        {
            return std::allocator_traits<value_alloc_type>::allocate(alloc, n);
        }
        return pointer{};
    }

    constexpr void deallocate(pointer p, size_type n) noexcept
    {
        if (p)
        {
            std::allocator_traits<value_alloc_type>::deallocate(alloc, p, n);
        }
    }

    constexpr void create_storage(size_type n)
    {
        storage.start = allocate(n);
        storage.finish = storage.start;
        storage.end_of_allocation = storage.start + n;
    }

    static_vector_data storage;
    [[no_unique_address]] value_alloc_type alloc;
};

template <typename R, typename T>
concept container_compatible_range = std::ranges::input_range<R>
    && std::convertible_to<std::ranges::range_reference_t<R>, T>;

template <typename Iter>
concept legacy_input_iterator = std::convertible_to<
    typename std::iterator_traits<Iter>::iterator_category, std::input_iterator_tag>;

template <typename Iter>
concept legacy_forward_iterator = legacy_input_iterator<Iter>
    && std::convertible_to<typename std::iterator_traits<Iter>::iterator_category,
                           std::forward_iterator_tag>;

struct from_range_t
{
    explicit from_range_t() = default;
};
}  // namespace detail

template <typename T, typename Allocator = std::allocator<T>>
class static_vector : protected detail::static_vector_base<T, Allocator>
{
private:
    using base = detail::static_vector_base<T, Allocator>;
    using value_alloc_type = typename base::value_alloc_type;
    using alloc_traits = std::allocator_traits<value_alloc_type>;
    using base::allocate;
    using base::create_storage;
    using base::deallocate;
    using base::get_value_allocator;
    using base::storage;

public:
    using value_type = T;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = typename base::pointer;
    using const_pointer = typename alloc_traits::const_pointer;
    using size_type = typename alloc_traits::size_type;
    using difference_type = typename alloc_traits::difference_type;
    using allocator_type = Allocator;

    using base::get_allocator;

private:
    template <typename Iter>
    class static_vector_iterator
    {
        friend class static_vector;

    public:
        using iterator_type = Iter;
        using value_type = typename std::iterator_traits<iterator_type>::value_type;
        using reference = typename std::iterator_traits<iterator_type>::reference;
        using pointer = typename std::iterator_traits<iterator_type>::pointer;
        using difference_type = typename std::iterator_traits<iterator_type>::difference_type;
        using iterator_category = typename std::iterator_traits<iterator_type>::iterator_category;
        using iterator_concept = std::contiguous_iterator_tag;

    private:
        iterator_type i;

    public:
        // required by std::forward_iterator
        constexpr static_vector_iterator() noexcept : i{} {}

        // required by std::input_or_output_iterator
        constexpr auto operator*() const noexcept -> reference { return *i; }

        // required by contiguous iterator, only valid when we iterate over real values
        constexpr auto operator->() const noexcept -> pointer { return std::to_address(i); }

        // required by std::input_or_output_iterator
        constexpr auto operator++() noexcept -> static_vector_iterator&
        {
            ++i;
            return *this;
        }

        // required by std::forward_iterator (atleast the return value, the expression itself
        // required by input_or_output_iterator)
        constexpr auto operator++(int) noexcept -> static_vector_iterator
        {
            static_vector_iterator tmp(*this);
            ++(*this);
            return tmp;
        }

        // required by std::bidirectional_iterator
        constexpr auto operator--() noexcept -> static_vector_iterator&
        {
            --i;
            return *this;
        }

        // required by std::bidirectional_iterator
        constexpr auto operator--(int) noexcept -> static_vector_iterator
        {
            static_vector_iterator tmp(*this);
            --(*this);
            return tmp;
        }

        // required by std::random_access_iterator
        constexpr auto operator+=(difference_type n) noexcept -> static_vector_iterator&
        {
            i += n;
            return *this;
        }

        // required by std::random_access_iterator
        constexpr auto operator-=(difference_type n) noexcept -> static_vector_iterator&
        {
            i += (-n);
            return *this;
        }

        // required by std::random_access_iterator
        constexpr auto operator[](difference_type n) const noexcept -> reference { return i[n]; }

        // required by std::random_access_iterator
        friend constexpr auto operator+(static_vector_iterator x, difference_type n) noexcept
            -> static_vector_iterator
        {
            x += n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator+(difference_type n, static_vector_iterator x) noexcept
            -> static_vector_iterator
        {
            x += n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator-(static_vector_iterator x, difference_type n) noexcept
            -> static_vector_iterator
        {
            x -= n;
            return x;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator-(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> difference_type
        {
            return x.i - y.i;
        }

        // required by std::forward_iterator
        friend constexpr auto operator==(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> bool
        {
            return x.i == y.i;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator<(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> bool
        {
            return x.i < y.i;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator>(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> bool
        {
            return y.i < x.i;
        }

        // required by std::random_access_iterator
        friend constexpr auto operator<=(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> bool
        {
            return !(y.i < x.i);
        }

        // required by std::random_access_iterator
        friend constexpr auto operator>=(
            const static_vector_iterator& x, const static_vector_iterator& y
        ) noexcept -> bool
        {
            return !(x.i < y.i);
        }

    private:
        // helper for consuming containers
        constexpr explicit static_vector_iterator(static_vector::pointer i) noexcept
            : i{std::move(i)}
        {
        }
    };

    /**
     * @brief Struct to construct a temporary static_vector value_type with the allocators construct
     * and destroy methods. Used by emplace, since the passed arguments could alias an element in
     * the array
     */
    struct temporary_value
    {
        value_alloc_type& allocator;
        /**
         * @brief Union for the value to decouple the implicit construction of v from the
         * construction of the temporary_value object
         */
        union
        {
            value_type v;
        };
        constexpr auto base() -> value_type* { return std::addressof(v); }
        constexpr auto get() -> value_type& { return *base(); }
        template <typename... Args>
        constexpr explicit temporary_value(value_alloc_type& a, Args&&... args) : allocator{a}
        {
            alloc_traits::construct(allocator, base(), std::forward<Args>(args)...);
        }
        constexpr ~temporary_value() { alloc_traits::destroy(allocator, base()); }
    };

public:
    using iterator = static_vector_iterator<pointer>;
    using const_iterator = static_vector_iterator<const_pointer>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

private:
    // shorthands because who the fuck allowed these long typedefs
    using assign_alloc_on_move = typename alloc_traits::propagate_on_container_move_assignment;
    using assign_alloc_on_copy = typename alloc_traits::propagate_on_container_copy_assignment;
    using swap_allocs = typename alloc_traits::propagate_on_container_swap;
    using alloc_always_equal = typename alloc_traits::is_always_equal;

public:
    static_assert(
        std::is_same_v<typename std::iterator_traits<iterator>::difference_type, difference_type>
            && std::is_same_v<
                typename std::iterator_traits<const_iterator>::difference_type, difference_type>,
        "difference_type of allocator and iterators don't match"
    );

    static_assert(
        std::is_same_v<typename allocator_type::value_type, value_type>,
        "value_type of allocator and container don't match"
    );

    /***********************************************************************************************
     * Section: Functions not required by any named container requirement, but added for convenience
     **********************************************************************************************/

    // construct a static_vector with n uninitialized values, setting the allocator to a and using
    // that
    constexpr explicit static_vector(size_type n, const allocator_type& a = allocator_type{})
        : base{n, a}
    {
    }

    // return the capacity of the array, i.e. the max number of elements it can hold
    constexpr auto capacity() const noexcept -> size_type
    {
        return static_cast<size_type>(this->storage.end_of_allocation - this->storage.start);
    }

    // resize the capacity to n
    constexpr void resize(size_type n)
    {
        if (n == capacity())
        {
            return;
        }
        if (n == 0)
        {
            delete_content_and_null();
            return;
        }
        if (capacity() == 0)
        {
            create_storage(n);
            return;
        }

        typename base::static_vector_data tmp;
        tmp.swap_data(this->storage);
        create_storage(n);
        auto elements_to_construct = std::min<size_type>(
            static_cast<size_type>(tmp.finish - tmp.start), n
        );

        if constexpr (std::is_nothrow_move_constructible_v<value_type>)
        {
            pointer new_end = tmp.start + elements_to_construct;
            construct_from_range(
                std::make_move_iterator(tmp.start), std::make_move_iterator(new_end)
            );
        }
        else
        {
            try
            {
                const_pointer const_start = tmp.start;
                const_pointer const_end = tmp.start + elements_to_construct;
                construct_from_range(const_start, const_end);
            }
            catch (...)
            {
                delete_content();
                this->storage.swap_data(tmp);
                throw;
            }
        }
        destroy_at_end(tmp.finish, tmp.start);
        deallocate(tmp.start, static_cast<size_type>(tmp.finish - tmp.start));
    }
    // mostly exists to enable C++23 reservable_container
    constexpr void reserve(size_type n)
    {
        if (n > capacity())
        {
            resize(n);
        }
    }
    // return a pointer to the underlying data
    constexpr auto data() noexcept -> pointer { return std::to_address(this->storage.start); }
    // return a pointer to the underlying data
    constexpr auto data() const noexcept -> const_pointer
    {
        return std::to_address(this->storage.start);
    }

    /***********************************************************************************************
     * Section: Functions required by Container and AllocatorAwareContainer
     **********************************************************************************************/

    // default constructor
    constexpr static_vector(
    ) noexcept(std::is_nothrow_default_constructible_v<value_alloc_type>) = default;

    // destructor
    constexpr ~static_vector() noexcept { destroy_at_end(this->storage.start); }

    // copy constructor
    constexpr static_vector(const static_vector& x)
        : base{
              x.capacity(),
              std::allocator_traits<allocator_type>::select_on_container_copy_construction(
                  x.get_allocator()
              )
          }
    {
        safe_construct_from_range(x.cbegin(), x.cend());
    }

    // copy assignment
    constexpr auto operator=(const static_vector& x) -> static_vector&
    {
        if (this == std::addressof(x))
        {
            return *this;
        }
        // capacity mismatch? need to reallocate and copy construct values
        if (capacity() != x.capacity())
        {
            delete_content();
            if constexpr (assign_alloc_on_copy::value)
            {
                get_value_allocator() = x.get_value_allocator();
            }
            create_storage(x.capacity());
            construct_from_range(x.cbegin(), x.cend());
            return *this;
        }
        // now -> capacities match, reuse storage
        // dispatch to assign
        // unequal allocators if we don't assign require reallocation however
        if constexpr (assign_alloc_on_copy::value)
        {
            if constexpr (!alloc_always_equal::value)
            {
                // TODO: maybe remove the need for the bool? (requires allocating our storage with
                // x's allocator first before assigning)
                bool need_alloc = false;
                if (get_value_allocator() != x.get_value_allocator())
                {
                    delete_content();
                    need_alloc = true;
                }
                get_value_allocator() = x.get_value_allocator();
                if (need_alloc)
                {
                    create_storage(x.capacity());
                    construct_from_range(x.cbegin(), x.cend());
                    return *this;
                }
            }
            else
            {
                get_value_allocator() = x.get_value_allocator();
            }
        }
        assign_impl(x.cbegin(), x.cend(), static_cast<difference_type>(x.size()));
        return *this;
    }

    // move constructor
    constexpr static_vector(static_vector&& x) noexcept = default;

    // move assignment
    constexpr auto operator=(static_vector&& x
    ) noexcept(assign_alloc_on_move::value || alloc_always_equal::value) -> static_vector&
    {
        if (this == std::addressof(x))
        {
            return *this;
        }
        if (x.capacity() == 0)
        {
            delete_content_and_null();
            if constexpr (assign_alloc_on_move::value)
            {
                get_value_allocator() = std::move(x.get_value_allocator());
            }
            return *this;
        }
        // allocators unequal and we don't assign? -> can't just take x's stuff and need to move
        // element-wise
        if constexpr (!assign_alloc_on_move::value && !alloc_always_equal::value)
        {
            if (get_value_allocator() != x.get_value_allocator())
            {
                if (capacity() != x.capacity())
                {
                    delete_content();
                    create_storage(x.capacity());
                    construct_from_range(
                        std::make_move_iterator(x.begin()), std::make_move_iterator(x.end())
                    );
                    return *this;
                }
                assign_impl(
                    std::make_move_iterator(x.begin()), std::make_move_iterator(x.end()), x.size()
                );
                return *this;
            }
        }
        // allocators equal or we assign the allocator anyway? can simply take x's stuff
        delete_content_and_null();
        if constexpr (assign_alloc_on_move::value)
        {
            get_value_allocator() = std::move(x.get_value_allocator());
        }
        this->storage.swap_data(x.storage);
        return *this;
    }
    constexpr auto begin() noexcept -> iterator { return iterator{this->storage.start}; }
    constexpr auto begin() const noexcept -> const_iterator
    {
        return const_iterator{this->storage.start};
    }
    constexpr auto end() noexcept -> iterator { return iterator{this->storage.finish}; }
    constexpr auto end() const noexcept -> const_iterator
    {
        return const_iterator{this->storage.finish};
    }
    constexpr auto cbegin() const noexcept -> const_iterator
    {
        return const_iterator{this->storage.start};
    }
    constexpr auto cend() const noexcept -> const_iterator
    {
        return const_iterator{this->storage.finish};
    }
    constexpr void swap(static_vector& x) noexcept
    {
        // swapping on unequal allocators that don't get swapped is undefined behavior, we
        // graciously assert that
        if constexpr (!swap_allocs::value && !alloc_always_equal::value)
        {
            assert(
                get_value_allocator() == x.get_value_allocator()
                && "Swapping static_vectors with unswappable and unequal allocators "
                   "is undefined behavior"
            );
        }
        this->storage.swap_data(x.storage);
        if constexpr (swap_allocs::value)
        {
            std::swap(get_value_allocator(), x.get_value_allocator());
        }
    }
    constexpr auto size() const noexcept -> size_type
    {
        return static_cast<size_type>(this->storage.finish - this->storage.start);
    }
    constexpr auto max_size() const noexcept -> size_type
    {
        return alloc_traits::max_size(get_value_allocator());
    }
    [[nodiscard]] constexpr auto empty() const noexcept -> bool
    {
        return this->storage.start == this->storage.finish;
    }

    friend constexpr auto operator==(const static_vector& x, const static_vector& y) noexcept
        -> bool
    {
        return std::equal(x.begin(), x.end(), y.begin(), y.end());
    }

    friend constexpr auto operator!=(const static_vector& x, const static_vector& y) noexcept
        -> bool
    {
        return !(x == y);
    }

    friend constexpr void swap(static_vector& x, static_vector& y) noexcept { x.swap(y); }

    /***********************************************************************************************
     * Section: Functions required by ReversibleContainer
     **********************************************************************************************/

    constexpr auto rbegin() noexcept -> reverse_iterator
    {
        return std::make_reverse_iterator(end());
    }
    constexpr auto rbegin() const noexcept -> const_reverse_iterator
    {
        return std::make_reverse_iterator(end());
    }
    constexpr auto rend() noexcept -> reverse_iterator
    {
        return std::make_reverse_iterator(begin());
    }
    constexpr auto rend() const noexcept -> const_reverse_iterator
    {
        return std::make_reverse_iterator(begin());
    }
    constexpr auto crbegin() const noexcept -> const_reverse_iterator
    {
        return std::make_reverse_iterator(end());
    }
    constexpr auto crend() const noexcept -> const_reverse_iterator
    {
        return std::make_reverse_iterator(begin());
    }

    /***********************************************************************************************
     * Section: Functions required by AllocatorAwareContainer
     **********************************************************************************************/

    constexpr explicit static_vector(const allocator_type& a) noexcept : base{a} {}
    constexpr static_vector(
        const static_vector& x, const typename std::type_identity_t<allocator_type>& a
    )
        : base{x.capacity(), a}
    {
        safe_construct_from_range(x.cbegin(), x.cend());
    }
    constexpr static_vector(
        static_vector&& x, const typename std::type_identity_t<allocator_type>& a
    )
        : base{a}
    {
        if constexpr (!alloc_always_equal::value)
        {
            if (get_value_allocator() != x.get_value_allocator())
            {
                create_storage(x.capacity());
                safe_construct_from_range(
                    std::make_move_iterator(x.begin()), std::make_move_iterator(x.end())
                );
                x.delete_content_and_null();
                return;
            }
        }
        this->storage.swap_data(x.storage);
    }

    /***********************************************************************************************
     * Section: Functions required by SequenceContainer
     **********************************************************************************************/

    constexpr static_vector(
        size_type n, const value_type& x, const allocator_type& a = allocator_type{}
    )
        : base{n, a}
    {
        safe_construct_from_elem(x, n);
    }
    template <detail::legacy_input_iterator Iterator>
    constexpr static_vector(
        Iterator first, Iterator last, const allocator_type& a = allocator_type{}
    )
        : base{a}
    {
        init_without_size(first, last);
    }

    template <detail::legacy_forward_iterator Iterator>
    constexpr static_vector(
        Iterator first, Iterator last, const allocator_type& a = allocator_type{}
    )
        : base{a}
    {
        init_with_size(first, last, static_cast<size_type>(std::distance(first, last)));
    }

    template <detail::container_compatible_range<value_type> R>
    constexpr static_vector(
        detail::from_range_t, R&& range, const allocator_type& a = allocator_type{}
    )
        : base{a}
    {
        if constexpr (std::ranges::forward_range<R> || std::ranges::sized_range<R>)
        {
            init_with_size(
                std::ranges::begin(range), std::ranges::end(range),
                static_cast<size_type>(std::ranges::distance(range))
            );
        }
        else
        {
            init_without_size(std::ranges::begin(range), std::ranges::end(range));
        }
    }

    constexpr static_vector(
        std::initializer_list<value_type> il, const allocator_type& a = allocator_type{}
    )
        : static_vector{il.begin(), il.end(), a}
    {
    }
    constexpr auto operator=(std::initializer_list<value_type> il) -> static_vector&
    {
        if (capacity() == il.size())
        {
            std::copy_n(il.begin(), size(), begin());
            construct_from_range(il.begin() + size(), il.end());
        }
        else
        {
            delete_content();
            create_storage(il.size());
            construct_from_range(il.begin(), il.end());
        }
        return *this;
    }
    template <typename... Args>
    constexpr auto emplace(const_iterator p, Args&&... args) -> iterator
    {
        if (p == this->cend())
        {
            // emplace_back does a space check, arguably reuse code <-> obfuscated
            auto it = end();
            emplace_back(std::forward<Args>(args)...);
            return it;
        }
        check_space(static_cast<size_type>(1));
        temporary_value tmp{get_value_allocator(), std::forward<Args>(args)...};
        iterator p_ = begin() + (p - cbegin());
        pointer old_finish = this->storage.finish;
        construct_single_at_end(std::move(back()));
        std::shift_right(std::to_address(p_), old_finish, 1);
        *p_ = std::move(tmp.get());
        return p_;
    }
    constexpr auto insert(const_iterator p, const_reference t) -> iterator { return emplace(p, t); }
    constexpr auto insert(const_iterator p, value_type&& t) -> iterator
    {
        return emplace(p, std::move(t));
    }
    constexpr auto insert(const_iterator p, size_type n, const_reference t) -> iterator
    {
        if (n == 0) [[unlikely]]
        {
            return begin() + (p - cbegin());
        }
        check_space(n);
        if (p == cend())
        {
            auto it = end();
            construct_from_elem(t, n);
            return it;
        }
        const auto num_elems_to_shift = static_cast<size_type>(std::distance(p, cend()));
        const pointer old_finish = this->storage.finish;
        const const_pointer new_finish = this->storage.finish + n;
        const pointer begin_of_new_range = this->storage.start + (p - cbegin());
        if (num_elems_to_shift > n)
        {
            // more elements to shift to make space than new element? some will be move constructed,
            // some move assigned through shift, every new value is copy assigned
            pointer begin_of_construct_range = this->storage.finish - n;
            while (this->storage.finish != new_finish)
            {
                construct_single_at_end(std::move(*begin_of_construct_range++));
            }
            // in theory, the static cast is wrong, since we COULD add more elements than
            // representable by the difference between last and first. However, calling size()
            // afterwards is already undefined behavior, so who cares?
            std::shift_right(begin_of_new_range, old_finish, static_cast<difference_type>(n));
            std::fill(begin_of_new_range, begin_of_new_range + n, t);
        }
        else
        {
            // more elements to add than to shift? every existing element is move construced, some
            // new elements copy construced, some copy assigned
            // order in which which elements are handled matters, because an exception should leave
            // the vector in a valid state
            auto num_insert_construct = n - num_elems_to_shift;
            pointer begin_of_construct_range = begin_of_new_range;
            while (num_insert_construct-- != 0)
            {
                construct_single_at_end(t);
            }
            while (this->storage.finish != new_finish)
            {
                construct_single_at_end(std::move(*begin_of_construct_range++));
            }
            std::fill(begin_of_new_range, old_finish, t);
        }
        return iterator{begin_of_new_range};
    }
    template <detail::legacy_input_iterator Iterator>
    constexpr auto insert(const_iterator p, Iterator first, Iterator last) -> iterator
    {
        if (p == cend())
        {
            while (first != last)
            {
                emplace_back(*first++);
            }
        }
        else if (first != last) [[likely]]
        {
            // can't know size of input range, but inserting in the middle requires knowing how much
            // to shift other elements by, so first create temporary
            static_vector tmp{first, last, get_value_allocator()};
            insert(p, std::make_move_iterator(tmp.begin()), std::make_move_iterator(tmp.end()));
        }
        return begin() + (p - cbegin());
    }
    // TODO: this is basically insert(n, x) but with iterators, building a views::repeat over n and
    // x would allow this to be reused, allowing to deduplicate code, views::repeat is C++23
    // however...
    template <detail::legacy_forward_iterator Iterator>
    constexpr auto insert(const_iterator p, Iterator first, Iterator last) -> iterator
    {
        if (first == last) [[unlikely]]
        {
            return begin() + (p - cbegin());
        }
        const auto num_input = std::distance(first, last);
        check_space(num_input);
        if (p == cend())
        {
            auto it = end();
            construct_from_range(first, last);
            return it;
        }
        const auto num_elems_to_shift = std::distance(p, cend());
        const pointer old_finish = this->storage.finish;
        const const_pointer new_finish = this->storage.finish + num_input;
        const pointer begin_of_new_range = this->storage.start + (p - cbegin());
        if (num_elems_to_shift > num_input)
        {
            pointer begin_of_construct_range = this->storage.finish - num_input;
            while (this->storage.finish != new_finish)
            {
                construct_single_at_end(std::move(*begin_of_construct_range++));
            }
            std::shift_right(begin_of_new_range, old_finish, num_input);
            std::copy(first, last, begin_of_new_range);
        }
        else
        {
            auto mid = first;
            std::advance(mid, num_elems_to_shift);
            auto begin_insert_construct = mid;
            pointer begin_of_construct_range = begin_of_new_range;
            while (begin_insert_construct != last)
            {
                construct_single_at_end(*begin_insert_construct++);
            }
            while (this->storage.finish != new_finish)
            {
                construct_single_at_end(std::move(*begin_of_construct_range++));
            }
            std::copy(first, mid, begin_of_new_range);
        }
        return iterator{begin_of_new_range};
    }

    constexpr auto insert(const_iterator p, std::initializer_list<value_type> il) -> iterator
    {
        return insert(p, il.begin(), il.end());
    }
    constexpr auto erase(const_iterator q) noexcept(std::is_move_assignable_v<value_type>)
        -> iterator
    {
        iterator p = begin() + (q - cbegin());
        std::shift_left(p, end(), 1);
        alloc_traits::destroy(get_value_allocator(), --(this->storage.finish));
        return p;
    }
    constexpr auto erase(
        const_iterator q1, const_iterator q2
    ) noexcept(std::is_move_assignable_v<value_type>) -> iterator
    {
        iterator p1 = begin() + (q1 - cbegin());
        if (q2 != cend())
        {
            std::shift_left(p1, end(), std::distance(q1, q2));
        }
        destroy_at_end(std::to_address(p1) + (cend() - q2));
        return p1;
    }
    constexpr void clear() noexcept { destroy_at_end(this->storage.start); }
    template <detail::legacy_input_iterator Iterator>
    constexpr void assign(Iterator first, Iterator last)
    {
        iterator start = begin();
        while (first != last && start != end())
        {
            *start++ = *first++;
        }
        if (first == last)
        {
            destroy_at_end(std::to_address(start));
        }
        else
        {
            while (first != last)
            {
                emplace_back(*first++);
            }
        }
    }
    template <detail::legacy_forward_iterator Iterator>
    constexpr void assign(Iterator first, Iterator last)
    {
        const auto n = std::distance(first, last);
        check_space(n, this->storage.start);
        assign_impl(first, last, n);
    }

    constexpr void assign(std::initializer_list<value_type> il) { assign(il.begin(), il.end()); }
    // TODO: like with insert(n, x), a views::repeat over n and t would allow reuse of assign_impl
    constexpr void assign(
        size_type n, const_reference t
    ) noexcept(std::is_nothrow_copy_constructible_v<value_type> && std::is_nothrow_copy_assignable_v<value_type>)
    {
        check_space(n, this->storage.start);
        const auto s = size();
        if (n > s)
        {
            std::fill(this->storage.start, this->storage.finish, t);
            construct_from_elem(t, n - s);
        }
        else
        {
            pointer mid = std::fill_n(this->storage.start, n, t);
            destroy_at_end(mid);
        }
    }

    /***********************************************************************************************
     * Section: Functions optionally by SequenceContainer
     **********************************************************************************************/

    constexpr auto front() noexcept -> reference
    {
        assert(size() > 0);
        return *begin();
    }
    constexpr auto front() const noexcept -> const_reference
    {
        assert(size() > 0);
        return *cbegin();
    }
    constexpr auto back() noexcept -> reference
    {
        assert(size() > 0);
        auto tmp = end();
        --tmp;
        return *tmp;
    }
    constexpr auto back() const noexcept -> const_reference
    {
        assert(size() > 0);
        auto tmp = cend();
        --tmp;
        return *tmp;
    }
    template <typename... Args>
    constexpr auto emplace_front(Args&&... args) -> reference
    {
        check_space(static_cast<size_type>(1));
        if (size() == 0)
        {
            construct_single_at_end(std::forward<Args>(args)...);
            return front();
        }
        temporary_value tmp{get_value_allocator(), std::forward<Args>(args)...};
        pointer old_finish = this->storage.finish;
        construct_single_at_end(std::move(back()));
        std::shift_right(this->storage.start, old_finish, 1);
        *(this->storage.start) = std::move(tmp.get());
        return front();
    }
    template <typename... Args>
    constexpr auto emplace_back(Args&&... args) -> reference
    {
        check_space(static_cast<size_type>(1));
        construct_single_at_end(std::forward<Args>(args)...);
        return back();
    }
    constexpr void push_front(const_reference t) { emplace_front(t); }
    constexpr void push_front(value_type&& t) { emplace_front(std::move(t)); }
    constexpr void push_back(const_reference t) { emplace_back(t); }
    constexpr void push_back(value_type&& t) { emplace_back(std::move(t)); }
    constexpr void pop_front() noexcept(std::is_nothrow_move_assignable_v<value_type>)
    {
        std::shift_left(begin(), end(), 1);
        alloc_traits::destroy(get_value_allocator(), --(this->storage.finish));
    }
    constexpr void pop_back() noexcept
    {
        alloc_traits::destroy(get_value_allocator(), --(this->storage.finish));
    }
    constexpr auto operator[](size_type n) noexcept -> reference
    {
        assert(n < size());
        return *(begin() + static_cast<difference_type>(n));
    }
    constexpr auto operator[](size_type n) const noexcept -> const_reference
    {
        assert(n < size());
        return *(cbegin() + static_cast<difference_type>(n));
    }
    constexpr auto at(size_type n) -> reference
    {
        if (n >= size())
        {
            throw std::out_of_range("static_vector access out of range");
        }
        return this->operator[](n);
    }
    constexpr auto at(size_type n) const -> const_reference
    {
        if (n >= size())
        {
            throw std::out_of_range("static_vector access out of range");
        }
        return this->operator[](n);
    }

private:
    constexpr void check_space(size_type n, pointer begin)
    {
        if (static_cast<size_type>(this->storage.end_of_allocation - begin) < n)
        {
            throw std::range_error{
                "Inserting element(s) into a static_vector that does not have enough space"
            };
        }
    }

    constexpr void check_space(size_type n) { check_space(n, this->storage.finish); }
    constexpr void check_space(difference_type n, pointer begin)
    {
        check_space(static_cast<size_type>(n), begin);
    }
    constexpr void check_space(difference_type n) { check_space(static_cast<size_type>(n)); }

    constexpr void delete_content() noexcept
    {
        destroy_at_end(this->storage.start);
        deallocate(this->storage.start, capacity());
    }

    constexpr void delete_content_and_null() noexcept
    {
        delete_content();
        typename base::static_vector_data tmp{std::move(this->storage)};
    }

    constexpr void destroy_at_end(pointer end, pointer new_end) noexcept
    {
        while (new_end != end)
        {
            alloc_traits::destroy(get_value_allocator(), std::to_address(--end));
        }
    }

    constexpr void destroy_at_end(pointer new_end) noexcept
    {
        while (new_end != this->storage.finish)
        {
            alloc_traits::destroy(get_value_allocator(), std::to_address(--storage.finish));
        }
    }

    template <std::input_iterator Iterator>
    constexpr void construct_from_range(Iterator begin, size_type n)
    {
        while (n-- > 0)
        {
            construct_single_at_end(*begin++);
        }
    }

    template <std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
    constexpr void construct_from_range(Iterator begin, Sentinel end)
    {
        while (begin != end)
        {
            construct_single_at_end(*begin++);
        }
    }

    template <std::input_iterator Iterator>
    constexpr void safe_construct_from_range(Iterator begin, size_type n)
    {
        try
        {
            construct_from_range(begin, n);
        }
        catch (...)
        {
            clear();
            throw;
        }
    }

    template <std::random_access_iterator Iterator>
    constexpr void safe_construct_from_range(Iterator begin, size_type n)
    {
        try
        {
            Iterator end = begin + n;
            construct_from_range(begin, end);
        }
        catch (...)
        {
            clear();
            throw;
        }
    }

    template <std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
    constexpr void safe_construct_from_range(Iterator begin, Sentinel end)
    {
        try
        {
            construct_from_range(begin, end);
        }
        catch (...)
        {
            clear();
            throw;
        }
    }

    constexpr void construct_from_elem(const value_type& v, size_type n)
    {
        while (n-- != 0)
        {
            construct_single_at_end(v);
        }
    }

    constexpr void safe_construct_from_elem(const value_type& v, size_type n)
    {
        try
        {
            construct_from_elem(v, n);
        }
        catch (...)
        {
            clear();
            throw;
        }
    }

    template <typename... Args>
    constexpr void construct_single_at_end(Args&&... args)
    {
        alloc_traits::construct(
            get_value_allocator(), std::to_address(this->storage.finish),
            std::forward<Args>(args)...
        );
        ++(this->storage.finish);
    }

    template <std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
    constexpr void init_with_size(Iterator first, Sentinel last, size_type n)
    {
        create_storage(n);
        safe_construct_from_range(first, last);
    }

    template <std::input_iterator Iterator, std::sentinel_for<Iterator> Sentinel>
    constexpr void init_without_size(Iterator first, Sentinel last)
    {
        auto n = size_type{0};
        try
        {
            while (first != last)
            {
                resize(++n);
                construct_single_at_end(*first++);
            }
        }
        catch (...)
        {
            clear();
            throw;
        }
    }

    template <std::forward_iterator Iterator>
    constexpr void assign_impl(Iterator first, Iterator last, difference_type n)
    {
        const size_type s = size();
        if (static_cast<size_type>(n) > s)
        {
            Iterator mid = first;
            std::advance(mid, s);
            std::copy(first, mid, this->storage.start);
            construct_from_range(mid, last);
        }
        else
        {
            pointer mid = std::copy(first, last, this->storage.start);
            destroy_at_end(mid);
        }
    }
};

template <
    detail::legacy_input_iterator Iterator,
    typename Alloc = std::allocator<typename std::iterator_traits<Iterator>::value_type>>
static_vector(Iterator, Iterator, Alloc = Alloc())
    -> static_vector<typename std::iterator_traits<Iterator>::value_type, Alloc>;

template <
    std::ranges::input_range R, typename Alloc = std::allocator<std::ranges::range_value_t<R>>>
static_vector(detail::from_range_t, R&&, Alloc = Alloc())
    -> static_vector<std::ranges::range_value_t<R>, Alloc>;

}  // namespace memory
inline constexpr memory::detail::from_range_t from_range{};
}  // namespace lola

#endif /* SRC_MEMORY_STATIC_VECTOR_H */
