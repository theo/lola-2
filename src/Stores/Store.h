/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\author Max Görner
\author Christian Koch
\author Lukas Zech
\status updated
\brief Interface for Stores. Uses std::function wrappers to abstract from some Store
without inheritance
*/

#pragma once

/*
 * Forward declarations
 */

class NetState;

template <typename Payload, typename State = NetState, bool isEmpty = false>
class Store;

class Mara;

#include <Core/Dimensions.h>
#include <Memory/Mara.h>
#include <Net/NetState.h>

#include <functional>

// forward declaration for friend functions
namespace StoreCreator
{
template <typename Payload, typename UnderlyingStore>
void createBindings(Store<Payload>* store, UnderlyingStore* ustore);
}

/*!
\example

State *s = NULL;

if (searchAndInsert(&s))
{
    // already there - work with *s;
}
else
{
    // *s has just been created
}
*/
template <typename Payload, typename State, bool isEmpty>
class Store
{
    template <typename T, typename UnderlyingStore>
    friend void StoreCreator::createBindings(Store<T>* store, UnderlyingStore* ustore);

private:
    /// checks whether the marking represented by the given NetState is already stored.
    /// @param ns NetState that needs to be checked
    /// @param payload reference to address of payload data. If the marking is found, the
    /// address is set to the (already existing) payload entry of that marking, if a new
    /// marking was added the address will be set to a newly allocated payload entry.
    /// @param thread the index of the thread that requests this call. Values will range
    /// from 0 to (number_of_threads - 1). Used to allow using thread-local auxiliary data
    /// structures without locking any variables.
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    std::function<bool(State&, Payload**, threadid_t, bool)> _searchAndInsert;

    /// gets and removes a state from the store
    /// @param ns NetState where the removed state will be written to
    /// @return false, if store was already empty
    std::function<bool(State&, threadid_t)> _popState;

    /// @brief Function wrapper to call the deleter of the interfacing store on
    /// destruction
    std::function<void()> _deleter;

protected:
    /// the maximum number of threads this store has to work with.
    /// The value is used to create thread-local auxiliary data structures if needed.
    const threadid_t number_of_threads;

    /// the number of stored markings. It is stored as an array of integers, one value per
    /// thread, to avoid lost updates when counting concurrently. The sum of all values is
    /// the actual total number of markings found.
    int* markings;

    /// the number of calls to searchAndInsert(). It is stored as an array of integers,
    /// one value per thread, to avoid lost updates when counting concurrently. The sum of
    /// all values is the actual total number of calls made.
    int* calls;

public:
    /// retrieves the current total number of markings stored.
    /// If the Store is used while calling this function, the return value might differ
    /// from the actual value (that continues increasing concurrently).
    /// The value is exact when the search is finished.
    int get_number_of_markings();

    /// retrieves the current total number of calls to searchAndInsert().
    /// If the Store is used while calling this function, the return value might differ
    /// from the actual value (that continues increasing concurrently).
    /// The value is exact when the search is finished.
    int get_number_of_calls();

    /// constructor; initialize auxiliary data structures if needed.
    /// @param number_of_threads maximum number of threads that may work with this Store
    /// concurrently.
    explicit Store(threadid_t number_of_threads);

    /// destructor; frees all memory used for auxiliary data structures
    ~Store();

    /// checks whether the marking represented by the given NetState is already stored.
    /// @param ns NetState that needs to be checked
    /// @param payload reference to address of payload data. If the marking is found, the
    /// address is set to the (already existing) payload entry of that marking, if a new
    /// marking was added the address will be set to a newly allocated payload entry.
    /// @param thread the index of the thread that requests this call. Values will range
    /// from 0 to (number_of_threads - 1). Used to allow using thread-local auxiliary data
    /// structures without locking any variables.
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    inline bool searchAndInsert(
        State& ns, Payload** payload, threadid_t thread, bool noinsert = false
    )
    {
        // count calls
        calls[thread]++;

        bool ret = _searchAndInsert(ns, payload, thread, noinsert);

        if (!ret && !noinsert)
        {
            markings[thread]++;
        }
        return ret;
    };

    /// gets and removes a state from the store
    /// @param ns NetState where the removed state will be written to
    /// @return false, if store was already empty
    inline bool popState(State& ns, threadid_t theadIndex) { return _popState(ns, theadIndex); };
};

// partial specialisation used for EmptyStore
template <typename Payload, typename State>
class Store<Payload, State, true>
{
public:
    /// the number of stored markings. EmptyStores don't save anything, so we just save
    /// overalls and don't care for threads
    int markings;

    /// the number of calls to searchAndInsert. EmptyStores don't save anything, so we
    /// just save overalls and don't care for threads
    int calls;

    int tries;

    /// retrieves the current total number of markings stored.
    /// If the Store is used while calling this function, the return value might differ
    /// from the actual value (that continues increasing concurrently).
    /// The value is exact when the search is finished.
    int get_number_of_markings() { return markings; };

    /// retrieves the current total number of calls to searchAndInsert().
    /// If the Store is used while calling this function, the return value might differ
    /// from the actual value (that continues increasing concurrently).
    /// The value is exact when the search is finished.
    int get_number_of_calls() { return calls; };

    explicit Store() : markings(0), calls(0), tries(0) {};

    ~Store() {};

    /// checks whether the marking represented by the given NetState is already stored.
    /// @param ns NetState that needs to be checked
    /// @param payload reference to address of payload data. If the marking is found, the
    /// address is set to the (already existing) payload entry of that marking, if a new
    /// marking was added the address will be set to a newly allocated payload entry.
    /// @param thread the index of the thread that requests this call. Values will range
    /// from 0 to (number_of_threads - 1). Used to allow using thread-local auxiliary data
    /// structures without locking any variables.
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    inline bool searchAndInsert(
        State const& ns, Payload** payload, threadid_t thread, bool noinsert = false
    )
    {
        if (payload)
        {
            *payload = nullptr;
        }
        return false;
    };

    /// gets and removes a state from the store
    /// @param ns NetState where the removed state will be written to
    /// @return false, if store was already empty
    inline bool popState(State& ns, threadid_t theadIndex) { return false; };
};

template <typename Payload, typename State = NetState>
using EmptyStore = Store<Payload, State, true>;

#include <Stores/Store.inc>
