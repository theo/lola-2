/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Gregor Behnke
\author Lukas Zech
\status new

\brief a meta-store to test, whether a new store works correctly
*/

#pragma once

#include <Core/Dimensions.h>
#include <Net/Petrinet.h>
#include <Net/NetState.h>

template <typename Payload, typename CorrectStore, typename TestStore, typename State = NetState>
class CompareStore
{
public:
    /// creates new Compare-Store using the two given Stores
    CompareStore(CorrectStore* correct, TestStore* test);

    /// frees both components
    ~CompareStore();

    bool searchAndInsert(
        State& ns, Payload** payload, threadid_t threadIndex, bool noinsert = false
    );

    /// gets and removes a state from the store
    /// @param ns NetState where the removed state will be written to
    /// @return false, if store was already empty
    bool popState(State& ns, threadid_t threadIndex = 0);

private:
    /// correct store
    CorrectStore* correctStore;
    /// store to be tested
    TestStore* testStore;
};

#include <Stores/CompareStore.inc>
