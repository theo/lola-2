/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status new

\brief VectorStore implementation using binary prefix trees. Based on BinStore.
Relies on the assumption that different input vectors (possibly of different
length) are not prefix of another.
*/

#include <lolaconf.h>
#include <Stores/VectorStores/PrefixTreeStore.h>

template <typename T, PrefixTreeStorable V>
PrefixTreeStore<T, V>::PrefixTreeStore(
    Mara* mem, arrayindex_t _vectorlength, threadid_t _number_of_threads
)
    : MemoryMara(mem), vectorlen(elem_info<V>::byte_size * _vectorlength),
      number_of_threads(_number_of_threads), branch(nullptr), firstvector(nullptr),
      popVectorCache((ValueType**)MemoryMara->staticCalloc(sizeof(void*), _number_of_threads))
{
    pthread_rwlock_init(&rwlock, nullptr);
}

/// create a new branch in the decision tree at depth b.
template <typename T, PrefixTreeStorable V>
PrefixTreeStore<T, V>::Decision::Decision(bitarrayindex_t b)
    : bit(b), vector(nullptr), nextold(nullptr), nextnew(nullptr)
{
}

/// search for an input vector in the suffix tree and insert it, if it is not present
template <typename T, PrefixTreeStorable V>
bool PrefixTreeStore<T, V>::searchAndInsert(
    const V* in, bitarrayindex_t bitlen, T** payload, threadid_t, bool noinsert
)
{
    /// If a new decision record is inserted, * anchor must point to it
    Decision** anchor;

    /// the place where the new vector goes to
    ValueType** newvector;

    /// the input word we are currently dealing with
    arrayindex_t input_index = 0;

    /// the last input word to consider.
    const arrayindex_t max_input_index = (bitlen - 1) / elem_info<V>::bit_size;

    /// the number of significant bits in the last input word (aligned at msb)
    arrayindex_t max_input_numbits = bitlen % elem_info<V>::bit_size;
    if (!max_input_numbits)
    {
        max_input_numbits = elem_info<V>::bit_size;
    }

    /// the bits of the current input word we have NOT dealt with so far
    bitarrayindex_t input_bitstogo = elem_info<V>::bit_size;  // indicates start with msb

    // pointer to the current input word
    // the reinterpret_cast should always be valid, since ValueType is either V itself
    // or a pointer type, where casting to uintptr_t is always valid
    const ValueType* pInput = reinterpret_cast<const ValueType*>(in);

    /// the starting address of the current vector. In each vector, the payload is loacted before
    /// the marking data.
    ValueType* pPayload;

    /// the vector word we are currently investigating
    ValueType* pVector;

    /// the bits of the current vector word we have NOT dealt with so far
    bitarrayindex_t vector_bitstogo = elem_info<V>::bit_size;  // indicates start with msb

    /// the number of bits processed since reaching the current branch
    bitarrayindex_t position = 0;

    size_t payloadSize = payload_size<T>::value;

    // lock the current bucket to ensure thread safety
    if (noinsert)
    {
        pthread_rwlock_rdlock(&rwlock);
    }
    else
    {
        pthread_rwlock_wrlock(&rwlock);
    }

    // Is hash bucket empty? If so, assign to currentvector
    if (!(pPayload = firstvector))
    {
        if (noinsert)
        {
            pthread_rwlock_unlock(&rwlock);
            return false;
        }

        // Indeed, hash bucket is empty --> just insert vector, no branch yet.
        newvector = &firstvector;
    }
    else
    {
        // skip payload. Cast to char* is done to ensure skipping exaclty the right amount of bytes.
        pVector = (ValueType*)(((char*)pPayload) + payloadSize);

        // Here, hash bucket is not empty.
        anchor = &branch;

        while (true)  // just entered new suffix tree vector
        {
            // number of bits to compare at once, searching for differences. Used only to locate the
            // exact bit position of the difference
            bitarrayindex_t comparebits;

            // maximum number of vector words to consider. The actual vector (pVector) can be
            // smaller, but in this case a difference is found before reaching the end anyway (see
            // getInput rule 2).
            arrayindex_t vectorlen = ((bitlen - position) + (elem_info<V>::bit_size - 1))
                / elem_info<V>::bit_size;

            // test for good alignment
            if (input_bitstogo == vector_bitstogo)
            {
                // good alignment, can use memcmp
                if (!memcmp(pInput, pVector, vectorlen * elem_info<V>::byte_size))
                {
                    // match found, we're done
                    pthread_rwlock_unlock(&rwlock);

                    if (payload)
                    {
                        *payload = (T*)pPayload;
                    }
                    return true;
                }
                // difference found, skip to first differing word.
                while (*pInput == *pVector)  // reaching end of input is impossible
                {
                    position += elem_info<V>::bit_size, pVector++;
                    input_index++, ++pInput;
                }
                comparebits = elem_info<V>::bit_size >> 1;  // initialize binary search for
                                                            // differing bit
            }
            else  // bad alignment; input_bitstogo < vector_bitstogo, since vector_bitstogo is
                  // always VECTOR_WIDTH at this point
            {
                while (true)  // vector_bitstogo == VECTOR_WIDTH == VECTOR_WIDTH
                {
                    // compare remaining input bits with msb bits of current vector
                    if ((ValueType(*pInput << (elem_info<V>::bit_size - input_bitstogo)))
                        == (ValueType(
                            (*pVector >> (elem_info<V>::bit_size - input_bitstogo))
                            << (elem_info<V>::bit_size - input_bitstogo)
                        )))
                    {
                        // they're equal, input word done. Test for EOI
                        if (++input_index <= max_input_index)
                        {
                            // compare msb of next input word with the remaining bits of the current
                            // vector word
                            if ((ValueType(*(++pInput) >> input_bitstogo) << input_bitstogo)
                                == (ValueType(*pVector << input_bitstogo)))
                            {
                                // they're equal, vector word done. Test for EOV and repeat with
                                // next vector word.
                                if (--vectorlen)
                                {
                                    position += elem_info<V>::bit_size, pVector++;
                                }
                                else
                                {
                                    pthread_rwlock_unlock(&rwlock);
                                    if (payload)
                                    {
                                        *payload = (T*)pPayload;
                                    }
                                    return true;
                                }
                            }
                            else
                            {
                                // difference found. Update bitstogo variables and setup binary
                                // search for differing bit
                                vector_bitstogo -= input_bitstogo;
                                input_bitstogo = elem_info<V>::bit_size;
                                comparebits = vector_bitstogo >> 1;
                                break;
                            }
                        }
                        else
                        {
                            pthread_rwlock_unlock(&rwlock);
                            if (payload)
                            {
                                *payload = (T*)pPayload;
                            }
                            return true;
                        }
                    }
                    else
                    {
                        // difference found. Setup binary search for differing bit
                        comparebits = input_bitstogo >> 1;
                        break;
                    }
                }
            }

            // difference was found in current input and vector words. locate the first differing
            // bit using binary search.
            while (comparebits)
            {
                // test if next <comparebits> bits of input and vector are equal
                if ((ValueType(*pInput << (elem_info<V>::bit_size - input_bitstogo))
                     >> (elem_info<V>::bit_size - comparebits))
                    == (ValueType(*pVector << (elem_info<V>::bit_size - vector_bitstogo))
                        >> (elem_info<V>::bit_size - comparebits)))
                {
                    // they're equal, move forward
                    vector_bitstogo -= comparebits;
                    input_bitstogo -= comparebits;
                    if (comparebits > input_bitstogo)
                    {
                        comparebits = input_bitstogo >> 1;
                    }
                    if (comparebits > vector_bitstogo)
                    {
                        comparebits = vector_bitstogo >> 1;
                    }
                }
                else
                {
                    // they differ, repeat using halved comparebits
                    comparebits >>= 1;
                }
            }
            // we're now exactly at the bit that differs. Search for suiting branch
            // skipping all early branches
            while ((*anchor)
                   && (position + (elem_info<V>::bit_size - vector_bitstogo)) > (*anchor)->bit)
            {
                anchor = &((*anchor)->nextold);
            }
            // test whether there already is a branch at the differing bit
            if ((*anchor)
                && (*anchor)->bit == (position + (elem_info<V>::bit_size - vector_bitstogo)))
            {
                // Indeed, there is. Switch to that branch and repeat with new suffix tree vector
                pPayload = (*anchor)->vector;
                pVector = (ValueType*)(((char*)pPayload) + payloadSize);
                anchor = &((*anchor)->nextnew);

                position += (elem_info<V>::bit_size - vector_bitstogo) + 1;
                vector_bitstogo = elem_info<V>::bit_size;

                // skip the differing bit. We don't need to store it since its value is determined
                // by the old vector and the branch position.
                input_bitstogo--;
                if (input_index == max_input_index
                    && input_bitstogo + max_input_numbits <= elem_info<V>::bit_size)
                {
                    pthread_rwlock_unlock(&rwlock);
                    if (payload)
                    {
                        *payload = (T*)pPayload;
                    }
                    return true;
                }
                if (!input_bitstogo)
                {
                    input_index++, ++pInput, input_bitstogo = elem_info<V>::bit_size;
                }
            }
            else
            {
                // there isn't. Place to insert new branch is found.
                break;
            }
        }

        // in case we do not want to insert the state, stop right now
        if (noinsert)
        {
            pthread_rwlock_unlock(&rwlock);
            return false;
        }

        // state not found --> prepare for insertion
        void* mmm = MemoryMara->staticNew(sizeof(Decision));
        Decision* newdecision = new (mmm)
            Decision(position + (elem_info<V>::bit_size - vector_bitstogo));
        newdecision->nextold = *anchor;
        *anchor = newdecision;
        newdecision->nextnew = NULL;
        newvector = &(newdecision->vector);
        // the mismatching bit itself is not represented in the new vector
        position += (elem_info<V>::bit_size - vector_bitstogo) + 1;
        vector_bitstogo = elem_info<V>::bit_size;

        input_bitstogo--;
        // difference occurred in the very last bit of the input string
        if (input_index == max_input_index
            && input_bitstogo + max_input_numbits == elem_info<V>::bit_size)
        {
            // allocate space for payload if necessary
            if (payloadSize)
            {
                //*newvector = (V *) calloc(payloadSize, 1);
                *newvector = (ValueType*)MemoryMara->staticCalloc(payloadSize, 1);
                pPayload = *newvector;
            }

            pthread_rwlock_unlock(&rwlock);
            if (payload)
            {
                *payload = (T*)pPayload;
            }
            return false;
        }
        if (!input_bitstogo)
        {
            input_index++, ++pInput, input_bitstogo = elem_info<V>::bit_size;
        }
    }

    assert(bitlen >= position);
    // vector_bitstogo is always VECTOR_WIDTH here
    int newvectorlen = ((bitlen - position) + (elem_info<V>::bit_size - 1))
        / elem_info<V>::bit_size;
    // \TODO: Understand this construct and how it can be translated to a new
    //*newvector = (V *) calloc(payloadSize + newvectorlen * SIZEOF_V, 1);
    *newvector = (ValueType*)MemoryMara->staticCalloc(
        payloadSize + newvectorlen * elem_info<V>::byte_size, 1
    );
    pPayload = *newvector;
    pVector = (ValueType*)(((char*)pPayload) + payloadSize);

    // test for good alignment
    if (input_bitstogo == elem_info<V>::bit_size)
    {
        // good alignment, use memcpy
        memcpy(pVector, pInput, newvectorlen * elem_info<V>::byte_size);
        pthread_rwlock_unlock(&rwlock);
        if (payload)
        {
            *payload = (T*)pPayload;
        }
        return false;
    }
    else
    {
        // bad alignment, copy contents manually
        while (newvectorlen--)
        {
            *pVector |= ValueType(*pInput << (elem_info<V>::bit_size - input_bitstogo));
            pInput++;
            if (++input_index > max_input_index)
            {
                break;
            }
            *pVector |= ValueType(*pInput >> input_bitstogo);
            pVector++;
        }
        pthread_rwlock_unlock(&rwlock);
        if (payload)
        {
            *payload = (T*)pPayload;
        }
        return false;
    }
}

/*!
 * \brief  gets and removes a vector from the store
 */
template <typename T, PrefixTreeStorable V>
bool PrefixTreeStore<T, V>::popState(V*& out, threadid_t threadIndex)
{
    // create intermediate vector
    if (!(popVectorCache[threadIndex]))
    {
        popVectorCache[threadIndex] = (ValueType*)MemoryMara->staticNew(vectorlen);
    }

    // vector currently viewed
    ValueType* pPayload = nullptr;
    ValueType* pVector = nullptr;
    // anchor to a decision node
    Decision* anchor = nullptr;
    Decision** pAnchor = nullptr;

    // lock the current bucket to ensure thread safety
    pthread_rwlock_wrlock(&rwlock);

    if (!(pPayload = firstvector))
    {
        pthread_rwlock_unlock(&rwlock);
        return false;
    }
    // skip payload data
    pVector = (ValueType*)(((char*)pPayload) + payload_size<T>::value);

    // copy "first" vector to return it later
    memcpy(popVectorCache[threadIndex], pVector, vectorlen);
    out = reinterpret_cast<V*>(popVectorCache[threadIndex]);

    if (!(anchor = branch))
    {
        // no further markings in this bucket
        free(pPayload);
        firstvector = NULL;
        pthread_rwlock_unlock(&rwlock);
        return true;
    }

    // we start with the root of the decision tree
    pAnchor = &branch;
    // and search for the decision node differing as last from the "first" vector
    while (anchor->nextold)
    {
        pAnchor = &(anchor->nextold);
        anchor = anchor->nextold;
    }

    // alignment of both vectors (i.e. relative position of first bit of anchor->vector)
    bitarrayindex_t shift = (anchor->bit + 1) % elem_info<V>::bit_size;
    // remaining words to copy
    bitarrayindex_t remaining = (vectorlen * 8 - anchor->bit - 1) / elem_info<V>::bit_size;

    // go to position of first differing bit
    pVector += (anchor->bit / elem_info<V>::bit_size);
    // toggle bit
    *pVector ^= 1 << (elem_info<V>::bit_size - (anchor->bit % elem_info<V>::bit_size) - 1);

    // vector to be copied from
    ValueType* source = anchor->vector;

    // test for good alignment
    if (shift == 0)
    {
        // good alignment, use memcpy
        memcpy(++pVector, source, remaining * elem_info<V>::byte_size);
    }
    else
    {
        // bad alignment, copy manually
        // initially clear recent word
        *pVector =
            ((*pVector >> (elem_info<V>::bit_size - shift)) << (elem_info<V>::bit_size - shift));

        while (true)
        {
            // copy lower half
            *(pVector++) |= *source >> shift;
            if (remaining-- == 0)
            {
                // finished copying
                break;
            }
            // copy upper half
            *pVector = *(source++) << (elem_info<V>::bit_size - shift);
        }
    }

    // remove node
    *pAnchor = anchor->nextnew;
    anchor->nextnew = NULL;  // prevent it from being deleted when calling the destructor
    delete anchor;

    // return result
    pthread_rwlock_unlock(&rwlock);
    return true;
}
