/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status new

\brief VectorStore implementation using binary prefix trees. Based on BinStore.
Relies on the assumption that different input vectors (possibly of different
length) are not prefix of another.
*/

#pragma once

#include <Memory/Mara.h>
#include <Net/Petrinet.h>
#include <Stores/Concepts.h>
#include <type_traits>

template <typename T>
struct payload_size
{
    static constexpr size_t value = sizeof(T);
};

template <>
struct payload_size<void>
{
    static constexpr size_t value = 0;
};

template <typename V>
struct elem_info
{
    static constexpr size_t byte_size = sizeof(V);
    static constexpr size_t bit_size = sizeof(V) * 8;
    typedef V type;
};

template <typename V>
requires(std::is_pointer_v<V>)
struct elem_info<V>
{
    static constexpr size_t byte_size = sizeof(V);
    static constexpr size_t bit_size = sizeof(V) * 8;
    typedef std::uintptr_t type;
};

template <typename T, PrefixTreeStorable V = unsigned int>
class PrefixTreeStore
{
    using ValueType = elem_info<V>::type;

public:
    /// constructor (parameter needed only if elements can be retrieved from the store)
    explicit PrefixTreeStore(
        Mara* mem, arrayindex_t vectorlength, threadid_t number_of_threads = 1
    );
    /// destructor
    ~PrefixTreeStore() = default;

    /// searches for a vector and inserts if not found
    /// @param in vector to be seached for or inserted
    /// @param bitlen length of vector
    /// @param payload pointer to be set to the place where the payload of this state will be held
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    bool searchAndInsert(
        const V* in, bitarrayindex_t bitlen, T** payload, threadid_t, bool noinsert = false
    );

    /// gets and removes a vector from the store
    /// @param out place where the returned vector will be written to
    /// @param threadIndex the index of the thread that requests this call.
    /// @return false, if the store was already empty, otherwise true
    bool popState(V*& out, threadid_t threadIndex = 0);

    /// check if the store is empty
    /// @return true, if the store is empty
    bool empty() { return !firstvector; };
    Mara* MemoryMara;
    arrayindex_t vectorlen;

private:
    /// a binary decision node
    class Decision
    {
    public:
        /// constructor
        /// @param b index of first bit different from previous vector
        explicit Decision(bitarrayindex_t b);
        /// index of first bit different from previous vector
        bitarrayindex_t bit;
        /// remaining vector after first differing bit
        ValueType* vector;
        /// decision nodes differing later than this one
        Decision* nextold;
        /// decision nodes differing from the remaining vector of this node
        Decision* nextnew;
        /// destructor
        ~Decision() = default;
    };

    // first branch in decision tree; NULL as long as less than two elements
    Decision* branch;

    // the read-write mutex
    pthread_rwlock_t rwlock;

    // first vector; null as long as empty
    ValueType* firstvector;

    // full vector will be reconstructed here when popping vectors
    ValueType** popVectorCache;

    threadid_t number_of_threads;
};

#include <Stores/VectorStores/PrefixTreeStore.inc>
