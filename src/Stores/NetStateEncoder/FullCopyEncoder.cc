/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\status new

\brief NetStateEncoder implementation that copies the marking, while ignoring
capacity limitations. The copy operation isn't done at all if possible (just
passing the marking pointer), otherwise memcpy is used
*/

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/NetState.h>
#include <Stores/NetStateEncoder/FullCopyEncoder.h>

FullCopyEncoder::FullCopyEncoder(const arrayindex_t numWords, int numThreads)
    : CopyEncoder(numWords, numThreads)
{
}

FullCopyEncoder::~FullCopyEncoder() {}

// decodes a given encoded state and sets the netstate appropriately
void FullCopyEncoder::decodeState(NetState& ns, capacity_t* data)
{
    // just copy the data back to the NetState
    memcpy(ns.Current, data, insize * sizeof(capacity_t));
}

LTLFullCopyEncoder::LTLFullCopyEncoder(const arrayindex_t numWords, int numThreads)
    : LTLCopyEncoder(numWords, numThreads), nocopy(false)
{
}

LTLFullCopyEncoder::~LTLFullCopyEncoder() {}

// decodes a given encoded state and sets the netstate appropriately
void LTLFullCopyEncoder::decodeState(NetState& ns, capacity_t* data)
{
    // just copy the data back to the NetState
    memcpy(ns.Current, data, insize * sizeof(capacity_t));
    ns.buchistate = data[insize];
}
