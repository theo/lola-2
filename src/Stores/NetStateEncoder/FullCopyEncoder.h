/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\status new

\brief NetStateEncoder implementation that copies the marking, while ignoring
capacity limitations. The copy operation isn't done at all if possible (just
passing the marking pointer), otherwise memcpy is used
*/

#pragma once

#include <Stores/NetStateEncoder/CopyEncoder.h>

class FullCopyEncoder : public CopyEncoder
{
public:
    /// constructor
    /// @param numThreads maximum number of threads that may work with this
    /// NetStateEncoder concurrently.
    explicit FullCopyEncoder(const arrayindex_t, int numThreads);
    /// destructor
    ~FullCopyEncoder();

    /// decodes a given encoded state and sets the netstate appropriately
    /// @param ns NetState the decoded state will be written to
    /// @param data vector to be decoded
    virtual void decodeState(NetState& ns, capacity_t* data);
};

class LTLFullCopyEncoder : public LTLCopyEncoder
{
public:
    /// constructor
    /// @param numThreads maximum number of threads that may work with this
    /// NetStateEncoder concurrently.
    explicit LTLFullCopyEncoder(const arrayindex_t, int numThreads);
    /// destructor
    ~LTLFullCopyEncoder();

    /// decodes a given encoded state and sets the netstate appropriately
    /// @param ns NetState the decoded state will be written to
    /// @param data vector to be decoded
    virtual void decodeState(NetState& ns, capacity_t* data);

private:
    bool nocopy;
};
