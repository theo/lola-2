/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Gregor Behnke
\author Lukas Zech
\status new

\brief a meta-store to test, whether a new store works correctly
*/

#include <Stores/CompareStore.h>

template <typename Payload, typename CorrectStore, typename TestStore, typename State>
CompareStore<Payload, CorrectStore, TestStore, State>::CompareStore(
    CorrectStore* correct, TestStore* test
)
    : correctStore(correct), testStore(test)
{
}

template <typename Payload, typename CorrectStore, typename TestStore, typename State>
CompareStore<Payload, CorrectStore, TestStore, State>::~CompareStore()
{
    // TODO creates a segfault when correctStore gets freed..
    // delete correctStore;
    delete testStore;
}

template <typename Payload, typename CorrectStore, typename TestStore, typename State>
bool CompareStore<Payload, CorrectStore, TestStore, State>::searchAndInsert(
    State& ns, Payload** payload, threadid_t threadIndex, bool
)
{
    bool correctResult = correctStore->searchAndInsert(ns, payload, threadIndex);
    bool testResult = testStore->searchAndInsert(ns, payload, threadIndex);

    if (correctResult == testResult)
    {
        return correctResult;
    }
    RT::abort(ERROR::SYNTAX);
    assert(false);
    return true;  // cannot happen - only to silence compiler  LCOV_EXCL_LINE
}

/*!
 * \brief gets and removes a state from the store
 * \param ns NetState where the removed state will be written to
 * \return false, if store was already empty
 */
template <typename Payload, typename CorrectStore, typename TestStore, typename State>
bool CompareStore<Payload, CorrectStore, TestStore, State>::popState(
    State& ns, threadid_t threadIndex
)
{
    testStore->popState(ns, threadIndex);
    return correctStore->popState(ns, threadIndex);
}
