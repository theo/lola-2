/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status updated

\brief general-purpose base store consisting of two components: a
NetStateEncoder, that converts the current state into an input vector, and a
VectorStore, that stores the input vectors and checks whether they are already
known. Implementations for both components can be combined arbitrarily.
Specific components may have some functional or compatibility limitations,
though.
*/

#pragma once

#include <Stores/StateEncoder.h>
#include <Net/Petrinet.h>
#include <Memory/Mara.h>
#include <Stores/Concepts.h>
#include <Net/NetState.h>

template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
class PluginStore
{
    static_assert(is_storable<EncodedState>, "The State is not storable after encoding!");
    static_assert(
        std::is_member_function_pointer_v<decltype(&UnderlyingStore::searchAndInsert)>,
        "UnderlyingStore does not define a searchAndInsert!"
    );
    static_assert(
        std::is_member_function_pointer_v<decltype(&UnderlyingStore::popState)>,
        "UnderlyingStore does not define a popState!"
    );

public:
    /// creates new Store using the specified components. The given components are
    /// assumed to be used exclusively and are freed once the PluginStore gets
    /// destructed.
    PluginStore(
        StateEncoder<State, EncodedState>* _stateEncoder, UnderlyingStore* _actualStore, Mara* mem,
        threadid_t _number_of_threads
    );

    /// creates new Store using the specified components. The given components are
    /// assumed to be used exclusively and are freed once the PluginStore gets
    /// destructed.
    PluginStore(
        StateEncoder<State, EncodedState>* _stateEncoder, Mara* mem, StoreArgs... args,
        threadid_t _number_of_threads
    );

    /// frees both components
    ~PluginStore();

    bool searchAndInsert(
        State& ns, Payload** payload, threadid_t threadIndex, bool noinsert = false
    );

    /// gets and removes a state from the store
    /// @param ns NetState where the removed state will be written to
    /// @return false, if store was already empty
    bool popState(State& ns, threadid_t threadIndex = 0);

    bool empty() { return actualStore->empty(); };

private:
    /// used NetStateEncoder (given in constructor)
    StateEncoder<State, EncodedState>* stateEncoder;
    /// used VectorStore (given in constructor)
    UnderlyingStore* actualStore;
};

// some convenient templates based on commonly used types
template <typename Payload, typename UnderlyingStore, typename... StoreArgs>
using NSPluginStore = PluginStore<Payload, NetState, unsigned int*, UnderlyingStore, StoreArgs...>;

#include <Stores/StoreWrapper/PluginStore.inc>
