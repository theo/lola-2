/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status new

\brief VectorStore wrapper for bucketing.
*/

#include <Stores/StoreWrapper/HashingWrapperStore.h>

template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::HashingWrapperStore(
    Mara* mem, StoreArgs... args, arrayindex_t _number_of_buckets, threadid_t _number_of_threads
) requires(is_storable<State>)
    : number_of_buckets(_number_of_buckets)
{
    buckets = new UnderlyingStore*[number_of_buckets];
    // rwlocks =(pthread_rwlock_t*) calloc(sizeof(pthread_rwlock_t), number_of_buckets);

    for (hash_t i = 0; i < number_of_buckets; i++)
    {
        buckets[i] = new UnderlyingStore(mem, args..., _number_of_threads);
        // pthread_rwlock_init(rwlocks+i, NULL);
    }

    currentPopBucket = new arrayindex_t[_number_of_threads]();
}

template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::HashingWrapperStore(
    StateEncoder<State, unsigned int*>* enc, Mara* mem, StoreArgs... args,
    arrayindex_t _number_of_buckets, threadid_t _number_of_threads
) requires(!is_storable<State>)
    : number_of_buckets(_number_of_buckets), state_encoder(enc)
{
    buckets = new UnderlyingStore*[number_of_buckets];
    // rwlocks =(pthread_rwlock_t*) calloc(sizeof(pthread_rwlock_t), number_of_buckets);

    for (hash_t i = 0; i < number_of_buckets; i++)
    {
        buckets[i] = new UnderlyingStore(mem, args..., _number_of_threads);
        // pthread_rwlock_init(rwlocks+i, NULL);
    }

    currentPopBucket = new arrayindex_t[_number_of_threads]();
}

template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::~HashingWrapperStore()
{
    for (hash_t i = 0; i < number_of_buckets; i++)
    {
        delete buckets[i];
    }
    delete[] buckets;

    // for (hash_t i = 0; i < number_of_buckets; i++)
    //     pthread_rwlock_destroy(rwlocks+i);
    // free(rwlocks);

    delete[] currentPopBucket;
}

/// search for an input vector in the suffix tree and insert it, if it is not present
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
bool HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::searchAndInsert(
    State& in, bitarrayindex_t bitlen, hash_t hash, Payload** payload, threadid_t threadIndex,
    bool noinsert
) requires(is_storable<State>)
{
    /*if (!buckets[hash]) {
        // lock the current bucket to ensure thread safety
        pthread_rwlock_wrlock(rwlocks + hash);

        if(!buckets[hash])
            buckets[hash] = (*storeCreator)();

        pthread_rwlock_unlock(rwlocks + hash);
    }*/
    assert(hash >= 0 && hash < number_of_buckets);
    // since State is a storable type, we can assume that the UnderlyingStore can handle
    // in without further preprocessing, so we just choose the bucket and pass values
    return buckets[hash]->searchAndInsert(in, bitlen, payload, threadIndex, noinsert);
}

/// search for an input vector in the suffix tree and insert it, if it is not present
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
bool HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::searchAndInsert(
    State& in, Payload** payload, threadid_t threadIndex, bool noinsert
) requires(!is_storable<State>)
{
    /*if (!buckets[hash]) {
        // lock the current bucket to ensure thread safety
        pthread_rwlock_wrlock(rwlocks + hash);

        if(!buckets[hash])
            buckets[hash] = (*storeCreator)();

        pthread_rwlock_unlock(rwlocks + hash);
    }*/
    // since State is not storable, we use the passed StateEncoder to encode the state.
    // If the type State also has a HashCurrent member, we use their value instead of the
    // provided value
    bitarrayindex_t bitlen;
    unsigned int* input = state_encoder->encodeState(in, bitlen, threadIndex);
    if constexpr (has_hash_member<State>)
    {
        return buckets[in.HashCurrent % number_of_buckets]->searchAndInsert(
            input, bitlen, payload, threadIndex, noinsert
        );
    }
    else
    {
        return buckets[0]->searchAndInsert(in, bitlen, payload, threadIndex, noinsert);
    }
}

/*!
 * \brief  gets and removes a vector from the store
 */
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
bool HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::popState(
    State& out, threadid_t threadIndex
) requires(is_storable<State>)
{
    hash_t startingPopBucket = currentPopBucket[threadIndex];
    do
    {
        if (buckets[currentPopBucket[threadIndex]]->popState(out, threadIndex))
        {
            return true;
        }
        currentPopBucket[threadIndex] = (currentPopBucket[threadIndex] + 1) % number_of_buckets;
    } while (currentPopBucket[threadIndex] != startingPopBucket);
    return false;
}

/*!
 * \brief  gets and removes a vector from the store
 */
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
bool HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::popState(
    State& out, threadid_t threadIndex
) requires(!is_storable<State>)
{
    hash_t startingPopBucket = currentPopBucket[threadIndex];
    unsigned int* vec = nullptr;
    do
    {
        if (buckets[currentPopBucket[threadIndex]]->popState(vec, threadIndex))
        {
            state_encoder->decodeState(out, vec);
            return true;
        }
        currentPopBucket[threadIndex] = (currentPopBucket[threadIndex] + 1) % number_of_buckets;
    } while (currentPopBucket[threadIndex] != startingPopBucket);
    return false;
}

/*!
 * \brief Check if the store is empty.
 * \return If the store is empty
 */
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
bool HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::empty()
{
    // A better solution is needed if the number of buckets can be large
    // At this time, only the SweepLine method uses empty(), and only
    // with a very small number of buckets (per progress value and mode)
    for (hash_t i = 0; i < number_of_buckets; ++i)
        if (!buckets[i]->empty())
        {
            return false;
        }
    return true;
}

/*!
 * \brief Get the hash value of the last marking returned by popVector().
 * \return a hash value
 */
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
hash_t HashingWrapperStore<Payload, State, UnderlyingStore, StoreArgs...>::getLastHash(
    threadid_t threadIndex
)
{
    return currentPopBucket[threadIndex];
}
