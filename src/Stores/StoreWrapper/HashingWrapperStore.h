/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status updated

\brief Wrapper for a set of stores selected based on hash buckets. Creates the stores
 as part of its constructor
*/

#pragma once

#include <Core/Dimensions.h>
#include <Net/Petrinet.h>
#include <Memory/Mara.h>
#include <Stores/StateEncoder.h>
#include <Stores/Concepts.h>

#include <variant>

/// A HashingWrapperStore provides a hash based bucketing mechanism and can be combined
/// with any underlying Store. It creates a fixed number of buckets, each with its
/// own Store, and redirects the incoming searchAndInsert calls to one of them
/// depending on a hashvalue.
template <typename Payload, typename State, typename UnderlyingStore, typename... StoreArgs>
class HashingWrapperStore
{
    static_assert(
        std::is_member_function_pointer_v<decltype(&UnderlyingStore::searchAndInsert)>,
        "UnderlyingStore does not define a searchAndInsert!"
    );
    static_assert(
        std::is_member_function_pointer_v<decltype(&UnderlyingStore::popState)>,
        "UnderlyingStore does not define a popState!"
    );
    static_assert(
        std::is_member_function_pointer_v<decltype(&UnderlyingStore::empty)>,
        "UnderlyingStore does not define a empty!"
    );

public:
    // constructor (optional parameter _number_of_threads necessary if elements should be
    // retrievable by popVector())
    // This constructor can only be used if the type the Store stores is storable,
    // otherwise we require the use of an encoder
    HashingWrapperStore(
        Mara* mem, StoreArgs... args, arrayindex_t _number_of_buckets = 1,
        threadid_t _number_of_threads = 1
    ) requires(is_storable<State>);

    // constructor for non-storable states, meaning we require a StateEncoder
    HashingWrapperStore(
        StateEncoder<State, unsigned int*>* enc, Mara* mem, StoreArgs... args,
        arrayindex_t _number_of_buckets = 1, threadid_t _number_of_threads = 1
    ) requires(!is_storable<State>);
    /// destructor
    ~HashingWrapperStore();

    /// searches for a state and inserts if not found, signature only available iff
    /// type(state) is storable
    /// @param in state to be seached for or inserted
    /// @param bitlen size of the vector in bits
    /// @param hash of current state
    /// @param payload pointer to be set to the place where the payload of this state will
    /// be held
    /// @param threadIndex the index of the thread that requests this call. Values will
    /// range from 0 to (number_of_threads - 1). Used to allow using thread-local
    /// auxiliary data structures without locking any variables.
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    bool searchAndInsert(
        State& in, bitarrayindex_t bitlen, hash_t hash, Payload** payload, threadid_t threadIndex,
        bool noinsert = false
    ) requires(is_storable<State>);

    /// perform preprocessing of state, search in the underlying store and insert if not
    /// found, signature only available iff type(state) is not storable
    /// @param in state to be seached for or inserted
    /// @param hash of current state
    /// @param payload pointer to be set to the place where the payload of this state will
    /// be held
    /// @param threadIndex the index of the thread that requests this call. Values will
    /// range from 0 to (number_of_threads - 1). Used to allow using thread-local
    /// auxiliary data structures without locking any variables.
    /// @param noinsert if set to true only a search is done
    /// @return true, if the marking was found in the store, otherwise false.
    bool searchAndInsert(
        State& in, Payload** payload, threadid_t threadIndex, bool noinsert = false
    ) requires(!is_storable<State>);

    /// gets and removes a vector from the store
    /// @param out place where the returned vector will be written to
    /// @param threadIndex the index of the thread that requests this call.
    /// @return false, if the store was already empty, otherwise true
    bool popState(State& out, threadid_t threadIndex = 0) requires(is_storable<State>);

    /// gets and removes a vector from the store
    /// @param out place where the returned vector will be written to
    /// @param threadIndex the index of the thread that requests this call.
    /// @return false, if the store was already empty, otherwise true
    bool popState(State& out, threadid_t threadIndex = 0) requires(!is_storable<State>);

    /// check if the store is empty
    /// @return true, if the store is empty
    bool empty();

    /// return the hash value of the last marking returned by popState()
    hash_t getLastHash(threadid_t threadIndex = 0);

    UnderlyingStore** buckets;

private:
    arrayindex_t* currentPopBucket;
    arrayindex_t number_of_buckets;
    // we only want to save an encoder in case State is not storable
    // np_unique_address allows us to not waste space for a pointer that is not used
    [[no_unique_address]] std::conditional_t<
        !is_storable<State>, StateEncoder<State, unsigned int*>*, std::monostate>
        state_encoder;
};

// some convenient templates based on commonly used types
template <typename Payload, typename UnderlyingStore, typename... StoreArgs>
using VHashingWrapperStore =
    HashingWrapperStore<Payload, unsigned int*, UnderlyingStore, StoreArgs...>;

template <typename Payload, typename UnderlyingStore, typename... StoreArgs>
using NSHashingWrapperStore = HashingWrapperStore<Payload, NetState, UnderlyingStore, StoreArgs...>;

#include <Stores/StoreWrapper/HashingWrapperStore.inc>
