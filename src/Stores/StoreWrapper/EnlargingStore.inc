/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas Zech
\status updated

\brief general-purpose base store consisting of two components: a
NetStateEncoder, that converts the current state into a input vector, and a
VectorStore, that stores the input vectors and checks whether they are already
known. Implementations for both components can be combined arbitrarily.
Specific components may have some functional or compatibility limitations,
though.
*/

#include <Core/Dimensions.h>
#include <Stores/StoreWrapper/EnlargingStore.h>

template <
    typename Payload, typename EncodedState, PrefixTreeStorable EnlargedState,
    typename UnderlyingStore, typename... StoreArgs>
EnlargingStore<Payload, EncodedState, EnlargedState, UnderlyingStore, StoreArgs...>::EnlargingStore(
    StateEncoder<NetState, EncodedState>* _stateEncoder, UnderlyingStore* _actualStore, Mara* mem,
    threadid_t _number_of_threads, Petrinet* net
)
    : stateEncoder(_stateEncoder), actualStore(_actualStore),
      inputs(new EnlargedState*[_number_of_threads]), number_of_threads(_number_of_threads)
{
    auto* dummy = NetState::createNetStateFromInitial(net);
    bitarrayindex_t bitlen;
    auto res = stateEncoder->encodeState(*dummy, bitlen, 0);
    delete dummy;
    for (arrayindex_t i = 0; i < number_of_threads; i++)
    {
        inputs[i] = new EnlargedState
            [(bitlen + sizeof(std::remove_pointer_t<EncodedState>) * 8 - 1)
             / (sizeof(std::remove_pointer_t<EncodedState>) * 8)];
    }
}

template <
    typename Payload, typename EncodedState, PrefixTreeStorable EnlargedState,
    typename UnderlyingStore, typename... StoreArgs>
EnlargingStore<Payload, EncodedState, EnlargedState, UnderlyingStore, StoreArgs...>::EnlargingStore(
    StateEncoder<NetState, EncodedState>* _stateEncoder, Mara* mem, StoreArgs... args,
    threadid_t _number_of_threads, Petrinet* net
)
    : stateEncoder(_stateEncoder),
      actualStore(new UnderlyingStore(mem, args..., _number_of_threads)),
      inputs(new EnlargedState*[_number_of_threads]), number_of_threads(_number_of_threads)
{
    auto* dummy = NetState::createNetStateFromInitial(net);
    bitarrayindex_t bitlen;
    auto res = stateEncoder->encodeState(*dummy, bitlen, 0);
    delete dummy;
    for (arrayindex_t i = 0; i < number_of_threads; i++)
    {
        inputs[i] = new EnlargedState
            [(bitlen + sizeof(std::remove_pointer_t<EncodedState>) * 8 - 1)
             / (sizeof(std::remove_pointer_t<EncodedState>) * 8)];
    }
}

template <
    typename Payload, typename EncodedState, PrefixTreeStorable EnlargedState,
    typename UnderlyingStore, typename... StoreArgs>
EnlargingStore<
    Payload, EncodedState, EnlargedState, UnderlyingStore, StoreArgs...>::~EnlargingStore()
{
    delete stateEncoder;
    delete actualStore;
    for (arrayindex_t i = 0; i < number_of_threads; i++)
    {
        delete[] inputs[i];
    }
    delete[] inputs;
}

template <
    typename Payload, typename EncodedState, PrefixTreeStorable EnlargedState,
    typename UnderlyingStore, typename... StoreArgs>
bool EnlargingStore<Payload, EncodedState, EnlargedState, UnderlyingStore, StoreArgs...>::
    searchAndInsert(NetState& ns, Payload** payload, threadid_t threadIndex, bool noinsert)
{
    // fetch input vector
    bitarrayindex_t bitlen;
    EncodedState input = stateEncoder->encodeState(ns, bitlen, threadIndex);
    for (arrayindex_t i = 0; i < (bitlen + sizeof(std::remove_pointer_t<EncodedState>) * 8 - 1)
             / (sizeof(std::remove_pointer_t<EncodedState>) * 8);
         i++)
    {
        // copy value into enlarged field;
        // do bitshifting to fill additional bits for funsies;
        // need the static_cast to have enough space in intermediary values;
        // since we can't do shifting on pointer types, we use uintptr_t for those and
        // reinterpret_cast them back afterwards
        inputs[threadIndex][i] = reinterpret_cast<EnlargedState>(
            (static_cast<std::conditional_t<
                 std::is_pointer_v<EnlargedState>, std::uintptr_t, EnlargedState>>(input[i])
             << (sizeof(std::remove_pointer_t<EncodedState>) * 8))
            + input[i]
        );
    }

    return actualStore->searchAndInsert(
        inputs[threadIndex],
        bitlen * sizeof(EnlargedState) / sizeof(std::remove_pointer_t<EncodedState>), payload,
        threadIndex, noinsert
    );
}

/*!
 * \brief gets and removes a state from the store
 * \param ns Where the removed state will be written to
 * \return false, if store was already empty
 */
template <
    typename Payload, typename EncodedState, PrefixTreeStorable EnlargedState,
    typename UnderlyingStore, typename... StoreArgs>
bool EnlargingStore<Payload, EncodedState, EnlargedState, UnderlyingStore, StoreArgs...>::popState(
    NetState& ns, threadid_t threadIndex
)
{
    RT::log<info>("this store cannot return states");
    RT::abort(ERROR::COMMANDLINE);
    return false;
}
