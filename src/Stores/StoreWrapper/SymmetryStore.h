/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\author Lukas Zech
\status new

\brief A wrapper for the symmetry reduction that transforms a marking into
its canonical representative and passes it to another store.
*/

#pragma once

#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Symmetry/Symmetry.h>

template <typename Payload, typename UnderlyingStore>
class SymmetryStore
{
public:
    ~SymmetryStore()
    {
        delete G;
        delete actualStore;
    };
    SymmetryStore(Petrinet* n, UnderlyingStore* _actualStore, GeneratingSet* _G)
        : actualStore(_actualStore), net(n), G(_G) {};

    bool searchAndInsert(NetState& ns, Payload** payload, threadid_t thread, bool noinsert = false)
    {
        // canonize
        capacity_t* canrep = new capacity_t[this->net->Card[PL]];
        memcpy(canrep, ns.Current, this->net->Card[PL] * sizeof(capacity_t));

        canrep = G->canonize(canrep);
        capacity_t* rememberthemilk = ns.Current;
        ns.Current = canrep;  // pretend that canrep is current marking

        // pass on to actual store
        bool ret = actualStore->searchAndInsert(ns, payload, thread, noinsert);

        // reset current marking
        delete[] ns.Current;
        ns.Current = rememberthemilk;

        return ret;
    };

    bool popState(NetState& ns, threadid_t thread)
    {
        RT::log<err>("popState method not yet implemented for this store");
        RT::abort(ERROR::COMMANDLINE);
        return false;
    }

    bool empty() { return actualStore->empty(); };

private:
    /// the actual store
    UnderlyingStore* actualStore;
    Petrinet* net;

    /// The generating set of symmetries to be used
    GeneratingSet* G;
};

// deduction guide for SymmetryStore
// basically allow template parameter deduction from constructor call, taking
// the payload parameter from the underlying store
template <
    template <typename, typename...> typename UnderlyingStore, typename Payload, typename... Args>
SymmetryStore(Petrinet*, UnderlyingStore<Payload, Args...>*, GeneratingSet*)
    -> SymmetryStore<Payload, UnderlyingStore<Payload, Args...>>;
