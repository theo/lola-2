/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\author Lukas Zech
\status new

\brief A wrapper for the cycle reduction that internally uses another store and
only manipulates the searchAndInsert function.
*/

#pragma once

#include <Core/Runtime.h>
#include <Net/LinearAlgebra.h>
#include <Net/Petrinet.h>
#include <Memory/Mara.h>
#include <Net/NetState.h>

template <typename Payload, typename UnderlyingStore>
class CycleStore
{
public:
    ~CycleStore()
    {
        delete[] u;
        delete actualStore;
    };
    CycleStore(Petrinet* n, UnderlyingStore* _actualStore, int _k)
        : actualStore(_actualStore), u(new bool[n->Card[TR]]), k(_k), net(n)
    {
        // get and reduce incidence matrix (transitions are lines)
        Matrix m = this->net->getIncidenceMatrix(TR);
        m.reduce();

        size_t count = 0;
        for (arrayindex_t i = 0; i < this->net->Card[TR]; i++)
        {
            u[i] = !m.isSignificant(i);
            if (u[i])
            {
                ++count;
            }
        }
        RT::log("found {} transitions to cover the cycles", count);
        RT::log("cycle heuristic: {} ({})", k, RT::markup(MARKUP::PARAMETER, "--cycleheuristic"));

        // to avoid two concurrent reporters, silence one
    };
    bool searchAndInsert(NetState& ns, Payload** payload, threadid_t thread, bool noinsert = false)
    {
        // check whether state should be saved
        noinsert = true;

        for (arrayindex_t t = 0; t < this->net->Card[TR]; ++t)
        {
            if (u[t] and ns.Enabled[t])
            {
                noinsert = false;
                break;
            }
        }

        return actualStore->searchAndInsert(ns, payload, thread, noinsert);
    };

    bool popState(NetState& ns, threadid_t thread)
    {
        RT::log<err>("popState method not yet implemented for this store");
        RT::abort(ERROR::COMMANDLINE);
        return false;
    }

    bool empty() { return actualStore->empty(); };

private:
    /// the actual store
    UnderlyingStore* actualStore;

    Petrinet* net;

    /// an array storing which transitions cover the cycles
    bool* u;

    /// a heuristic parameter
    int k;
};

// deduction guide for CycleStore
// basically allow template parameter deduction from constructor call, taking
// the payload parameter from the underlying store
template <
    template <typename, typename...> typename UnderlyingStore, typename Payload, typename... Args>
CycleStore(Petrinet*, UnderlyingStore<Payload, Args...>*, int)
    -> CycleStore<Payload, UnderlyingStore<Payload, Args...>>;
