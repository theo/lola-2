/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Christian Koch
\author Lukas Zech
\status updated

\brief general-purpose base store consisting of two components: a
NetStateEncoder, that converts the current state into a input vector, and a
VectorStore, that stores the input vectors and checks whether they are already
known. Implementations for both components can be combined arbitrarily.
Specific components may have some functional or compatibility limitations,
though.
*/

#include <Core/Dimensions.h>
#include <Stores/StoreWrapper/PluginStore.h>
#include <Stores/StoreWrapper/HashingWrapperStore.h>

template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
PluginStore<Payload, State, EncodedState, UnderlyingStore, StoreArgs...>::PluginStore(
    StateEncoder<State, EncodedState>* _stateEncoder, UnderlyingStore* _actualStore, Mara* mem,
    threadid_t _number_of_threads
)
    : stateEncoder(_stateEncoder), actualStore(_actualStore)
{
}

template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
PluginStore<Payload, State, EncodedState, UnderlyingStore, StoreArgs...>::PluginStore(
    StateEncoder<State, EncodedState>* _stateEncoder, Mara* mem, StoreArgs... args,
    threadid_t _number_of_threads
)
    : stateEncoder(_stateEncoder),
      actualStore(new UnderlyingStore(mem, args..., _number_of_threads))
{
}

template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
PluginStore<Payload, State, EncodedState, UnderlyingStore, StoreArgs...>::~PluginStore()
{
    delete stateEncoder;
    delete actualStore;
}

template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
bool PluginStore<Payload, State, EncodedState, UnderlyingStore, StoreArgs...>::searchAndInsert(
    State& ns, Payload** payload, threadid_t threadIndex, bool noinsert
)
{
    // fetch input vector
    bitarrayindex_t bitlen;
    EncodedState input = stateEncoder->encodeState(ns, bitlen, threadIndex);

    // if underlying store is a hashing store, we need to fetch a hashvalue
    // is a bit unsafe to check if a member function unique to HashingWrapperStores
    // exists, but right now I don't know a better way and using a HashingWrapperStore
    // inside a PluginStore is unintended anyway right now
    if constexpr (handles_hash<UnderlyingStore, Payload, EncodedState>)
    {
        if constexpr (has_hash_member<State>)
        {
            return actualStore->searchAndInsert(
                input, bitlen, ns.HashCurrent, payload, threadIndex
            );
        }
        else
        {
            return actualStore->searchAndInsert(input, bitlen, 0, payload, threadIndex);
        }
    }
    else
    {
        return actualStore->searchAndInsert(input, bitlen, payload, threadIndex);
    }
}

/*!
 * \brief gets and removes a state from the store
 * \param ns Where the removed state will be written to
 * \return false, if store was already empty
 */
template <
    typename Payload, typename State, typename EncodedState, typename UnderlyingStore,
    typename... StoreArgs>
bool PluginStore<Payload, State, EncodedState, UnderlyingStore, StoreArgs...>::popState(
    State& ns, threadid_t threadIndex
)
{
    // pointer to be set to popped vector
    EncodedState vec = nullptr;

    // try to get a vector from the store
    bool result = actualStore->popState(vec, threadIndex);
    if (result)
    {
        // decode vector to state and write to ns
        stateEncoder->decodeState(ns, vec);
    }
    return result;
}
