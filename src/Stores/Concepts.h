/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas Zech
\status new

\brief Header defining C++20 concepts to be used in Stores
*/
#pragma once

#include <concepts>
#include <type_traits>

// Concept to shorten the relation that some type is storable in structural (vector)
// Stores
// This currently only possible for unsigned int* (bitvectors), so this is simple
template <typename T>
concept is_storable =
    std::is_same_v<std::add_pointer_t<std::remove_cv_t<std::remove_pointer_t<T>>>, unsigned int*>;

// Just a simplification for declarations in the PrefixTreeStore
// is_storable can probably be changed to this (removing a top level pointer like above),
// but only after changing VBloomStore and VSTLStore to accept more than unsigned int*
// \TODO Update VBloomStore and VSTLStore to also accept these types
template <typename T>
concept PrefixTreeStorable = std::is_trivially_copyable_v<T>
    && (std::is_fundamental_v<T> || std::is_pointer_v<T>);

// Concept to check if some type has a member called HashCurrent
template <typename T>
concept has_hash_member = requires { T::HashCurrent; };

template <typename T, typename P, typename S = unsigned int*>
concept handles_hash = requires(T store) {
                           {
                               store.searchAndInsert(
                                   std::declval<S&>(), std::declval<bitarrayindex_t>(),
                                   std::declval<hash_t>(), std::declval<P**>(),
                                   std::declval<threadid_t>(), std::declval<bool>()
                               )
                               } -> std::convertible_to<bool>;
                       };
