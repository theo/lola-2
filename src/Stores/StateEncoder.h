/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas Zech
\status new

\brief General template definitions for a StateEncoder. Used with stores to encode some
state (NetState, Arbitrary array, etc..) to something else
*/

#pragma once

#include <Core/Dimensions.h>
#include <Net/Petrinet.h>
#include <Core/Runtime.h>

template <typename S, typename C>
class StateEncoder
{
protected:
    /// the maximum number of threads this StateEncoder has to work with.
    /// The value is used to create thread-local auxiliary data structures if needed.
    int numThreads;

public:
    /// constructor; initialize auxiliary data structures if needed.
    /// @param _numThreads maximum number of threads that may work with this
    /// StateEncoder concurrently.
    explicit StateEncoder(int _numThreads) : numThreads(_numThreads) {}

    /// destructor; frees all memory used for auxiliary data structures
    virtual ~StateEncoder() {}

    /// converts the given State into some output data.
    /// @param ns input: State that needs to be converted
    /// @param bitlen output: reference to length of encoded data (in bits). Will be
    /// filled by the method.
    /// @param threadIndex input: the index of the thread that requests this call. Values
    /// will range from 0 to (numThreads - 1). Used to allow using thread-local auxiliary
    /// data structures without locking any variables.
    /// @return the encoded data.
    virtual C encodeState(S& ns, bitarrayindex_t& bitlen, arrayindex_t threadIndex) = 0;

    /// decodes a given encoded state and sets the state appropriately
    /// @param ns State the decoded state will be written to
    /// @param data vector to be decoded
    virtual void decodeState(S& ns, C data)
    {
        RT::log<err>("This encoder cannot decode states!");
        RT::abort(ERROR::COMMANDLINE);
    };
};

// useful typedefs
typedef StateEncoder<NetState, capacity_t*> NetStateEncoder;
