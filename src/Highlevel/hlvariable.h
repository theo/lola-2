#ifndef SRC_HIGHLEVEL_HLVARIABLE_H
#define SRC_HIGHLEVEL_HLVARIABLE_H

#include <Highlevel/sort.h>

class hlplace;

class hlvariable
{
public:
    static int card;
    static int placecard;
    const char* name;
    sort* so;
    int offset;
    static hlvariable** hash_table;
    static hlvariable** placehash_table;
    static hlvariable* lookup(char*, bool isplace = false);
    static void insert(hlvariable*, bool isplace = false);
    static void init();
    hlplace* place;     // NULL if not place variable (for findlow)
    bool diff;          // true if place variable for handling difference multisets
    hlvariable* child;  // if tuple sort, link to variables for components
    hlvariable* next;   // if tuple component, link to next component
};

#endif /* SRC_HIGHLEVEL_HLVARIABLE_H */
