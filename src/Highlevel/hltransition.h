#ifndef SRC_HIGHLEVEL_HLTRANSITION_H
#define SRC_HIGHLEVEL_HLTRANSITION_H

#include <Frontend/Parser/ValueAutomaton.h>
#include <Highlevel/condition.h>
#include <Highlevel/hlvariable.h>

class hlarc;

class hltransition
{
public:
    char* name;
    static int card;
    condition* guard;
    hlarc* pre;
    hlarc* post;
    static hltransition** hash_table;
    static hltransition* lookup(const char*);
    static void insert(hltransition*);
    static void init();
    bool* getvars(hlvariable**, int);
    bool* vars;
    bool open;       // StableMarking: true = quasiliveness not determined
    bool quasilive;  // StableMarking, if not open: true = transition quasilive, false = transition
                     // not quasilive
    ValueAutomaton* firingmodes;
    int cardfiringmodes;
    int offset;
    int index;
};

class hlarc
{
public:
    bool pre;
    hlplace* place;
    hltransition* transition;
    hlmultiterm* inscription;
    hlarc* next;
    int skeletonMult;
};

#endif /* SRC_HIGHLEVEL_HLTRANSITION_H */
