#ifndef SRC_HIGHLEVEL_HLPLACE_H
#define SRC_HIGHLEVEL_HLPLACE_H

#include <Highlevel/sort.h>
#include <Highlevel/hlvariable.h>

class hlplace
{
public:
    char* name;
    static int card;
    sort* so;
    int offset;
    int index;
    static hlplace** hash_table;
    static hlplace* lookup(char*);
    static void insert(hlplace*);
    static void init();
    int* initialmarking;
    int skeletonInitialmarking;
    bool runplace;       // never changes marking due to structural equivalence of inscriptions
    bool constantplace;  // never changes token count due to numerical equaulity of arc inscriptions
    bool open;           // for StableMarking: true = stability not yet determined
    bool stable;  // for StableMarking: if not open: true = proven to be constant, false = proven to
                  // be nonconstant
    hlvariable** placevariables;
};

#endif /* SRC_HIGHLEVEL_HLPLACE_H */
