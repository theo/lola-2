#ifndef SRC_HIGHLEVEL_SORT_H
#define SRC_HIGHLEVEL_SORT_H

#include <string>

typedef enum
{
    SO_FIN,
    SO_CYC,
    SO_INT,
    SO_PRO,
    SO_DOT,
    SO_DIFF
} tSortTag;
class sort
{
public:
    char* name;
    int vectorlength;  // overall length of tuple
    int size;
    tSortTag tag;
    static sort** hash_table;
    static void insert(sort*);
    static sort* lookup(char*);
    static void init();
    static sort* dotsort;
    virtual std::string value2name(int) = 0;
};

class constant;

class finitesort : public sort
{
public:
    finitesort(tSortTag t, int n)
    {
        cyclic = (t == SO_FIN) ? false : true;
        symbol = new constant*[n];
        size = n;
        tag = t;
        vectorlength = 1;
    }
    constant** symbol;
    bool cyclic;
    int lower;  // only relevant for SO_INT
    int upper;  // only relevant for SO_INT
    virtual std::string value2name(int);
};

class productsort : public sort
{
public:
    productsort(int n)
    {
        cardsubsorts = n;
        subsort = new sort*[n];
        size = 1;
        tag = SO_PRO;
        vectorlength = 0;
    }
    int cardsubsorts;
    sort** subsort;
    void addSubSort(int i, sort* s)
    {
        subsort[i] = s;
        size *= s->size;
        vectorlength += s->vectorlength;
    }
    virtual std::string value2name(int);
};

class dotsort : public sort
{
public:
    dotsort()
    {
        size = 1;
        tag = SO_DOT;
        vectorlength = 1;
    }
    virtual std::string value2name(int);
};

class constant
{
public:
    char* name;
    sort* so;
    int index;
    static constant** hash_table;
    static void insert(constant*);
    static constant* lookup(char*);
    static void init();
};

class setconstant
{
public:
    char* name;
    sort* so;
    int* index;
    int card;
    static setconstant** hash_table;
    static void insert(setconstant*);
    static setconstant* lookup(char*);
    static void init();
};

#endif /* SRC_HIGHLEVEL_SORT_H */
