#ifndef SRC_HIGHLEVEL_CONDITION_H
#define SRC_HIGHLEVEL_CONDITION_H

#include <sstream>

#include <Frontend/Parser/ValueAutomaton.h>
#include <Highlevel/hlvariable.h>
#include <Highlevel/hlterm.h>
#include <Highlevel/hlmultiterm.h>

class condition
{
public:
    virtual ValueAutomaton* evaluate(hlvariable**, int) = 0;  // return array of all satisfying
                                                              // assignments
    virtual void print(std::ostringstream& out) = 0;
    virtual bool* getvars(hlvariable**, int) = 0;
};

class andcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    int cardsub;
    condition** sub;
    void print(std::ostringstream& out)
    {
        out << "(";
        for (int i = 0; i < cardsub; i++)
        {
            sub[i]->print(out);
            if (i < cardsub - 1)
                out << " && ";
        }
        out << ")";
    }
};

class orcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    int cardsub;
    condition** sub;
    void print(std::ostringstream& out)
    {
        out << "(";
        for (int i = 0; i < cardsub; i++)
        {
            sub[i]->print(out);
            if (i < cardsub - 1)
                out << " || ";
        }
        out << ")";
    }
};

class eqcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " == ";
        sub2->print(out);
        out << ")";
    }
};

class neqcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " != ";
        sub2->print(out);
        out << ")";
    }
};

class ltcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " < ";
        sub2->print(out);
        out << ")";
    }
};

class lecondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " <= ";
        sub2->print(out);
        out << ")";
    }
};

class gtcondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " > ";
        sub2->print(out);
        out << ")";
    }
};

class gecondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    hlterm* sub1;
    hlterm* sub2;
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " >= ";
        sub2->print(out);
        out << ")";
    }
};

class nocondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int);
    void print(std::ostringstream& out) { out << "NONE"; }
};

class runplacecondition : public condition
{
public:
    ValueAutomaton* evaluate(hlvariable**, int);
    bool* getvars(hlvariable**, int card)
    {
        bool* result = new bool[card];
        memset(result, 0, sizeof(bool) * card);
        return result;
    }
    hlplace* p;
    hlmultiterm* inscription;
    void print(std::ostringstream& out) {}
};

#endif /* SRC_HIGHLEVEL_CONDITION_H */
