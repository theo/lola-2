#ifndef SRC_HIGHLEVEL_HLMULTITERM_H
#define SRC_HIGHLEVEL_HLMULTITERM_H

#include <Frontend/Parser/ValueAutomaton.h>
#include <Highlevel/sort.h>
#include <Highlevel/hlvariable.h>
#include <Highlevel/hlplace.h>

typedef enum
{
    CO_MT,
    AL_MT,
    TE_MT,
    SU_MT,
    DI_MT,
    TU_MT
} tMTTag;

class hlmultiterm
{
public:
    tMTTag tag;
    int nroftokens;
    sort* so;
    virtual int* evaluate(int*, hlvariable**, int) = 0;  // returns multiset of sort so
    bool matchable;  // simple enough for inferring firing modes from initial marking
    virtual ValueAutomaton* match(hlplace*) = 0;  // must be called only if matchable=true
    virtual void print(std::ostringstream& out) = 0;
    virtual bool* getvars(hlvariable**, int) = 0;
    virtual bool equivalent(hlmultiterm*) = 0;
    virtual ValueAutomaton* initial2automaton(hlplace*) = 0;
    virtual void adddiffvars() = 0;
    virtual ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size) = 0;
};

class coefficienthlmultiterm : public hlmultiterm
{
public:
    int coefficient;
    hlmultiterm* submt;
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    coefficienthlmultiterm(sort*, int, hlmultiterm*);
    void print(std::ostringstream& out)
    {
        out << coefficient;
        out << " * ";
        submt->print(out);
    }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*);
    void adddiffvars() { submt->adddiffvars(); }
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
    ValueAutomaton* createcoefficientfindlowautomaton(
        hlvariable** tobeused, int size, int cardin, int cardout
    );
};

class allhlmultiterm : public hlmultiterm
{
public:
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    allhlmultiterm(sort*);
    void print(std::ostringstream& out) { out << "ALL(" << so->name << ")"; }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*) { return NULL; }
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
    void adddiffvars() {}
};

class termhlmultiterm : public hlmultiterm
{
public:
    hlterm* subterm;
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    termhlmultiterm(sort*, hlterm*);
    void print(std::ostringstream& out)
    {
        out << "(";
        subterm->print(out);
        out << ")";
    }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*);
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
    void adddiffvars() {}
};

class sumhlmultiterm : public hlmultiterm
{
public:
    int card;
    hlmultiterm** sub;
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    sumhlmultiterm(sort*, int, hlmultiterm**);
    void print(std::ostringstream& out)
    {
        out << "(";
        for (int i = 0; i < card; i++)
        {
            sub[i]->print(out);
            if (i < card - 1)
                out << " + ";
        }
        out << ")";
    }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*);
    void adddiffvars()
    {
        for (int i = 0; i < card; i++)
        {
            sub[i]->adddiffvars();
        }
    }
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
    ValueAutomaton* createsumfindlowautomaton(
        hlvariable** tobeused, int size, int firstsummand, int cardsummands, int cardin, int cardout
    );
};

class diffhlmultiterm : public hlmultiterm
{
public:
    hlmultiterm* sub1;
    hlmultiterm* sub2;
    int carddiffvars;
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    diffhlmultiterm(sort*, hlmultiterm*, hlmultiterm*);
    void print(std::ostringstream& out)
    {
        out << "(";
        sub1->print(out);
        out << " - ";
        sub2->print(out);
        out << ")";
    }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*) { return NULL; }
    hlvariable** diffvars;
    void adddiffvars()
    {
        carddiffvars = 0;
        diffvars = new hlvariable*[sub2->nroftokens];
        for (int i = 0; i < sub2->nroftokens; i++)
        {
            diffvars[i] = new hlvariable();
            diffvars[i]->diff = true;
            diffvars[i]->name = "diff";
            diffvars[i]->so = sub2->so;
            hlvariable::insert(diffvars[i], true);
        }
        sub1->adddiffvars();
        sub2->adddiffvars();
    }
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
};

class tuplehlmultiterm : public hlmultiterm
{
public:
    int card;
    hlterm** sub;
    int* evaluate(int*, hlvariable**, int);
    ValueAutomaton* match(hlplace*) { return NULL; }
    tuplehlmultiterm(sort*, int, hlterm**);
    void print(std::ostringstream& out)
    {
        out << "[";
        for (int i = 0; i < card; i++)
        {
            sub[i]->print(out);
            if (i < card - 1)
                out << ", ";
        }
        out << "]";
    }
    bool* getvars(hlvariable**, int);
    bool equivalent(hlmultiterm*);
    ValueAutomaton* initial2automaton(hlplace*);
    void adddiffvars() {}
    ValueAutomaton* createfindlowautomaton(hlvariable** tobeused, int size);
};

#endif /* SRC_HIGHLEVEL_HLMULTITERM_H */
