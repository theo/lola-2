#ifndef SRC_HIGHLEVEL_HLTERM_H
#define SRC_HIGHLEVEL_HLTERM_H

#include <sstream>
#include <Highlevel/hlvariable.h>

typedef enum
{
    TE_VAR,
    TE_CON,
    TE_SUC,
    TE_PRE,
    TE_PL
} tHlTermTag;

class hlterm
{
public:
    tHlTermTag tag;
    sort* so;
    virtual int evaluate(int*, hlvariable**, int) = 0;
    virtual void print(std::ostringstream& out) = 0;
    virtual bool* getvars(hlvariable**, int) = 0;
    virtual bool equivalent(hlterm*) = 0;
    virtual void processrunplace(int, int, int*, int**, int*, int*) = 0;
};

class variableterm : public hlterm
{
public:
    hlvariable* var;
    variableterm(hlvariable* v);
    int evaluate(int*, hlvariable**, int);
    bool* getvars(hlvariable**, int);
    void print(std::ostringstream& out) { out << var->name; }
    bool equivalent(hlterm*);
    void processrunplace(int, int, int*, int**, int*, int*);
    int getoffset(hlvariable**, int);  // position in assignment vector
};

class succterm : public hlterm
{
public:
    hlterm* subterm;
    int evaluate(int*, hlvariable**, int);
    succterm(hlterm*);
    bool* getvars(hlvariable**, int);
    void print(std::ostringstream& out)
    {
        out << "succ(";
        subterm->print(out);
        out << ")";
    }
    bool equivalent(hlterm*);
    void processrunplace(int, int, int*, int**, int*, int*);
};

class predterm : public hlterm
{
public:
    hlterm* subterm;
    int evaluate(int*, hlvariable**, int);
    predterm(hlterm*);
    bool* getvars(hlvariable**, int);
    void print(std::ostringstream& out)
    {
        out << "pred(";
        subterm->print(out);
        out << ")";
    }
    bool equivalent(hlterm*);
    void processrunplace(int, int, int*, int**, int*, int*);
};

class constantterm : public hlterm
{
public:
    int value;
    int evaluate(int*, hlvariable**, int);
    constantterm(int);
    bool* getvars(hlvariable**, int);
    void print(std::ostringstream& out) { out << value; }
    bool equivalent(hlterm*);
    void processrunplace(int, int, int*, int**, int*, int*);
};

#endif /* SRC_HIGHLEVEL_HLTERM_H */
