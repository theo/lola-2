/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief implementation of class ModularStructure for modular petri nets
*/

#include "ModularStructure.h"

#include <cstddef>

#include "Core/Dimensions.h"
#include "Memory/Util.h"

ModularStructure::ModularStructure()
{
    cardModules = 0;
    Name = new const char**[3];
}

void lola::ModularStructure::debug_print() const
{
    RT::print<VERB::FULL>("Modules:\n{}", modules);
    RT::print<VERB::FULL>("Instances:\n{}", instances);
    RT::print<VERB::FULL>("Fusionsets:\n{}", fusionsets);
}
