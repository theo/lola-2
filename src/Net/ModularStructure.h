/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\author Lukas Zech
\status wip 21.03.2023

\brief definition of class ModularStructure for modular petri nets
*/

#ifndef SRC_NET_MODULARSTRUCTURE_H
#define SRC_NET_MODULARSTRUCTURE_H

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <cstdint>
#include <memory>
#include <span>
#include <string>

#include "Core/Dimensions.h"
#include "InputOutput/Logger.h"
#include "Memory/Span.h"
#include "Memory/static_vector.h"
#include "Net/NetState.h"
#include "Net/Petrinet.h"

enum class modstruct_name_t
{
    MOD = 0,   ///< Index for Modules
    INST = 1,  ///< Index for Instances
    FS = 2     ///< Index for Fusionsets
};

class ModularStructure
{
public:
    /// Name[MOD]  -> names of modules
    /// Name[INST] -> names of instances
    /// Name[FS]   -> names of fusion sets
    const char*** Name;

    /// MODULES:

    arrayindex_t cardModules;

    /// all modules are saved as Petrinets without a marking
    Petrinet** module;  /// module[4] = pointer to the fifth module

    /// Index of the first interface transition for each module:
    /// the transitions are sorted such that internal transitions follow external ones
    arrayindex_t* firstInterfaceTransition;

    /// INSTANCES:

    arrayindex_t cardInstances;
    arrayindex_t* instanceModule;  /// instanceModule[3] = index of module for the fourth instance
    capacity_t** instanceInitial;  /// intial marking for each instance

    /// FUSIONSETS:

    arrayindex_t cardFusionSets;      /// Number of total fusion sets in the modular structure
    arrayindex_t* cardTransitionsFS;  /// Number of transitions per fusionset

    /// fusionSetTransition[2][1] = The module-local index of the second participating transition of
    /// the third FS fusionSetModules[2][1] = index of the module containing the second particing
    /// transition of the third FS fusionSetInstances[2][1] = index of the instance to which the
    /// second particing transition of the third FS refers
    arrayindex_t** fusionSetTransition;
    arrayindex_t** fusionSetModule;
    arrayindex_t** fusionSetInstance;

    ModularStructure();
};

namespace lola
{
namespace detail
{

struct module_accessor_t
{
};
struct instance_accessor_t
{
};
struct fusionset_accessor_t
{
};
struct name_accessor_t
{
};
struct marking_accessor_t
{
};
struct participant_accessor_t
{
};

struct FusionParticipant
{
    auto operator[](node_t) -> arrayindex_t& { return transition; }
    auto operator[](node_t) const -> const arrayindex_t& { return transition; }
    auto operator[](module_accessor_t) -> arrayindex_t& { return module; }
    auto operator[](module_accessor_t) const -> const arrayindex_t& { return module; }
    auto operator[](instance_accessor_t) -> arrayindex_t& { return instance; }
    auto operator[](instance_accessor_t) const -> const arrayindex_t& { return instance; }

    arrayindex_t transition;
    arrayindex_t module;
    arrayindex_t instance;
};

}  // namespace detail

inline constexpr auto MOD = detail::module_accessor_t{};
inline constexpr auto INST = detail::instance_accessor_t{};
inline constexpr auto FS = detail::fusionset_accessor_t{};
inline constexpr auto NAME = detail::name_accessor_t{};
inline constexpr auto MARK = detail::marking_accessor_t{};
inline constexpr auto PARTS = detail::participant_accessor_t{};

using Place = arrayindex_t;
using Transition = arrayindex_t;
using Marking = memory::SpanMD<const capacity_t, memory::detail::dextents<arrayindex_t, 1>>;
using MarkingList = memory::SpanMD<const capacity_t, memory::detail::dextents<arrayindex_t, 2>>;

class Module : public ::Petrinet
{
public:
    Module(std::string_view name, arrayindex_t num_places, arrayindex_t num_initial_markings)
        : name{name}, initial_markings(num_initial_markings * num_places),
          card_markings{num_initial_markings}
    {
        Card[PL] = num_places;
    }

    auto get_name() const -> std::string_view { return name; }
    auto operator[](detail::name_accessor_t) const -> std::string_view { return name; }

    auto get_markings() const -> MarkingList
    {
        return MarkingList{
            initial_markings.data(), card_markings, static_cast<std::size_t>(Card[PL])
        };
    }
    auto operator[](detail::marking_accessor_t) const -> MarkingList { return get_markings(); }

    auto add_marking() -> std::span<capacity_t>
    {
        return {
            initial_markings.insert(initial_markings.cend(), Card[PL], {}),
            static_cast<std::size_t>(Card[PL])
        };
    }

private:
    std::string_view name;
    memory::static_vector<capacity_t> initial_markings;
    arrayindex_t card_markings;

public:
    arrayindex_t first_interface_transition;
};

class Instance
{
private:
    using __Marking = const capacity_t*;

public:
    Instance(
        std::string_view name, const Module& module, Marking marking, arrayindex_t place_offset
    )
        : name{name}, module{std::addressof(module)}, initial_marking{marking.data_handle()},
          offset{place_offset}
    {
    }

    Instance(
        std::string_view name, const Module& module, std::span<capacity_t> marking,
        arrayindex_t place_offset
    )
        : name{name}, module{std::addressof(module)}, initial_marking{marking.data()},
          offset{place_offset}
    {
    }
    auto get_name() const -> std::string_view { return name; }
    auto operator[](detail::name_accessor_t) const -> std::string_view { return name; }
    auto get_module() const -> const Module& { return *this->module; }
    auto operator[](detail::module_accessor_t) const -> const Module& { return *this->module; }
    auto get_marking() const -> Marking { return Marking{initial_marking, module->Card[PL]}; }
    auto operator[](detail::marking_accessor_t) const -> Marking { return get_marking(); }
    auto global_place(arrayindex_t place) const -> arrayindex_t { return offset + place; }

private:
    std::string_view name;
    const Module* module;
    __Marking initial_marking;
    arrayindex_t offset;
};

class FusionSet
{
public:
    FusionSet(std::string_view name, std::span<detail::FusionParticipant> participants)
        : name{name}, participants{participants}
    {
    }

    auto get_name() const -> std::string_view { return name; }
    auto operator[](detail::name_accessor_t) const -> std::string_view { return name; }
    auto get_participants() const -> std::span<detail::FusionParticipant> { return participants; }
    auto operator[](detail::participant_accessor_t) const -> std::span<detail::FusionParticipant>
    {
        return participants;
    }

private:
    std::string_view name;
    std::span<detail::FusionParticipant> participants;
};

class ModularStructure
{
public:
    ModularStructure(
        arrayindex_t num_modules, arrayindex_t num_instances, arrayindex_t num_fusionsets,
        arrayindex_t num_fusionparticipants
    )
        : modules(num_modules), instances(num_instances),
          fusionsets_internal(num_fusionparticipants), fusionsets(num_fusionsets)
    {
    }
    auto get_modules() const -> std::span<const Module> { return {modules}; }
    auto operator[](detail::module_accessor_t) -> std::span<Module> { return {modules}; }
    auto operator[](detail::module_accessor_t) const -> std::span<const Module>
    {
        return {modules};
    }
    auto get_instances() const -> std::span<const Instance> { return {instances}; }
    auto operator[](detail::instance_accessor_t) const -> std::span<const Instance>
    {
        return {instances};
    }
    auto get_fusionsets() const -> std::span<const FusionSet> { return {fusionsets}; }
    auto operator[](detail::fusionset_accessor_t) const -> std::span<const FusionSet>
    {
        return {fusionsets};
    }
    auto add_module(
        std::string_view name, arrayindex_t num_places, arrayindex_t num_initial_markings
    ) -> Module&
    {
        return modules.emplace_back(name, num_places, num_initial_markings);
    }
    auto add_instance(
        std::string_view name, const Module& module, Marking marking, arrayindex_t place_offset
    ) -> Instance&
    {
        update_globals(place_offset, module);
        return instances.emplace_back(name, module, marking, place_offset);
    }
    auto add_instance(
        std::string_view name, const Module& module, std::span<capacity_t> marking,
        arrayindex_t place_offset
    ) -> Instance&
    {
        update_globals(place_offset, module);
        return instances.emplace_back(name, module, marking, place_offset);
    }
    auto add_fusionset(std::string_view name, std::size_t size) -> FusionSet
    {
        auto begin_of_new_set = fusionsets_internal.insert(fusionsets_internal.cend(), size, {});
        return fusionsets.emplace_back(
            name, std::span{begin_of_new_set, fusionsets_internal.end()}
        );
    }
    auto get_global_place_count() const -> arrayindex_t { return global_place_count; }
    auto operator[](node_t) const -> arrayindex_t { return global_place_count; }

    void debug_print() const;

private:
    void update_globals(arrayindex_t place_offset, const Module& module)
    {
        if (place_offset >= global_place_count)
        {
            global_place_count = place_offset + module.Card[PL];
        }
    }

    memory::static_vector<Module> modules;
    memory::static_vector<Instance> instances;
    memory::static_vector<detail::FusionParticipant> fusionsets_internal;
    memory::static_vector<FusionSet> fusionsets;
    arrayindex_t global_place_count = 0;
};

}  // namespace lola

template <>
struct fmt::formatter<lola::Module> : fmt::formatter<std::string_view>
{
    auto format(const lola::Module& m, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "{}: {} Places, {} Transitions, Markings: {}", m.get_name(),
            m.Card[PL], m.Card[TR], m.get_markings()
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

template <>
struct fmt::formatter<lola::Instance> : fmt::formatter<std::string_view>
{
    auto format(const lola::Instance& i, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "{}: Module {}, Marking {}", i.get_name(), i.get_module(),
            i.get_marking()
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

template <>
struct fmt::formatter<lola::detail::FusionParticipant> : fmt::formatter<std::string_view>
{
    auto format(const lola::detail::FusionParticipant& fp, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "[T {}, M {}, I {}]", fp.transition, fp.module, fp.instance
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

template <>
struct fmt::formatter<lola::FusionSet> : fmt::formatter<std::string_view>
{
    auto format(const lola::FusionSet& fs, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(std::back_inserter(tmp), "{}: {}", fs.get_name(), fs.get_participants());
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

#endif /* SRC_NET_MODULARSTRUCTURE_H */
