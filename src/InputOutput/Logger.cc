/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas Zech
\status new
\ingroup g_io

\brief definition of data for logging
*/

#include <string_view>

#include <fmt/format.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/udp_sink.h>

#include <Core/Dimensions.h>
#include <InputOutput/Logger.h>

namespace RT
{

namespace detail
{
Internal::Internal() : indent_amount(0), have_setup_loggers(false), have_colored_output(false) {}

Internal& Internal::get_instance()
{
    static Internal s_instance;
    return s_instance;
}

result_t Internal::setup_loggers(gengetopt_args_info& args)
{
    if (have_setup_loggers)
    {
        return result_t::repeated;
    }
    // setup color arg for output
    // if the arg is given, we enforce colored output and the flag is set anyway
    have_colored_output = args.color_given;
    if (!have_colored_output)
    {
        // if the flag is not given it is set to false, but there are cases where we
        // can use color implicitely
        // socket given? We write to udp socket instead of
        // file, so can also output color
        // socket not given? We don't write to files if
        // both fields are default values, so can output color in that case aswell
        if (args.socket_given
            || (!strcmp(args.logging_arg, "localhost") && !strcmp(args.output_arg, "localhost")))
        {
            have_colored_output = true;
        }
    }

    // setup sinks for loggers
    std::vector<logging::sink_ptr> output_sinks;
    std::vector<logging::sink_ptr> logging_sinks;
    // we have at most 2 output locations, small improvement only
    output_sinks.reserve(2);
    logging_sinks.reserve(2);

    // setup stream sinks \todo add parameter to disable stream output/logging
    // altogether
    auto stdout_sink = std::make_shared<logging::sinks::stdout_color_sink_mt>();
    auto stderr_sink = std::make_shared<logging::sinks::stderr_color_sink_mt>();
    if (args.logging_given && !strcmp(args.logging_arg, "localhost"))
    {
        // -l without text implies redirecting logs to output stream/file/host
        args.logging_arg = args.output_arg;
        args.logport_arg = args.outport_arg;
        // redirect log to out? Then both have stdout sink
        // should also be the case when both redirect to each other, so no need
        // to also look at redirect_out_log
        output_sinks.push_back(stdout_sink);
        logging_sinks.push_back(stdout_sink);
    }
    else if (args.output_given && !strcmp(args.output_arg, "localhost"))
    {
        // -o without text implies redirecting output to logging stream/file/host
        args.output_arg = args.logging_arg;
        args.outport_arg = args.logport_arg;
        // don't redirect log but redirect out? both to stderr
        output_sinks.push_back(stderr_sink);
        logging_sinks.push_back(stderr_sink);
    }
    else
    {
        // no redirections
        output_sinks.push_back(stdout_sink);
        logging_sinks.push_back(stderr_sink);
    }

    // setup socket/file sinks
    if (args.socket_given)
    {
        // \todo Check for hostname validity?
        // create socket sink for output
        auto out_socket_sink = std::make_shared<logging::sinks::udp_sink_mt>(
            logging::sinks::udp_sink_config(args.output_arg, args.outport_arg)
        );
        output_sinks.push_back(out_socket_sink);
        // don't need to check for redirections, if one redirects to the other, then
        // both arguments have the same values, so we just need to check if they
        // differ and create another sink for logging aswell.
        if (!strcmp(args.output_arg, args.logging_arg) && args.outport_arg == args.logport_arg)
        {
            // both hostname and port are equal (so either redirection, same value or
            // both default) so just add sink to logging
            logging_sinks.push_back(out_socket_sink);
        }
        else
        {
            // something differs between both, so new sink for logging
            auto logging_socket_sink = std::make_shared<logging::sinks::udp_sink_mt>(
                logging::sinks::udp_sink_config(args.logging_arg, args.logport_arg)
            );
            logging_sinks.push_back(logging_socket_sink);
        }
    }
    else
    {
        // may have to write to files aswell
        bool need_extra = true;
        if (strcmp(args.output_arg, "localhost"))
        {
            // some filename for output given
            auto out_file_sink = std::make_shared<logging::sinks::rotating_file_sink_mt>(
                args.output_arg, MAX_OUTPUT_FILE_SIZE, MAX_OUTPUT_FILE_COUNT
            );
            output_sinks.push_back(out_file_sink);
            if (!strcmp(args.output_arg, args.logging_arg))
            {
                // both filenames the same, then add sink to logging aswell
                logging_sinks.push_back(out_file_sink);
                need_extra = false;
            }
        }
        if (need_extra && strcmp(args.logging_arg, "localhost"))
        {
            // some different filename given for logging
            auto logging_file_sink = std::make_shared<logging::sinks::rotating_file_sink_mt>(
                args.logging_arg, MAX_OUTPUT_FILE_SIZE, MAX_OUTPUT_FILE_COUNT
            );
            logging_sinks.push_back(logging_file_sink);
        }
    }

    out = std::make_unique<logging::logger>(
        "output_logger", begin(output_sinks), end(output_sinks)
    );

    out->set_pattern(fmt::format("[{0}] %v", markup(MARKUP::TOOL, "lola")));

    status = std::make_unique<logging::logger>(
        "status_logger", begin(logging_sinks), end(logging_sinks)
    );

    if (have_colored_output)
    {
        status->set_pattern(fmt::format("[{}][%^%L%$] %v", markup(MARKUP::TOOL, "lola")));
    }
    else
    {
        status->set_pattern(fmt::format("[{}][%L] %v", markup(MARKUP::TOOL, "lola")));
    }

    switch (args.loglevel_arg)
    {
    case loglevel_arg_trace:
        status->set_level(logging::level::trace);
        break;
    case loglevel_arg_debug:
        status->set_level(logging::level::debug);
        break;
    case loglevel_arg_info:
    default:
        status->set_level(logging::level::info);
        break;
    case loglevel_arg_warning:
        status->set_level(logging::level::warn);
        break;
    case loglevel_arg_error:
        status->set_level(logging::level::err);
        break;
    case loglevel_arg_critical:
        status->set_level(logging::level::critical);
        break;
    case loglevel_arg_off:
        status->set_level(logging::level::off);
        break;
    }

    switch (args.verb_arg)
    {
    case verb_arg_full:
    default:
        out->set_level(logging::level::trace);
        break;
    case verb_arg_aux:
        out->set_level(logging::level::debug);
        break;
    case verb_arg_output:
        out->set_level(logging::level::info);
        break;
    }

    have_setup_loggers = true;

    return result_t::success;
}

int& Internal::get_indent_amount() { return indent_amount; }

void Internal::indent_to(int i)
{
    // update indent
    indent_amount = i;
    if (i < 0)
    {
        indent_amount = 0;
    }
    // set indent in loggers
    // what is the new pattern?
    // "[{0}] {1:{2}}%v"
    //    ^    ^  ^
    //    |    |  |
    //    |    |  width of field in third argument -> indent
    //    |    character to fill into fixed-width field in second argument -> empty
    //    fill first argument -> marked-up "lola"
    detail::Internal::get_instance().out->set_pattern(
        fmt::format("[{0}]{1:{2}} %v", markup(MARKUP::TOOL, "lola"), "", indent_amount)
    );
    if (have_colored_output)
    {
        detail::Internal::get_instance().status->set_pattern(
            fmt::format("[{0}][%^%L%$]{1:{2}} %v", markup(MARKUP::TOOL, "lola"), "", indent_amount)
        );
    }
    else
    {
        detail::Internal::get_instance().status->set_pattern(
            fmt::format("[{0}][%L]{1:{2}} %v", markup(MARKUP::TOOL, "lola"), "", indent_amount)
        );
    }
}

logging::level::level_enum Internal::get_log_level() { return status->level(); }

bool Internal::do_color() { return have_colored_output; }

}  // namespace detail

void shutdown(int exit_code)
{
    detail::Internal::get_instance().out->flush();
    detail::Internal::get_instance().status->flush();
    spdlog::shutdown();
    _exit(exit_code);
}
}  // namespace RT
