/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief implementation of classes IO, Input, and Output
\author Niels
\status new
\ingroup g_io
*/

#include <InputOutput/InputOutput.h>

#include <Core/Runtime.h>
#include <InputOutput/Logger.h>

#include <fmt/ostream.h>

#include <cstdio>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>

IO::~IO()
{
    if (moved_from)
    {
        return;
    }

    RT::log(
        "closed {} file {}", RT::markup(MARKUP::OUTPUT, kind), RT::markup(MARKUP::FILE, filename)
    );

    // try to close file
    if (fp != stdout && fp != stdin)
    {
        const int ret = fclose(fp);

        // fclose returns 0 on success
        if (ret != 0) [[unlikely]]
        // LCOV_EXCL_START
        {
            RT::log<critical>(
                "could not close {} file {}", RT::markup(MARKUP::OUTPUT, kind),
                RT::markup(MARKUP::FILE, filename)
            );
            RT::abort(ERROR::FILE);
        }
        // LCOV_EXCL_STOP
    }
}

IO::IO(IO&& other) : filename(std::move(other.filename)), kind(std::move(other.kind)), fp(other.fp)
{
    other.fp = nullptr;
    other.moved_from = true;
}

IO& IO::operator=(IO&& other)
{
    filename = std::move(other.filename);
    kind = std::move(other.kind);
    fp = other.fp;
    moved_from = false;
    other.fp = nullptr;
    other.moved_from = true;
    return *this;
}

/*!
Operator to use IO objects with the same syntax as FILE pointers.

\return The FILE pointer wrapped by the IO object.
*/
Input::operator FILE*() const { return fp; }

/*!
Getter for the filename.

\return The filename as null-terminated string.
*/
auto Input::get_filename() const -> std::string_view { return filename; }

auto Input::get_kind() const -> std::string_view { return kind; }

auto Input::get_filename_string() const -> const std::string* { return &filename; }

/*!
\param fp  the FILE* to wrap
\param kind  the kind of the file (used in verbose output)
\param filename  the name of the file (used only in verbose output here)

\pre the reporter was set using IO::setReporter
\pre File fp was opened using fopen by the constructor of the child classes
Input or Output
\post If an error occurred opening the file, error with ERROR_FILE
*/
IO::IO(std::string_view _kind, std::string_view _filename, FILE* _fp)
    : filename(_filename), kind(_kind), fp(_fp)
{
    RT::data["files"][kind] = filename;

    // fopen returns NULL on error
    // LCOV_EXCL_START
    if (fp == nullptr) [[unlikely]]
    {
        RT::log<critical>(
            "could not open {} file {}", RT::markup(MARKUP::OUTPUT, kind),
            RT::markup(MARKUP::FILE, filename)
        );
        RT::abort(ERROR::FILE);
    }
    // LCOV_EXCL_STOP
}

/*!
\param _kind  the kind of the file (used in verbose output)
\param _filename  the name of the output file to open; if no filename or "-" is
given, the file written to the standard output

\pre the reporter was set using IO::setReporter
\post the file given with the file name is opened to write
\note errors are handled by the IO constructor
*/
Output::Output(std::string_view _kind, std::string_view _filename)
    : IO(_kind, _filename == "-" ? "stdout" : _filename,
         _filename == "-" ? stdout : fopen(_filename.data(), "w")),
      have_stream(filename == "-")
{
    RT::log(
        "writing {} to {}", RT::markup(MARKUP::OUTPUT, kind), RT::markup(MARKUP::FILE, filename)
    );
}

/*!
\param _kind  the kind of the file (used in verbose output)
\param _filename  the name of the input file to open; if no filename or "-" is
given, the file read from the standard input

\pre the reporter was set using IO::setReporter
\post the file given with the file name is opened to read
\note errors are handled by the IO constructor
*/
Input::Input(std::string_view _kind, std::string_view _filename)
    : IO(_kind, _filename == "-" ? "stdin" : _filename,
         _filename == "-" ? stdin : fopen(_filename.data(), "rb"))
{
    RT::log(
        "reading {} from {}", RT::markup(MARKUP::OUTPUT, kind), RT::markup(MARKUP::FILE, filename)
    );
}

/*!
This function highlights a certain part of the output file, given via
positions. It rewinds the output file and skips all lines until line
first_line-1. This line is used as context and is printed without highlighting.
Then, the first "error" line is printed. While doing so, the column information
are used to highlight the wished part. Finally, this part is further
highlighted using carets.

This is how highlighting would look for 40:24 - 40:28:
\verbatim

 39      CONSUME critical.1: 1;
 40      PRODUCE idle.1: 1, sems: 1;
                            ^~~~
\endverbatim

\param first_line    the first line where the output should be printed
\param first_column  the first column where the output should be highlighted
\param last_line     the last line where the output should be printed
\param last_column   the last column where the output should be highlighted

\pre We assume that the input file is not stdin. If it is, thus function
silently returns.
\post The output file position is at last_line + 1; that is, right after the
line given at last_line.
\note This function is const in the sense that it does not change the pointer
to the input file. It does, however, change the current position it it.

\todo Currently, the parameter last_line is ignored. As soon as we have an idea
how multi-line highlighting works, we shall implement this as well.
\todo Coloring is really basic. The line numbers should be dimmed.
\todo In the output, tabs are replaced by a single space. To resemble the input
file, we should use 8 spaces instead. This, however, also has an impact on the
position of the highlighted area.
*/
void Input::printExcerpt(
    std::string_view filename, int first_line, int first_column, int, int last_column
)
{
    // stdin cannot be rewound
    if (filename == "-")
    {
        return;
    }

    std::ifstream file{std::filesystem::path{filename}};

    if (!file.good())
    {
        RT::log<err>("Could not rewind file {} to print excerpt", filename);
        return;
    }

    for (int i = 0; i < first_line - 2; ++i)
    {
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        assert(!file.eof());
    }

    std::ostringstream msg;

    fmt::print(msg, "Excerpt from file:\n");
    std::string line;

    // print last line before the error (context)
    if (first_line > 1)
    {
        std::getline(file, line);
        assert(!file.eof());
        fmt::print(msg, "{:4d}  {}\n", first_line - 1, line);
    }

    // get error line
    std::getline(file, line);
    // error is eof, no excerpt printed
    if (file.eof() && first_column > line.size())
    {
        return;
    }

    // avoid copies
    std::string_view error_line{line};

    // print error line
    fmt::print(
        msg, "{:4d}  {}{}{}\n", first_line, error_line.substr(0, first_column - 1),
        RT::markup(
            MARKUP::BAD, "{}", error_line.substr(first_column - 1, last_column - first_column)
        ),
        error_line.substr(last_column - 1)
    );
    // print arrow indicating the error
    auto arrow = RT::markup(MARKUP::GOOD, "^{0:~<{1}}", "", last_column - first_column - 1);
    fmt::print(msg, "      {0:{1}}{2}", "", first_column - 1, arrow);

    RT::log<err>(msg.str());
}
