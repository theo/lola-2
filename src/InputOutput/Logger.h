/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas Zech
\status new
\ingroup g_io

\brief declaration of data for logging
*/

#pragma once

#include <ostream>
#include <type_traits>

#include <fmt/color.h>
#include <fmt/ostream.h>
#include <spdlog/spdlog.h>

#include <cmdline.h>
#include <lolaconf.h>

namespace logging = spdlog;

// move loglevels to enum class for easy access and type safety
enum class level : int
{
    trace = logging::level::trace,
    debug = logging::level::debug,
    info = logging::level::info,
    warn = logging::level::warn,
    err = logging::level::err,
    critical = logging::level::critical
};

using enum level;

enum class MARKUP
{
    TOOL,
    SUBTOOL,
    FILE,
    OUTPUT,
    GOOD,
    BAD,
    WARNING,
    IMPORTANT,
    PARAMETER,
    UNIMPORTANT,
};

enum class ERROR : int
{
    SYNTAX = 1,
    COMMANDLINE = 2,
    FILE = 3,
    THREADING = 4,
    NETWORK = 5,
    MEMORY = 6,
};

enum class VERB : int
{
    OUTPUT = logging::level::info,
    AUX = logging::level::debug,
    FULL = logging::level::trace
};

/**
 * @brief Formatting function for enum (classes). For any enum and enum class in the
 * global namespace, format them as their underlying type
 *
 * @tparam T
 */
template <typename T>
requires(std::is_enum_v<T>)
auto format_as(T f)
{
    return fmt::underlying(f);
}

namespace RT
{

constexpr std::string_view error_msg[] = {
    "",
    "syntax error",
    "command line error",
    "file input/output error",
    "thread error",
    "network error",
    "memory error",
};

/**
 * @brief Shutdown execution, flushing the loggers and calling _exit
 *
 */
void shutdown [[noreturn]] (int);

namespace detail
{

enum class result_t
{
    success,
    failure,
    repeated
};

// internal data structure to hide functionality and allow functions to only be called
// through namespace functions
class Internal
{
    friend void ::RT::shutdown(int);

public:
    Internal(const Internal&) = delete;
    Internal& operator=(const Internal&) = delete;

    static Internal& get_instance();

    result_t setup_loggers(gengetopt_args_info& args);

    int& get_indent_amount();

    void indent_to(int i);

    logging::level::level_enum get_log_level();

    bool do_color();

    template <VERB verb, typename... Args>
    void print(fmt::format_string<Args...> format_str, Args&&... fargs)
    {
        out->log(
            static_cast<logging::level::level_enum>(verb), format_str, std::forward<Args>(fargs)...
        );
    }

    template <VERB verb, typename MessageType>
    void print(MessageType&& msg)
    {
        out->log(static_cast<logging::level::level_enum>(verb), std::forward<MessageType>(msg));
    }
    template <level log_level, typename... Args>
    void log(fmt::format_string<Args...> format_str, Args&&... fargs)
    {
        status->log(
            static_cast<logging::level::level_enum>(log_level), format_str,
            std::forward<Args>(fargs)...
        );
    }
    template <level log_level, typename T>
    requires(!std::is_invocable_v<T>)
    void log(T&& msg)
    {
        status->log(static_cast<logging::level::level_enum>(log_level), std::forward<T>(msg));
    }

    auto get_out() const -> std::shared_ptr<logging::logger> { return out; }
    auto get_status() const -> std::shared_ptr<logging::logger> { return status; }

private:
    Internal();
    ~Internal() = default;

    // store indentation amount for output and logging messages
    int indent_amount;

    // whether we performed logger setup
    bool have_setup_loggers;

    // whether we have colored output
    bool have_colored_output;

    // logger for program output
    // while we don't need a logger for that, we can abuse output sinks for ease of use
    std::shared_ptr<logging::logger> out;

    // logging
    std::shared_ptr<logging::logger> status;
};

}  // namespace detail

/**
 * @brief Perform formatting and markup on a string with a given markup style.
 *
 * @tparam S The format string, inferred from argument
 * @tparam Args The arguments to be formatted into the string, inferred from argument
 * @param m Enum value for the markup
 * @param format_str The format string
 * @param fargs The arguments to be formatted into the string
 * @return std::string
 */
template <typename S, typename... Args>
inline std::string markup(MARKUP m, const S& format_str, Args&&... fargs)
{
    fmt::text_style style;
    if (detail::Internal::get_instance().do_color())
    {
        switch (m)
        {
        case MARKUP::TOOL:
            style = fmt::fg(fmt::terminal_color::magenta);
            break;
        case MARKUP::SUBTOOL:
            style = fmt::fg(fmt::terminal_color::cyan);
            break;
        case MARKUP::FILE:
            style = fmt::fg(fmt::terminal_color::blue) | fmt::emphasis::underline;
            break;
        case MARKUP::OUTPUT:
            style = fmt::fg(fmt::terminal_color::blue) | fmt::emphasis::bold;
            break;
        case MARKUP::GOOD:
            style = fmt::fg(fmt::terminal_color::green) | fmt::emphasis::bold;
            break;
        case MARKUP::BAD:
            style = fmt::fg(fmt::terminal_color::red) | fmt::emphasis::bold;
            break;
        case MARKUP::WARNING:
            style = fmt::fg(fmt::terminal_color::yellow) | fmt::emphasis::bold;
            break;
        case MARKUP::IMPORTANT:
            style = fmt::emphasis::bold;
            break;
        case MARKUP::PARAMETER:
            style = fmt::fg(fmt::terminal_color::cyan) | fmt::emphasis::bold;
            break;
        case MARKUP::UNIMPORTANT:
            style = fmt::fg(fmt::terminal_color::bright_black);
            break;
        }
    }
    std::string res = fmt::format(style, format_str, std::forward<Args>(fargs)...);
    return res;
};

/**
 * @brief Change the indentation level of lola output and log messages
 *
 * @param i The amount to change the indentation by (can be negative)
 */
inline void indent_by(int i)
{
    if (!i)
    {
        return;
    }
    int& indent_amount = detail::Internal::get_instance().get_indent_amount();
    indent_amount += i;
    if (indent_amount <= 0)
    {
        indent_amount = 0;
    }
    detail::Internal::get_instance().indent_to(indent_amount);
}

/**
 * @brief Set the indentation level of lola output and log messages. Simply an easy-access
 * function for RT::detail::Internal::get_instance().indent_to(i)
 *
 * @param i The amount to set the indentation to (negative values are converted to 0)
 */
inline void indent_to(int i) { detail::Internal::get_instance().indent_to(i); }

/**
 * @brief Print function with included formatting. Abstracts from whatever is used
 * internally to output messages. Template parameters can enforce verbosity check and pass
 * the verbosity
 *
 * @tparam verb The verbosity
 * @tparam Args The arguments to format into the format string, inferred from argument
 * @param format_str The format string
 * @param fargs The arguments to format into the string
 */
template <VERB verb = VERB::OUTPUT, typename... Args>
void print(fmt::format_string<Args...> format_str, Args&&... fargs)
{
    detail::Internal::get_instance().print<verb>(format_str, std::forward<Args>(fargs)...);
}

/**
 * @brief Print function without included formatting. Abstracts from whatever is used
 * internally to output messages. Template parameters can enforce verbosity check and pass
 * the verbosity
 *
 * @tparam verb The verbosity
 * @tparam MessageType Type of the message to print, inferred from argument
 * @param msg The message to print
 */
template <VERB verb = VERB::OUTPUT, typename MessageType>
inline void print(MessageType&& msg)
{
    detail::Internal::get_instance().print<verb>(std::forward<MessageType>(msg));
}

/**
 * @brief Log level for compile time checking
 *
 */
constexpr logging::level::level_enum active_log_level = LOLA_ACTIVE_LOG_LEVEL;

/**
 * @brief Macro to allow for compile time removal of RT::log calls
 *
 */
#define LOLA_LOG(LEVEL, ...)                                                \
    {                                                                       \
        if constexpr (static_cast<logging::level::level_enum>(level::LEVEL) \
                      >= RT::active_log_level)                              \
            RT::log<LEVEL>(__VA_ARGS__);                                    \
    }                                                                       \
    static_assert(true, "")  // require semicolon after macro

/**
 * @brief Log some function call. Used to put complex logging code into one call (to
 * remove it for example through LOLA_LOG macro). Also allows for more `readable' code by
 * separating log computation into their own scope.
 *
 * @tparam log_level The level of the log code. Removes execution based on `-L level'
 * commandline parameter.
 * @tparam Args Some invocable functional type + argument types
 * @param args The function and arguments to pass to the functional
 */
template <level log_level = level::info, typename... Args>
requires std::is_invocable_v<Args...>
inline void log(Args&&... fargs)
{
    // only do stuff if active log level is low enough
    if (static_cast<logging::level::level_enum>(log_level)
        >= detail::Internal::get_instance().get_log_level())
    {
        std::invoke(std::forward<Args>(fargs)...);
    }
}

/**
 * @brief Log text with included formatting. Abstracts from whatever is used
 * internally to output messages.
 *
 * @tparam log_level The level of the log code. Removes execution based on `-L level'
 * commandline parameter.
 * @tparam Args The argument types to format into the string
 * @param format_str The format string
 * @param args The arguments to format into the string
 */
template <level log_level = level::info, typename... Args>
inline void log(fmt::format_string<Args...> format_str, Args&&... fargs)
{
    detail::Internal::get_instance().log<log_level>(format_str, std::forward<Args>(fargs)...);
}

/**
 * @brief Log text without included formatting. Abstracts from whatever is used
 * internally to output messages.
 *
 * @tparam log_level The level of the log code. Removes execution based on `-L level'
 * commandline parameter.
 * @tparam MessageType of the output message
 * @param msg The thing to output
 */
template <level log_level = level::info, typename MessageType>
requires(!std::is_invocable_v<MessageType>)
inline void log(MessageType&& msg)
{
    detail::Internal::get_instance().log<log_level>(std::forward<MessageType>(msg));
}

template <bool log = false, typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>& newline(std::basic_ostream<CharT, Traits>& os)
{
    if constexpr (log)
    {
        fmt::print(
            os, "\n[{0}][{1:.<{2}}]{1:{3}} ", markup(MARKUP::TOOL, "lola"), "", 1,
            detail::Internal::get_instance().get_indent_amount()
        );
    }
    else
    {
        fmt::print(
            os, "\n[{0}]{1:{2}} ", markup(MARKUP::TOOL, "lola"), "",
            detail::Internal::get_instance().get_indent_amount()
        );
    }
    return os;
}

}  // namespace RT
