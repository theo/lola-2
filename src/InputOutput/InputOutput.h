/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\status new
\ingroup g_io

\brief declaration of classes IO, Input, and Output
*/

#pragma once

#include <Core/Dimensions.h>
#include <InputOutput/Logger.h>

#include <fmt/format.h>
#include <fmt/os.h>

#include <memory>
#include <string>
#include <string_view>
#include <ostream>

/*!
\brief wrapper class for file input and output

This class wraps convenience methods around a standard C-style FILE*. Its main
purpose is to automatically close files in the moment its IO object leaves the
scope.

\ingroup g_io
*/
class IO
{
protected:
    /// the filename (or empty in case of stdin/stdout)
    std::string filename;

    /// the kind of the file
    std::string kind;

    /// default constructor
    IO(std::string_view, std::string_view, FILE*);
    ~IO();
    IO(const IO&) = delete;
    IO& operator=(const IO&) = delete;
    IO(IO&&);
    IO& operator=(IO&&);

    /// the filepointer
    FILE* fp;
    bool moved_from = false;
};

/*!
\brief wrapper class for output files
\ingroup g_io
*/
class Output : public IO
{
public:
    /// output to file (given kind and optional filename)
    Output(std::string_view, std::string_view = "-");

    template <typename... Args>
    void print(fmt::format_string<Args...> format_str, Args&&... fargs) const
    {
        if (have_stream)
        {
            RT::print<VERB::FULL>(format_str, std::forward<Args>(fargs)...);
        }
        else
        {
            fmt::print(fp, format_str, std::forward<Args>(fargs)...);
        }
    }

private:
    const bool have_stream;
};

/*!
\brief wrapper class for input files
\ingroup g_io
*/
class Input : public IO
{
public:
    /// input from file (given kind and optional filename)
    Input(std::string_view, std::string_view = "-");

    /// given the location of some text prints an excerpt of the file
    static void printExcerpt(
        std::string_view filename, int first_line, int first_column, int last_line, int last_column
    );

    /// implicit cast to FILE* (return fp)
    operator FILE*() const;

    /// return filename
    auto get_filename() const -> std::string_view;

    auto get_kind() const -> std::string_view;

    auto get_filename_string() const -> const std::string*;
};
