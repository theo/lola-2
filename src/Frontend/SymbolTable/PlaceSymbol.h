/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status approved 25.01.2012
\ingroup g_symboltable

\brief definition of class PlaceSymbol
*/

#ifndef SRC_FRONTEND_SYMBOLTABLE_PLACESYMBOL_H
#define SRC_FRONTEND_SYMBOLTABLE_PLACESYMBOL_H

#include <forward_list>

#include <fmt/format.h>

#include <Core/Dimensions.h>
#include <Frontend/SymbolTable/Symbol.h>
#include "Frontend/SymbolTable/SymbolTable.h"
#include <Frontend/Parser/ParserPnml.h>
#include <Highlevel/hlplace.h>

/*!
\brief a symbol table entry for a place

Place symbols carry their name as key and capacity and an initial marking as
payload. While capacity is in the context of place declaration (thus, part of
constructor), initial marking is specified separately (thus, setter).
Additional information is number of pre-transitions and number of
post-transitions. This information is updated while parsing transitions.

\note The members cardPost and cardPre are used for later data structures for
arcs.
\note The capacity UINT_MAX denotes the absence of a capacity.
\note This class does not allocated dynamic memory.

\ingroup g_symboltable

\todo Dopplungen behandeln.
*/
class PlaceSymbol : public Symbol
{
public:
    PlaceSymbol(const char* k, capacity_t cap);

    /// getter for capacity
    capacity_t getCapacity() const;
    /// getter for initial marking
    capacity_t getInitialMarking() const;
    /// getter for number of pre-transitions
    arrayindex_t getCardPre() const;
    /// getter for number of post-transitions
    arrayindex_t getCardPost() const;

    /// adder for initial marking
    void addInitialMarking(capacity_t);
    /// incrementor for number of post-transitions
    void notifyPost();
    /// incrementor for number of pre-transitions
    void notifyPre();

    /// index of the nested unit this place belongs to (0 = no unit)
    nestedunit* nested;

    /// the maximum number of tokens that must be representable for this place
    capacity_t capacity;
    /// the initial number of tokens on this place
    capacity_t initialMarking;
    /// the number of transitions that consume from this place
    arrayindex_t cardPost;
    /// the number of transitions that produce on this place
    arrayindex_t cardPre;
    /// whether a formula is referring to me

    hlplace* origin;  // HL place where this LL place stems from, NULL if original net was PT

    bool occurspositive;  // there exists an atomic proposition where p occurs as positive place (or
                          // p is in pre(t) for some UNFIREABLE(t)
    bool occursnegative;  // there exists an atomic proposition where p occurs as negative place (or
                          // p is in pre(t) for some FIREABLE(t)

    // PlaceSymbol * copy();
};

namespace parsing
{
class NestedUnit;

class Place : public Symbol
{
public:
    Place(arrayindex_t index, capacity_t cap) : Symbol{index}, capacity{cap} {}
    auto get_card_pre() const -> arrayindex_t { return card_pre; }
    auto get_card_post() const -> arrayindex_t { return card_post; }
    void inc_pre() { ++card_pre; }
    void inc_post() { ++card_post; }

    SymbolTable<NestedUnit>::pointer nested = nullptr;
    hlplace* origin = nullptr;
    bool occurs_positive = false;
    bool occurs_negative = false;
    capacity_t capacity;

private:
    arrayindex_t card_post = 0;
    arrayindex_t card_pre = 0;
};
}  // namespace parsing

#endif /* SRC_FRONTEND_SYMBOLTABLE_PLACESYMBOL_H */
