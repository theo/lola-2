/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status approved 25.01.2012
\ingroup g_symboltable

\brief definition of class TransitionSymbol
*/

#pragma once

#include <Core/Dimensions.h>
#include <Frontend/SymbolTable/Symbol.h>
#include <Frontend/SymbolTable/ArcList.h>
#include <Highlevel/hltransition.h>

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <forward_list>

/*!
\brief a symbol table entry for a transition

Transition symbols carry name as key and a list of incoming arcs, a list of
outgoing arcs, and a fairness assumption as payload. All information is
available in the context of transition definition, thus all information is
provided in the constructor

\note The members cardPost and cardPre, Post, and Pre are used for later data
structures for arcs.
\note All dynamic memory allocated by this class is deallocated in the
destructor.

\ingroup g_symboltable

\todo Dopplungen behandeln.
*/
class TransitionSymbol : public Symbol
{
public:
    TransitionSymbol(const char*, fairnessAssumption_t, ArcList*, ArcList*);
    ~TransitionSymbol();

    /// getter for number of incoming arcs
    unsigned int getCardPre() const;
    /// getter for number of outgoing arcs
    unsigned int getCardPost() const;
    /// getter for incoming arcs
    ArcList* getPre() const;
    /// getter for number of post-places
    ArcList* getPost() const;
    /// getter for fairness assumption
    fairnessAssumption_t getFairness() const;

public:
    /// the fairness assumption specified for this tarnsition
    fairnessAssumption_t fairness;

    /// the number of places that this transition produces to
    unsigned int cardPost;
    /// the number of places that this transition consumes from
    unsigned int cardPre;
    /// the outgoing arcs of this transition
    ArcList* Post;
    /// the ingoing arcs of this transition
    ArcList* Pre;
    /// for LL transition: the HL transition it is derived from
    hltransition* origin;
    bool findlow;    /// whether the Findlow criterion is satisfied for this skeleton transition
    bool duplicate;  /// whether this transition has a pre-set identical to another transition (for
                     /// Findlow)
};

namespace parsing
{
class Transition : public Symbol
{
public:
    explicit Transition(
        fairnessAssumption_t fair_ = NO_FAIRNESS,
        std::forward_list<Arc>&& pre_ = std::forward_list<Arc>{},
        std::forward_list<Arc>&& post_ = std::forward_list<Arc>{}
    );
    auto get_card_pre() const -> std::forward_list<Arc>::size_type { return card_pre; }
    auto get_card_post() const -> std::forward_list<Arc>::size_type { return card_post; }
    auto get_fairness() const -> fairnessAssumption_t { return fairness; }
    void add_pre(SymbolTable<Place>::reference source, mult_t mult);
    void add_post(SymbolTable<Place>::reference target, mult_t mult);
    auto get_pre() const -> const std::forward_list<Arc>& { return pre; }
    auto get_post() const -> const std::forward_list<Arc>& { return post; }
    auto is_external() const -> bool { return external; }
    void set_external() { external = true; }

    hltransition* origin = nullptr;

private:
    std::forward_list<Arc> pre;
    std::size_t card_pre = 0;
    std::forward_list<Arc> post;
    std::size_t card_post = 0;
    fairnessAssumption_t fairness;
    bool external = false;

public:
    bool findlow = false;
    bool duplicate = false;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::Transition> : fmt::formatter<std::string_view>
{
    auto format(const parsing::Transition& t, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "#: {}, Ext: {}, Fair: {}, Pre: {}, Post: {}", t.index,
            t.is_external(), fmt::underlying(t.get_fairness()), t.get_pre(), t.get_post()
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};
