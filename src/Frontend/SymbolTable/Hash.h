/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * @file Hash.h
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definition of our own hash function for strings
 * @version 0.1
 * @date 2024-05-13
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef SRC_FRONTEND_SYMBOLTABLE_HASH_H
#define SRC_FRONTEND_SYMBOLTABLE_HASH_H

#include <cstddef>
#include <numeric>
#include <string>
#include <string_view>

namespace parsing
{

namespace detail
{
inline auto hash_it(std::string_view txt, std::size_t init = 0) -> std::size_t
{
    // SDBM Algorithm from
    // http://www.ntecs.de/projects/guugelhupf/doc/html/x435.html
    return std::accumulate(
        txt.begin(), txt.end(), init,
        [](const std::size_t hash, const char c) { return c + (hash << 6) + (hash << 16) - hash; }
    );
}
}  // namespace detail

struct hash
{
    using is_transparent = void;
    auto operator()(const char* txt) const -> std::size_t { return detail::hash_it(txt); }
    auto operator()(std::string_view txt) const -> std::size_t { return detail::hash_it(txt); }
    auto operator()(const std::string& txt) const -> std::size_t { return detail::hash_it(txt); }
};
}  // namespace parsing

#endif /* SRC_FRONTEND_SYMBOLTABLE_HASH_H */
