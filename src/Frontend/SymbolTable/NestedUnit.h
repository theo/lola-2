/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * @file NestedUnit.h
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @version 0.1
 * @date 2024-08-07
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef SRC_FRONTEND_SYMBOLTABLE_NESTEDUNIT_H
#define SRC_FRONTEND_SYMBOLTABLE_NESTEDUNIT_H

#include <forward_list>

#include "fmt/format.h"
#include "fmt/ranges.h"

#include "Frontend/SymbolTable/PlaceSymbol.h"
#include "Frontend/SymbolTable/SymbolTable.h"

namespace parsing
{

class NestedUnit
{
public:
    auto get_places() const -> const std::forward_list<SymbolTable<Place>::pointer>&
    {
        return places;
    }
    void add_place(SymbolTable<Place>::reference place) { places.push_front(&place); }
    auto is_leaf_unit() const -> bool { return leaf_unit; }
    void set_inner_unit() { leaf_unit = false; }

    SymbolTable<NestedUnit>::pointer parent = nullptr;

private:
    std::forward_list<SymbolTable<Place>::pointer> places;
    bool leaf_unit = true;

public:
    bool marked = false;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::NestedUnit> : fmt::formatter<std::string>
{
    auto format(const parsing::NestedUnit& nu, fmt::format_context& ctx) const
    {
        std::string tmp;
        std::vector<std::string_view> f;
        std::transform(
            nu.get_places().begin(), nu.get_places().end(), std::back_inserter(f),
            [](const auto p) { return std::string_view{p->first}; }
        );
        fmt::format_to(std::back_inserter(tmp), "Places: {}, Leaf: {}", f, nu.is_leaf_unit());
        if (nu.parent)
        {
            fmt::format_to(std::back_inserter(tmp), " Parent: {}", nu.parent->first);
        }

        return fmt::formatter<std::string>::format(tmp, ctx);
    }
};

// why is this here? because of forward declarations, the use of p.nested does not work in
// PlaceSymbol.h
// TODO: maybe move the printing specifications to own header
template <>
struct fmt::formatter<parsing::Place> : fmt::nested_formatter<capacity_t>
{
    auto format(const parsing::Place& p, fmt::format_context& ctx) const
    {
        return write_padded(
            ctx,
            [=, this](auto out)
            {
                if (p.nested)
                {
                    return fmt::format_to(
                        out, "#: {}, Cap: {}, #Pre: {}, #Post: {}, Nested: {}", nested(p.index),
                        nested(p.capacity), nested(p.get_card_pre()), nested(p.get_card_post()),
                        p.nested->first
                    );
                }
                return fmt::format_to(
                    out, "#: {}, Cap: {}, #Pre: {}, #Post: {}", nested(p.index), nested(p.capacity),
                    nested(p.get_card_pre()), nested(p.get_card_post())
                );
            }
        );
    }
};

#endif /* SRC_FRONTEND_SYMBOLTABLE_NESTEDUNIT_H */
