/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief definition of class ParserMNet

container for all information parsed in ParserMlola.yy
*/

#pragma once

#include <memory>
#include <string>

#include <Core/Dimensions.h>
#include "Core/Runtime.h"
#include <Frontend/ModularSymbols/FusionSetSymbol.h>
#include <Frontend/ModularSymbols/InstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleSymbol.h>
#include <Frontend/SymbolTable/SymbolTable.h>
#include "InputOutput/InputOutput.h"
#include <Net/ModularStructure.h>
#include "Frontend/ModularSymbols/AliasSymbol.h"

class ParserMNet
{
public:
    ParserMNet();
    ~ParserMNet();

    SymbolTable* ModuleTable;
    SymbolTable* InstanceTable;
    SymbolTable* FusionSetTable;

    ModularStructure* symboltable2modularstructure();
};

namespace parsing
{
class ModularStructure
{
    friend class NetParser;
    friend auto parse_modular_symbols(Input input_file) -> std::unique_ptr<ModularStructure>;
    friend void parse_modular_symbols_benchmark(Input input_file);

public:
    auto symboltable2modularstructure() -> std::unique_ptr<lola::ModularStructure>;

    void debug_print() const;

    auto get_modules() const -> const SymbolTable<Module>& { return modules; }
    auto get_instances() const -> const SymbolTable<Instance>& { return instances; }
    auto get_fusionvectors() const -> const SymbolTable<FusionVector>& { return fusionvectors; }
    auto get_aliases() const -> const SymbolTable<Alias>& { return aliases; }

    template <typename Key>
    auto find_module(const Key& key) -> SymbolTable<Module>::iterator
    {
        return modules.find(key);
    }

    template <typename Key>
    auto find_instance(const Key& key) -> SymbolTable<Instance>::iterator
    {
        return instances.find(key);
    }

    template <typename Key>
    auto find_fusionvector(const Key& key) -> SymbolTable<FusionVector>::iterator
    {
        return fusionvectors.find(key);
    }

    template <typename Key>
    auto find_alias(const Key& key) -> SymbolTable<Alias>::iterator
    {
        return aliases.find(key);
    }

private:
    auto add_module(std::string&& key) -> std::pair<SymbolTable<Module>::iterator, bool>
    {
        return modules.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)), std::forward_as_tuple()
        );
    }

    auto add_instance(
        std::string&& key, SymbolTable<Module>::reference module, arrayindex_t index_offset
    ) -> std::pair<SymbolTable<Instance>::iterator, bool>
    {
        return instances.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)),
            std::forward_as_tuple(module, index_offset)
        );
    }

    auto add_fusionvector(std::string&& key) -> std::pair<SymbolTable<FusionVector>::iterator, bool>
    {
        return fusionvectors.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)),
            std::forward_as_tuple(instances.size())
        );
    }

    auto add_alias(
        std::string&& key, SymbolTable<Instance>::const_reference instance,
        SymbolTable<Place>::const_reference place
    ) -> std::pair<SymbolTable<Alias>::iterator, bool>
    {
        return aliases.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)),
            std::forward_as_tuple(std::addressof(instance), std::addressof(place))
        );
    }

    SymbolTable<Module> modules{static_cast<std::size_t>(RT::args.sizeofsymboltable_arg)};
    SymbolTable<Instance> instances{static_cast<std::size_t>(RT::args.sizeofsymboltable_arg)};
    SymbolTable<FusionVector> fusionvectors{static_cast<std::size_t>(RT::args.sizeofsymboltable_arg)
    };
    SymbolTable<Alias> aliases{};
};
}  // namespace parsing
