/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief implementation of class ParserMNet

container for all information parsed in ParserMlola.yy
*/

#include "Frontend/ModularParser/ParserMNet.h"

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Frontend/ModularSymbols/FusionSetSymbol.h>
#include <Frontend/ModularSymbols/InstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleInstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleSymbol.h>
#include <Frontend/SymbolTable/PlaceSymbol.h>
#include <Frontend/SymbolTable/TransitionSymbol.h>
#include <Net/ModularStructure.h>
#include <fmt/format.h>

#include <cstddef>
#include <map>
#include <memory>
#include <numeric>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "InputOutput/Logger.h"

ParserMNet::ParserMNet()
{
    ModuleTable = new SymbolTable();
    InstanceTable = new SymbolTable();
    FusionSetTable = new SymbolTable();
}

ParserMNet::~ParserMNet()
{
    delete ModuleTable;
    delete InstanceTable;
    delete FusionSetTable;
}

ModularStructure* ParserMNet::symboltable2modularstructure()
{
    enum
    {
        MOD = static_cast<int>(modstruct_name_t::MOD),
        INST = static_cast<int>(modstruct_name_t::INST),
        FS = static_cast<int>(modstruct_name_t::FS)
    };
    ModularStructure* modStruct = new ModularStructure();

    // MODULES

    // allocate memory for modules
    arrayindex_t cardModules = ModuleTable->getCard();
    modStruct->cardModules = cardModules;
    modStruct->module = new Petrinet*[cardModules];
    modStruct->firstInterfaceTransition = new arrayindex_t[cardModules];
    modStruct->Name[MOD] = new const char*[cardModules];

    arrayindex_t cardInstances = InstanceTable->getCard();  // + instances for modules without
                                                            // instances

    ModuleSymbol* moduleSym = reinterpret_cast<ModuleSymbol*>(ModuleTable->first());

    for (arrayindex_t i = 0; i < cardModules; i++)
    {
        moduleSym->setIndex(i);
        modStruct->Name[MOD][i] = moduleSym->getKey();

        // set indicies for places & transitions and sort the transitions
        modStruct->firstInterfaceTransition[i] = moduleSym->setIndices();

        // parse module net
        modStruct->module[i] = moduleSym->symboltable2modnet();

        // for modules without instances, we need another instance
        if (moduleSym->Instances.empty())
            cardInstances++;

        moduleSym = reinterpret_cast<ModuleSymbol*>(ModuleTable->next());
    }

    // INSTANCES

    // allocate memory for instances
    modStruct->cardInstances = cardInstances;
    modStruct->instanceModule = new arrayindex_t[cardInstances];
    modStruct->instanceInitial = new capacity_t*[cardInstances];
    modStruct->Name[INST] = new const char*[cardInstances];

    arrayindex_t j = 0;  // index for instances

    for (moduleSym = reinterpret_cast<ModuleSymbol*>(ModuleTable->first()); moduleSym;
         moduleSym = reinterpret_cast<ModuleSymbol*>(ModuleTable->next()))
    {
        // if no instance is defined for this module, we create an instance for it
        if (moduleSym->Instances.empty())
        {
            modStruct->instanceModule[j] = moduleSym->getIndex();
            modStruct->instanceInitial[j] = moduleSym->getInitialMarking();
            modStruct->Name[INST][j] = moduleSym->getKey();
            moduleSym->instanceIndex = j;
            j++;
        }

        std::map<PlaceSymbol*, capacity_t>* lastMarking = NULL;  // marking of the previous instance
                                                                 // of moduleSym might be the same
                                                                 // as the current one
        capacity_t* defaultMarking = NULL;  // default marking of the module, if needed

        for (std::vector<InstanceSymbol*>::iterator instSym = moduleSym->Instances.begin();
             instSym != moduleSym->Instances.end(); instSym++)
        {
            (*instSym)->setIndex(j);
            modStruct->instanceModule[j] = moduleSym->getIndex();
            modStruct->Name[INST][j] = (*instSym)->getKey();

            // if no marking is defined for this instance we use the marking of the module
            if (!((*instSym)->hasMarking))
            {
                if (defaultMarking == NULL)
                    defaultMarking = moduleSym->getInitialMarking();
                modStruct->instanceInitial[j] = defaultMarking;
                lastMarking = NULL;  // no marking was defined for the current instance
            }
            else
            {
                if (!(lastMarking == NULL) && (lastMarking == (*instSym)->marking))  // several
                                                                                     // consecutive
                                                                                     // instances
                                                                                     // can share
                                                                                     // one marking
                    modStruct->instanceInitial[j] = modStruct->instanceInitial[j - 1];
                else
                {
                    // process the marking

                    modStruct->instanceInitial[j] =
                        new capacity_t[moduleSym->PlaceTable->getCard()];
                    PlaceSymbol* ps;

                    for ((ps = reinterpret_cast<PlaceSymbol*>(moduleSym->PlaceTable->first())); ps;
                         ps = reinterpret_cast<PlaceSymbol*>(moduleSym->PlaceTable->next()))
                    {
                        std::map<PlaceSymbol*, capacity_t>::iterator mark_pair =
                            (*instSym)->marking->find(ps);

                        if (mark_pair != (*instSym)->marking->end())  // = "if a marking is defined
                                                                      // for this place"
                            modStruct->instanceInitial[j][ps->index] = mark_pair->second;
                        else
                            modStruct->instanceInitial[j][ps->index] = 0;  // default marking, in
                                                                           // case no marking is
                                                                           // explicitly defined for
                                                                           // this place
                    }
                    lastMarking = (*instSym)->marking;
                }
            }

            j++;
        }
    }

    // FUSIONSETS

    // allocate memory for fusionsets
    arrayindex_t cardFS = FusionSetTable->getCard();
    modStruct->cardFusionSets = cardFS;
    if (cardFS > 0)
    {
        modStruct->cardTransitionsFS = new arrayindex_t[cardFS];
        modStruct->fusionSetTransition = new arrayindex_t*[cardFS];
        modStruct->fusionSetModule = new arrayindex_t*[cardFS];
        modStruct->fusionSetInstance = new arrayindex_t*[cardFS];
        modStruct->Name[FS] = new const char*[cardFS];
    }

    int k = 0;  // index for fusionsets

    for (FusionSetSymbol* fsSym = reinterpret_cast<FusionSetSymbol*>(FusionSetTable->first());
         fsSym; fsSym = reinterpret_cast<FusionSetSymbol*>(FusionSetTable->next()))
    {
        fsSym->setIndex(k);
        modStruct->Name[FS][k] = fsSym->getKey();

        // allocate memory for that fusionset depending on its size
        int cardTransitions = fsSym->Transitions.size();
        modStruct->cardTransitionsFS[k] = cardTransitions;
        modStruct->fusionSetTransition[k] = new arrayindex_t[cardTransitions];
        modStruct->fusionSetModule[k] = new arrayindex_t[cardTransitions];
        modStruct->fusionSetInstance[k] = new arrayindex_t[cardTransitions];

        int i = 0;
        for (std::pair<ModuleInstanceSymbol*, TransitionSymbol*> t_pair : fsSym->Transitions)
        {
            arrayindex_t inst;
            arrayindex_t mod;
            arrayindex_t t;

            if (t_pair.first->isInstance)
            {
                InstanceSymbol* instance = reinterpret_cast<InstanceSymbol*>(t_pair.first);
                inst = instance->getIndex();
                mod = instance->module->getIndex();
            }
            else
            {
                ModuleSymbol* module = reinterpret_cast<ModuleSymbol*>(t_pair.first);
                inst = module->instanceIndex;
                mod = module->getIndex();
            }

            modStruct->fusionSetTransition[k][i] = t_pair.second->getIndex();
            modStruct->fusionSetInstance[k][i] = inst;
            modStruct->fusionSetModule[k][i] = mod;

            i++;
        }

        k++;
    }

    return modStruct;
}

namespace parsing
{
auto ModularStructure::symboltable2modularstructure() -> std::unique_ptr<lola::ModularStructure>
{
    namespace rg = std::ranges;
    namespace vw = std::views;

    // TODO(lukas): do this while parsing
    const auto num_fusionparticipants = std::accumulate(
        fusionvectors.cbegin(), fusionvectors.cend(), std::size_t{0},
        [](std::size_t acc, const auto& elem) { return acc + elem.second.get_transitions().size(); }
    );

    auto ms = std::make_unique<lola::ModularStructure>(
        modules.size(), instances.size(), fusionvectors.size(), num_fusionparticipants
    );

    auto handle_modules = [&](std::tuple<SymbolTable<Module>::reference, arrayindex_t> elem)
    {
        auto [module_with_name, index] = elem;
        auto& [name, modsym] = module_with_name;
        auto& module = ms->add_module(
            name, modsym.get_places().size(), modsym.get_initial_markings().size()
        );
        modsym.index = index;
        module.first_interface_transition = modsym.get_first_interface_transition();
        modsym.to_lola_module(module);
        rg::for_each(
            vw::zip(modsym.get_initial_markings(), vw::iota(0)),
            [&, this](std::tuple<Module::Marking&, arrayindex_t> elem)
            {
                auto [marking, index] = elem;
                auto lola_marking = module.add_marking();
                marking.index = index;
                rg::for_each(
                    marking.marking,
                    [&, this](const Module::Marking::map_type::value_type entry)
                    {
                        const auto [place_name, marking_value] = entry;
                        const auto index = modsym.find_place(place_name)->second.index;
                        lola_marking[index] = marking_value;
                    }
                );
            }
        );
    };

    rg::for_each(vw::zip(modules, vw::iota(0)), std::move(handle_modules));

    auto handle_instances = [&](std::tuple<SymbolTable<Instance>::reference, arrayindex_t> elem)
    {
        auto [instance_with_name, index] = elem;
        auto& [name, inst_sym] = instance_with_name;
        const auto mod_index = inst_sym.get_module().second.index;
        const auto marking_index = inst_sym.initial_marking->index;
        const auto& module = ms->get_modules()[mod_index];
        auto marking = module.get_markings()[marking_index];
        ms->add_instance(name, module, marking, inst_sym.get_offset());
        inst_sym.index = index;
    };

    rg::for_each(vw::zip(instances, vw::iota(0)), std::move(handle_instances));

    auto handle_fusion_sets =
        [&, this](std::tuple<SymbolTable<FusionVector>::reference, arrayindex_t> elem)
    {
        auto [fs_with_name, index] = elem;
        auto& [name, fs_sym] = fs_with_name;
        auto fusionset = ms->add_fusionset(name, fs_sym.get_transitions().size());
        fs_sym.index = index;
        rg::for_each(
            vw::zip(fs_sym.get_transitions(), fusionset.get_participants()),
            [&, this](std::tuple<
                      FusionVector::vector_type::const_reference, lola::detail::FusionParticipant&>
                          entry)
            {
                const auto [fusion_participant, lola_participant] = entry;
                const auto [inst_name, trans_with_name] = fusion_participant;
                const auto& [trans_name, trans_sym] = trans_with_name;
                const auto& inst_sym = instances.find(inst_name)->second;
                lola_participant.transition = trans_sym.index;
                lola_participant.instance = inst_sym.index;
                lola_participant.module = inst_sym.get_module().second.index;
            }
        );
    };

    rg::for_each(vw::zip(fusionvectors, vw::iota(0)), std::move(handle_fusion_sets));

    return ms;
}

void ModularStructure::debug_print() const
{
    RT::print<VERB::FULL>(
        "Modules:\n{}\nInstances:\n{}\nFusion Vectors:\n{}\nAliases:\n{}", fmt::join(modules, "\n"),
        fmt::join(instances, "\n"), fmt::join(fusionvectors, "\n"), fmt::join(aliases, "\n")
    );
}

}  // namespace parsing
