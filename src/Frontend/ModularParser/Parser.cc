/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * @file Parser.cc
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions of parser functions
 * @version 0.1
 * @date 2024-07-16
 *
 * @copyright Copyright (c) 2024
 *
 */

#include "Parser.h"

#include <cstdio>

#include <algorithm>
#include <charconv>
#include <chrono>
#include <filesystem>
#include <memory>
#include <string>
#include <string_view>
#include <system_error>
#include <tuple>
#include <vector>
#include <utility>

#include <fmt/format.h>
#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_iterators.hpp>

#include "Core/Dimensions.h"
#include "Core/Runtime.h"
#include "Formula/StatePredicate/AtomicBooleanPredicate.h"
#include "Formula/StatePredicate/FalsePredicate.h"
#include "Formula/StatePredicate/Term.h"
#include "Formula/StatePredicate/TruePredicate.h"
#include "Frontend/ModularParser/ParserMNet.h"
#include "Frontend/ModularSymbols/ModuleSymbol.h"
#include "Frontend/Parser/ast-system-k.h"
#include "InputOutput/InputOutput.h"
#include "InputOutput/Logger.h"

namespace
{
namespace rg = std::ranges;
namespace vw = std::views;
using iter = rapidxml::node_iterator<char>;

template <std::integral num_t>
num_t xml_to_numerical(const rapidxml::xml_node<>& node)
{
    using namespace std::string_view_literals;

    auto num = num_t{0};
    auto value = std::string_view{node.value(), node.value_size()};
    // remove whitespace before content, since std::from_chars does not ignore preceding
    // whitespace and all other methods require std::string (which is an unnecessary
    // copy)

    // remove_prefix(n) with n > size() (i.e. find_first_not_of returns beyond the end
    // -> no whitespace) is undefined behavior
    value.remove_prefix(std::min(value.find_first_not_of(" \t\n\f\r\v"sv), value.size()));

    auto [_, error_code] = std::from_chars(value.begin(), value.end(), num);
    if (error_code == std::errc::result_out_of_range) [[unlikely]]
    {
        RT::log<err>("number '{}' is too large to handle", value);
        RT::abort(ERROR::FILE);
    }
    return num;
}

}  // namespace

namespace parsing
{

// declaration, definition is in LexerMlola.ll because of global variables
void parse_mlola_symbols(Input input_file, ModularStructure& modular_structure);

void parse_pt_symbols(rapidxml::xml_node<>* net, Module& module)
{
    // helper variables
    auto current_place_index = arrayindex_t{0};
    auto current_transition_index = arrayindex_t{0};  // only for transitions that don't have names
    auto initial_marking = parsing::Module::Marking::map_type{};
    auto recheck_arcs = std::vector<std::tuple<std::string_view, std::string_view, mult_t>>{};
    const rapidxml::xml_node<>* nested_unit_node = nullptr;

    // function to handle a single node of the net specification (place, transition, arc, nested
    // unit specification)
    auto handle_net_node = [&](const rapidxml::xml_node<>& node)
    {
        auto node_type = std::string_view{node.name(), node.name_size()};
        if (node_type == "place")
        {
            // places -> parse into module directly
            auto* pnml_place_name = node.first_attribute("id");
            // allow unspecified name
            // TODO: rather error when place does not have a name?
            auto place_name = pnml_place_name
                ? std::string(pnml_place_name->value(), pnml_place_name->value_size())
                : fmt::format("p{}", current_place_index);

            auto [place_iter, not_exisiting_yet] = module.add_place(
                std::move(place_name), current_place_index++, MAX_CAPACITY
            );

            if (!not_exisiting_yet) [[unlikely]]
            {
                RT::log<err>("place '{}' name used twice", place_iter->first);
                RT::abort(ERROR::FILE);
            }
            if (auto* marking_node = node.first_node("initialMarking"); marking_node != nullptr)
            {
                auto* content = marking_node->first_node("text");
                if (!content)
                {
                    // early return possible since nothing relevant comes after
                    return;
                }
                auto place_marking = xml_to_numerical<capacity_t>(*content);
                // no need to waste space if no information is added
                if (place_marking != 0)
                {
                    initial_marking.emplace(place_iter->first, place_marking);
                }
            }
        }
        else if (node_type == "transition")
        {
            // transition -> parse into module directly
            auto* pnml_transition_name = node.first_attribute("id");
            // allow unspecified name
            // TODO: rather error when transition does not have a name?
            auto transition_name = pnml_transition_name
                ? std::string(pnml_transition_name->value(), pnml_transition_name->value_size())
                : fmt::format("t{}", current_transition_index);

            auto [trans_iter, not_existing_yet] = module.add_transition(std::move(transition_name));
            ++current_transition_index;
            if (!not_existing_yet) [[unlikely]]
            {
                RT::log<err>("transition '{}' name used twice", trans_iter->first);
                RT::abort(ERROR::FILE);
            }
        }
        else if (node_type == "arc")
        {
            // arc -> check if source and target are known
            //        yes -> add to parsing structure
            //        no -> remember info and add later
            // TODO: iterate over all attributes and stop early vs. find twice?
            auto* source_node = node.first_attribute("source");
            auto* target_node = node.first_attribute("target");
            auto source_name = std::string_view{source_node->value(), source_node->value_size()};
            auto target_name = std::string_view{target_node->value(), target_node->value_size()};
            auto weight = mult_t{1};
            if (auto* weight_node = node.first_node("inscription"); weight_node != nullptr)
            {
                auto* content = weight_node->first_node("text");
                if (content)
                {
                    auto [_, error_code] = std::from_chars(
                        content->value(), content->value() + content->value_size(), weight
                    );
                    if (error_code == std::errc::result_out_of_range) [[unlikely]]
                    {
                        RT::log<err>(
                            "weight of arc '{} -> {}' is too large to handle: {}", source_name,
                            target_name, std::string_view{content->value(), content->value_size()}
                        );
                        RT::abort(ERROR::FILE);
                    }
                }
            }
            // if for some reason a place and transition are called the same -> assume p->t arc
            bool is_pre_arc = true;
            auto place_iter = module.find_place(source_name);
            if (place_iter == module.get_places().end())
            {
                place_iter = module.find_place(target_name);
                is_pre_arc = false;
            }
            // in theory, this check is redundant in some cases
            auto trans_iter = module.find_transition(source_name);
            if (trans_iter == module.get_transitions().end())
            {
                trans_iter = module.find_transition(target_name);
            }
            // one of the end points unknown? -> check later
            if (place_iter == module.get_places().end()
                || trans_iter == module.get_transitions().end())
            {
                recheck_arcs.emplace_back(source_name, target_name, weight);
                return;
            }
            if (is_pre_arc)
            {
                trans_iter->second.add_pre(*place_iter, weight);
            }
            else
            {
                trans_iter->second.add_post(*place_iter, weight);
            }
        }
        else if (node_type == "toolspecific")
        {
            auto* tool_attr = node.first_attribute("tool");
            auto tool_name = std::string_view{tool_attr->value(), tool_attr->value_size()};
            if (tool_name == "nupn")
            {
                nested_unit_node = &node;
            }
        }
    };

    LOLA_LOG(trace, "traversing XML nodes");

    std::for_each(
        rapidxml::node_iterator<char>(net), rapidxml::node_iterator<char>(),
        [&](rapidxml::xml_node<>& node)
        {
            if (std::string_view{node.name(), node.name_size()} == "page")
            {
                std::for_each(
                    rapidxml::node_iterator<char>(&node), rapidxml::node_iterator<char>(),
                    handle_net_node
                );
            }
        }
    );

    module.add_default_marking(std::move(initial_marking));

    LOLA_LOG(trace, "finished XML nodes, re-checking arcs");

    std::ranges::for_each(
        recheck_arcs,
        [&](auto& arc)
        {
            // TODO: delegate logic since this is the same as above?
            auto [source_name, target_name, weight] = arc;
            bool is_pre_arc = true;
            auto place_iter = module.find_place(source_name);
            if (place_iter == module.get_places().end())
            {
                place_iter = module.find_place(target_name);
                is_pre_arc = false;
            }
            auto trans_iter = module.find_transition(source_name);
            if (trans_iter == module.get_transitions().end())
            {
                trans_iter = module.find_transition(target_name);
            }
            if (place_iter == module.get_places().end())
            {
                RT::log<err>(
                    "node {} in arc '{} -> {}' does not exist", place_iter->first, source_name,
                    target_name
                );
                RT::abort(ERROR::FILE);
            }
            if (trans_iter == module.get_transitions().end())
            {
                RT::log<err>(
                    "node {} in arc '{} -> {}' does not exist", trans_iter->first, source_name,
                    target_name
                );
                RT::abort(ERROR::FILE);
            }
            if (is_pre_arc)
            {
                trans_iter->second.add_pre(*place_iter, weight);
            }
            else
            {
                trans_iter->second.add_post(*place_iter, weight);
            }
        }
    );

    LOLA_LOG(trace, "finished arcs, checking nested units");

    if (!nested_unit_node)
    {
        return;
    }

    auto* structure_node = nested_unit_node->first_node("structure");

    if (!structure_node)
    {
        return;
    }

    assert(structure_node->first_attribute("safe"));

    auto handle_nested_unit = [&](const rapidxml::xml_node<>& unit_node)
    {
        using namespace std::string_view_literals;

        assert(unit_node.first_node("places"));
        auto places = std::string_view{
            unit_node.first_node("places")->value(), unit_node.first_node("places")->value_size()
        };
        assert(unit_node.first_attribute("id"));
        // we don't care if a unit with that name exists already -> just add places/subunits
        auto [unit_iter, _] = module.add_nested_unit(std::string{
            unit_node.first_attribute("id")->value(), unit_node.first_attribute("id")->value_size()
        });

        auto whitespace = " \t\n\f\r\v"sv;

        auto next_place = places.find_first_not_of(whitespace);
        while (next_place != std::string_view::npos)
        {
            auto end_of_next_place = places.find_first_of(whitespace, next_place);
            auto place_name = places.substr(next_place, end_of_next_place - next_place);

            auto place_iter = module.find_place(place_name);
            if (place_iter != module.get_places().end())
            {
                unit_iter->second.add_place(*place_iter);
                place_iter->second.nested = std::addressof(*unit_iter);
            }
            next_place = places.find_first_not_of(whitespace, end_of_next_place);
        }

        assert(unit_node.first_node("subunits"));
        auto subunits = std::string_view{
            unit_node.first_node("subunits")->value(),
            unit_node.first_node("subunits")->value_size()
        };

        auto next_subunit = subunits.find_first_not_of(whitespace);
        if (next_subunit == std::string_view::npos)
        {
            return;
        }
        else
        {
            unit_iter->second.set_inner_unit();
        }
        do
        {
            auto end_of_next_subunit = subunits.find_first_of(whitespace, next_subunit);
            auto subunit_name = subunits.substr(next_subunit, end_of_next_subunit - next_subunit);
            auto subunit_iter = module.find_nested_unit(subunit_name);
            if (subunit_iter == module.get_nested_units().end())
            {
                auto [added_iter, _] = module.add_nested_unit(std::string{subunit_name});
                subunit_iter = added_iter;
            }
            subunit_iter->second.parent = std::addressof(*unit_iter);
            next_subunit = subunits.find_first_not_of(whitespace, end_of_next_subunit);
        } while (next_subunit != std::string_view::npos);
    };

    if (std::string_view{
            structure_node->first_attribute("safe")->value(),
            structure_node->first_attribute("safe")->value_size()
        }
        == "true")
    {
        RT::args.safe_given = true;
        RT::args.encoder_arg = encoder_arg_bit;
        std::ranges::for_each(module.places, [](auto& place) { place.second.capacity = 1; });
        std::for_each(
            rapidxml::node_iterator<char>(structure_node), rapidxml::node_iterator<char>(),
            [&](const rapidxml::xml_node<>& node)
            {
                if (std::string_view{node.name(), node.name_size()} == "unit")
                {
                    handle_nested_unit(node);
                }
            }
        );
    }
}

void parse_pnml_symbols(Input input_file, Module& module)
{
    RT::log("reading pnml");
    auto file_size = std::filesystem::file_size(input_file.get_filename());

    std::string file_content(file_size, '\0');
    if (file_size != std::fread(file_content.data(), 1, file_size, input_file)
        && std::ferror(input_file))
    {
        RT::log<critical>(
            "Read error when accessing {} file {}",
            RT::markup(MARKUP::OUTPUT, input_file.get_kind()),
            RT::markup(MARKUP::FILE, input_file.get_filename())
        );
        RT::abort(ERROR::FILE);
    }

    LOLA_LOG(trace, "copied file {} to memory", input_file.get_filename());

    auto doc = rapidxml::xml_document<>{};
    doc.parse<rapidxml::parse_non_destructive>(file_content.data());

    LOLA_LOG(trace, "parsed PNML file {}", input_file.get_filename());

    if (!(doc.first_node() && doc.first_node()->first_node("net")))
    {
        RT::log<err>("Input pnml file '{}' does not have a 'net' tag", input_file.get_filename());
        RT::abort(ERROR::FILE);
    }
    auto* net_node = doc.first_node()->first_node("net");
    auto* type_attr = net_node->first_attribute("type");
    if (!type_attr)
    {
        RT::log<err>(
            "Input pnml file '{}' does not have a 'type' attribute for the net",
            input_file.get_filename()
        );
        RT::abort(ERROR::FILE);
    }
    auto net_type = std::string_view{type_attr->value(), type_attr->value_size()};

    if (net_type.find("ptnet") != std::string_view::npos)
    {
        LOLA_LOG(trace, "parsing ptnet from {}", input_file.get_filename());
        parse_pt_symbols(net_node, module);
        LOLA_LOG(trace, "finished parsing ptnet from {}", input_file.get_filename());
    }
    else if (net_type.find("symmetricnet") != std::string_view::npos)
    {
        // TODO
    }
    else
    {
        RT::log<err>("Input pnml file '{}' contains unknown net type", input_file.get_filename());
        RT::abort(ERROR::SYNTAX);
    }
}

auto parse_modular_symbols(Input input_file) -> std::unique_ptr<ModularStructure>
{
    auto ms = std::make_unique<ModularStructure>();
    if (RT::args.pnmlnet_given)
    {
        RT::log("input: PNML file ({})", RT::markup(MARKUP::PARAMETER, "--pnmlnet"));
        auto [module_iter, _1] = ms->add_module("m_0");  // short name for SSO
        parse_pnml_symbols(std::move(input_file), module_iter->second);
        auto [instance_iter, _2] = ms->add_instance("i_0", *module_iter, 0);
        instance_iter->second.initial_marking = module_iter->second.get_initial_markings().begin();
    }
    else
    {
        RT::log("input: mlola file ({})", RT::markup(MARKUP::PARAMETER, "--mlola"));
        parse_mlola_symbols(std::move(input_file), *ms);
    }
    return ms;
}

void parse_modular_symbols_benchmark(Input input_file)
{
    auto ms = std::make_unique<ModularStructure>();
    std::chrono::duration<double, std::micro> parsing_time;
    auto model = std::filesystem::path{input_file.get_filename()}.parent_path().stem().string();
    if (RT::args.pnmlnet_given)
    {
        RT::currentInputFile = &input_file;
        auto before = std::chrono::high_resolution_clock::now();
        ReadPnmlFile();
        auto after = std::chrono::high_resolution_clock::now();
        parsing_time = after - before;
    }
    else if (RT::args.mlola_given)
    {
        auto before = std::chrono::high_resolution_clock::now();
        auto [module_iter, _] = ms->add_module("m_0");  // short name for SSO
        parse_pnml_symbols(std::move(input_file), module_iter->second);
        auto after = std::chrono::high_resolution_clock::now();
        parsing_time = after - before;
    }
    fmt::print("{}:{}\n", model, parsing_time.count());
}

namespace
{
auto resolve_alias(std::string_view alias, const ModularStructure& ms) -> Alias
{
    const auto iter_alias = ms.get_aliases().find(alias);
    if (iter_alias == ms.get_aliases().cend()) [[unlikely]]
    {
        RT::log<warn>("Place name '{}' is not an alias", alias);
        auto contains_place = [alias](const auto instance)
        {
            const auto& [name, sym] = instance;
            return sym.get_module().second.get_places().contains(alias);
        };
        assert(
            (rg::count_if(ms.get_instances(), contains_place) <= 1)
            && "Place name occures in multiple instances!"
        );
        auto iter_instance = rg::find_if(ms.get_instances(), contains_place);
        if (iter_instance == ms.get_instances().end())
        {
            RT::log<err>("unknown place: {}", alias);
            RT::abort(ERROR::SYNTAX);
        }
        auto iter_place = iter_instance->second.get_module().second.get_places().find(alias);
        return {std::addressof(*iter_instance), std::addressof(*iter_place)};
    }
    return iter_alias->second;
}
}  // namespace

void parse_xml_term(
    rapidxml::xml_node<>* term, const ModularStructure& ms, FormalSum& current_ap,
    Term current_factor = Term{1}
)
{
    const auto node_name = std::string_view{term->name(), term->name_size()};
    if (node_name == "integer-constant")
    {
        Term copy = current_factor;
        copy.multiply(xml_to_numerical<int>(*term));
        current_ap.constant.add(copy);
        return;
    }
    if (node_name == "tokens-count")
    {
        for (const auto& place_node :
             rg::subrange{iter{term}, iter{}}
                 | vw::filter(
                     [](const auto& place)
                     { return std::string_view{place.name(), place.name_size()} == "place"; }
                 ))
        {
            const auto alias_name = std::string_view{place_node.value(), place_node.value_size()};
            const auto [instance, place] = resolve_alias(alias_name, ms);
            if (!place->second.in_empty_siphon)
            {
                current_ap.add_term(*instance, *place, current_factor);
            }
        }
        return;
    }
    if (node_name == "integer-sum")
    {
        rg::for_each(
            iter{term}, iter{}, [&ms, &current_ap, current_factor](auto& summand)
            { parse_xml_term(&summand, ms, current_ap, current_factor); }
        );
        return;
    }
    if (node_name == "integer-difference")
    {
        parse_xml_term(term->first_node(), ms, current_ap, current_factor);
        current_factor.multiply(-1);
        parse_xml_term(term->last_node(), ms, current_ap, current_factor);
        return;
    }
    RT::log<err>("unexpected XML node: {}; expected term", node_name);
    RT::abort(ERROR::SYNTAX);
}

namespace
{

enum parse_mode : bool
{
    OPTIMIZED = 0,
    UNOPTIMIZED = 1
};

auto parse_xml_term(rapidxml::xml_node<>* term, const ModularStructure& ms) -> kc::tTerm
{
    auto node_name = std::string_view{term->name(), term->name_size()};
    if (node_name == "integer-constant")
    {
        auto t = kc::IntConstant();
        t->st = std::make_shared<SimpleTerm>(
            std::string_view{}, std::string_view{}, xml_to_numerical<uint64_t>(*term)
        );
        return t;
    }
    if (node_name == "tokens-count")
    {
        kc::tTerm_list place_aggr = kc::NiltTerm_list();
        rg::for_each(
            rg::subrange(iter{term}, iter{})
                | vw::filter(
                    [](const rapidxml::xml_node<>& place)
                    { return std::string_view{place.name(), place.name_size()} == "place"; }
                ),
            [&place_aggr, &ms](const rapidxml::xml_node<>& place_node)
            {
                const auto alias_name = std::string_view{
                    place_node.value(), place_node.value_size()
                };
                const auto [instance, place] = resolve_alias(alias_name, ms);
                auto t = kc::Complex();
                t->st = std::make_shared<SimpleTerm>(instance->first, place->first, 1);
                place_aggr = kc::ConstTerm_list(t, place_aggr);
            }
        );
        return kc::TokenCount(place_aggr);
    }
    if (node_name == "integer-sum")
    {
        kc::tTerm_list aggr = kc::NiltTerm_list();
        rg::for_each(
            iter{term}, iter{}, [&aggr, &ms](rapidxml::xml_node<>& summand)
            { aggr = kc::ConstTerm_list(parse_xml_term(&summand, ms), aggr); }
        );
        return kc::IntSum(aggr);
    }
    if (node_name == "integer-difference")
    {
        auto t1 = parse_xml_term(term->first_node(), ms);
        auto t2 = parse_xml_term(term->last_node(), ms);
        return kc::IntDifference(t1, t2);
    }
    RT::log<err>("unexpected XML node: {}; expected term", node_name);
    RT::abort(ERROR::SYNTAX);
}

template <parse_mode mode>
auto parse_xml_formula(rapidxml::xml_node<>* formula, const ModularStructure& ms)
    -> kc::tStatePredicate
{
    auto node_name = std::string_view{formula->name(), formula->name_size()};
    if (node_name == "exists-path")
    {
        return kc::ExPath(parse_xml_formula<mode>(formula->first_node(), ms));
    }
    if (node_name == "all-paths")
    {
        return kc::AllPath(parse_xml_formula<mode>(formula->first_node(), ms));
    }
    if (node_name == "globally")
    {
        return kc::Always(parse_xml_formula<mode>(formula->first_node(), ms));
    }
    if (node_name == "finally")
    {
        return kc::Eventually(parse_xml_formula<mode>(formula->first_node(), ms));
    }
    if (node_name == "next")
    {
        return kc::NextState(parse_xml_formula<mode>(formula->first_node(), ms));
    }
    if (node_name == "until")
    {
        auto phi = parse_xml_formula<mode>(formula->first_node("before")->first_node(), ms);
        auto psi = parse_xml_formula<mode>(formula->first_node("reach")->first_node(), ms);
        return kc::Until(phi, psi);
    }
    if (node_name == "negation" || node_name == "not")
    {
        auto sub = parse_xml_formula<mode>(formula->first_node(), ms);
        if constexpr (mode == UNOPTIMIZED)
        {
            return kc::Negation(sub);
        }
        switch (sub->shape)
        {
        case AT_TEMP:
        {
            return kc::Negation(sub);
        }
        case AT_DL:
        case AT_FIR:
        case AT_COMP:
        {
            sub->formula->negate();
            return sub;
        }
        case AT_TRUE:
        {
            auto* tmp = sub->formula->negate();
            delete sub->formula;
            sub->formula = tmp;
            sub->shape = AT_FALSE;
            return sub;
        }
        case AT_FALSE:
        {
            auto* tmp = sub->formula->negate();
            delete sub->formula;
            sub->formula = tmp;
            sub->shape = AT_TRUE;
            return sub;
        }
        case AT_AND:
        {
            sub->formula->negate();
            sub->shape = AT_OR;
            return sub;
        }
        case AT_OR:
        {
            sub->formula->negate();
            sub->shape = AT_AND;
            return sub;
        }
        }
    }
    if (node_name == "conjunction" || node_name == "and")
    {
        if constexpr (mode == UNOPTIMIZED)
        {
            kc::tConjunction_list conj_aggr = kc::NiltConjunction_list();
            rg::for_each(
                iter{formula}, iter{},
                [&conj_aggr, &ms](rapidxml::xml_node<>& sub)
                {
                    conj_aggr = kc::ConstConjunction_list(
                        parse_xml_formula<mode>(&sub, ms), conj_aggr
                    );
                }
            );
            return kc::ConjunctionList(conj_aggr);
        }
        auto begin = iter{formula};
        if (begin == iter{})
        {
            auto e = kc::Elementary();
            e->pred = new TruePredicate(Petrinet::InitialNet);
            e->shape = AT_TRUE;
            auto aggr = AtomicProposition(e);
            aggr->formula = e->pred;
            aggr->shape = AT_TRUE;
            return aggr;
        }
        auto aggr = parse_xml_formula<mode>(&*begin, ms);
        ++begin;
        for (auto& sub : rg::subrange{begin, iter{}})
        {
            auto current = parse_xml_formula<mode>(&sub, ms);
            // FALSE cases
            if (current->shape == AT_FALSE)
            {
                auto e = kc::Elementary();
                e->pred = new FalsePredicate(Petrinet::InitialNet);
                e->shape = AT_FALSE;
                auto res = kc::AtomicProposition(e);
                res->formula = e->pred;
                res->shape = AT_FALSE;
                return res;
            }
            // TRUE CASES
            if (aggr->shape == AT_TRUE)
            {
                aggr = current;
            }
            else if (current->shape == AT_TRUE)
            {
            }
            // TEMPORAL CASES
            else if (aggr->shape == AT_TEMP || current->shape == AT_TEMP)
            {
                aggr = kc::Conjunction(aggr, current);
            }
            // COMBINATIONS AND / NOT AND
            else if (aggr->shape == AT_AND && current->shape != AT_AND)
            {
                static_cast<AtomicBooleanPredicate*>(aggr->formula)->addSub(current->formula);
            }
            else if (aggr->shape != AT_AND && current->shape == AT_AND)
            {
                static_cast<AtomicBooleanPredicate*>(current->formula)->addSub(aggr->formula);
                aggr = current;
            }
            else if (aggr->shape == AT_AND && current->shape == AT_AND)
            {
                static_cast<AtomicBooleanPredicate*>(aggr->formula)
                    ->merge(static_cast<AtomicBooleanPredicate*>(current->formula));
            }
            // NOT AND / NOT AND
            else
            {
                auto e = kc::Elementary();
                auto* tmp = new AtomicBooleanPredicate(Petrinet::InitialNet, true);
                tmp->addSub(aggr->formula);
                tmp->addSub(current->formula);
                e->pred = tmp;
                e->shape = AT_AND;
                aggr = kc::AtomicProposition(e);
                aggr->formula = e->pred;
                aggr->shape = AT_AND;
            }
        }
        if (aggr->shape == AT_TRUE)
        {
            auto e = kc::Elementary();
            e->pred = new TruePredicate(Petrinet::InitialNet);
            e->shape = AT_TRUE;
            aggr = AtomicProposition(e);
            aggr->formula = e->pred;
            aggr->shape = AT_TRUE;
        }
        return aggr;
    }
    if (node_name == "disjunction" || node_name == "or")
    {
        if constexpr (mode == UNOPTIMIZED)
        {
            kc::tDisjunction_list disj_aggr = kc::NiltDisjunction_list();
            rg::for_each(
                iter{formula}, iter{},
                [&disj_aggr, &ms](rapidxml::xml_node<>& sub)
                {
                    disj_aggr = kc::ConstDisjunction_list(
                        parse_xml_formula<mode>(&sub, ms), disj_aggr
                    );
                }
            );
            return kc::DisjunctionList(disj_aggr);
        }
        auto begin = iter{formula};
        if (begin == iter{})
        {
            auto e = kc::Elementary();
            e->pred = new FalsePredicate(Petrinet::InitialNet);
            e->shape = AT_FALSE;
            auto aggr = AtomicProposition(e);
            aggr->formula = e->pred;
            aggr->shape = AT_FALSE;
            return aggr;
        }
        auto aggr = parse_xml_formula<mode>(&*begin, ms);
        ++begin;
        for (auto& sub : rg::subrange{begin, iter{}})
        {
            auto current = parse_xml_formula<mode>(&sub, ms);
            // TRUE cases
            if (current->shape == AT_TRUE)
            {
                auto e = kc::Elementary();
                e->pred = new TruePredicate(Petrinet::InitialNet);
                e->shape = AT_TRUE;
                auto res = kc::AtomicProposition(e);
                res->formula = e->pred;
                res->shape = AT_TRUE;
                return res;
            }
            // FALSE CASES
            if (aggr->shape == AT_FALSE)
            {
                aggr = current;
            }
            else if (current->shape == AT_FALSE)
            {
            }
            // TEMPORAL CASES
            else if (aggr->shape == AT_TEMP || current->shape == AT_TEMP)
            {
                aggr = kc::Disjunction(aggr, current);
            }
            // COMBINATIONS OR / NOT OR
            else if (aggr->shape == AT_OR && current->shape != AT_OR)
            {
                static_cast<AtomicBooleanPredicate*>(aggr->formula)->addSub(current->formula);
            }
            else if (aggr->shape != AT_OR && current->shape == AT_OR)
            {
                static_cast<AtomicBooleanPredicate*>(current->formula)->addSub(aggr->formula);
                aggr = current;
            }
            else if (aggr->shape == AT_OR && current->shape == AT_OR)
            {
                static_cast<AtomicBooleanPredicate*>(aggr->formula)
                    ->merge(static_cast<AtomicBooleanPredicate*>(current->formula));
            }
            // NOT OR / NOT OR
            else
            {
                auto e = kc::Elementary();
                auto* tmp = new AtomicBooleanPredicate(Petrinet::InitialNet, false);
                tmp->addSub(aggr->formula);
                tmp->addSub(current->formula);
                e->pred = tmp;
                e->shape = AT_OR;
                aggr = kc::AtomicProposition(e);
                aggr->formula = e->pred;
                aggr->shape = AT_OR;
            }
        }
        if (aggr->shape == AT_FALSE)
        {
            auto e = kc::Elementary();
            e->pred = new FalsePredicate(Petrinet::InitialNet);
            e->shape = AT_FALSE;
            aggr = AtomicProposition(e);
            aggr->formula = e->pred;
            aggr->shape = AT_FALSE;
        }
        return aggr;
    }
    if (node_name == "is-fireable" || node_name == "deadlock")
    {
        RT::log<warn>("Fireable or deadlock not supported");
        return kc::tStatePredicate();
    }
    if (node_name == "integer-le")
    {
        if constexpr (mode == UNOPTIMIZED)
        {
            auto t1 = parse_xml_term(formula->first_node(), ms);
            auto t2 = parse_xml_term(formula->last_node(), ms);
            return kc::AtomicProposition(kc::IntegerLE(t1, t2));
        }
        auto ap = FormalSum{};
        parse_xml_term(formula->first_node(), ms, ap);
        parse_xml_term(formula->last_node(), ms, ap, Term{-1});
        auto e = Elementary();
        e->pred = new AtomicStatePredicate(ap);
        e->shape = AT_COMP;
        auto res = AtomicProposition(e);
        res->formula = e->pred;
        res->shape = AT_COMP;
        return res;
    }
    RT::log<err>("unexpected XML element: {}", node_name);
    RT::abort(ERROR::SYNTAX);
}

template <parse_mode mode>
auto parse_xml_property(const rapidxml::xml_node<>& property, const ModularStructure& ms)
    -> kc::tFormula
{
    auto property_name = std::string_view{
        property.first_node("id")->value(), property.first_node("id")->value_size()
    };
    auto* formula_node = property.first_node("formula");
    auto formula_type = std::string_view{
        formula_node->first_node()->name(), formula_node->first_node()->name_size()
    };
    if (formula_type == "place-bound")
    {
        RT::log<warn>("place-bound formula '{}' not supported", property_name);
        return kc::tFormula();
    }
    return kc::StatePredicateFormula(parse_xml_formula<mode>(formula_node->first_node(), ms));
}

}  // namespace

auto read_file(Input input_file) -> std::string
{
    RT::log("reading xml formula");
    auto file_size = std::filesystem::file_size(input_file.get_filename());

    std::string file_content(file_size, '\0');
    if (file_size != std::fread(file_content.data(), 1, file_size, input_file)
        && std::ferror(input_file))
    {
        RT::log<critical>(
            "Read error when accessing {} file {}",
            RT::markup(MARKUP::OUTPUT, input_file.get_kind()),
            RT::markup(MARKUP::FILE, input_file.get_filename())
        );
        RT::abort(ERROR::FILE);
    }

    LOLA_LOG(trace, "copied file {} to memory", input_file.get_filename());
    return file_content;
}

void parse_xml_formulas(Input input_file, const ModularStructure& ms)
{
    auto file_content = read_file(std::move(input_file));
    auto doc = rapidxml::xml_document<>{};
    doc.parse<rapidxml::parse_non_destructive>(file_content.data());

    LOLA_LOG(trace, "parsed XML file {}", input_file.get_filename());

    auto* propertyset_node = doc.first_node();

    for (const auto& property : rg::subrange(iter{propertyset_node}, iter{})
             | vw::filter([](const rapidxml::xml_node<>& node)
                          { return std::string_view{node.name(), node.name_size()} == "property"; }
             ))
    {
        auto* id_node = property.first_node("id");
        auto property_name = std::string_view{id_node->value(), id_node->value_size()};
        if (RT::args.selectformula_given)
        {
            if (property_name != RT::args.selectformula_arg)
            {
                continue;
            }
        }
        auto formula = parse_xml_property<OPTIMIZED>(property, ms);
        formula->id = new char[property_name.size() + 1]();
        rg::copy(property_name, formula->id);
        Task::CompoundPlanning(formula);
    }
}

auto parse_and_modularize_xml_formula(Input input_file, const ModularStructure& ms)
    -> std::vector<kc::tFormula>
{
    auto file_content = read_file(std::move(input_file));
    auto doc = rapidxml::xml_document<>{};
    doc.parse<rapidxml::parse_non_destructive>(file_content.data());

    LOLA_LOG(trace, "parsed XML file {}", input_file.get_filename());

    auto* propertyset_node = doc.first_node();
    auto formulas = rg::subrange(iter{propertyset_node}, iter{})
        | vw::filter([](const rapidxml::xml_node<>& node)
                     { return std::string_view{node.name(), node.name_size()} == "property"; })
        | vw::transform([&ms](const rapidxml::xml_node<>& property)
                        { return parse_xml_property<UNOPTIMIZED>(property, ms); }
        );  // could use std::ranges::to
            // at some point
    return std::vector(formulas.begin(), formulas.end());
}

}  // namespace parsing
