/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief lexic for modular petri nets
*/

%option yylineno
%option nounput
%option noyywrap
%option prefix="mlola_"
%option warn

%{
    #include <Core/Dimensions.h>
    #include <Core/Runtime.h>
    #include <Frontend/Parser/error.h>
    #include <Frontend/SymbolTable/ArcList.h>
    #include <InputOutput/InputOutput.h>

    #include <Frontend/ModularParser/ParserMlola.hh>

    #include <memory>
    #include <stack>
    #include <tuple>
    #include <utility>

    namespace
    {
    parsing::location* loc;
    std::stack<std::tuple<YY_BUFFER_STATE, Input, parsing::location>>* input_file_stack;
    Input* current_input_file;
    }

    // Signature of yylex for the scanner
    #define YY_DECL parsing::NetParser::symbol_type net_lex()

    /*!
    \brief This macro is executed prior to the matched rule's action.

     We use this macro to set set #mlola_lloc to the positions of #mlola_text. It further
    manages the current column number #mlola_colno. See Flex's manual
    for more information on the macro.
    */
    #define YY_USER_ACTION loc->columns(yyleng);

%}


%x IN_COMMENT
%x INCLUDE
%x PNML

%%
%{
    // step location for new token
    loc->step();
%}

 /* from http://flex.sourceforge.net/manual/How-can-I-match-C_002dstyle-comments_003f.html */
"/*"                  { BEGIN(IN_COMMENT); }
"//".*                { /* comments */}
<IN_COMMENT>"*/"      { BEGIN(INITIAL); loc->step(); }
<IN_COMMENT>[^*\n\r]+ { /* comments */ }
<IN_COMMENT>"*"       { /* comments */ }
<IN_COMMENT>[\n\r]+   { /* comments */ loc->lines(yyleng); }
"{"[^\n\r]*"}"        { /* comments */ }

<INCLUDE,PNML>[ \t]*  { /* eat the whitespace before filename */ loc->step(); }
<INCLUDE,PNML>[\n\r]+ { loc->lines(yyleng); loc->step(); }
<INCLUDE>PNML         { BEGIN(PNML); return parsing::NetParser::make_KEY_PNML(*loc); }
<PNML>[^ \t\n\r;]+    {
                        BEGIN(INITIAL);
                        std::filesystem::path new_file{mlola_text};
                        if(new_file.is_relative())
                        {
                            auto parent_path = std::filesystem::path{current_input_file->get_filename()}.parent_path();
                            new_file = (parent_path / new_file).lexically_normal();
                        }
                        return parsing::NetParser::make_IDENTIFIER(new_file.string(), *loc);
                      }
<INCLUDE>[^ \t\n\r;]+ { /* file name parsed */
                        // determine path to include file
                        // absolute -> easy
                        // relative -> take relative to current file
                        std::filesystem::path new_file{mlola_text};
                        std::string new_file_path;
                        if(new_file.is_absolute())
                        {
                            new_file_path = new_file.string();
                        }
                        else
                        {
                            auto parent_path = std::filesystem::path{current_input_file->get_filename()}.parent_path();
                            new_file_path = (parent_path / new_file).lexically_normal().string();
                        }
                        // save current location value to pass to parser
                        parsing::location curr_loc = *loc;
                        // push current state to stack
                        input_file_stack->emplace(YY_CURRENT_BUFFER, std::move(*current_input_file), curr_loc);
                        // create new input
                        *current_input_file = Input{"module", new_file_path};
                        // update yyin
                        mlola_in = *current_input_file;
                        // change lexer to new buffer
                        mlola__switch_to_buffer(mlola__create_buffer(mlola_in, YY_BUF_SIZE));

                        BEGIN(INITIAL);

                        // update location for next token
                        *loc = parsing::location{current_input_file->get_filename_string()};

                        // return filename
                        return parsing::NetParser::make_IDENTIFIER(mlola_text, curr_loc);
                      }

MODULE                { return parsing::NetParser::make_KEY_MODULE(*loc); }
NET                   { return parsing::NetParser::make_KEY_NET(*loc); }
INCLUDE               { BEGIN(INCLUDE); return parsing::NetParser::make_KEY_INCLUDE(*loc); }
INSTANCES             { return parsing::NetParser::make_KEY_INSTANCES(*loc); }
FUSIONSET             { return parsing::NetParser::make_KEY_FUSIONSET(*loc); }
FUSION\ SET           { return parsing::NetParser::make_KEY_FUSIONSET(*loc); }
ALIASES               { return parsing::NetParser::make_KEY_ALIASES(*loc); }
\-\>                  { return parsing::NetParser::make_KEY_MAPS(*loc); }

CONSUME               { return parsing::NetParser::make_KEY_CONSUME(*loc); }
FAIR                  { return parsing::NetParser::make_KEY_FAIR(*loc); }
MARKING               { return parsing::NetParser::make_KEY_MARKING(*loc); }
PLACE                 { return parsing::NetParser::make_KEY_PLACE(*loc); }
PRODUCE               { return parsing::NetParser::make_KEY_PRODUCE(*loc); }
SAFE                  { return parsing::NetParser::make_KEY_SAFE(*loc); }
STRONG                { return parsing::NetParser::make_KEY_STRONG(*loc); }
TRANSITION            { return parsing::NetParser::make_KEY_TRANSITION(*loc); }
WEAK                  { return parsing::NetParser::make_KEY_WEAK(*loc); }

\:\:                  { return parsing::NetParser::make_SCOPE_OPERATOR(*loc);}
\:                    { return parsing::NetParser::make_COLON(*loc); }
,                     { return parsing::NetParser::make_COMMA(*loc); }
\;                    { return parsing::NetParser::make_SEMICOLON(*loc); }

[\n\r]+               { /* whitespace */ loc->lines(yyleng); loc->step(); }
[\t ]+                { /* whitespace */ loc->step(); }


[0-9]+                { return parsing::NetParser::make_NNNUMBER(mlola_text, *loc); }
[[:alpha:]_][[:alnum:]_]*  { return parsing::NetParser::make_IDENTIFIER(mlola_text, *loc); }

<<EOF>>               { return parsing::NetParser::make_END(*loc); }

.                     { parsing::yyerrors(mlola_text, *loc, "lexical error"); }

%%

/* main function of the parser */
namespace parsing
{
    void parse_mlola_symbols(Input input_file, ModularStructure& ms)
    {
        // setup globals with stack lifetime
        current_input_file = &input_file;
        location input_location{current_input_file->get_filename_string()};
        loc = &input_location;
        mlola_in = input_file;
        std::stack<std::tuple<YY_BUFFER_STATE, Input, parsing::location>> input_stack;
        input_file_stack = &input_stack;
        // begin parsing
        parsing::NetParser parser(ms);
        parser.parse();
    }

    void NetParser::error (const parsing::location& loc, const std::string& message)
    {
        yyerrors(mlola_text, loc, "{}", message);
    }

    void cleanup_include()
    {
        // delete current buffer
        mlola__delete_buffer(YY_CURRENT_BUFFER);
        // move previous state from stack
        auto [old_buffer, old_file, old_loc] = std::move(input_file_stack->top());
        input_file_stack->pop();
        // restore previous file
        std::swap(*current_input_file, old_file);
        // restore previous buffer
        mlola__switch_to_buffer(old_buffer);
        // restore previous location
        *loc = std::move(old_loc);
        loc->step();
    }
}
