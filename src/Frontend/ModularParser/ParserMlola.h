/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief header mlola Parser
\author Judith Overath
\status wip 21.03.2023

Allows including the parser function for mlola files, defined in ParserMlola.yy
*/
#pragma once

#include <vector>
#include <map>
#include <Core/Dimensions.h>
#include <Frontend/ModularParser/ParserMNet.h>
#include <Frontend/ModularSymbols/InstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleInstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleSymbol.h>
#include <Frontend/SymbolTable/PlaceSymbol.h>
#include <Frontend/SymbolTable/TransitionSymbol.h>

// ll temporary data only needed during parsing is stored here
struct ParserHelper
{
    ParserMNet* parsedNet;
    ModuleSymbol* currentModule;
    capacity_t currentCapacity;
    std::vector<InstanceSymbol*> currentInstances;
    std::vector<std::pair<ModuleInstanceSymbol*, TransitionSymbol*>> currentFusionset;
    std::map<PlaceSymbol*, capacity_t>* instanceMarking;
};

// main function of the parser
ParserMNet* mlola_parser();
