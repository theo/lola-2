/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/**
 * @file Parser.h
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations of parser functions
 * @version 0.1
 * @date 2024-05-13
 *
 * @copyright Copyright (c) 2024
 *
 */

#ifndef SRC_FRONTEND_MODULARPARSER_PARSER_H
#define SRC_FRONTEND_MODULARPARSER_PARSER_H

#include "Frontend/ModularParser/ParserMNet.h"
#include "Frontend/Parser/ast-system-k.h"
#include "InputOutput/InputOutput.h"

#include <memory>
#include <vector>

namespace parsing
{
/**
 * @brief Function to parse a modular structure from a file. Owns the file and closes it at the end.
 * Condtionally calls the bison parser or the pnml parser
 *
 * @param input_file The file to be read
 * @return std::unique_ptr<ModularStructure>: Parsed symbol tables
 */
auto parse_modular_symbols(Input input_file) -> std::unique_ptr<ModularStructure>;
void parse_modular_symbols_benchmark(Input input_file);
void parse_formula(Input input_file, const ModularStructure& ms);
void parse_xml_formulas(Input input_file, const ModularStructure& ms);
auto parse_and_modularize_xml_formula(Input input_file, const ModularStructure& ms)
    -> std::vector<kc::tFormula>;

}  // namespace parsing

#endif /* SRC_FRONTEND_MODULARPARSER_PARSER_H */
