/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief mlola syntax
\author Judith Overath
\author Lukas Zech
\status wip 02.05.2024


Parses a modular petri net in mlola syntax into a ParserMNet.
*/

/* C++ parser interface */
%skeleton "lalr1.cc"
/* require bison version */
%require "3.3.2"
/* standard yylex is not typesafe, this creates make_TOKEN functions for the lexer */
%define api.token.constructor
/* use c++ variant types, to use c++ types as semantic values */
%define api.value.type variant
%define api.namespace { parsing }
%define api.parser.class { NetParser }
%locations
%define parse.error verbose
%define api.prefix {net_}

// container to store temporary data while parsing, defined in ParserMNet.h
%parse-param { parsing::ModularStructure& parsed_structure };

%code requires {
    #include <Core/Dimensions.h>
    #include <Frontend/ModularParser/ParserMNet.h>
    #include <Frontend/SymbolTable/ArcList.h>
    #include <InputOutput/InputOutput.h>

    #include <forward_list>
    #include <string>
    #include <string_view>
    #include <utility>
    #include <vector>

    namespace parsing
    {
        void cleanup_include();
    }
}

%code {
    #include <Core/Runtime.h>
    #include <Frontend/ModularSymbols/FusionSetSymbol.h>
    #include <Frontend/ModularSymbols/InstanceSymbol.h>
    #include <Frontend/ModularSymbols/ModTransitionSymbol.h>
    #include <Frontend/ModularSymbols/ModuleSymbol.h>
    #include <Frontend/ModularSymbols/ModuleInstanceSymbol.h>
    #include <Frontend/Parser/error.h>
    #include <Frontend/SymbolTable/SymbolTable.h>
    #include <Frontend/SymbolTable/PlaceSymbol.h>
    #include <InputOutput/InputOutput.h>

    #include <algorithm>
    #include <cassert>

    // declare our yylex for the parser
    parsing::NetParser::symbol_type net_lex();

    namespace parsing
    {
        void parse_pnml_symbols(Input input_file, Module& module);
    }

    namespace
    {
        // helper while parsing
        parsing::SymbolTable<parsing::Module>::iterator current_module_symbol;
        parsing::SymbolTable<parsing::FusionVector>::iterator current_fusionvector_symbol;
        std::vector<parsing::SymbolTable<parsing::Instance>::iterator> current_instances;
        arrayindex_t current_place_index = 0;
        capacity_t current_capacity = 0;
        arrayindex_t current_index_offset = 0;
        parsing::SymbolTable<parsing::Instance>::node_type dummy_instance{};
    }
}

%token KEY_MODULE     "keyword MODULE"
%token KEY_NET        "keyword NET"
%token KEY_INCLUDE    "keyword INCLUDE"
%token KEY_PNML       "keyword PNML"
%token KEY_INSTANCES  "keyword INSTANCES"
%token KEY_FUSIONSET  "keyword FUSIONSET"
%token KEY_CONSUME    "keyword CONSUME"
%token KEY_FAIR       "keyword FAIR"
%token KEY_PLACE      "keyword PLACE"
%token KEY_MARKING    "keyword MARKING"
%token KEY_PRODUCE    "keyword PRODUCE"
%token KEY_SAFE       "keyword SAFE"
%token KEY_STRONG     "keyword STRONG"
%token KEY_TRANSITION "keyword TRANSITION"
%token KEY_WEAK       "keyword WEAK"
%token KEY_ALIASES      "keyword ALIAS"
%token KEY_MAPS       "keyword ->"
%token SCOPE_OPERATOR "::"
%token COLON          "colon"
%token COMMA          "comma"
%token SEMICOLON      "semicolon"
%token END 0          "end of file"
/* typed terminals */
%token <std::string> IDENTIFIER     "identifier"
%token <std::string> NNNUMBER       "number"
/* typed non-terminals */
%type <std::string> name
%type <std::forward_list<parsing::Arc>> arclist
%type <fairnessAssumption_t> fairness
%type <parsing::SymbolTable<Instance>::iterator> instancename
%type <parsing::Module::Marking::map_type> markinglist
%type <std::pair<std::string_view, capacity_t>> marking
%type <std::forward_list<parsing::Module::Marking>::const_iterator> instancemarking
%type <std::size_t> opt_number

%%

mlola_net:
  module // at least one module is required
  modules_or_instances
  aliases
  fusionvectors
;




/* .MLOLA module + instance specifications */

modules_or_instances: // module & instances in any order
  %empty
| modules_or_instances module
| modules_or_instances instances
;

instances: // specification of multiple instances of one module sharing the same marking
  KEY_INSTANCES name
    {
        // check if module exists and remember it
        current_module_symbol = parsed_structure.find_module($2);
        auto& [module_name, _] = *current_module_symbol;
        if(current_module_symbol == parsed_structure.modules.end()) [[unlikely]]
        {
            yyerrors(module_name, @2, "module '{}' does not exist", module_name);
        }
        // erase preemptively created instance
        dummy_instance = parsed_structure.instances.extract(module_name);
        assert(current_instances.empty());
    }
  instancelist instancemarking SEMICOLON
    {
        // add the current parsed marking to every instance parsed from the instancelist
        std::ranges::for_each(current_instances, [=,this](auto iter){ iter->second.initial_marking = $5; });
        current_instances.clear();
    }
;

instancemarking:
  %empty
    {
        $$ = current_module_symbol->second.get_initial_markings().begin();
    }
| KEY_MARKING markinglist
    {
        $$ = current_module_symbol->second.add_initial_marking(std::move($2));
    }
;

instancelist: // a list of names for instances with at least one item
  instancelist COMMA instancename
    {
        current_instances.push_back($3);
    }
| instancename
    {
        current_instances.push_back($1);
    }
;

instancename:
  name
    {
        // check if the name is already used for a module
        const auto& modules = parsed_structure.get_modules();
        if(modules.contains($1)) [[unlikely]]
        {
            yyerrors($1, @1, "instance name '{}' already used for a module", $1);
        }

        if(!dummy_instance.empty())
        {
            dummy_instance.key() = std::move($1);
            auto res = parsed_structure.instances.insert(std::move(dummy_instance));
            if(!res.inserted) [[unlikely]]
            {
                yyerrors(res.position->first, @1, "instance name '{}' used twice", res.position->first);
            }
            $$ = res.position;
        }
        else
        {
            // create Instancesymbol for that name if it isnt already used for an instance
            const auto [iter, not_existing_yet] = parsed_structure.add_instance(std::move($1), *current_module_symbol, current_index_offset);
            current_index_offset += current_module_symbol->second.get_places().size();
            const auto& [instance_name, _] = *iter;
            if (!not_existing_yet) [[unlikely]]
            {
                yyerrors(instance_name, @1, "instance name '{}' used twice", instance_name);
            }

            $$ = iter;
        }
    }
;

markinglist:
  %empty
    {
        $$ = parsing::Module::Marking::map_type(1);
    }
| marking
    {
        // try to insert
        // ensure that $1 is lvalue reference, since we could need it after the insert -> no move
        auto& new_entry = $1;
        auto [iter, inserted] = $$.insert(new_entry);
        if(!inserted)
        {
            // exists already? add marking like in module definition
            iter->second += $1.second;
        }
    }
| markinglist COMMA marking
    {
        auto& new_entry = $3;
        auto [iter, inserted] = $1.insert(new_entry);
        if(!inserted)
        {
            iter->second += $3.second;
        }
        $$ = std::move($1);
    }
;

marking: // marking of one place for an instance
  name opt_number
    {
        // check if the place exists in the module of the instance
        auto& [module_name, module] = *current_module_symbol;
        auto iter = module.get_places().find($1);
        if (iter == module.get_places().end()) [[unlikely]]
        {
            yyerrors($1, @1, "place '{}' does not exist in module '{}'", $1, module_name);
        }

        // build marking pair and add it to the marking list used in the action for instances
        $$ = std::pair<std::string_view, capacity_t>(iter->first, $2);
    }
;

opt_number:
  %empty
    {
        $$ = 1;
    }
| COLON NNNUMBER
    {
        $$ = std::stoull($2);
    }
;

module:
  KEY_MODULE name
    {
        const auto& instances = parsed_structure.get_instances();
        if(instances.find($2) != instances.end()) [[unlikely]]
        {
            yyerrors($2, @2, "module name '{}' already used for an instance", $2);
        }

        auto [iter, not_existing_yet] = parsed_structure.add_module(std::move($2));
        if(!not_existing_yet) [[unlikely]]
        {
            yyerrors(std::get<const std::string>(*iter), @2, "module name '{}' is used twice", std::get<const std::string>(*iter));
        }
        current_module_symbol = iter;
        assert(current_place_index == 0);
    }
  KEY_NET netspec
    {
        current_place_index = 0;
        // preemptively add module as an instance incase no instances will be declared, will be deleted with the
        // first instance declaration
        std::string instance_name = current_module_symbol->first;
        auto [instance_iter, _] = parsed_structure.add_instance(std::move(instance_name), *current_module_symbol, current_index_offset);
        current_index_offset += current_module_symbol->second.get_places().size();
        instance_iter->second.initial_marking = current_module_symbol->second.get_initial_markings().begin();
    }
;

netspec: // module net can be directly specified or in a file
  KEY_INCLUDE name net END { cleanup_include(); } SEMICOLON // file will be switched, see LexerMlola.ll
| KEY_INCLUDE KEY_PNML name SEMICOLON
    {
        parse_pnml_symbols({"module", $3}, current_module_symbol->second);
    }
| net
;

/* .LOLA net specification; mostly copied from Frontend/Parser/ParserNet.yy */

net:
  KEY_PLACE placelists SEMICOLON    // declare places
  KEY_MARKING markinglist SEMICOLON // specify initial marking
    {
        current_module_symbol->second.add_default_marking(std::move($5));
    }
  transitionlist                    // define transitions & arcs
;

placelists:
  capacity placelist    // several places may share unqiue capacity
| placelists SEMICOLON capacity placelist
;

capacity:
  %empty
    {
        // empty capacity = unlimited capacity
        current_capacity = static_cast<capacity_t>(MAX_CAPACITY);
    }
| KEY_SAFE COLON
    {
        // SAFE without number = 1-SAFE
        current_capacity = 1;
    }
| KEY_SAFE NNNUMBER COLON
    {
        // at most k tokens expected on these places
        current_capacity = std::stoul($2);
    }
;

placelist:
  placelist COMMA name
    {
        auto& [module_name, module] = *current_module_symbol;
        auto [iter, not_existing_yet] = module.add_place(std::move($3), current_place_index, current_capacity);
        ++current_place_index;
        if(!not_existing_yet) [[unlikely]]
        {
            yyerrors(iter->first, @3, "place name '{}' used twice in module '{}'", iter->first, module_name);
        }
    }
| name
    {
        auto& [module_name, module] = *current_module_symbol;
        auto [iter, not_existing_yet] = module.add_place(std::move($1), current_place_index, current_capacity);
        ++current_place_index;
        if(!not_existing_yet) [[unlikely]]
        {
            yyerrors(iter->first, @1, "place name '{}' used twice in module '{}'", iter->first, module_name);
        }
    }
;

transitionlist:
  transition
| transitionlist transition
;

transition:
  KEY_TRANSITION name fairness
  KEY_CONSUME arclist SEMICOLON
  KEY_PRODUCE arclist SEMICOLON
    {
        auto& [module_name, module] = *current_module_symbol;
        auto [iter, not_existing_yet] = module.add_transition(std::move($2), $3, std::move($5), std::move($8));
        if(!not_existing_yet) [[unlikely]]
        {
            yyerrors(iter->first, @2, "transition name '{}' used twice in module '{}'", iter->first, module_name);
        }
    }
;

fairness:
  %empty
    {
        // no fairness given
        $$ = NO_FAIRNESS;
    }
| KEY_WEAK KEY_FAIR
    {
        $$ = WEAK_FAIRNESS;
    }
| KEY_STRONG KEY_FAIR
    {
        $$ = STRONG_FAIRNESS;
    }
;

arclist:
  %empty
    {
        $$ = std::forward_list<parsing::Arc>{};
    }
| name opt_number // single arc
    {
        auto& [module_name, module] = *current_module_symbol;
        auto iter = module.find_place($1);
        if(iter == module.places.end()) [[unlikely]]
        {
            yyerrors($1, @1, "place '{}' does not exist in module '{}'", $1, module_name);
        }
        $$.emplace_front(*iter, static_cast<mult_t>($2));
    }
| arclist COMMA name opt_number
    {
        // check for duplicate
        auto arc_iter = std::ranges::find_if($1, [=,this](const auto a){ return a.get_place().first == $3; });
        if (arc_iter != $1.end())
        {
            //duplicate detected
            arc_iter->add_multiplicity(static_cast<mult_t>($4));
        }
        else
        {
            auto& [module_name, module] = *current_module_symbol;
            auto iter = module.find_place($3);
            if(iter == module.places.end()) [[unlikely]]
            {
                yyerrors($3, @3, "place '{}' does not exist in module '{}'", $3, module_name);
            }
            $1.emplace_front(*iter, static_cast<mult_t>($4));
        }
        $$ = std::move($1);
    }
;



/* .MLOLA fusion set definitions */

fusionvectors:
  %empty
| fusionvectors fusionvector
;

fusionvector:
  KEY_FUSIONSET name
    {
        auto [iter, not_existing_yet] = parsed_structure.add_fusionvector(std::move($2));
        if(!not_existing_yet) [[unlikely]]
        {
            yyerrors(iter->first, @2, "fusion set name '{}' used twice", iter->first);
        }
        current_fusionvector_symbol = iter;
    }
    fusionlist SEMICOLON
;

fusionlist:
  fusiontransition
| fusionlist COMMA fusiontransition
;

fusiontransition: // a transition of a fusion set, which belongs to a module or an instance
  name SCOPE_OPERATOR name // name of the instance/module :: name of the transition
    {
        auto iter_instance = parsed_structure.find_instance($1);
        if(iter_instance == parsed_structure.instances.end()) [[unlikely]]
        {
            auto iter_module = parsed_structure.find_module($1);
            if(iter_module == parsed_structure.modules.end()) [[unlikely]]
            {
                yyerrors($1, @1, "no module or instance with name '{}' defined", $1);
            }
            else
            {
                yyerrors($1, @1, "module {} with non-empty set of instances used for fusion vector definition, use an instance instead", $1);
            }
        }
        auto& [instance_name, instance] = *iter_instance;
        auto& [module_name, module] = *instance.module;
        auto iter_transition = module.find_transition($3);
        if(iter_transition == module.transitions.end()) [[unlikely]]
        {
            yyerrors($3, @3, "no transition named '{}' defined in module '{}' for instance '{}'", $3, module_name, $1);
        }
        current_fusionvector_symbol->second.add_transition(instance_name, *iter_transition);
    }
;

/* general definitions */

name: // any name may be an ident or number
  IDENTIFIER
    {
        $$ = std::move($1);
    }
| NNNUMBER
    {
        $$ = std::move($1); // result is string version of number
    }
;

aliases:
  %empty
| KEY_ALIASES alias_list SEMICOLON
;

alias_list:
  alias
| alias_list COMMA alias
;

alias:
  name KEY_MAPS name SCOPE_OPERATOR name
    {
        auto iter_instance = parsed_structure.find_instance($3);
        if(iter_instance == parsed_structure.instances.end()) [[unlikely]]
        {
            auto iter_module = parsed_structure.find_module($3);
            if(iter_module == parsed_structure.modules.end()) [[unlikely]]
            {
                yyerrors($3, @3, "no module or instance with name '{}' defined", $3);
            }
            else
            {
                yyerrors($3, @3, "module {} with non-empty set of instances used for alias definition, use an instance instead", $3);
            }
        }
        auto& [instance_name, instance] = *iter_instance;
        auto& [module_name, module] = *instance.module;
        auto iter_place = module.find_place($5);
        if(iter_place == module.places.end()) [[unlikely]]
        {
            yyerrors($5, @5, "no place named '{}' defined in module '{}' for instance '{}'", $3, module_name, $1);
        }
        auto [iter_alias, not_existing_yet] = parsed_structure.add_alias(std::move($1), *iter_instance, *iter_place);
        if (!not_existing_yet)
        {
            yyerrors(iter_alias->first, @1, "Alias '{}' defined twice!", iter_alias->first);
        }
    }
;

%%
