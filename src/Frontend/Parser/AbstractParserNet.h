/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief abstract class AbstractParserNet, used for ParserMNet and ParserPTNet

code that is needed for both classes
*/

#pragma once

#include <Frontend/SymbolTable/SymbolTable.h>
#include <Net/Petrinet.h>

// code needed for Parser/ParserPTNet and ModularParser/ParserMNet
class AbstractParserNet
{
public:
    /// a symbol table for places
    SymbolTable* PlaceTable;
    /// a symbol table for transitions
    SymbolTable* TransitionTable;

    Petrinet* symboltable2netwomarking(Petrinet* pppnnn = NULL);

    template <typename T>
    static void sortarcs(arrayindex_t* indexarray, T* multarray, arrayindex_t card)
    {
        arrayindex_t pivot = indexarray[0];
        arrayindex_t blue = 0;
        arrayindex_t white = 1;
        arrayindex_t red = card;

        while (red > white)
        {
            if (indexarray[white] < pivot)
            {
                arrayindex_t tmp = indexarray[white];
                T tmpmult = multarray[white];
                indexarray[white] = indexarray[blue];
                multarray[white++] = multarray[blue];
                indexarray[blue] = tmp;
                multarray[blue++] = tmpmult;
            }
            else
            {
                // indexarray[white > pivot] (assume absence of duplicates in indexarray)
                arrayindex_t tmp = indexarray[white];
                T tmpmult = multarray[white];
                indexarray[white] = indexarray[--red];
                multarray[white] = multarray[red];
                indexarray[red] = tmp;
                multarray[red] = tmpmult;
            }
        }
        if (blue > 1)
        {
            sortarcs<T>(indexarray, multarray, blue);
        }
        if (card - red > 1)
        {
            sortarcs<T>(indexarray + red, multarray + red, card - red);
        }
    }
};
