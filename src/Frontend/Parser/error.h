/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\status new

\ingroup g_frontend

\brief Definition of the frontend error reporting function.
*/

#pragma once

#include <Core/Runtime.h>

#include <Frontend/ModularParser/location.hh>

#include <filesystem>
#include <string_view>
#include <type_traits>

namespace parsing
{
template <typename... Args>
[[noreturn]] void yyerrors(
    const std::string_view token, parsing::location loc, fmt::format_string<Args...> format_str,
    Args&&... fargs
)
{
    RT::log<err>(format_str, std::forward<Args>(fargs)...);

    // only print filename and excerpt if we read from a file
    if (*loc.begin.filename != "-")
    {
        std::string base_name = std::filesystem::path(*loc.begin.filename).filename();
        RT::log<err>(
            "{}:{}:{} - error near '{}'", RT::markup(MARKUP::FILE, base_name), loc.begin.line,
            loc.begin.column, token
        );

        Input::printExcerpt(
            *loc.begin.filename, loc.begin.line, loc.begin.column, loc.end.line, loc.end.column
        );
    }
    else
    {
        RT::log<err>("{}:{} - error near '{}'", loc.begin.line, loc.begin.column, token);
    }

    RT::abort(ERROR::SYNTAX);
}
}  // namespace parsing

/// error function for lexers and parsers
template <typename L, typename... Args>
[[noreturn]] void yyerrors(
    const std::string_view token, L loc, fmt::format_string<Args...> format_str, Args&&... fargs
)
{
    RT::log<err>(format_str, std::forward<Args>(fargs)...);

    // only print filename and excerpt if we read from a file/stdin
    if (RT::currentInputFile)
    {
        RT::log<err>(
            "{}:{}:{} - error near '{}'",
            RT::markup(
                MARKUP::FILE,
                basename(const_cast<char*>(RT::currentInputFile->get_filename().data()))
            ),
            loc.first_line, loc.first_column, token
        );

        Input::printExcerpt(
            RT::currentInputFile->get_filename(), loc.first_line, loc.first_column, loc.last_line,
            loc.last_column
        );
    }
    else
    {
        RT::log<err>("{}:{} - error near '{}'", loc.first_line, loc.first_column, token);
    }

    RT::abort(ERROR::SYNTAX);
}
