/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief lexic for formulas
\author Karsten
\status approved 25.01.2012
\ingroup g_frontend

\todo Herausfinden, ob es Probleme bei zu langen Kommentaren/Bezeichnern gibt.
Idee: Maximallänge angeben.
\todo Präfix hinzufügen?
*/

/* yylineno: we want line numbering
   nounput: we don't need yyunput()
   noyywrap: we don't support multiple formula files */
%option yylineno
%option nounput
%option noyywrap
%option prefix="ptformula_"
%option warn

%{
#include <Frontend/Parser/ast-system-k.h>       // for kc namespace
#include <Frontend/Parser/ast-system-yystype.h> // for YYSTYPE
#include <Frontend/Parser/ParserFormula.hh>
#include <charconv>
#include "Frontend/Parser/error.h"

namespace
{
parsing::location* loc;
}
#define YY_DECL parsing::FormulaParser::symbol_type ptformula_lex()
#define YY_USER_ACTION loc->columns(yyleng);

%}

%s IN_COMMENT

%%
%{
    loc->step();
%}

 /* from http://flex.sourceforge.net/manual/How-can-I-match-C_002dstyle-comments_003f.html */
"/*"                   { BEGIN(IN_COMMENT); }
<IN_COMMENT>"*/"       { BEGIN(INITIAL); loc->step(); }
<IN_COMMENT>[^*\n\r]+  { /* comments */ }
<IN_COMMENT>"*"        { /* comments */ }
<IN_COMMENT>[\n\r]     { /* comments */ loc->lines(yyleng); }
"{"[^\n\r]*"}"         { /* comments */ }

FIREABLE               { return parsing::FormulaParser::make_FIREABLE(*loc); }
INITIAL                { return parsing::FormulaParser::make_INITIAL(*loc); }
DEADLOCK               { return parsing::FormulaParser::make_DEADLOCK(*loc); }
MAX	                   { return parsing::FormulaParser::make_MAX(*loc); }

FORMULA                { return parsing::FormulaParser::make_FORMULA(*loc); }
AND                    { return parsing::FormulaParser::make_AND(*loc); }
NOT                    { return parsing::FormulaParser::make_NOT(*loc); }
OR                     { return parsing::FormulaParser::make_OR(*loc); }
XOR                    { return parsing::FormulaParser::make_XOR(*loc); }
TRUE                   { return parsing::FormulaParser::make_TRUE(*loc); }
FALSE                  { return parsing::FormulaParser::make_FALSE(*loc); }

ALLPATH                { return parsing::FormulaParser::make_ALLPATH(*loc); }
EXPATH                 { return parsing::FormulaParser::make_EXPATH(*loc); }

ALWAYS                 { return parsing::FormulaParser::make_ALWAYS(*loc); }
EVENTUALLY             { return parsing::FormulaParser::make_EVENTUALLY(*loc); }
UNTIL                  { return parsing::FormulaParser::make_UNTIL(*loc); }
NEXTSTATE              { return parsing::FormulaParser::make_NEXTSTATE(*loc); }
RELEASE                { return parsing::FormulaParser::make_RELEASE(*loc); }

REACHABLE              { return parsing::FormulaParser::make_REACHABLE(*loc); }
INVARIANT              { return parsing::FormulaParser::make_INVARIANT(*loc); }
IMPOSSIBLE             { return parsing::FormulaParser::make_IMPOSSIBLE(*loc); }

[AGEFXUR]+             { return parsing::FormulaParser::make_CTLOPERATOR(kc::mkcasestring(ptformula_text), *loc); }

\;                     { return parsing::FormulaParser::make_semicolon(*loc); }
\:                     { return parsing::FormulaParser::make_colon(*loc); }
\:\:                   { return parsing::FormulaParser::make_DCOLON(*loc); }
\<\-\>                 { return parsing::FormulaParser::make_iff(*loc); }
!=                     { return parsing::FormulaParser::make_notequal(*loc); }
\<\>                   { return parsing::FormulaParser::make_notequal(*loc); }
\-\>                   { return parsing::FormulaParser::make_implies(*loc); }
=                      { return parsing::FormulaParser::make_equals(*loc); }
\+                     { return parsing::FormulaParser::make_plus(*loc); }
\-                     { return parsing::FormulaParser::make_minus(*loc); }
\*                     { return parsing::FormulaParser::make_times(*loc); }
\(                     { return parsing::FormulaParser::make_leftparenthesis(*loc); }
\)                     { return parsing::FormulaParser::make_rightparenthesis(*loc); }
[>]                    { return parsing::FormulaParser::make_greaterthan(*loc); }
[<]                    { return parsing::FormulaParser::make_lessthan(*loc); }
[#]                    { return parsing::FormulaParser::make_notequal(*loc); }
[>]=                   { return parsing::FormulaParser::make_greaterorequal(*loc); }
[<]=                   { return parsing::FormulaParser::make_lessorequal(*loc); }

oo                     { return parsing::FormulaParser::make_omega(*loc); }

[\n\r]+                 {  /* whitespace */ loc->lines(yyleng); loc->step(); }
[\t ]+                  {  /* whitespace */ loc->step(); }

"-"?[0-9]+             { int val; auto [_, error_code] = std::from_chars(ptformula_text, ptformula_text + yyleng, val);
                         if (error_code == std::errc::result_out_of_range) [[unlikely]]
                         {
                             parsing::yyerrors(ptformula_text, *loc, "input integer in formula too large");
                         }
                         return parsing::FormulaParser::make_NNNUMBER(kc::mkinteger(val), *loc);
                       }
[^,;:()\t \n\r\{\}]+   { return parsing::FormulaParser::make_IDENTIFIER(kc::mkcasestring(ptformula_text), *loc); }

<<EOF>>                { return parsing::FormulaParser::make_END(*loc); }

.                      { parsing::yyerrors(ptformula_text, *loc, "lexical error"); }

%%

namespace parsing
{
    void parse_formula(Input input_file, const ModularStructure& ms)
    {
        location input_loc{input_file.get_filename_string()};
        loc = &input_loc;
        ptformula_in = input_file;
        parsing::FormulaParser parser{ms, {}, Term{1}};
        parser.parse();

    }

    void FormulaParser::error(const parsing::location& loc, const std::string& message)
    {
        yyerrors(ptformula_text, loc, "{}", message);
    }
}
