/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief abstract class AbstractParserNet, used for ParserMNet and ParserPTNet

code that is needed for both classes
*/

#include <Core/Dimensions.h>
#include <Frontend/Parser/AbstractParserNet.h>
#include <Frontend/SymbolTable/ArcList.h>
#include <Frontend/SymbolTable/PlaceSymbol.h>
#include <Frontend/SymbolTable/TransitionSymbol.h>

/// parses a petrinet without its marking from symboltables
Petrinet* AbstractParserNet::symboltable2netwomarking(Petrinet* pppnnn)
{
    if (!pppnnn)
        pppnnn = new Petrinet();
    /*********************************************
     * 1. Allocate memory for basic net structure *
     *********************************************/

    // 1.1 set cardinalities
    const arrayindex_t cardPL = PlaceTable->getCard();
    const arrayindex_t cardTR = TransitionTable->getCard();
    pppnnn->Card[PL] = cardPL;
    pppnnn->Card[TR] = cardTR;
    pppnnn->CardSignificant = UINT_MAX;  // mark as "still to be computed"

    // 1.2 allocate arrays for node (places and transitions) names, arcs, and multiplicities
    for (int type = PL; type <= TR; type++)
    {
        pppnnn->Name[type] = new const char*[pppnnn->Card[type]];
        pppnnn->thesymbol[type] = new Symbol*[pppnnn->Card[type]];
        for (int direction = PRE; direction <= POST; direction++)
        {
            pppnnn->CardArcs[type][direction] = new arrayindex_t[pppnnn->Card[type]];
            pppnnn->Arc[type][direction] = new arrayindex_t*[pppnnn->Card[type]];
            pppnnn->Mult[type][direction] = new mult_t*[pppnnn->Card[type]];
        }
        pppnnn->OnlyPre[type] = new arrayindex_t*[pppnnn->Card[type]];
        pppnnn->GreaterPre[type] = new arrayindex_t*[pppnnn->Card[type]];
        pppnnn->PreEqualPost[type] = new arrayindex_t*[pppnnn->Card[type]];
        pppnnn->GreaterPost[type] = new arrayindex_t*[pppnnn->Card[type]];
        pppnnn->OnlyPost[type] = new arrayindex_t*[pppnnn->Card[type]];
        pppnnn->CardOnlyPre[type] = new arrayindex_t[pppnnn->Card[type]];
        pppnnn->CardGreaterPre[type] = new arrayindex_t[pppnnn->Card[type]];
        pppnnn->CardPreEqualPost[type] = new arrayindex_t[pppnnn->Card[type]];
        pppnnn->CardGreaterPost[type] = new arrayindex_t[pppnnn->Card[type]];
        pppnnn->CardOnlyPost[type] = new arrayindex_t[pppnnn->Card[type]];
    }

    /********************************
     * 2. Allocate memory for places *
     *********************************/

    pppnnn->Capacity = new capacity_t[cardPL];

    /***********************************************
     * 3. Copy data from the symbol table to places *
     ************************************************/

    // fill all information that is locally available in symbols, allocate node specific arrays
    PlaceSymbol* ps;
    for ((ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->first())); ps;
         ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->next()))
    {
        int i = ps->index;
        const arrayindex_t tempCardPre = ps->getCardPre();
        const arrayindex_t tempCardPost = ps->getCardPost();

        // we take care of the place name (not destructed by SymbolTable)
        pppnnn->Name[PL][i] = ps->getKey();
        pppnnn->thesymbol[PL][i] = ps;
        pppnnn->CardArcs[PL][PRE][i] = tempCardPre;
        pppnnn->CardArcs[PL][POST][i] = tempCardPost;

        // Pre- and post-transitions are recorded in a single array consisting of 5 separate
        // sections:
        //  1. t | t is only pre-transition
        //  2. t | t is pre- and post-transition, W(t,p) > W(p,t)
        //  3. t | t is pre and post-transition, W(t,p) = W(p,t)
        //  4. t | t is pre and post-transition, W(t,p) < W(p,t)
        //  5. t | t is only post-transition
        //  Every section may be empty.
        //  The following information can be derived:
        //  pre-transitions: Sec 1-4: start: pppnnn->Arc[PL][PRE][i], size:
        //  pppnnn->CardArcs[PL][PRE][i] post-transition: Sec 2-5: start: pppnnn->Arc[PL][POST][i] =
        //  pppnnn->Arc[PL][PRE][i] + size(Sec1), size: pppnnn->CardArcs[PL][POST][i] =
        //  tempCardPost; increasing transitions: Sec 1-2: start: pppnnn->Increasing[i] =
        //  pppnnn->Arc[PL][PRE][i], size: pppnnn->CardIncreasing[i] decreasing transitions: Sec
        //  4-5: start: pppnnn->Decreasing[i], size: pppnnn->CardDecreasing[i];

        // allocate memory for place's arcs (is copied later with transitions, size is upper approx
        // - realloced lateron)
        pppnnn->Arc[PL][PRE][i] = new arrayindex_t[tempCardPre + tempCardPost];
        pppnnn->Arc[PL][POST][i] = pppnnn->Arc[PL][PRE][i] + tempCardPre;
        pppnnn->Mult[PL][PRE][i] = new mult_t[tempCardPre + tempCardPost];
        pppnnn->Mult[PL][POST][i] = pppnnn->Mult[PL][PRE][i] + tempCardPre;

        // capacity
        pppnnn->Capacity[i] = ps->getCapacity();
    }

    /*************************************
     * 4. Allocate memory for transitions *
     **************************************/

    // allocate memory for static data
    pppnnn->Fairness = new fairnessAssumption_t[cardTR];

    /****************************************************
     * 5. Copy data from the symbol table to transitions *
     *****************************************************/

    // current_arc is used for filling in arcs and multiplicities of places
    // calloc: no arcs there yet
    arrayindex_t* current_arc_post = new arrayindex_t[cardPL];
    arrayindex_t* current_arc_pre = new arrayindex_t[cardPL];
    memset(current_arc_pre, 0, cardPL * sizeof(arrayindex_t));
    memset(current_arc_post, 0, cardPL * sizeof(arrayindex_t));

    TransitionSymbol* ts;
    for (ts = reinterpret_cast<TransitionSymbol*>(TransitionTable->first()); ts;
         ts = reinterpret_cast<TransitionSymbol*>(TransitionTable->next()))
    {
        int i = ts->index;
        const arrayindex_t tempCardPre = ts->getCardPre();
        const arrayindex_t tempCardPost = ts->getCardPost();

        // we need to take care of the name (not destructed by SymbolTable)
        pppnnn->Name[TR][i] = ts->getKey();
        pppnnn->thesymbol[TR][i] = ts;
        pppnnn->CardArcs[TR][PRE][i] = tempCardPre;
        pppnnn->CardArcs[TR][POST][i] = tempCardPost;

        // allocate memory for transition's arcs

        // list of arcs consists of 5 separate sections:
        // 1. pure pre-places: W(t,p) = 0
        // 2. decreased places: 0 < W(t,p) < W(p,t)
        // 3. test places: 0 != W(t,p) = W(p,t)
        // 4. increased places: W(t,p) > W(p,t) > 0
        // 5. pure post-places: W(p,t) = 0

        // This array (a single one for every transition) is shared for
        // - pppnnn->Arc[TR][PRE][i]
        // - pppnnn->Arc[TR][POST][i]
        // - pppnnn->DeltaT[PRE][i]
        // - pppnnn->DeltaT[POST][i]

        pppnnn->Arc[TR][PRE][i] = new arrayindex_t[tempCardPre + tempCardPost];
        pppnnn->Arc[TR][POST][i] = pppnnn->Arc[TR][PRE][i] + tempCardPre;
        pppnnn->Mult[TR][PRE][i] = new mult_t[tempCardPre + tempCardPost];
        pppnnn->Mult[TR][POST][i] = pppnnn->Mult[TR][PRE][i] + tempCardPre;

        pppnnn->Fairness[i] = ts->getFairness();

        // copy arcs (for transitions AND places)
        ArcList* al;
        arrayindex_t j;
        for (al = ts->getPre(), j = 0; al; al = al->getNext(), j++)
        {
            const arrayindex_t k = al->getPlace()->getIndex();
            pppnnn->Arc[TR][PRE][i][j] = k;
            pppnnn->Arc[PL][POST][k][current_arc_post[k]] = i;
            pppnnn->Mult[PL][POST][k][(current_arc_post[k])++] = pppnnn->Mult[TR][PRE][i][j] =
                al->getMultiplicity();
        }
        for (al = ts->getPost(), j = 0; al; al = al->getNext(), j++)
        {
            const arrayindex_t k = al->getPlace()->getIndex();
            pppnnn->Arc[TR][POST][i][j] = k;
            pppnnn->Arc[PL][PRE][k][current_arc_pre[k]] = i;
            pppnnn->Mult[PL][PRE][k][(current_arc_pre[k])++] = pppnnn->Mult[TR][POST][i][j] =
                al->getMultiplicity();
        }
    }

    for (arrayindex_t p = 0; p < pppnnn->Card[PL]; p++)
    {
        if (pppnnn->CardArcs[PL][PRE][p])
            sortarcs<mult_t>(
                pppnnn->Arc[PL][PRE][p], pppnnn->Mult[PL][PRE][p], pppnnn->CardArcs[PL][PRE][p]
            );
        if (pppnnn->CardArcs[PL][POST][p])
            sortarcs<mult_t>(
                pppnnn->Arc[PL][POST][p], pppnnn->Mult[PL][POST][p], pppnnn->CardArcs[PL][POST][p]
            );
    }
    for (arrayindex_t t = 0; t < pppnnn->Card[TR]; t++)
    {
        if (pppnnn->CardArcs[TR][PRE][t])
            sortarcs<mult_t>(
                pppnnn->Arc[TR][PRE][t], pppnnn->Mult[TR][PRE][t], pppnnn->CardArcs[TR][PRE][t]
            );
        if (pppnnn->CardArcs[TR][POST][t])
            sortarcs<mult_t>(
                pppnnn->Arc[TR][POST][t], pppnnn->Mult[TR][POST][t], pppnnn->CardArcs[TR][POST][t]
            );
    }

    delete[] current_arc_pre;
    delete[] current_arc_post;

    return pppnnn;
}
