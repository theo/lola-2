/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief formula syntax
\author <unknown>
\status new
\ingroup g_frontend

Parses a formula in LoLA syntax.
*/

/* C++ parser interface */
%skeleton "lalr1.cc"
/* require bison version */
%require "3.3.2"
/* standard yylex is not typesafe, this creates make_TOKEN functions for the lexer */
%define api.token.constructor
/* use c++ variant types, to use c++ types as semantic values */
%define api.value.type variant
%define api.namespace { parsing }
%define api.parser.class { FormulaParser }
%locations
%define parse.error verbose
%define api.prefix {ptformula_}
%define api.location.type {parsing::location}

%parse-param { const parsing::ModularStructure& ms }
// only apparent way to have this not be in global storage...
%parse-param { parsing::FormalSum current_ap }
%parse-param { parsing::Term current_factor }

%code requires {
    #include "Frontend/ModularParser/location.hh"
    #include "Frontend/ModularParser/ParserMNet.h"
    #include "Frontend/Parser/ast-system-k.h"
}

%code {
    #include <lolaconf.h>
    #include <Core/Dimensions.h>
    #include <CoverGraph/CoverGraph.h>
    #include <Frontend/SymbolTable/ArcList.h>
    #include <Frontend/SymbolTable/PlaceSymbol.h>
    #include <Frontend/SymbolTable/TransitionSymbol.h>
    #include <Frontend/SymbolTable/SymbolTable.h>
    #include <Frontend/Parser/ParserPTNet.h>
    #include <Frontend/Parser/error.h>
    #include <Net/Petrinet.h>
    #include <Portfolio/portfoliomanager.h>
    #include <Formula/StatePredicate/DeadlockPredicate.h>
    #include <Formula/StatePredicate/AtomicBooleanPredicate.h>
    #include <Formula/StatePredicate/FireablePredicate.h>
    #include <Formula/StatePredicate/TruePredicate.h>
    #include <Formula/StatePredicate/FalsePredicate.h>
    #include "Formula/StatePredicate/MagicNumber.h"
    #include "Formula/StatePredicate/Term.h"

    #include <limits.h>
    #include <libgen.h>
    #include <cstdarg>
    #include <cstdio>
    #include <string>
    #include <set>
    #include <forward_list>
    #include <algorithm>

    #ifdef RERS
        extern bool * rers_place;
    #endif

    tShape oppShape(tShape);

    parsing::FormulaParser::symbol_type ptformula_lex();

    std::set<arrayindex_t> target_place;
    std::set<arrayindex_t> target_transition;

    namespace rg = std::ranges;
}

%token RELEASE           "temporal operator RELEASE"
%token NEXTSTATE         "temporal operator NEXTSTATE"
%token INITIAL           "keyword INITIAL"
%token DEADLOCK          "keyword DEADLOCK"
%token FORMULA           "keyword FORMULA"
%token MAX           	   "keyword MAX"
%token AND               "Boolean conjuction"
%token NOT               "Boolean negation"
%token OR                "Boolean disjunction"
%token XOR               "Boolean exclusive disjunction"
%token iff               "Boolean iff"
%token ALLPATH           "path quantifier ALLPATH"
%token ALWAYS            "temporal operator ALWAYS"
%token EVENTUALLY        "temporal operator EVENTUALLY"
%token EXPATH            "path quantifier EXPATH"
%token UNTIL             "temporal operator UNTIL"
%token REACHABLE         "keyword REACHABLE"
%token INVARIANT         "keyword INVARIANT"
%token IMPOSSIBLE        "keyword IMPOSSIBLE"
%token notequal          "not-equals sign"
%token implies           "Boolean implication"
%token equals            "equals sign"
%token plus              "plus sign"
%token minus             "minus sign"
%token times             "multiplication sign"
%token leftparenthesis   "opening parenthesis"
%token rightparenthesis  "closing parenthesis"
%token greaterthan       "greater-than sign"
%token lessthan          "less-than sign"
%token greaterorequal    "greater-than-or-equal sign"
%token lessorequal       "less-than-or-equal sign"
%token semicolon         "semicolon"
%token TRUE              "Boolean TRUE"
%token FALSE             "Boolean FALSE"
%token FIREABLE          "keyword FIREABLE"
%token omega             "omega"
%token colon             "colon"
%token DCOLON            "double colon"
%token END 0             "end of file"

// typed terminals
%token <kc::casestring> CTLOPERATOR       "CTL* operator"
%token <kc::casestring> IDENTIFIER          "identifier"
%token <kc::integer>    NNNUMBER            "number"

// typed non-terminals
%type <kc::tFormula> compoundformula
%type <kc::tFormula> formula
%type <kc::tFormula> computeboundformula
%type <kc::tStatePredicate> statepredicate
%type <kc::tAtomicProposition> atomic_proposition
%type <kc::casestring> identifier

// precedences (lowest written first, e.g. PLUS/MINUS) and precedences
%nonassoc REACHABLE INVARIANT IMPOSSIBLE
%left iff
%left implies
%left OR XOR
%left AND
%right NOT
%left ALLPATH EXPATH CTLOPERATOR UNTIL ALWAYS EVENTUALLY NEXTSTATE
%nonassoc lessthan lessorequal greaterthan greaterorequal  equals notequal
%left plus minus
%left times

%%

compoundformula:
  formula { Task::CompoundPlanning($1);}
| compoundformula colon formula {Task::CompoundPlanning($3);}
;
formula:
  FORMULA statepredicate semicolon
    { $$ = StatePredicateFormula($2); }
| FORMULA statepredicate
    { $$ = StatePredicateFormula($2); }
| statepredicate
    { $$ = StatePredicateFormula($1); }
| statepredicate semicolon
    { $$ = StatePredicateFormula($1); }
| computeboundformula
    { $$ = $1; }
;

statepredicate:
  leftparenthesis statepredicate rightparenthesis
    { $$ = $2; }
| atomic_proposition
    {
	if($1->pred->magicnumber == MAGIC_NUMBER_TRUE)
	{
		delete $1->pred;
		$1 -> pred = new TruePredicate(Petrinet::InitialNet);
	}
	else if($1->pred->magicnumber == MAGIC_NUMBER_FALSE)
	{
		delete $1->pred;
		$1 -> pred = new FalsePredicate(Petrinet::InitialNet);
	}
	$$ = AtomicProposition($1);
	$$ -> shape = $1 -> shape;
	$$ -> formula = $1 -> pred;
    }
| NOT statepredicate
    {
	switch($2 -> shape)
	{
	case AT_TEMP: 	$$ = Negation($2);
			break;
	case AT_DL:
	case AT_FIR:
	case AT_COMP: 	$2 -> formula = $2 -> formula -> negate();
			$$ = $2;
			break;
	case AT_TRUE: 	delete($2 -> formula);
			$2 -> formula = new FalsePredicate(Petrinet::InitialNet);
			$2 -> shape = AT_FALSE;
			$$ = $2;
			break;
	case AT_FALSE: 	delete($2 -> formula);
			$2 -> formula = new TruePredicate(Petrinet::InitialNet);
			$2 -> shape = AT_TRUE;
			$$ = $2;
			break;
	case AT_AND: 	$2 -> formula -> negate();
			$$ = $2;
			$$ -> shape = AT_OR;
			break;
	case AT_OR: 	$2 -> formula -> negate();
			$$ = $2;
			$$ -> shape = AT_AND;
			break;
	}
    }
| statepredicate AND statepredicate
    {
	if($1 -> shape == AT_TEMP || $3 -> shape == AT_TEMP)
	{
		$$ = Conjunction($1, $3);
		$$ -> shape = AT_TEMP;
	}
	else if($1 -> shape == AT_FALSE || $3 -> shape == AT_FALSE)
	{
		delete $1 -> formula;
		delete $3 -> formula;
		$$ = $1;
		$$ -> formula = new FalsePredicate(Petrinet::InitialNet);
		$$ -> shape = AT_FALSE;
	}
	else if($1 -> shape == AT_TRUE)
	{
		$$ = $3;
		$$ -> shape = $3 -> shape;
	}
	else if($3 -> shape == AT_TRUE)
	{
		$$ = $1;
		$$ -> shape = $1 -> shape;
	}
	else if(($1->shape == AT_AND) && (($3 -> shape == AT_OR) || ($3 -> shape == AT_FIR) || ($3 -> shape == AT_COMP) || ($3->shape == AT_DL)))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($1 -> formula) -> addSub($3->formula);
		$$ = $1;
	}
	else if(($3->shape == AT_AND) && (($1 -> shape == AT_OR) || ($1 -> shape == AT_COMP) || ($1 -> shape == AT_DL) || ($1 -> shape == AT_FIR)))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula) -> addSub($1->formula);
		$$ = $3;
	}
	else if(($1->shape == AT_AND) && ($3 -> shape == AT_AND))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula)->merge(reinterpret_cast<AtomicBooleanPredicate *>($1->formula));
		$$ = $3;
	}
	else // both $1 and $3 are AT_FIR, AT_COMP, AT_DL or AT_OR
	{
		AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		result -> addSub($1->formula);
		result -> addSub($3->formula);
        auto temp = Elementary();
        temp->pred = result;
        temp->shape = AT_AND;
		$$ = AtomicProposition(temp);
		$$ -> formula = result;
		$$ -> shape = AT_AND;
	}
    }
| statepredicate OR statepredicate
    {
	if($1 -> shape == AT_TEMP || $3 -> shape == AT_TEMP)
	{
		$$ = Disjunction($1, $3);
		$$ -> shape = AT_TEMP;
	}
	else if($1 -> shape == AT_TRUE || $3 -> shape == AT_TRUE)
	{
		delete $1 -> formula;
		delete $3 -> formula;
		$$ = $1;
		$$ -> formula = new TruePredicate(Petrinet::InitialNet);
		$$ -> shape = AT_TRUE;
	}
	else if($1 -> shape == AT_FALSE)
	{
		$$ = $3;
		$$ -> shape = $3 -> shape;
	}
	else if($3 -> shape == AT_FALSE)
	{
		$$ = $1;
		$$ -> shape = $1 -> shape;
	}
	else if(($1->shape == AT_OR) && (($3 -> shape == AT_AND) || ($3 -> shape == AT_COMP) || ($3 -> shape == AT_FIR) || ($3->shape == AT_DL) ))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($1 -> formula) -> addSub($3->formula);

		$$ = $1;
	}
	else if(($3->shape == AT_OR) && (($1 -> shape == AT_AND) || ($1 -> shape == AT_COMP) || ($1->shape == AT_FIR) || ($1->shape == AT_DL)))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula) -> addSub($1->formula);
		$$ = $3;
	}
	else if(($1->shape == AT_OR) && ($3 -> shape == AT_OR))
	{
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula)->merge(reinterpret_cast<AtomicBooleanPredicate *>($1->formula));
		$$ = $3;
	}
	else // both $1 and $3 are AT_COMP,AT_FIR,AT_DL or AT_OR
	{
		AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		result -> addSub($1->formula);
		result -> addSub($3->formula);
        auto temp = Elementary();
        temp->pred = result;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = result;
		$$ -> shape = AT_OR;
	}
    }
| statepredicate XOR statepredicate
    {
	// translate into (p & -q) | (-p & q)

	if($1 -> shape == AT_TEMP || $3 -> shape == AT_TEMP)
	{
		$$ = ExclusiveDisjunction($1, $3);
		$$ -> shape = AT_TEMP;
	}
	else if($1 -> shape == AT_TRUE)
	{
		$3 -> formula = $3 -> formula -> negate();
		$$ = $3;
		$$ -> shape = oppShape($3->shape);
	}
	else if($3 -> shape == AT_TRUE)
	{
		$1 -> formula = $1 -> formula -> negate();
		$$ = $1;
		$$ -> shape = oppShape($1->shape);
	}
	else if($3 -> shape == AT_FALSE)
	{
		$$ = $1;
	}
	else if($1 -> shape == AT_FALSE)
	{
		$$ = $3;
	}
	else if(($1 -> shape == AT_AND) && (($3 -> shape == AT_COMP)|| ($3->shape == AT_FIR) || ($3->shape == AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_*
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(c);
		d -> addSub($3->formula); // d = (-p & q)
		reinterpret_cast<AtomicBooleanPredicate *>($1 -> formula) ->addSub(b);     // $1 = (p & -q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub($1->formula);
		a -> addSub(d); // a = result
        auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)||($1->shape == AT_FIR) || ($1->shape == AT_DL)) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_ELEM
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_OR
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(c);
		d -> addSub($1->formula); // d = (p & -q)
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula) ->addSub(b);     // $3 = (-p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub($3->formula); // a = result
        auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)||($1->shape == AT_FIR) || ($1->shape == AT_DL)) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_ELEM
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_AND
		reinterpret_cast<AtomicBooleanPredicate *>(c) -> addSub($1 -> formula); // c = p & -q
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(b);
		d -> addSub($3->formula); // d = (-p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(c); // a = result
        auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_OR) && (($3 -> shape == AT_COMP)||($3->shape == AT_FIR) || ($3->shape == AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_ELEM
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_AND
		reinterpret_cast<AtomicBooleanPredicate *>(c) -> addSub($3 -> formula); // c = -p & q
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(b);
		d -> addSub($1->formula); // d = (p & -q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(c); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)||($1->shape == AT_FIR) || ($1->shape == AT_DL)) && (($3 -> shape == AT_COMP)|| ($3->shape == AT_FIR)||($3->shape==AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_ELEM
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_AND
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(b);
		d -> addSub($1->formula); // d = (p & -q)
		AtomicBooleanPredicate * e = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		e -> addSub(c);
		e -> addSub($3->formula); // d = (p & -q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(e); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_AND) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_OR
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
        static_cast<AtomicBooleanPredicate*>($1->formula)->addSub(b); // d = (p & -q)
        static_cast<AtomicBooleanPredicate*>($3->formula)->addSub(c); // d = (-p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub($1->formula);
		a -> addSub($3->formula); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_AND) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_AND
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
		static_cast<AtomicBooleanPredicate *>(b) -> merge(static_cast<AtomicBooleanPredicate*>($1->formula)); // p & -q
		AtomicBooleanPredicate * e = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		e -> addSub(c);
		e -> addSub($3->formula); // e = (-p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub(e); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_OR) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_AND
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_OR
		static_cast<AtomicBooleanPredicate *>(b) -> merge(static_cast<AtomicBooleanPredicate*>($3->formula)); // -p & q
		AtomicBooleanPredicate * e = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		e -> addSub(c);
		e -> addSub($1->formula); // e = (p & -q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub(e); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else // (($1 -> shape == AT_OR) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_AND
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_AND
		static_cast<AtomicBooleanPredicate *>(b) -> addSub($3->formula); // -p & q
		static_cast<AtomicBooleanPredicate *>(c) -> addSub($1->formula); // p & -q
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub(c); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
    }
| statepredicate implies statepredicate
    {
	if($1 -> shape == AT_TEMP || $3 -> shape == AT_TEMP)
	{
		$$ = Implication($1, $3);
		$$ -> shape = AT_TEMP;
	}
	else if($1 -> shape == AT_FALSE || $3 -> shape == AT_TRUE)
	{
		delete $1 -> formula;
		delete $3 -> formula;
		$$ = $1;
		$$ -> formula = new TruePredicate(Petrinet::InitialNet);
		$$ -> shape = AT_TRUE;
	}
	else if($1 -> shape == AT_TRUE)
	{
		$$ = $3;
		$$ -> shape = $3 -> shape;
	}
	else if($3 -> shape == AT_FALSE)
	{
		$1->formula = $1 -> formula -> negate();
		$$ = $1;
		$$ -> shape = oppShape($1 -> shape);
	}
	else if(($1->shape == AT_AND) && (($3 -> shape == AT_AND) || ($3 -> shape == AT_COMP)|| ($3->shape == AT_FIR) || ($3->shape == AT_DL)))
	{
		 $1 -> formula = $1 -> formula ->negate();
		reinterpret_cast<AtomicBooleanPredicate *>($1 -> formula) -> addSub($3->formula);
		$$ = $1;
		$$ -> shape = AT_OR;
	}
	else if(($3->shape == AT_OR) && (($1 -> shape == AT_OR) || ($1 -> shape == AT_COMP)||($1->shape == AT_FIR) || ($1->shape == AT_DL)))
	{
		$1 -> formula = $1-> formula -> negate();
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula) -> addSub($1->formula);
		$$ = $3;
	}
	else if(($1->shape == AT_AND) && ($3 -> shape == AT_OR))
	{
		$1 -> formula = $1 -> formula -> negate();
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula)->merge(reinterpret_cast<AtomicBooleanPredicate *>($1->formula));
		$$ = $3;
	}
	else // both $1 and $3 are AT_* or AT_OR
	{
		$1 -> formula = $1 -> formula -> negate();
		AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		result -> addSub($1->formula);
		result -> addSub($3->formula);
		auto temp = Elementary();
        temp->pred = result;
        temp->shape = AT_OR;
        $$ = AtomicProposition(temp);
		$$ -> formula = result;
		$$ -> shape = AT_OR;
	}
    }
| statepredicate iff statepredicate
    {
	// translate into (p & q) | (-p & -q)

	if($1 -> shape == AT_TEMP || $3 -> shape == AT_TEMP)
	{
		$$ = Equivalence($1, $3);
		$$ -> shape = AT_TEMP;
	}
	else if($1 -> shape == AT_FALSE)
	{
		$3 -> formula = $3 -> formula -> negate();
		$$ = $3;
		$$ -> shape = oppShape($3->shape);
	}
	else if($3 -> shape == AT_FALSE)
	{
		$1 -> formula = $1 -> formula -> negate();
		$$ = $1;
		$$ -> shape = oppShape($1->shape);
	}
	else if($3 -> shape == AT_TRUE)
	{
		$$ = $1;
	}
	else if($1 -> shape == AT_TRUE)
	{
		$$ = $3;
	}
	else if(($1 -> shape == AT_AND) && (($3 -> shape == AT_COMP)||($3->shape == AT_FIR) || ($3->shape == AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_ELEM
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(c);
		d -> addSub(b); // d = (-p & -q)
		reinterpret_cast<AtomicBooleanPredicate *>($1 -> formula) ->addSub($3->formula);     // $1 = (p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub($1->formula);
		a -> addSub(d); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)|| ($1->shape == AT_FIR) || ($1->shape == AT_DL)) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_ELEM
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_OR
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(c);
		d -> addSub(b); // d = (-p & -q)
		reinterpret_cast<AtomicBooleanPredicate *>($3 -> formula) ->addSub($1->formula);     // $3 = (p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub($3->formula); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)||($1->shape == AT_FIR) || ($1->shape == AT_DL)) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_ELEM
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_AND
		reinterpret_cast<AtomicBooleanPredicate *>(c) -> addSub(b); // c = -p & -q
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub($1->formula);
		d -> addSub($3->formula); // d = (p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(c); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_OR) && (($3 -> shape == AT_COMP)||($3->shape==AT_FIR) || ($3->shape == AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_ELEM
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_AND
		reinterpret_cast<AtomicBooleanPredicate *>(c) -> addSub(b); // c = -p & -q
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub($3->formula);
		d -> addSub($1->formula); // d = (p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(c); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if((($1 -> shape == AT_COMP)|| ($1->shape == AT_FIR) || ($1->shape == AT_DL)) && (($3 -> shape == AT_COMP)||($3->shape == AT_FIR)||($3->shape == AT_DL)))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_ELEM
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_AND
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(b);
		d -> addSub(c); // d = (-p & -q)
		AtomicBooleanPredicate * e = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		e -> addSub($1->formula);
		e -> addSub($3->formula); // e = (p & q)
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub(e); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_AND) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_OR
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub(b);
		d -> addSub(c); // d = (-p & -q)
		reinterpret_cast<AtomicBooleanPredicate *>($1->formula)->merge(reinterpret_cast<AtomicBooleanPredicate *>($3->formula)); //$1 = p&q
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(d);
		a -> addSub($1->formula); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_AND) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $3 -> formula -> copy(NULL);
		b = b -> negate();  //  -q, AT_AND
		StatePredicate * c = $1->formula -> copy(NULL);
		c = c -> negate(); // -p, AT_OR
		reinterpret_cast<AtomicBooleanPredicate *>(b) -> addSub(c); // -p & -q
		reinterpret_cast<AtomicBooleanPredicate *>($1->formula)->addSub($3->formula); // $1 = p&q
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub($1->formula); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else if(($1 -> shape == AT_OR) && ($3 -> shape == AT_AND))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_AND
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_OR
		reinterpret_cast<AtomicBooleanPredicate *>(b) -> addSub(c); // -p & -q
		reinterpret_cast<AtomicBooleanPredicate *>($3->formula)->addSub($1->formula); //$3 = p&q
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub($3->formula); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
	else // (($1 -> shape == AT_OR) && ($3 -> shape == AT_OR))
	{
		StatePredicate * b = $1 -> formula -> copy(NULL);
		b = b -> negate();  //  -p, AT_AND
		StatePredicate * c = $3->formula -> copy(NULL);
		c = c -> negate(); // -q, AT_AND
		reinterpret_cast<AtomicBooleanPredicate *>(b) -> merge(reinterpret_cast<AtomicBooleanPredicate *>(c)); // -p & -q
		AtomicBooleanPredicate * d = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
		d -> addSub($1->formula);
		d -> addSub($3->formula); // d = p&q
		AtomicBooleanPredicate * a = new AtomicBooleanPredicate(Petrinet::InitialNet,false);
		a -> addSub(b);
		a -> addSub(d); // a = result
		auto temp = Elementary();
        temp->pred = a;
        temp->shape = AT_OR;
		$$ = AtomicProposition(temp);
		$$ -> formula = a;
		$$ -> shape = AT_OR;
	}
    }
| ALLPATH statepredicate
    { $$ = AllPath($2); }
| EXPATH statepredicate
    { $$ = ExPath($2); }
| ALWAYS statepredicate
    { $$ = Always($2); }
| EVENTUALLY statepredicate
    { $$ = Eventually($2); }
| CTLOPERATOR statepredicate
    {
        kc::tStatePredicate result = $2;
        std::string op($1->name);
        for (int i = op.size()-1; i >= 0; i--)
        {
            if (op[i] == 'A') result = AllPath(result);
            if (op[i] == 'E') result = ExPath(result);
            if (op[i] == 'F') result = Eventually(result);
            if (op[i] == 'G') result = Always(result);
            if (op[i] == 'X') result = NextState(result);
            if (op[i] == 'U') yyerrors($1->name, @1, "operator 'U' is not allowed here");
            if (op[i] == 'R') yyerrors($1->name, @1, "operator 'R' is not allowed here");
        }
        $$ = result;
    }
| leftparenthesis statepredicate UNTIL statepredicate rightparenthesis
    { $$ = Until($2, $4); }
| leftparenthesis statepredicate RELEASE statepredicate rightparenthesis
    { $$ = Release($2, $4); }
| leftparenthesis statepredicate CTLOPERATOR statepredicate rightparenthesis
    {
        std::string op($3->name);
        if (op == "R") {
            $$ = Release($2, $4);
        } else if (op == "U") {
            $$ = Until($2, $4);
        } else {
            yyerrors($3->name, @3, "operator '{}' is not allowed here", $3->name);
        }
    }
| NEXTSTATE statepredicate
    { $$ = NextState($2); }
| REACHABLE statepredicate
    { $$ = ExPath(Eventually($2)); }
| INVARIANT statepredicate
    { $$ = AllPath(Always($2)); }
| IMPOSSIBLE statepredicate
    {
	$$ = AllPath(Always(Negation($2)));
    }
;

atomic_proposition:
  term equals { current_factor = Term{-1}; } term
    {
	AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
	$$ = Elementary();
	$$ -> shape = AT_AND;
	$$ -> pred = result;
    result->addSub(new AtomicStatePredicate(current_ap));
    current_ap.invert();
    result->addSub(new AtomicStatePredicate(current_ap));
    current_ap.reset();
    current_factor = Term{1};
    }
| term notequal { current_factor = Term{-1}; } term
    {
	AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
	$$ = Elementary();
	$$ -> shape = AT_OR;
    result->addSub(new AtomicStatePredicate(current_ap));
    current_ap.invert();
    result->addSub(new AtomicStatePredicate(current_ap));
    $$->pred = result->negate();
    current_ap.reset();
    current_factor = Term{1};
    }
| term greaterthan { current_ap.invert(); } term
    {
    current_ap.constant.add(1);
	AtomicStatePredicate * result = new AtomicStatePredicate(current_ap);
	$$ = Elementary();
	$$ -> shape = AT_COMP;
	$$ -> pred = result;
    current_ap.reset();
    }
| term greaterorequal { current_ap.invert(); } term
    {
	AtomicStatePredicate * result = new AtomicStatePredicate(current_ap);
	$$ = Elementary();
	$$ -> shape = AT_COMP;
	$$ -> pred = result;
    current_ap.reset();
    }
| term lessthan { current_factor = Term{-1}; } term
    {
    current_ap.constant.add(1);
    AtomicStatePredicate * result = new AtomicStatePredicate(current_ap);
	$$ = Elementary();
	$$ -> shape = AT_COMP;
	$$ -> pred = result;
    current_ap.reset();
    current_factor = Term{1};
    }
| term lessorequal { current_factor = Term{-1}; } term
    {
	AtomicStatePredicate * result = new AtomicStatePredicate(current_ap);
	$$ = Elementary();
	$$ -> shape = AT_COMP;
	$$ -> pred = result;
    current_ap.reset();
    current_factor = Term{1};
    }
| TRUE
    {
	TruePredicate * result = new TruePredicate(Petrinet::InitialNet);
	$$ = Elementary();
	$$ -> shape = AT_TRUE;
	$$ -> pred = result;
    }
| FALSE
    {
	FalsePredicate * result = new FalsePredicate(Petrinet::InitialNet);
	$$ = Elementary();
	$$ -> shape = AT_FALSE;
	$$ -> pred = result;
    }
| FIREABLE leftparenthesis identifier rightparenthesis
    {
        ::Symbol *t = ParserPTNet::currentsymbols->TransitionTable->lookup($3->name);
        if (UNLIKELY(t == NULL))
        {
            yyerrors($3->name, @3, "transition '{}' unknown", $3->name);
        }
	if(t -> inEmptySiphon)
	{
		FalsePredicate * result = new FalsePredicate(Petrinet::InitialNet);
		$$ = Elementary();
		$$ -> shape = AT_FIR;
		$$ -> pred = result;

	}
	else
	{
		FireablePredicate * result = new FireablePredicate(Petrinet::InitialNet,t->getIndex(),true);
		target_transition.insert(t->getIndex());
		$$ = Elementary();
		$$ -> shape = AT_FIR;
		$$ -> pred = result;
	}
    }
| INITIAL
    {
	AtomicBooleanPredicate * result = new AtomicBooleanPredicate(Petrinet::InitialNet,true);
	for(::Symbol * sy = ParserPTNet::currentsymbols->PlaceTable -> first(); sy; sy = ParserPTNet::currentsymbols->PlaceTable -> next())
	{
		if(sy->inEmptySiphon) continue;
		capacity_t m0 = ((PlaceSymbol *) sy)->getInitialMarking();

		// insert p <= m0
		::Term * T1 = new ::Term();
		T1 -> place  = sy->index;
		T1 -> mult = 1;
		::Term * T2 = new ::Term();
		T2 -> place = UINT32_MAX;
		T2 -> mult = -m0;
		T1 -> append(T2);
		result -> addSub(new AtomicStatePredicate(Petrinet::InitialNet,T1));

		// insert p >= m0
		T1 = new ::Term();
		T1 -> place = sy->index;
		T1 -> mult = -1;
		T2 = new ::Term();
		T2 -> place = UINT32_MAX;
		T2 -> mult = m0;
		T1 -> append(T2);
		result -> addSub(new AtomicStatePredicate(Petrinet::InitialNet,T1));
	}
	$$ = Elementary();
	$$ -> shape = AT_AND;
	$$ -> pred = result;
    }
| DEADLOCK
    {
	DeadlockPredicate * result = new DeadlockPredicate(Petrinet::InitialNet,true);
	$$ = Elementary();
	$$ -> shape = AT_DL;
	$$ -> pred = result;
    }
;

computeboundformula:
	MAX leftparenthesis term rightparenthesis
	{
		$$ = CompBound();
		$$ -> formula = new AtomicStatePredicate(current_ap,true);
	}

term:
  leftparenthesis term rightparenthesis
| identifier
    {
        const auto iter_alias = ms.get_aliases().find($1->name);
        if (iter_alias == ms.get_aliases().cend()) [[unlikely]]
        {
            RT::log<warn>("Place name '{}' which is not an alias used without instance, consider adding an alias or instance", $1->name);
            auto contains_place = [alias_name = std::string_view{$1->name}] (parsing::SymbolTable<Instance>::const_reference instance)
            {
                const auto& [inst_name, inst_sym] = instance;
                return inst_sym.get_module().second.get_places().contains(alias_name);
            };
            assert((rg::count_if(ms.get_instances(), contains_place) <= 1) && "Place name occurs in multiple instances!");
            auto iter_instance = rg::find_if(ms.get_instances(), contains_place);
            if (iter_instance == ms.get_instances().end())
            {
                yyerrors($1->name, @1, "unknown place: '{}'", $1->name);
            }
            auto iter_place = iter_instance->second.get_module().second.get_places().find($1->name);
            const auto & [_, place_sym] = *iter_place;
            if (!place_sym.in_empty_siphon)
            {
                current_ap.add_term(*iter_instance, *iter_place, current_factor);
            }
        }
        else
        {
            const auto [instance, place] = iter_alias->second;
            const auto& [_, place_sym] = *place;
            if(!place_sym.in_empty_siphon)
            {
                current_ap.add_term(*instance, *place, current_factor);
                // target_place.insert(place_sym.index);
            }
        }
    }
| identifier DCOLON identifier
    {
        auto iter_instance = ms.get_instances().find($1->name);
        if (iter_instance == ms.get_instances().end()) [[unlikely]]
        {
            auto iter_module = ms.get_modules().find($1->name);
            if(iter_module == ms.get_modules().end()) [[unlikely]]
            {
                yyerrors($1->name, @1, "no module or instance with name '{}' defined", $1->name);
            }
            else
            {
                yyerrors($1->name, @1, "module {} with non-empty set of instances used for term definition, use an instance instead", $1->name);
            }
        }
        const auto& [_, modsym] = iter_instance->second.get_module();
        const auto iter_place = modsym.get_places().find($3->name);
        if (iter_place == modsym.get_places().end()) [[unlikely]]
        {
            yyerrors($3->name, @3, "no place named '{}' defined in instance '{}'", $3->name, $1->name);
        }
        const auto& [name, place_sym] = *iter_place;
        if(!place_sym.in_empty_siphon)
        {
            current_ap.add_term(*iter_instance, *iter_place, current_factor);
            // target_place.insert(place_sym.index);
        }
    }
| NNNUMBER
    {
        Term copy = current_factor;
        copy.multiply($1->value);
        current_ap.constant.add(copy);
    }
| term plus term
| term minus { current_factor.multiply(-1); } term { current_factor.multiply(-1); }
| NNNUMBER times { current_factor.multiply($1->value); } term
    {
        current_factor.divide($1->value);
    }
| omega
    {
      Term copy = current_factor;
      copy.multiply(OMEGA);
      current_ap.constant.add(copy);
      if (RT::args.search_arg != search_arg_covergraph)
      {
          RT::log<warn>("{}: omega markings used without {}",
              RT::markup(MARKUP::WARNING, "warning"),
              RT::markup(MARKUP::PARAMETER, "--search=cover"));
      }
    }
;

identifier:
  IDENTIFIER     { $$ = $1; }
| CTLOPERATOR  { $$ = $1; }
;

%%

tShape oppShape(tShape s)
{
	switch(s)
	{
	case AT_COMP: return AT_COMP;
	case AT_FIR: return AT_FIR;
	case AT_DL: return AT_DL;
	case AT_TEMP: return AT_TEMP;
	case AT_AND: return AT_OR;
	case AT_OR: return AT_AND;
	case AT_TRUE: return AT_FALSE;
	case AT_FALSE: return AT_TRUE;
	}
}
