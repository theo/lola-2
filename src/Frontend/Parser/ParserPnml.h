#pragma once
#include <Core/Dimensions.h>
#include <sstream>

class ParserPTNet;

ParserPTNet* ReadPnmlFile();
ParserPTNet* unfoldHLNet();
ParserPTNet* unfoldHLNetStableMarking();
void ReadPnmlFormula();
char* ReadPnmlFormulaId();
void ReadHLPnmlFormula();

#define HLHASHTABLESIZE 10000000

extern bool HighLevelNet;

class PlaceSymbol;

class nestedunit
{
public:
    char* name;               // name of unit
    PlaceSymbol* p;           // an element of this unit
    nestedunit* nextinunit;   // next element of same (or parent) unit
    nestedunit* firstinunit;  // first element of this unit;
    nestedunit* nextunit;   // points to first element of next unit, only set for first elements of
                            // units
    nestedunit* firstunit;  // points to first element of first unit, only set for first elements of
                            // units
    bool leafunit;          // true if unit does not contain subunits
    bool marked;
};
