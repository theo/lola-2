/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef SRC_FRONTEND_MODULARSYMBOLS_ALIASSYMBOL_H
#define SRC_FRONTEND_MODULARSYMBOLS_ALIASSYMBOL_H

#include "Frontend/ModularSymbols/InstanceSymbol.h"
#include "Frontend/SymbolTable/Hash.h"
#include "Frontend/SymbolTable/SymbolTable.h"
#include "Frontend/SymbolTable/PlaceSymbol.h"

namespace parsing
{
struct Alias
{
    SymbolTable<Instance>::const_pointer instance;
    SymbolTable<Place>::const_pointer place;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::Alias> : fmt::formatter<std::string_view>
{
    auto format(const parsing::Alias& a, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(std::back_inserter(tmp), "{}::{}", a.instance->first, a.place->first);
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

template <>
struct std::hash<parsing::Alias>
{
    auto operator()(parsing::Alias key) const -> std::size_t
    {
        return parsing::detail::hash_it(
            key.place->first, parsing::detail::hash_it(key.instance->first)
        );
    }
};

template <>
struct std::equal_to<parsing::Alias>
{
    auto operator()(parsing::Alias lhs, parsing::Alias rhs) const -> bool
    {
        return (lhs.instance->first == rhs.instance->first)
            && (lhs.place->first == rhs.place->first);
    }
};

#endif /* SRC_FRONTEND_MODULARSYMBOLS_ALIASSYMBOL_H */
