/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief implementation of class FusionSetSymbol
*/
#pragma once

#include <Frontend/ModularSymbols/InstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleInstanceSymbol.h>
#include <Frontend/SymbolTable/Hash.h>
#include <Frontend/SymbolTable/Symbol.h>
#include <Frontend/SymbolTable/TransitionSymbol.h>

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <string_view>
#include <vector>

class FusionSetSymbol : public Symbol
{
public:
    FusionSetSymbol(const char* k) : Symbol(k) {};

    std::vector<std::pair<ModuleInstanceSymbol*, TransitionSymbol*>> Transitions;
};

namespace parsing
{
class FusionVector
{
    friend class NetParser;

public:
    using vector_type = std::unordered_map<
        std::string_view, SymbolTable<Transition>::reference, hash, std::equal_to<>>;

    explicit FusionVector(std::size_t num_instances) : transitions{num_instances} {}
    auto get_transitions() const -> const vector_type& { return transitions; }

private:
    auto add_transition(std::string_view instance, SymbolTable<Transition>::reference transition)
        -> bool
    {
        transition.second.set_external();
        auto [_, success] = transitions.emplace(
            std::piecewise_construct, std::forward_as_tuple(instance),
            std::forward_as_tuple(transition)
        );
        return success;
    }

    vector_type transitions;

public:
    arrayindex_t index;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::FusionVector> : fmt::formatter<std::string_view>
{
    auto format(const parsing::FusionVector& fv, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "#: {} with Transitions: {}", fv.index, fv.get_transitions()
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};
