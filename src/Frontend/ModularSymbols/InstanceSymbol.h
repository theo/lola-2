/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief definition of class InstanceSymbol
*/
#pragma once

#include <Core/Dimensions.h>
#include <Frontend/ModularSymbols/ModuleInstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleSymbol.h>
#include <Frontend/SymbolTable/SymbolTable.h>
#include <Frontend/SymbolTable/PlaceSymbol.h>

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <string>
#include <string_view>
#include <vector>

class InstanceSymbol : public ModuleInstanceSymbol
{
public:
    InstanceSymbol(const char* k, ModuleSymbol* m);
    ~InstanceSymbol();

    std::map<PlaceSymbol*, capacity_t>* marking;
    bool hasMarking;
    bool duplicatedMarking;  // set if marking is already used in another InstanceSymbol
    ModuleSymbol* module;

    void setMarking(std::map<PlaceSymbol*, capacity_t>* marking, bool duplicated);
};

namespace parsing
{
class Instance
{
    friend class NetParser;

public:
    explicit Instance(SymbolTable<Module>::reference module, arrayindex_t index_offset)
        : module{std::addressof(module)}, index_offset{index_offset}
    {
    }

    auto get_module() const -> SymbolTable<Module>::const_reference { return *module; }
    auto get_offset() const -> arrayindex_t { return index_offset; }

    std::forward_list<Module::Marking>::const_iterator initial_marking;

private:
    SymbolTable<Module>::pointer module;
    arrayindex_t index_offset;

public:
    arrayindex_t index;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::Instance> : fmt::formatter<std::string_view>
{
    auto format(const parsing::Instance& i, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "#: {}, Module: {}, Offset: {}, Marking: {}", i.index,
            i.get_module().first, i.get_offset(),
            std::distance(i.get_module().second.get_initial_markings().begin(), i.initial_marking)
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};
