/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief definition of class ModuleSymbol
*/
#pragma once

#include <fmt/format.h>
#include <fmt/ranges.h>

#include <forward_list>
#include <ranges>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "Core/Runtime.h"
#include "Frontend/ModularSymbols/ModuleInstanceSymbol.h"
#include "Frontend/Parser/AbstractParserNet.h"
#include "Frontend/SymbolTable/Hash.h"
#include "Frontend/SymbolTable/NestedUnit.h"
#include "Frontend/SymbolTable/SymbolTable.h"
#include "Frontend/SymbolTable/TransitionSymbol.h"
#include "Net/ModularStructure.h"
#include "rapidxml/rapidxml.hpp"

class InstanceSymbol;

class ModuleSymbol : public ModuleInstanceSymbol, public AbstractParserNet
{
public:
    ModuleSymbol(const char* k);
    ~ModuleSymbol();

    std::vector<InstanceSymbol*> Instances;

    // this index is needed for symboltable2modularstructure in ParserMNet.cc:
    // if we create an instance for this module, because no index was defined in the parsed file,
    // we can use this index to reference the instance we created
    arrayindex_t instanceIndex;

    Petrinet* symboltable2modnet();
    capacity_t* getInitialMarking();

    // returns an index to the first interface tranisition of the module
    arrayindex_t setIndices();
};

namespace parsing
{
class Module
{
    friend class NetParser;
    friend void parse_pt_symbols(rapidxml::xml_node<>* net, Module& module);

public:
    struct Marking
    {
        using map_type = std::unordered_map<std::string_view, capacity_t, hash, std::equal_to<>>;
        map_type marking;
        arrayindex_t index;

        Marking() = default;
        Marking(map_type&& marking) : marking{std::move(marking)} {}
    };

    using marking_list = std::forward_list<Marking>;

    auto get_initial_markings() const -> std::ranges::subrange<
        marking_list::const_iterator, marking_list::const_iterator,
        std::ranges::subrange_kind::sized>
    {
        return {
            initial_markings.cbegin(), initial_markings.cend(),
            static_cast<std::size_t>(card_markings)
        };
    }

    auto get_initial_markings() -> std::ranges::subrange<
        marking_list::iterator, marking_list::iterator, std::ranges::subrange_kind::sized>
    {
        return {
            initial_markings.begin(), initial_markings.end(),
            static_cast<std::size_t>(card_markings)
        };
    }

    auto get_places() const -> const SymbolTable<Place>& { return places; }

    auto get_transitions() const -> const SymbolTable<Transition>& { return transitions; }

    auto get_nested_units() const -> const SymbolTable<NestedUnit>& { return nested_units; }

    auto get_first_interface_transition() -> arrayindex_t;

    void to_lola_module(lola::Module& module);

    template <typename Key>
    auto find_place(const Key& key) -> SymbolTable<Place>::iterator
    {
        return places.find(key);
    }

    template <typename Key>
    auto find_transition(const Key& key) -> SymbolTable<Transition>::iterator
    {
        return transitions.find(key);
    }

    template <typename Key>
    auto find_nested_unit(const Key& key) -> SymbolTable<NestedUnit>::iterator
    {
        return nested_units.find(key);
    }

private:
    void add_default_marking(Marking::map_type&& marking)
    {
        initial_markings.emplace_front(std::move(marking));
        ++card_markings;
    }
    auto add_initial_marking(Marking::map_type&& marking)
        -> std::forward_list<Marking>::const_iterator
    {
        auto it = initial_markings.emplace_after(initial_markings.cbegin(), std::move(marking));
        ++card_markings;
        return it;
    }

    auto add_place(std::string&& key, arrayindex_t index, capacity_t cap)
        -> std::pair<SymbolTable<Place>::iterator, bool>
    {
        return places.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)),
            std::forward_as_tuple(index, cap)
        );
    }

    auto add_transition(
        std::string&& key, fairnessAssumption_t fair = NO_FAIRNESS,
        std::forward_list<Arc>&& pre = std::forward_list<Arc>{},
        std::forward_list<Arc>&& post = std::forward_list<Arc>{}
    ) -> std::pair<SymbolTable<Transition>::iterator, bool>
    {
        return transitions.emplace(
            std::piecewise_construct, std::forward_as_tuple(std::move(key)),
            std::forward_as_tuple(fair, std::move(pre), std::move(post))
        );
    }

    auto add_nested_unit(std::string&& key) -> std::pair<SymbolTable<NestedUnit>::iterator, bool>
    {
        return nested_units.emplace(std::move(key), NestedUnit{});
    }

    SymbolTable<Place> places{static_cast<std::size_t>(RT::args.sizeofsymboltable_arg)};
    SymbolTable<Transition> transitions{static_cast<std::size_t>(RT::args.sizeofsymboltable_arg)};
    SymbolTable<NestedUnit> nested_units{};
    marking_list initial_markings{};
    arrayindex_t card_markings{0};

public:
    arrayindex_t index;
};
}  // namespace parsing

template <>
struct fmt::formatter<parsing::Module::Marking> : fmt::formatter<std::string_view>
{
    auto format(const parsing::Module::Marking& m, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(std::back_inserter(tmp), "[#: {}, Mapping: {}]", m.index, m.marking);
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};

template <>
struct fmt::formatter<parsing::Module> : fmt::formatter<std::string_view>
{
    auto format(const parsing::Module& m, fmt::format_context& ctx) const
    {
        std::string tmp;
        fmt::format_to(
            std::back_inserter(tmp), "#: {}, Places: {}, Transitions: {}, Markings: {}, NU: {}",
            m.index, m.get_places(), fmt::join(m.get_transitions(), "\n"),
            fmt::join(m.get_initial_markings(), "\n"), fmt::join(m.get_nested_units(), "\n")
        );
        return fmt::formatter<std::string_view>::format(tmp, ctx);
    }
};
