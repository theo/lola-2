/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief implementation of class InstanceSymbol
*/

#include <Core/Dimensions.h>
#include <Frontend/ModularSymbols/InstanceSymbol.h>
#include <Frontend/ModularSymbols/ModuleSymbol.h>
#include <Frontend/SymbolTable/Symbol.h>
#include <Frontend/SymbolTable/SymbolTable.h>

#include <map>
#include <string>
#include <utility>

InstanceSymbol::InstanceSymbol(const char* k, ModuleSymbol* m) : ModuleInstanceSymbol(k)
{
    module = m;
    isInstance = true;
    hasMarking = false;
}

InstanceSymbol::~InstanceSymbol()
{
    if (!duplicatedMarking)
    {
        delete marking;
    }
}

void InstanceSymbol::setMarking(std::map<PlaceSymbol*, capacity_t>* mark, bool duplicated)
{
    if (!(mark == NULL))
        hasMarking = true;
    duplicatedMarking = duplicated;
    marking = mark;
}
