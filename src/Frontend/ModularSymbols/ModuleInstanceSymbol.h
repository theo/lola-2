/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief abstract class ModuleInstanceSymbol, used for ModuleSymbol and InstanceSymbol

this class is needed for fusionsets, which transitions can be part of a module or instance
*/
#pragma once

#include <Frontend/SymbolTable/Symbol.h>

class ModuleInstanceSymbol : public Symbol
{
public:
    ModuleInstanceSymbol(const char* k) : Symbol(k) {};

    bool isInstance;
};
