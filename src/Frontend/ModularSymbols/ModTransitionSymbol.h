/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief class ModTransitionSymbol for Modular Petrinets
*/

#pragma once

#include <Frontend/SymbolTable/TransitionSymbol.h>

class ModTransitionSymbol : public TransitionSymbol
{
public:
    ModTransitionSymbol(
        const char* k, fairnessAssumption_t f = NO_FAIRNESS, ArcList* pr = NULL, ArcList* po = NULL
    )
        : TransitionSymbol(k, f, pr, po)
    {
        isExternal = false;
    }

    bool isExternal;
};
