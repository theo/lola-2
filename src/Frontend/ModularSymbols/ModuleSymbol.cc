/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief implementation of class ModuleSymbol
*/

#include "ModuleSymbol.h"

#include <cstddef>
#include <forward_list>
#include <memory>
#include <string>
#include <tuple>
#include <utility>

#include "Core/Dimensions.h"
#include "Core/Runtime.h"
#include "Frontend/ModularSymbols/ModTransitionSymbol.h"
#include "Frontend/ModularSymbols/ModuleInstanceSymbol.h"
#include "Frontend/Parser/AbstractParserNet.h"
#include "Frontend/SymbolTable/ArcList.h"
#include "Frontend/SymbolTable/PlaceSymbol.h"
#include "Frontend/SymbolTable/SymbolTable.h"
#include "Frontend/SymbolTable/TransitionSymbol.h"
#include "Net/ModularStructure.h"
#include "Net/Petrinet.h"

ModuleSymbol::ModuleSymbol(const char* k) : ModuleInstanceSymbol(k)
{
    isInstance = false;
    PlaceTable = new SymbolTable();
    TransitionTable = new SymbolTable();
}

ModuleSymbol::~ModuleSymbol()
{
    delete PlaceTable;
    delete TransitionTable;
}

arrayindex_t ModuleSymbol::setIndices()
{
    const arrayindex_t cardPL = PlaceTable->getCard();
    const arrayindex_t cardTR = TransitionTable->getCard();
    PlaceSymbol* ps;
    arrayindex_t i;
    for ((ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->first())), (i = 0); ps;
         ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->next()), i++)
    {
        ps->setIndex(i);
    }

    i = 0;
    ModTransitionSymbol* ts;

    // collect all internal transitions first
    for ((ts = reinterpret_cast<ModTransitionSymbol*>(TransitionTable->first())); ts;
         ts = reinterpret_cast<ModTransitionSymbol*>(TransitionTable->next()))
    {
        if (!ts->isExternal)
        {
            ts->setIndex(i);
            i++;
        }
    }
    arrayindex_t first_ext = i;
    // we want all external transitions at the end
    for ((ts = reinterpret_cast<ModTransitionSymbol*>(TransitionTable->first())); ts;
         ts = reinterpret_cast<ModTransitionSymbol*>(TransitionTable->next()))
    {
        if (ts->isExternal)
        {
            ts->setIndex(i);
            i++;
        }
    }
    return first_ext;
}

Petrinet* ModuleSymbol::symboltable2modnet()
{
    Petrinet* modularNet = symboltable2netwomarking();

    // initial marking will be specified for each instance, not for a module
    // initial marking in this module is just the default marking for every instance
    modularNet->Initial = NULL;

    return modularNet;
}

capacity_t* ModuleSymbol::getInitialMarking()  // get the default marking for the instances
{
    capacity_t* marking = new capacity_t[PlaceTable->getCard()];

    PlaceSymbol* ps;
    for ((ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->first())); ps;
         ps = reinterpret_cast<PlaceSymbol*>(PlaceTable->next()))
    {
        int i = ps->index;
        marking[i] = ps->getInitialMarking();
    }

    return marking;
}

namespace parsing
{

namespace
{
template <bool do_increase>
struct partition_helper
{
    using difference_type = arrayindex_t;
    auto operator*() -> partition_helper& { return *this; }
    auto operator++() -> partition_helper&
    {
        if constexpr (do_increase)
        {
            ++i;
        }
        else
        {
            --i;
        }
        return *this;
    }
    auto operator++(int) -> partition_helper
    {
        partition_helper tmp{*this};
        ++(*this);
        return tmp;
    }
    auto operator=(SymbolTable<Transition>::reference t) -> partition_helper&
    {
        t.second.index = i;
        return *this;
    }
    arrayindex_t i;
};
}  // namespace

auto Module::get_first_interface_transition() -> arrayindex_t
{
    const arrayindex_t card_transitions = transitions.size();
    auto [_1, _2, end_of_internals] = std::ranges::partition_copy(
        transitions, partition_helper<false>{static_cast<arrayindex_t>(transitions.size() - 1)},
        partition_helper<true>{0}, [](const auto t) { return t.second.is_external(); }
    );
    return end_of_internals.i;
}

void Module::to_lola_module(lola::Module& module)
{
    module.Initial = nullptr;
    const arrayindex_t card_places = places.size();
    const arrayindex_t card_transitions = transitions.size();
    module.Card[TR] = card_transitions;
    module.CardSignificant = UINT_MAX;
    for (int type : {PL, TR})
    {
        module.Name[type] = new const char*[module.Card[type]];
        module.thesymbol[type] = new ::Symbol*[module.Card[type]];
        for (int direction : {PRE, POST})
        {
            module.CardArcs[type][direction] = new arrayindex_t[module.Card[type]];
            module.Arc[type][direction] = new arrayindex_t*[module.Card[type]];
            module.Mult[type][direction] = new mult_t*[module.Card[type]];
        }
        module.OnlyPre[type] = new arrayindex_t*[module.Card[type]];
        module.GreaterPre[type] = new arrayindex_t*[module.Card[type]];
        module.PreEqualPost[type] = new arrayindex_t*[module.Card[type]];
        module.GreaterPost[type] = new arrayindex_t*[module.Card[type]];
        module.OnlyPost[type] = new arrayindex_t*[module.Card[type]];
        module.CardOnlyPre[type] = new arrayindex_t[module.Card[type]];
        module.CardGreaterPre[type] = new arrayindex_t[module.Card[type]];
        module.CardPreEqualPost[type] = new arrayindex_t[module.Card[type]];
        module.CardGreaterPost[type] = new arrayindex_t[module.Card[type]];
        module.CardOnlyPost[type] = new arrayindex_t[module.Card[type]];
    }
    module.Capacity = new capacity_t[card_places];

    auto copy_from_place_sym = [&, this](SymbolTable<Place>::const_reference place_sym)
    {
        const auto& [name, sym] = place_sym;
        const arrayindex_t index = sym.index;
        const arrayindex_t temp_card_pre = sym.get_card_pre();
        const arrayindex_t temp_card_post = sym.get_card_post();
        module.Name[PL][index] = name.c_str();
        module.CardArcs[PL][PRE][index] = temp_card_pre;
        module.CardArcs[PL][POST][index] = temp_card_post;

        // Pre- and post-transitions are recorded in a single array consisting of 5 separate
        // sections:
        //  1. t | t is only pre-transition
        //  2. t | t is pre- and post-transition, W(t,p) > W(p,t)
        //  3. t | t is pre and post-transition, W(t,p) = W(p,t)
        //  4. t | t is pre and post-transition, W(t,p) < W(p,t)
        //  5. t | t is only post-transition
        //  Every section may be empty.
        //  The following information can be derived:
        //  pre-transitions: Sec 1-4: start: module.Arc[PL][PRE][index], size:
        //  module.CardArcs[PL][PRE][index] post-transition: Sec 2-5: start:
        //  module.Arc[PL][POST][index] = module.Arc[PL][PRE][index] + size(Sec1), size:
        //  module.CardArcs[PL][POST][index] = temp_card_post; increasing transitions: Sec 1-2:
        //  start: module.Increasing[index] = module.Arc[PL][PRE][index], size:
        //  module.CardIncreasing[index] decreasing transitions: Sec 4-5: start:
        //  module.Decreasing[index], size: module.CardDecreasing[index];

        // allocate memory for place's arcs (is copied later with transitions, size is upper approx
        // - realloced lateron)
        module.Arc[PL][PRE][index] = new arrayindex_t[temp_card_pre + temp_card_post];
        module.Arc[PL][POST][index] = module.Arc[PL][PRE][index] + temp_card_pre;
        module.Mult[PL][PRE][index] = new mult_t[temp_card_pre + temp_card_post];
        module.Mult[PL][POST][index] = module.Mult[PL][PRE][index] + temp_card_pre;

        // capacity
        module.Capacity[index] = sym.capacity;
    };

    std::ranges::for_each(places, std::move(copy_from_place_sym));

    module.Fairness = new fairnessAssumption_t[card_transitions];

    auto current_arc_pre = std::make_unique<arrayindex_t[]>(card_places);
    auto current_arc_post = std::make_unique<arrayindex_t[]>(card_places);

    auto copy_from_transition_sym =
        [&, this](SymbolTable<Transition>::const_reference transition_sym)
    {
        const auto& [name, sym] = transition_sym;
        const arrayindex_t index = sym.index;
        const arrayindex_t temp_card_pre = sym.get_card_pre();
        const arrayindex_t temp_card_post = sym.get_card_post();
        module.Name[TR][index] = name.c_str();
        module.CardArcs[TR][PRE][index] = temp_card_pre;
        module.CardArcs[TR][POST][index] = temp_card_post;

        // list of arcs consists of 5 separate sections:
        // 1. pure pre-places: W(t,p) = 0
        // 2. decreased places: 0 < W(t,p) < W(p,t)
        // 3. test places: 0 != W(t,p) = W(p,t)
        // 4. increased places: W(t,p) > W(p,t) > 0
        // 5. pure post-places: W(p,t) = 0

        // This array (a single one for every transition) is shared for
        // - module.Arc[TR][PRE][index]
        // - module.Arc[TR][POST][index]
        // - module.DeltaT[PRE][index]
        // - module.DeltaT[POST][index]

        module.Arc[TR][PRE][index] = new arrayindex_t[temp_card_pre + temp_card_post];
        module.Arc[TR][POST][index] = module.Arc[TR][PRE][index] + temp_card_pre;
        module.Mult[TR][PRE][index] = new mult_t[temp_card_pre + temp_card_post];
        module.Mult[TR][POST][index] = module.Mult[TR][PRE][index] + temp_card_pre;

        module.Fairness[index] = sym.get_fairness();

        // copy arcs (for transitions AND places)
        std::ranges::for_each(
            std::views::zip(sym.get_pre(), std::views::iota(0)),
            [&, this](std::tuple<const Arc&, int> elem)
            {
                const auto& [arc, j] = elem;
                const arrayindex_t k = arc.get_place().second.index;
                module.Arc[TR][PRE][index][j] = k;
                module.Arc[PL][POST][k][current_arc_post[k]] = index;
                module.Mult[PL][POST][k][(current_arc_post[k])++] = module.Mult[TR][PRE][index][j] =
                    arc.get_multiplicity();
            }
        );

        std::ranges::for_each(
            std::views::zip(sym.get_post(), std::views::iota(0)),
            [&, this](std::tuple<const Arc&, int> elem)
            {
                const auto& [arc, j] = elem;
                const arrayindex_t k = arc.get_place().second.index;
                module.Arc[TR][POST][index][j] = k;
                module.Arc[PL][PRE][k][current_arc_pre[k]] = index;
                module.Mult[PL][PRE][k][(current_arc_pre[k])++] = module.Mult[TR][POST][index][j] =
                    arc.get_multiplicity();
            }
        );
    };

    std::ranges::for_each(transitions, std::move(copy_from_transition_sym));

    std::ranges::for_each(
        std::views::iota(0, card_places),
        [&, this](const arrayindex_t p)
        {
            if (module.CardArcs[PL][PRE][p])
                AbstractParserNet::sortarcs<mult_t>(
                    module.Arc[PL][PRE][p], module.Mult[PL][PRE][p], module.CardArcs[PL][PRE][p]
                );
            if (module.CardArcs[PL][POST][p])
                AbstractParserNet::sortarcs<mult_t>(
                    module.Arc[PL][POST][p], module.Mult[PL][POST][p], module.CardArcs[PL][POST][p]
                );
        }
    );

    std::ranges::for_each(
        std::views::iota(0, card_transitions),
        [&, this](const arrayindex_t t)
        {
            if (module.CardArcs[TR][PRE][t])
                AbstractParserNet::sortarcs<mult_t>(
                    module.Arc[TR][PRE][t], module.Mult[TR][PRE][t], module.CardArcs[TR][PRE][t]
                );
            if (module.CardArcs[TR][POST][t])
                AbstractParserNet::sortarcs<mult_t>(
                    module.Arc[TR][POST][t], module.Mult[TR][POST][t], module.CardArcs[TR][POST][t]
                );
        }
    );
}
}  // namespace parsing
