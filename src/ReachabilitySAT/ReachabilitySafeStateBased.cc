/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a save Petri net to a boolean formula where AtomicStatePredicates
are calculated based on the final encoded marking.
*/

#include <ReachabilitySAT/ReachabilitySafeStateBased.h>
#include <algorithm>

// useful macros for addressing or readability
#define VAR_PLACE(START, INDEX) (START - INDEX)
#define VAR_ATOM(A) (AddressOffset - Atomic[A]->magicnumber)
#define VAR_FIRE(S) (S)
#define VAR_EXTRA(E) (E)

ReachabilitySafeStateBased::ReachabilitySafeStateBased(Petrinet* n, StatePredicate* f, int p)
{
    net = n;

    Computed = new bool[net->Card[PL]];
    memset(Computed, false, net->Card[PL] * sizeof(bool));
    spFormula = f;
    portfolio_id = p;
    minRounds = 1;
    skippedRounds = 0;

    CardAtomicChange = 0;

    // Ordering of transitions, look into my bachelor's thesis for explanation :)

    initial = NetState::createNetStateFromInitial(net);

    TransitionHelper = new arrayindex_t[TRANSITION_ROW_COUNT(net->Card[TR])];
    arrayindex_t currActivated = 0;
    arrayindex_t UnorderedCount = 0;
    arrayindex_t currNonActivated = initial->CardEnabled;
    for (arrayindex_t t = 0; t < net->Card[TR]; t++)
    {
        if (initial->Enabled[t])
        {
            TRANSITION_ORD_REV(t) = currActivated;
            currActivated++;
            for (arrayindex_t p = 0; p < net->CardArcs[TR][POST][t]; p++)
            {
                Computed[net->Arc[TR][POST][t][p]] = true;
            }
        }
        else
        {
            bool allPre = true;
            for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
            {
                // todo maybe change for safe nets, since this is equal to initial==0 there
                if (net->Mult[TR][PRE][t][p] > net->Initial[net->Arc[TR][PRE][t][p]]
                    && !Computed[net->Arc[TR][PRE][t][p]])
                {
                    allPre = false;
                }
            }
            if (allPre)
            {
                TRANSITION_ORD_REV(t) = currNonActivated;
                currNonActivated++;
                for (arrayindex_t p = 0; p < net->CardArcs[TR][POST][t]; p++)
                {
                    Computed[net->Arc[TR][POST][t][p]] = true;
                }
            }
            else
            {
                TRANSITION_ORD_REV(t) = TRANSITION_UNORDERED_INDEX;
                TRANSITION_ORD(UnorderedCount) = t;
                UnorderedCount++;
            }
        }
    }

    bool changed = true;
    arrayindex_t start = 0;
    while (changed)
    {
        changed = false;
        for (arrayindex_t i = start; i < UnorderedCount; i++)
        {
            arrayindex_t t = TRANSITION_ORD(i);
            bool allPre = true;
            for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
            {
                // todo see above
                if (net->Mult[TR][PRE][t][p] > net->Initial[net->Arc[TR][PRE][t][p]]
                    && !Computed[net->Arc[TR][PRE][t][p]])
                {
                    allPre = false;
                }
            }
            if (allPre)
            {
                changed = true;
                TRANSITION_ORD_REV(t) = currNonActivated;
                currNonActivated++;
                for (arrayindex_t p = 0; p < net->CardArcs[TR][POST][t]; p++)
                {
                    Computed[net->Arc[TR][POST][t][p]] = true;
                }
                if (i != start)
                {
                    arrayindex_t temp = TRANSITION_ORD(i);
                    TRANSITION_ORD(i) = TRANSITION_ORD(start);
                    TRANSITION_ORD(start) = temp;
                    i--;
                }
                start++;
            }
        }
    }
    for (arrayindex_t t = 0; t < net->Card[TR]; t++)
    {
        if (TRANSITION_ORD_REV(t) != TRANSITION_UNORDERED_INDEX)
        {
            TRANSITION_ORD(TRANSITION_ORD_REV(t)) = t;
        }
    }

    TransitionCount = currNonActivated;
    memset(Computed, false, sizeof(bool) * net->Card[PL]);
}

ReachabilitySafeStateBased::~ReachabilitySafeStateBased()
{
    delete[] Computed;
    delete initial;
    delete[] Atomic;
    delete[] AtomicChange;
    delete[] TransitionHelper;
}

bool ReachabilitySafeStateBased::areFormalSumsEqual(
    AtomicStatePredicate* a, AtomicStatePredicate* b
)
{
    // sums can only be equal if they're the same length
    if (a->cardPos == b->cardPos && a->cardNeg == b->cardNeg)
    {
        arrayindex_t i;
        // sums can only be equal if all summands with positive multiplicities are the same place
        // and same multiplicity
        for (i = 0;
             i < a->cardPos && a->posPlaces[i] == b->posPlaces[i] && a->posMult[i] == b->posMult[i];
             i++)
            ;
        if (i == a->cardPos)
        {
            // finally the sums are only equal if the same holds for negative multiplicities
            // if the loop runs through all places, i should be cardNeg
            for (i = 0; i < a->cardNeg && a->negPlaces[i] == b->negPlaces[i]
                 && a->negMult[i] == b->negMult[i];
                 i++)
                ;
            return i == a->cardNeg;
        }
    }
    return false;
}

void ReachabilitySafeStateBased::PreProcessAtomics()
{
    AtomicCount = spFormula->countAtomic();
    Atomic = new AtomicStatePredicate*[AtomicCount];
    spFormula->collectAtomic(Atomic);
    // first thing, hash calculation for duplicate detection
    // for hashfunction ask Karsten
    arrayindex_t nextUnique = 0;
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        int hash = 0, summin = 0, summax = 0;
        for (int i = 0; i < Atomic[a]->cardPos; i++)
        {
            hash += Atomic[a]->posMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->posPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            summax += Atomic[a]->posMult[i];
        }
        for (int i = 0; i < Atomic[a]->cardNeg; i++)
        {
            hash -= Atomic[a]->negMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->negPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            summin -= Atomic[a]->negMult[i];
        }
        // Since we're talking about safe nets, summands are either 0 or 1 (times a constant),
        // therefore we can determine a minimum and maximum. If the needed threshold is outside that
        // range, we can simplify
        if (Atomic[a]->threshold < summax)
        {
            if (Atomic[a]->threshold >= summin)
            {
                // If the treshold is in range, we need to calculate that Atomic
                Atomic[a]->magicnumber = hash;
                // First determine if the sum is a duplicate
                arrayindex_t uniques;
                for (uniques = 0; uniques < nextUnique; uniques++)
                {
                    if (Atomic[a]->magicnumber == Atomic[uniques]->magicnumber
                        && areFormalSumsEqual(Atomic[a], Atomic[uniques]))
                    {
                        Atomic[a]->magicnumber = uniques;
                        break;
                    }
                }
                // If not, we sort the Atomics by "uniqueness" by swapping a newly found unique sum
                // to the front
                if (uniques == nextUnique)
                {
                    if (a != nextUnique)
                    {
                        AtomicStatePredicate* temp = Atomic[a];
                        Atomic[a] = Atomic[nextUnique];
                        Atomic[nextUnique] = temp;
                    }
                    // also need to check if its a new longest sum
                    arrayindex_t length;
                    if ((length = Atomic[nextUnique]->cardPos + Atomic[nextUnique]->cardNeg)
                        > CardAtomicChange)
                        CardAtomicChange = length;
                    // also save the minimum for convenience, more on that later
                    Atomic[nextUnique]->sum = summin;
                    nextUnique++;
                }
            }
            else
            {
                // threshold below minimum? sum can never be below
                Atomic[a]->magicnumber = FORMULA_RESULT_UNSOLVABLE;
            }
        }
        else
        {
            // threshold greater than maximum? sum always below
            Atomic[a]->magicnumber = FORMULA_RESULT_SATISFIED;
        }
    }
    AtomicCount = nextUnique;
    AtomicChange = new int[AtomicCount * CardAtomicChange];
    // now after knowing how many unique sums there are we create index arrays
    // which would sort the sum by multiplicity
    // for this we create index arrays for positive and negative multiplicities
    // and sort them, but we can still save them in the same array, since that's
    // also a consistent order
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        Atomic[a]->magicnumber = 0;
        arrayindex_t card = Atomic[a]->cardPos + Atomic[a]->cardNeg;
        arrayindex_t i = 0, j = Atomic[a]->cardPos, min = j;
        if (Atomic[a]->cardNeg < min)
            min = Atomic[a]->cardNeg;
        for (arrayindex_t k = 0; k < min; k++)
        {
            ATOM_SORTED_SUM(a, i) = i;
            ATOM_SORTED_SUM(a, j) = i;
            i++;
            j++;
        }
        while (i < Atomic[a]->cardPos)
        {
            ATOM_SORTED_SUM(a, i) = i;
            i++;
        }
        while (i < Atomic[a]->cardNeg)
        {
            ATOM_SORTED_SUM(a, j) = i;
            i++;
            j++;
        }
        std::sort(
            AtomicChange + a * CardAtomicChange,
            AtomicChange + a * CardAtomicChange + Atomic[a]->cardPos,
            [&](const unsigned int& x, const unsigned int& y)
            { return Atomic[a]->posMult[x] < Atomic[a]->posMult[y]; }
        );
        std::sort(
            AtomicChange + a * CardAtomicChange + Atomic[a]->cardPos,
            AtomicChange + a * CardAtomicChange + card,
            [&](const unsigned int& x, const unsigned int& y)
            { return Atomic[a]->negMult[x] < Atomic[a]->negMult[y]; }
        );
    }
}

void ReachabilitySafeStateBased::create_initial()
{
    for (arrayindex_t p = net->Card[PL] - 1; p >= 0; p--)
    {
        // every place has variable, the literal has the negation sign depending on initial marking
        // mkLit(..,sign) sets the negation sign if sign==true and not if the literal is supposed to
        // be true (sign==false)
        S->addClause(mkLit(S->newVar(), !(net->Initial[p])));
    }
    AddressOffset = S->nVars() - 1;
}

void ReachabilitySafeStateBased::create_phi(arrayindex_t t)
{
    // create new variables, can't reuse their returned addresses since the new variables
    // aren't traversed in a particular order
    for (arrayindex_t p = 0; p < net->Card[PL]; p++)
    {
        S->newVar();
    }
    // then for firing the pre-places of the previos step (starting point AddressOffset)
    // need to be marked
    for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
    {
        arrayindex_t place = net->Arc[TR][PRE][t][p];
        S->addClause(mkLit(VAR_PLACE(AddressOffset, place)), ~mkLit(VAR_FIRE(step)));
    }
    // current starting point
    arrayindex_t address = S->nVars() - 1;
    // places that lose tokens are unmarked when firing
    for (arrayindex_t i = 0; i < net->CardDeltaT[PRE][t]; i++)
    {
        arrayindex_t p = net->DeltaT[PRE][t][i];
        Computed[p] = true;
        S->addClause(~mkLit(VAR_PLACE(address, p)), ~mkLit(VAR_FIRE(step)));
    }
    // places that gain tokens are marked when firing
    for (arrayindex_t i = 0; i < net->CardArcs[TR][POST][t]; i++)
    {
        arrayindex_t p = net->Arc[TR][POST][t][i];
        Computed[p] = true;
        S->addClause(mkLit(VAR_PLACE(address, p)), ~mkLit(VAR_FIRE(step)));
    }
    for (arrayindex_t p = 0; p < net->Card[PL]; p++)
    {
        // places that change when firing are copied when not firing
        if (Computed[p])
        {
            Computed[p] = false;
            S->addClause(~mkLit(AddressOffset), mkLit(address), mkLit(VAR_FIRE(step)));
            S->addClause(mkLit(AddressOffset), ~mkLit(address), mkLit(VAR_FIRE(step)));
        }
        // places that don't change are always copied
        else
        {
            S->addClause(~mkLit(AddressOffset), mkLit(address));
            S->addClause(mkLit(AddressOffset), ~mkLit(address));
        }
        AddressOffset--;
        address--;
    }
    step++;
    AddressOffset = S->nVars() - 1;
}

void ReachabilitySafeStateBased::create_statebased_atomic(arrayindex_t a)
{
    // Calculating the atomic starts with two variables, one indicating the sum initially >=0, but
    // not >=1
    arrayindex_t p = 0, n = Atomic[a]->cardPos, PreviousPlaceValueCount = 2,
                 card = Atomic[a]->cardPos + Atomic[a]->cardNeg;
    arrayindex_t prev = S->newVar();
    S->addClause(~mkLit(prev));
    S->addClause(mkLit(S->newVar()));
    capacity_t mult;
    arrayindex_t place;
    arrayindex_t tmp;
    arrayindex_t diff;
    bool isPos;
    // As long as there are summands left, we calculate stuff
    while (p != Atomic[a]->cardPos || n != card)
    {
        // We take a summand with positive multiplicity if there are no more negative ones or we
        // have one of both and the positive one has a lower multiplicity than the negative one
        isPos =
            (n == card
             || (p != Atomic[a]->cardPos
                 && Atomic[a]->posMult[ATOM_SORTED_SUM(a, p)]
                     < Atomic[a]->negMult[ATOM_SORTED_SUM(a, n)]));
        if (isPos)
        {
            mult = Atomic[a]->posMult[ATOM_SORTED_SUM(a, p)];
            place = VAR_PLACE(AddressOffset, Atomic[a]->posPlaces[ATOM_SORTED_SUM(a, p)]);
            p++;
        }
        else
        {
            mult = Atomic[a]->negMult[ATOM_SORTED_SUM(a, n)];
            place = VAR_PLACE(AddressOffset, Atomic[a]->negPlaces[ATOM_SORTED_SUM(a, n)]);
            n++;
        }
        // calculating stuff for TRANSLATE_STEP
        diff = PreviousPlaceValueCount - mult;
        tmp = PreviousPlaceValueCount;
        if (diff > 0)
            tmp = mult;
        TRANSLATE_STEP(0, 0, tmp, place, isPos)
        PreviousPlaceValueCount += mult;
    }
    // The amount of variables needed to calculate the formal sum based on the final marking is
    // constant w.r.t PathLength, meaning for any translation they're the same. To save on
    // calculation time, we save the amount of variables needed to add to the AddressOffset (which
    // depends on PathLength) to obtain the address of the sums threshold
    if (ATOM_NOT_CALC_YET(a))
    {
        // Now we save information to easier calculate duplicates of this sum
        // We want to save a value such that simply adding a treshold would yield
        // the variable address of the encoded threshold.
        // For this, we first take S->nVars() - 1, this yields the address of the minimum
        // value for the sum. To convert a treshold-value to a treshold-address, we need to add
        // the minimum value (since that is index 0). To describe '<= x', we have to write 'not >=
        // x+1', which means we need the address of one value greater, resulting in another -1. Then
        // we subtract the AddressOffset. The amount of variables after the AddressOffset is
        // constant, meaning we don't need to update this for every new round, we just add this
        // value to the AddressOffset of a specific translation unit to get the value we want.
        Atomic[a]->sum += S->nVars() - 2 - AddressOffset;
        // Finally we set the magicnumber (encoding duplicates and other things) to a negative value
        // The negative value implies that the sum was already calculated once, meaning
        // Atomic[a]->sum holds the value needed to calculate the threshold address. The value
        // itself is the offset added to AddressOffset to obtain the address of the threshold.
        // (Since the value is negative, we can also simply subtract from AddressOffset instead of
        // abs())
        Atomic[a]->magicnumber = -1 * (Atomic[a]->sum - Atomic[a]->threshold);
    }
}

translation_result_t ReachabilitySafeStateBased::create_atomic(
    AtomicStatePredicate* f, vec<Lit>& lits
)
{
    translation_result_t result = FORMULA_RESULT_INDETERMINATE;
    arrayindex_t a = -1;
    while (f != Atomic[++a])
        ;
    // should be self-explanatory based on macros
    if (ATOM_SATISFIED(a))
    {
        result = FORMULA_RESULT_SATISFIED;
    }
    else if (ATOM_UNSOLVABLE(a))
    {
        result = FORMULA_RESULT_UNSOLVABLE;
    }
    else if (ATOM_ALREADY_CALC(a))
    {
        lits.push(~mkLit(VAR_ATOM(a)));
    }
    else
    {
        // Here Atomic[a] is a duplicate we've not translated before (even another translation
        // unit), therefore we first calculate the threshold address
        Atomic[a]->magicnumber = -1 * Atomic[Atomic[a]->magicnumber]->sum - f->threshold;
        lits.push(~mkLit(VAR_ATOM(a)));
    }
    return result;
}

translation_result_t ReachabilitySafeStateBased::create_fireable(
    arrayindex_t t, vec<Lit>& lits, arrayindex_t extra
)
{
    // Fireable is simple, just one pre-place? Then that needs to be marked
    // More than one pre-place? Add clause that all of them need to be marked
    translation_result_t fireableResult = FORMULA_RESULT_SATISFIED;
    if (net->CardArcs[TR][PRE][t] == 1)
    {
        fireableResult = FORMULA_RESULT_INDETERMINATE;
        lits.push(mkLit(VAR_PLACE(AddressOffset, net->Arc[TR][PRE][t][0])));
    }
    else
    {
        for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
        {
            fireableResult = FORMULA_RESULT_INDETERMINATE;
            arrayindex_t place = VAR_PLACE(AddressOffset, net->Arc[TR][PRE][t][p]);
            // If the parent requires renaming the fireable, then all the clauses require the
            // renaming variable for the Implication "Fireable->pre-place marked"
            if (extra)
            {
                S->addClause(mkLit(place), ~mkLit(VAR_EXTRA(extra)));
            }
            else
            {
                S->addClause(mkLit(place));
            }
        }
    }

    return fireableResult;
}

translation_result_t ReachabilitySafeStateBased::create_notfireable(arrayindex_t t, vec<Lit>& lits)
{
    // NotFireable is even easier, add that any pre-place should'nt be marked to the parent clause
    // Differentiating between 1 or more pre-places only saves loop overhead
    translation_result_t fireableResult = FORMULA_RESULT_UNSOLVABLE;
    if (net->CardArcs[TR][PRE][t] == 1)
    {
        fireableResult = FORMULA_RESULT_INDETERMINATE;
        lits.push(~mkLit(VAR_PLACE(AddressOffset, net->Arc[TR][PRE][t][0])));
    }
    else
    {
        for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
        {
            fireableResult = FORMULA_RESULT_INDETERMINATE;
            arrayindex_t place = VAR_PLACE(AddressOffset, net->Arc[TR][PRE][t][p]);
            lits.push(~mkLit(place));
        }
    }

    return fireableResult;
}

translation_result_t ReachabilitySafeStateBased::create_whole_formula()
{
    // Reset some helper fields
    step = 0;
    // Declare variables for fire/copy decision for minisat
    for (arrayindex_t i = 0; i < PathLength * TransitionCount; i++)
        S->newVar();

    // Create initial formula which also initializes the remaining helper fields
    ReachabilitySafeStateBased::create_initial();
    // Create phi^t for every transition for every 'major step'
    for (arrayindex_t k = 0; k < PathLength; k++)
    {
        for (arrayindex_t t = 0; t < TransitionCount; t++)
        {
            ReachabilitySafeStateBased::create_phi(TRANSITION_ORD(t));
        }
    }
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        create_statebased_atomic(a);
    }
    return create_spFormula();
}

void ReachabilitySafeStateBased::printAssignments(std::ostringstream& result)
{
    result << "Firing sequence for "
           << portfoliomanager::name[portfoliomanager::roottask[portfolio_id]] << ": ";
    bool any = false;
    for (arrayindex_t i = 0; i < PathLength * TransitionCount; i++)
    {
        if (S->model[VAR_FIRE(i)] == l_True)
        {
            result << (any ? " -> " : "") << net->Name[TR][TRANSITION_ORD(i % TransitionCount)];
            any = true;
        }
    }
    if (!any)
    {
        result << "Initial state is witness/counterexample";
    }
}
