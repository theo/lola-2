/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a general Petri net to a boolean formula.
*/

#pragma once

#include <ReachabilitySAT/ReachabilitySafe.h>

// again macros for more readable code
// same definitions as the ones in ReachabilitySafe, now just for places
// since places are no longer guaranteed to be safe, we treat them like atomics
#define PLACE_ROW_COUNT(P) ((P << 2) + (P << 1))
#define PLACE_VAR_COUNT(P) (PlaceHelper[PLACE_ROW_COUNT(P)])
#define PLACE_NEG_OFFSET(P) (PlaceHelper[PLACE_ROW_COUNT(P) + 1])
#define PLACE_VAR_ADDRESS(P) (PlaceHelper[PLACE_ROW_COUNT(P) + 2])
#define PLACE_ALLOWED_MAX(P) (PlaceHelper[PLACE_ROW_COUNT(P) + 3])
#define PLACE_MIN_VAL(P) (PlaceHelper[PLACE_ROW_COUNT(P) + 4])
#define PLACE_MAX_VAL(P) (PlaceHelper[PLACE_ROW_COUNT(P) + 5])

class ReachabilityGeneral : public ReachabilitySafe
{
public:
    ReachabilityGeneral(Petrinet* n, StatePredicate* f, int p);
    ~ReachabilityGeneral();
    virtual void PreProcessAtomics();

protected:
    /// @brief Helper fields for places, akin to atomics
    int* PlaceHelper;
    // Overridden funtions for new functionality
    virtual translation_result_t create_whole_formula();
    void create_initial();
    void create_phi(arrayindex_t t);
    translation_result_t create_atomic(AtomicStatePredicate* f, vec<Lit>& lits);
    translation_result_t create_fireable(arrayindex_t t, vec<Lit>& lits, arrayindex_t extra);
    translation_result_t create_notfireable(arrayindex_t t, vec<Lit>& lits);
    // for simplicity some inline functions that to some checks in case of translate-time
    // contradictions
    inline arrayindex_t GetPlaceMinRoundsGreater(arrayindex_t p, mult_t m);
    inline arrayindex_t GetPlaceMinRoundsLower(arrayindex_t p, mult_t m);
};
