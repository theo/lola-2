/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Simple Headerfile for common definitions for Reachability2Minisat translation
*/

#pragma once

// #include <minisat/ThirdParty/zlib/zlib.h>
#include <minisat/utils/System.h>
#include <minisat/utils/ParseUtils.h>
#include <minisat/utils/Options.h>
#include <minisat/core/Dimacs.h>
#include <minisat/simp/SimpSolver.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Formula/StatePredicate/StatePredicate.h>
#include <Formula/StatePredicate/AtomicBooleanPredicate.h>
#include <Formula/StatePredicate/MagicNumber.h>

#include <Portfolio/portfoliomanager.h>

#include <iomanip>

using namespace Minisat;
using std::chrono::duration;
using std::chrono::high_resolution_clock;
using std::chrono::microseconds;

// if you change the values, adjust them in minisat2(..)!
enum reachability_result_t
{
    REACHABILITY_PROPERTY_TRUE = 0,
    REACHABILITY_PROPERTY_INDETERMINATE = 1,
    REACHABILITY_PROPERTY_FALSE = 2,
};

typedef int translation_result_t;

// These are just simple constants used for preliminary results of the formula translation
// Why not enum? In translation a value > 0 is also used (meaning formula is currently
// unsolvable, but the value indicates the round in which it may be solvable based on
// contradictions)
#define FORMULA_RESULT_UNKNOWN -3
#define FORMULA_RESULT_UNSOLVABLE -2
#define FORMULA_RESULT_SATISFIED -1
#define FORMULA_RESULT_INDETERMINATE 0

// Simple macros for memory management concerning extra memory for transitions, here only used
// to order transitions
#define TRANSITION_ROW_COUNT(T) (T << 2)
// Reverse index means for a given transition index t the index of t in the order
#define TRANSITION_ORD_REV(T) (TransitionHelper[TRANSITION_ROW_COUNT(T)])
// Returns the i-th transition according to the order
#define TRANSITION_ORD(T) (TransitionHelper[TRANSITION_ROW_COUNT(T) + 1])
// Special index to describe transitions which are not ordered
#define TRANSITION_UNORDERED_INDEX -1
#define TRANSITION_IS_ORDERED(T) (TRANSITION_ORD_REV(T) != TRANSITION_UNORDERED_INDEX)

// somehow neccessary, no idea why
void printStats(Solver& solver);

class Reachability
{
public:
    virtual reachability_result_t lola2minisat(int k) final;
    virtual arrayindex_t GetCurrentMinRounds() { return minRounds; };
    virtual ~Reachability() = default;
    // This function includes work that could be done in the constructor, but since the different
    // implementations inheriting from this do different work which is not directly inheritable,
    // this is put into its own function and overridden
    virtual void PreProcessAtomics() = 0;
    static void setUpTranslation();

protected:
    /// Minisat options
    static IntOption verb;
    static BoolOption pre;
    static StringOption dimacs;
    static IntOption cpu_lim;
    static IntOption mem_lim;
    static bool didSetup;
    /// Fields relating to transition ordering, ordering is assumed to happen in any translation,
    /// therefore these are in the header
    arrayindex_t TransitionCount;
    arrayindex_t* TransitionHelper;
    /// Net for the property and the initial state
    Petrinet* net;
    NetState* initial;
    /// Current timestep
    arrayindex_t step;
    SimpSolver* S;
    StatePredicate* spFormula;
    arrayindex_t minRounds;
    /// Maximum path length, given as input
    arrayindex_t PathLength;
    arrayindex_t skippedRounds;
    int portfolio_id;

    // The functions for translating a StatePredicate formula are "interfaced" here. For any
    // implementation, only the implementations for atomic predicates (AtomicStatePredicate and
    // FireablePredicate) are needed in create_atomic, create_fireable and create_notfireable
    // respectively. All other functions work the same for any translation approach if said atomic
    // implementations follow some conventions. They are marked as virtual and final so that the
    // compiler notifies of accidental overrides.

    /// @brief Translates an AtomicStatePredicate into a SAT formula
    /// @param f The Predicate to be translated
    /// @param lits The vector to save a literal representing the predicate.
    /// @return The preliminary result as decribed above for translation_result_t
    virtual translation_result_t create_atomic(AtomicStatePredicate* f, vec<Lit>& lits) = 0;
    /// @brief Translates a FireablePredicate (with sign=true) into a SAT formula
    /// @param t The transition number of the Predicate
    /// @param lits The vector to (possibly) add variables representing the predicate, under current
    ///             implementations, its assumed that a variables representing the predicate is
    ///             added when the transition t has only one pre-place
    /// @param extra The address of a variable if the parent of the predicate needed to be renamed.
    /// Any
    ///              clause added for the predicate therefore needs to be the implication
    ///              extra->clause Assumed to be 0 when no relevant renaming happened
    /// @return The preliminary result as decribed above for translation_result_t
    virtual translation_result_t create_fireable(
        arrayindex_t t, vec<Lit>& lits, arrayindex_t extra
    ) = 0;
    /// @brief Translates a FireablePredicate (with sign=false) into a SAT formula
    /// @param t The transition number of the Predicate
    /// @param lits The vector to add variables representing the predicate, under current
    /// implementations,
    ///             its assumed that a variables representing the predicate is added every time
    /// @return The preliminary result as decribed above for translation_result_t
    virtual translation_result_t create_notfireable(arrayindex_t t, vec<Lit>& lits) = 0;
    // The following functions are used to translate higher level subformulas, namely deadlock,
    // nodeadlock, and and or by translating the subformulas. Implementations details are provided
    // in Reachabiliy.cc, since the user should'nt need to call these themselves
    virtual translation_result_t create_deadlock(arrayindex_t extra) final;
    virtual translation_result_t create_nodeadlock(vec<Lit>& lits) final;
    virtual translation_result_t create_and(
        AtomicBooleanPredicate* formula, arrayindex_t extra
    ) final;
    virtual translation_result_t create_or(AtomicBooleanPredicate* formula, vec<Lit>& lits) final;
    // Creating the top-level formula is also done in a separate function. The functionality is
    // basically the same as translating an and with just one child.
    virtual translation_result_t create_spFormula() final;
    // Finally a function to create the whole formula, often done by translating the behaviour of
    // the net followed by the formula
    virtual translation_result_t create_whole_formula() = 0;
    // Output function to print the transition firing sequence of a solution
    virtual void printAssignments(std::ostringstream& result) = 0;
    void setRoundSkip(const double t, const double s);
};
