/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a save Petri net to a boolean formula, combining two methods to calculate
AtomicStatePredicates
*/

#include <ReachabilitySAT/ReachabilitySafe.h>

// again macros for addressing/readability
#define VAR_ATOM(START, INDEX) ((START) - (INDEX))
#define VAR_FIRE(S) (S)
#define VAR_EXTRA(E) (E)

ReachabilitySafe::ReachabilitySafe(Petrinet* n, StatePredicate* f, int p)
    : ReachabilitySafeStateBased(n, f, p)
{
    allStatebased = false;
    nextStatebased = 0;
}

ReachabilitySafe::~ReachabilitySafe() { delete[] AtomicHelper; }

void ReachabilitySafe::PreProcessAtomics()
{
    AtomicCount = spFormula->countAtomic();
    Atomic = new AtomicStatePredicate*[AtomicCount];
    spFormula->collectAtomic(Atomic);
    // again start with hashing
    // basically the same as StateBased, except where noted
    arrayindex_t nextUnique = 0;
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        int hash = 0, summin = 0, summax = 0;
        for (int i = 0; i < Atomic[a]->cardPos; i++)
        {
            hash += Atomic[a]->posMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->posPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            summax += Atomic[a]->posMult[i];
        }
        for (int i = 0; i < Atomic[a]->cardNeg; i++)
        {
            hash -= Atomic[a]->negMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->negPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            summin -= Atomic[a]->negMult[i];
        }
        if (Atomic[a]->threshold < summax)
        {
            if (Atomic[a]->threshold >= summin)
            {
                Atomic[a]->magicnumber = hash;
                arrayindex_t uniques;
                for (uniques = 0; uniques < nextUnique; uniques++)
                {
                    if (Atomic[a]->magicnumber == Atomic[uniques]->magicnumber
                        && areFormalSumsEqual(Atomic[a], Atomic[uniques]))
                    {
                        Atomic[a]->magicnumber = uniques;
                        break;
                    }
                }
                if (uniques == nextUnique)
                {
                    if (a != nextUnique)
                    {
                        AtomicStatePredicate* temp = Atomic[a];
                        Atomic[a] = Atomic[nextUnique];
                        Atomic[nextUnique] = temp;
                    }
                    arrayindex_t length;
                    if ((length = Atomic[nextUnique]->cardPos + Atomic[nextUnique]->cardNeg)
                        > CardAtomicChange)
                        CardAtomicChange = length;
                    // no longer save just the minimum, but the size of the range [min,max]
                    // used to calculate an estimate on how many variables will be needed for
                    // statebased
                    Atomic[nextUnique]->sum = summax - summin;
                    nextUnique++;
                }
            }
            else
            {
                Atomic[a]->magicnumber = FORMULA_RESULT_UNSOLVABLE;
            }
        }
        else
        {
            Atomic[a]->magicnumber = FORMULA_RESULT_SATISFIED;
        }
    }
    // since we now look at saving values for each transition in AtomicChange, we need to check if
    // there are more transitions than summands in the longest sum, since the array is faster as
    // 2D matrix with some wasted space than completely dynamic
    if (CardAtomicChange < TransitionCount)
        CardAtomicChange = TransitionCount;
    AtomicCount = nextUnique;
    AtomicChange = new int[AtomicCount * CardAtomicChange];
    memset(AtomicChange, 0, sizeof(int) * AtomicCount * CardAtomicChange);
    AtomicHelper = new int[ATOM_ROW_COUNT(AtomicCount)];
    // instead of creating an index array for sorting we calculate how much each transition
    // changes the sum of each atomic
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        // initialize value for estimating round to change the implementation
        // for explanation look into the header for this file
        double changeImplRound = (double)(Atomic[a]->sum * net->Card[PL]) / net->Card[TR];
        Atomic[a]->evaluate(*initial);
        // initialize values helping with calculations
        arrayindex_t varcount = 0;
        arrayindex_t minval = 0;
        // Since we want to sort later, we repurpose the AtomicHelper as an index array,
        // since the different fields are only set later
        AtomicHelper[a] = a;
        // Then we loop over the sum to determine how transitions related to the different places
        // change the sum
        arrayindex_t min = Atomic[a]->cardPos;
        if (min > Atomic[a]->cardNeg)
            min = Atomic[a]->cardNeg;
        arrayindex_t p = 0;
        arrayindex_t t;
        arrayindex_t place;
        // We can save loop iterations (and therefore a linear amount of comparisons) by computing
        // negative and positive summands at the same time while p is a valid index for both
        while (p < min)
        {
            place = Atomic[a]->posPlaces[p];
            // Pre-Transitions of a place with positive multiplicity add to the sum
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][PRE][place][i]
                        * Atomic[a]->posMult[p];
            }
            // Post-Transitions of a place with positive multiplicity subtract from the sum
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][POST][place][i]
                        * Atomic[a]->posMult[p];
            }
            place = Atomic[a]->negPlaces[p];
            // Pre-Transitions of a place with negative multiplicity subtract from the sum
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][PRE][place][i]
                        * Atomic[a]->negMult[p];
            }
            // Post-Transitions of a place with negative multiplicity add to the sum
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][POST][place][i]
                        * Atomic[a]->negMult[p];
            }
            p++;
        }
        // The linear comparison get swapped with a single comparison, either here or the loop below
        while (p < Atomic[a]->cardPos)
        {
            // see above
            place = Atomic[a]->posPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][PRE][place][i]
                        * Atomic[a]->posMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][POST][place][i]
                        * Atomic[a]->posMult[p];
            }
            p++;
        }
        while (p < Atomic[a]->cardNeg)
        {
            // see above
            place = Atomic[a]->negPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][PRE][place][i]
                        * Atomic[a]->negMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][POST][place][i]
                        * Atomic[a]->negMult[p];
            }
            p++;
        }
        // after calculation we accumulate the overall change for an approximate minimum, need later
        for (arrayindex_t t = 0; t < TransitionCount; t++)
        {
            varcount += abs(ATOMCHANGE(a, t));
            if (ATOMCHANGE(a, t) < 0)
            {
                minval -= ATOMCHANGE(a, t);
            }
        }
        // then write values and finish round calculation
        ATOM_MIN_VAL(a) = minval;
        Atomic[a]->magicnumber = ceil(sqrt(changeImplRound / varcount));
        if (RT::args.satvariablebound_given)
            Atomic[a]->upper_bound = varcount;
    }
    // finally sort by square of round
    std::sort(
        AtomicHelper, AtomicHelper + AtomicCount,
        [&](const unsigned int& x, const unsigned int& y)
        { return Atomic[x]->magicnumber < Atomic[y]->magicnumber; }
    );
    // and write it to the appropriate places
    for (arrayindex_t a = AtomicCount - 1; a >= 0; a--)
    {
        ATOM_ORDER(a) = AtomicHelper[a];
    }
}

void ReachabilitySafe::updateAtomics()
{
    // With two possible implementations, we also have to update some fields when the implementation
    // changes Since only PathBased change, we iterate over those
    for (arrayindex_t x = nextStatebased; x < AtomicCount; x++)
    {
        arrayindex_t a = ATOM_ORDER(x);
        // Check if current round is the changing round
        if (PathLength >= Atomic[a]->magicnumber)
        {
            // Atomic[a] changes from pathbased to statebased
            Atomic[a]->magicnumber = 0;
            arrayindex_t card = Atomic[a]->cardPos + Atomic[a]->cardNeg;
            arrayindex_t i = 0, j = Atomic[a]->cardPos, min = j;
            // basically we need to setup the index array again
            if (Atomic[a]->cardNeg < min)
                min = Atomic[a]->cardNeg;
            int summin = 0;
            for (arrayindex_t k = 0; k < min; k++)
            {
                ATOM_SORTED_SUM(a, i) = i;
                ATOM_SORTED_SUM(a, j) = i;
                summin -= Atomic[a]->negMult[i];
                i++;
                j++;
            }
            while (i < Atomic[a]->cardPos)
            {
                ATOM_SORTED_SUM(a, i) = i;
                i++;
            }
            while (i < Atomic[a]->cardNeg)
            {
                ATOM_SORTED_SUM(a, j) = i;
                summin -= Atomic[a]->negMult[i];
                i++;
                j++;
            }
            Atomic[a]->sum = summin;
            std::sort(
                AtomicChange + a * CardAtomicChange,
                AtomicChange + a * CardAtomicChange + Atomic[a]->cardPos,
                [&](const unsigned int& x, const unsigned int& y)
                { return Atomic[a]->posMult[x] < Atomic[a]->posMult[y]; }
            );
            std::sort(
                AtomicChange + a * CardAtomicChange + Atomic[a]->cardPos,
                AtomicChange + a * CardAtomicChange + card,
                [&](const unsigned int& x, const unsigned int& y)
                { return Atomic[a]->negMult[x] < Atomic[a]->negMult[y]; }
            );
            nextStatebased++;
        }
    }
    allStatebased = nextStatebased == AtomicCount;
}

translation_result_t ReachabilitySafe::create_whole_formula()
{
    if (!allStatebased)
        updateAtomics();
    //  Reset some helper fields
    step = 0;
    // Declare variables for fire/copy decision for minisat
    for (arrayindex_t i = 0; i < PathLength * TransitionCount; i++)
        S->newVar();
    // Can save some function calls later down the line if we use the functions for StateBased when
    // appropriate
    if (allStatebased)
    {
        // Create initial formula which also initializes the remaining helper fields
        ReachabilitySafeStateBased::create_initial();
        // Create phi^t for every transition for every 'major step'
        for (arrayindex_t k = 0; k < PathLength; k++)
        {
            for (arrayindex_t t = 0; t < TransitionCount; t++)
            {
                ReachabilitySafeStateBased::create_phi(TRANSITION_ORD(t));
            }
        }
    }
    else
    {
        // Create initial formula which also initializes the remaining helper fields
        create_initial();
        // Create phi^t for every transition for every 'major step'
        for (arrayindex_t k = 0; k < PathLength; k++)
        {
            for (arrayindex_t t = 0; t < TransitionCount; t++)
            {
                create_phi(t);
            }
        }
    }
    for (arrayindex_t a = 0; a < nextStatebased; a++)
    {
        create_statebased_atomic(ATOM_ORDER(a));
    }
    return create_spFormula();
}

void ReachabilitySafe::create_initial()
{
    // Well one could argue this is bad, having linear amount of function calls instead
    // of putting the loop in the function
    // Maybe a good compiler can optimize some of this, but this is just done since the
    // implementation for general nets uses create_initial_atomic, but with different input
    // parameters, since there is no ordering there Could maybe work on testing this, obligatory
    // todo
    for (arrayindex_t a = AtomicCount - 1; a >= nextStatebased; a--)
    {
        create_initial_atomic(ATOM_ORDER(a));
    }
    ReachabilitySafeStateBased::create_initial();
}

void ReachabilitySafe::create_initial_atomic(arrayindex_t a)
{
    // set values and create initial variables
    ATOM_VAR_COUNT(a) = 2;
    ATOM_NEG_OFFSET(a) = Atomic[a]->sum;
    // why this comparison here instead at a higher level, since this function is called multiple
    // times with the same result here? Branch prediction should cause low overhead and in this
    // function we need both AtomicHelper and Atomic, therefore good caching reduces memory
    // accesses. At higher levels in the code only one of the two is used at a time currently,
    // meaning the other needs to be fetched first
    if (RT::args.satvariablebound_given)
    {
        // upper_bound temporarily saved the estimate after a single round
        ATOM_ALLOWED_MAX(a
        ) = ceil(RT::args.satvariablebound_arg * PathLength * Atomic[a]->upper_bound);
    }
    arrayindex_t varindex = S->newVar();
    S->addClause(~mkLit(varindex));
    varindex = S->newVar();
    S->addClause(mkLit(varindex));
    ATOM_VAR_ADDRESS(a) = varindex;
}

void ReachabilitySafe::create_phi(arrayindex_t t)
{
    // See create_initial for the reasoning here
    for (arrayindex_t a = AtomicCount - 1; a >= nextStatebased; a--)
    {
        create_phi_atomic(ATOM_ORDER(a), t, false);
    }
    ReachabilitySafeStateBased::create_phi(TRANSITION_ORD(t));
}

void ReachabilitySafe::create_phi_atomic(arrayindex_t a, arrayindex_t t, bool willCopyAnyway)
{
    // now the fun begins
    // first we find the value for prev again, look explanation at TODO to see what this value is
    // used for
    arrayindex_t prev = VAR_ATOM(ATOM_VAR_ADDRESS(a), ATOM_VAR_COUNT(a) - 1);
    // only need to do stuff if t actually changes the sum
    if (!willCopyAnyway && ATOMCHANGE(a, t) != 0)
    {
        // first setup for step translation with some helping fields assumed to be present in the
        // macro
        bool isPos = ATOMCHANGE(a, t) > 0;
        arrayindex_t mult = ATOMCHANGE(a, t);
        if (!isPos)
        {
            mult = abs(ATOMCHANGE(a, t));
            // more values in negative direction mean greater offset to translate value<->index
            ATOM_NEG_OFFSET(a) -= mult;
        }
        arrayindex_t diff = ATOM_VAR_COUNT(a) - mult;
        arrayindex_t min = ATOM_VAR_COUNT(a);
        if (diff > 0)
            min = mult;
        if (RT::args.satvariablebound_given && (ATOM_ALLOWED_MAX(a) -= mult) < 0)
        {
            // if we restrict the amount of variables, its possible that we can't freely add new
            // variables

            // after updating ATOM_ALLOWED_MAX above, the field holds the amount of variables over
            // the limit, just with a minus before it
            arrayindex_t offset = -1 * ATOM_ALLOWED_MAX(a);
            // we then have to update the helping values, since some clauses and variables are not
            // added now, the code adding those should be skipped
            if (offset > min)
            {
                // if the amount not added skips (atleast) a full loop, we also need to update the
                // boundaries of other loops

                if (diff > 0)
                {
                    diff = ATOM_VAR_COUNT(a) - offset;
                }
                else
                {
                    diff = offset - mult;
                }
            }
            // then correctly update the amount of variables now
            ATOM_VAR_COUNT(a) += mult + ATOM_ALLOWED_MAX(a);
            // Since we now miss the clauses that implicitely set the topmost (bottommost) variable
            // to false (true), we add clauses explicitely for that
            if (isPos)
            {
                TRANSLATE_STEP(offset, 0, min, VAR_FIRE(step), isPos)
                S->addClause(~mkLit(VAR_ATOM(S->nVars() - 1, ATOM_VAR_COUNT(a) - 1)));
            }
            else
            {
                TRANSLATE_STEP(0, offset, min, VAR_FIRE(step), isPos)
                ATOM_NEG_OFFSET(a) -= ATOM_ALLOWED_MAX(a);
                S->addClause(mkLit(VAR_ATOM(S->nVars() - 1, 0)));
            }

            ATOM_ALLOWED_MAX(a) = 0;
        }
        else
        {
            // otherwise simply translate step
            TRANSLATE_STEP(0, 0, min, VAR_FIRE(step), isPos)
            ATOM_VAR_COUNT(a) += mult;
        }
        ATOM_VAR_ADDRESS(a) = S->nVars() - 1;
    }
    else
    {
        // otherwise just copy
        for (arrayindex_t i = ATOM_VAR_COUNT(a) - 1; i >= 0; i--, prev++)
        {
            arrayindex_t curr = S->newVar();
            S->addClause(mkLit(prev), ~mkLit(curr));
            S->addClause(~mkLit(prev), mkLit(curr));
        }
        ATOM_VAR_ADDRESS(a) = S->nVars() - 1;
    }
}

translation_result_t ReachabilitySafe::create_atomic(AtomicStatePredicate* f, vec<Lit>& lits)
{
    translation_result_t result = FORMULA_RESULT_INDETERMINATE;
    arrayindex_t a = -1;
    // need to find the atomic that is the current child so we can use its variable
    while (f != Atomic[++a])
        ;
    // macros should make things self-explanatory again
    if (ATOM_SATISFIED(a))
    {
        result = FORMULA_RESULT_SATISFIED;
    }
    else if (ATOM_UNSOLVABLE(a))
    {
        result = FORMULA_RESULT_UNSOLVABLE;
    }
    else if (ATOM_ALREADY_CALC(a))
    {
        lits.push(~mkLit(AddressOffset - Atomic[a]->magicnumber));
    }
    else if (ATOM_STATEBASED_DUPL(a))
    {
        Atomic[a]->magicnumber = -1 * (Atomic[Atomic[a]->magicnumber]->sum - f->threshold);
        lits.push(~mkLit(AddressOffset - Atomic[a]->magicnumber));
    }
    else
    {
        // here we know that A is PathBased
        if (ATOM_IS_DUPL(a))
            a = Atomic[a]->magicnumber;
        // convert threshold into index and check validity both at top and bottom
        if (f->threshold - ATOM_NEG_OFFSET(a) >= 0)
        {
            if (f->threshold < ATOM_VAR_COUNT(a) + ATOM_NEG_OFFSET(a) - 1)
            {
                // if valid, push literal
                lits.push(
                    ~mkLit(VAR_ATOM(ATOM_VAR_ADDRESS(a), f->threshold - ATOM_NEG_OFFSET(a) + 1))
                );
            }
            else
            {
                // if index too large, always satisfied
                result = FORMULA_RESULT_SATISFIED;
            }
        }
        else
        {
            // if index too low, never satisfied
            // use approximation for minimum to calculate when the index is valid
            // but if the minimum is 0, then it never changes, therefore threshold is never valid
            // index
            result = ATOM_MIN_VAL(a) <= 0
                ? FORMULA_RESULT_UNSOLVABLE
                : (Atomic[a]->sum - Atomic[a]->threshold) / ATOM_MIN_VAL(a) + 1;
        }
    }
    return result;
}
