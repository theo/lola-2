/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Implementation of some translation approach indepenedent functions
*/

#include <ReachabilitySAT/Reachability.h>
#include <chrono>

#define VAR_EXTRA(E) (E)

// Minisat Options:
// TODO: adapt code so Minisat Options work for Siphon property aswell
IntOption Reachability::verb = IntOption(
    "MAIN", "verb", "Verbosity level (0=silent, 1=some, 2=more).", 0, IntRange(0, 2)
);
BoolOption Reachability::pre = BoolOption(
    "MAIN", "pre", "Completely turn on/off any preprocessing.", true
);
StringOption Reachability::dimacs = StringOption(
    "MAIN", "dimacs", "If given, stop after preprocessing and write the result to this file."
);
IntOption Reachability::cpu_lim = IntOption(
    "MAIN", "cpu-lim", "Limit on CPU time allowed in seconds.\n", std::numeric_limits<int>::max(),
    IntRange(0, std::numeric_limits<int>::max())
);
IntOption Reachability::mem_lim = IntOption(
    "MAIN", "mem-lim", "Limit on memory usage in megabytes.\n", std::numeric_limits<int>::max(),
    IntRange(0, std::numeric_limits<int>::max())
);
// Other static members:
bool Reachability::didSetup = false;

void Reachability::setUpTranslation()
{
    if (!didSetup)
    {
        // parse options
        if (RT::args.satvariablebound_given)
        {
            if (RT::args.satvariablebound_arg < 0)
            {
                RT::args.satvariablebound_arg = 0;
            }
            else if (RT::args.satvariablebound_arg > 1)
            {
                RT::args.satvariablebound_arg = 1;
            }
        }
        char* original;
        if (RT::args.minisatargs_given)
        {
            original = RT::args.minisatargs_arg;
        }
        else
        {
            original = new char[1];
            original[0] = '\0';
        }
        // parse minisat options string
        int argcc = 0;
        char* passed_string = new char[strlen(original) + 5];
        strcpy(passed_string, "x x ");  // dummy file name
        strcpy(passed_string + 4, original);
        for (int i = 0; i < strlen(passed_string); i++)
        {
            if (passed_string[i] > ' ' && passed_string[i + 1] <= ' ')
            {
                // count ends of visible char sequences
                argcc++;
            }
        }
        char** argvv = new char*[argcc];
        int argnr = 0;
        int j = strlen(passed_string);
        for (int i = 0; i < j; i++)
        {
            if (i == 0 || (passed_string[i] > ' ' && passed_string[i - 1] == '\0'))
            {
                argvv[argnr++] = passed_string + i;
            }
            if (passed_string[i] <= ' ')
            {
                passed_string[i] = '\0';
            }
        }
        // parse minisat options
        parseOptions(argcc, argvv, true);
        didSetup = true;
    }
}

translation_result_t Reachability::create_spFormula()
{
    translation_result_t result;
    vec<Lit> lits;
    bool addLits = false;
    if (spFormula->isatomic)
    {
        addLits = true;
        result = create_atomic((AtomicStatePredicate*)spFormula, lits);
    }
    else if (spFormula->isfireable)
    {
        FireablePredicate* f = (FireablePredicate*)(spFormula);
        // only need translation for ordered transitions
        if (TRANSITION_IS_ORDERED(f->t))
        {
            if (net->CardArcs[TR][PRE][f->t])
            {
                // current implementations work more efficient if lits
                // is used if t has only one pre-place
                addLits = net->CardArcs[TR][PRE][f->t] == 1;
                if (f->sign)
                {
                    result = create_fireable(f->t, lits, 0);
                }
                else
                {
                    addLits = true;
                    result = create_notfireable(f->t, lits);
                }
            }
            else
            {
                // hacky method to calculate result for transitions with no pre-places
                result = FORMULA_RESULT_UNSOLVABLE + f->sign;
            }
        }
        else
        {
            // hacky method to calculate result based on sign in unordered transitions
            result = FORMULA_RESULT_SATISFIED - f->sign;
        }
    }
    else if (spFormula->magicnumber == MAGIC_NUMBER_DEADLOCK)
    {
        result = create_deadlock(0);
    }
    else if (spFormula->magicnumber == MAGIC_NUMBER_NODEADLOCK)
    {
        // create_nodeadlock is basically Fireable over any transition
        // to represent this "any", Fireable-representing literals are saved
        // to lits
        addLits = true;
        result = create_nodeadlock(lits);
    }
    else
    {
        // now only and/or are left
        AtomicBooleanPredicate* b = (AtomicBooleanPredicate*)spFormula;
        // if the formula is and, we need to add a clause for every subformula
        if (b->isAnd)
        {
            result = create_and(b, 0);
        }
        // if the formula is or, we need to add one clause, literals being atomics or variables for
        // subformulae
        else
        {
            addLits = true;
            result = create_or(b, lits);
        }
    }
    // preliminary result handling
    // UNKNOWN, SATISFIED and UNSOLVABLE let us end
    // INDETERMINATE may need lits vector add
    // Value > 0 may need minRounds update
    switch (result)
    {
    case FORMULA_RESULT_UNKNOWN:
    case FORMULA_RESULT_SATISFIED:
    case FORMULA_RESULT_UNSOLVABLE:
        break;
    case FORMULA_RESULT_INDETERMINATE:
        if (addLits)
            S->addClause(lits);
        break;
    default:
        minRounds = result > minRounds ? result : minRounds;
        break;
    }
    return result;
}

translation_result_t Reachability::create_deadlock(arrayindex_t extra)
{
    // we can initialize with SATISFIED, since not changing the value (meaning no transitions)
    // would mean that we have a deadlock
    translation_result_t deadlockResult = FORMULA_RESULT_SATISFIED;
    // AND (t=0..Card[TR]) OR (p^i from pre(t)) not p^i,W(p^i,t),step
    // a deadlock means that every transition is not fireable
    // so we add the clause for not fireable for every transition
    for (arrayindex_t i = 0; i < TransitionCount; i++)
    {
        arrayindex_t t = TRANSITION_ORD(i);
        translation_result_t fireableResult;
        vec<Lit> lits;
        // similar to spFormula, with the exception that sign is always false
        if (net->CardArcs[TR][PRE][t])
        {
            fireableResult = create_notfireable(t, lits);
        }
        else
        {
            fireableResult = FORMULA_RESULT_UNSOLVABLE;
        }
        // SATISFIED is nice, but all need to be satisfied, therefore we just continue
        // if all are SATISFIED, then the default deadlockResult isn't changed, therefore
        // also SATISFIED
        // UNSOLVABLE means the transition is always activated, meaning no deadlock possible
        // INDETERMINATE may require renamed liteal add
        switch (fireableResult)
        {
        case FORMULA_RESULT_SATISFIED:
            continue;
        case FORMULA_RESULT_UNSOLVABLE:
            deadlockResult = FORMULA_RESULT_UNSOLVABLE;
            break;
        case FORMULA_RESULT_INDETERMINATE:
            if (extra)
            {
                lits.push(~mkLit(VAR_EXTRA(extra)));
            }
            S->addClause(lits);
            // Here deadlockResult is either INDETERMINATE (=0), SATISFIED (=-1) or >0
            // We don't want to update in the latter case, since we know the deadlock is currently
            // not solvable but we still want to find the highest round where it could be
            if (deadlockResult < 0)
                deadlockResult = FORMULA_RESULT_INDETERMINATE;
            continue;
        default:
            deadlockResult = fireableResult > deadlockResult ? fireableResult : deadlockResult;
            continue;
        }
        break;
    }
    return deadlockResult;
}

translation_result_t Reachability::create_nodeadlock(vec<Lit>& lits)
{
    // OR (t=0..Card[TR]) AND (p^i from pre(t)) p^i,W(p^i,t),step
    // no deadlock means that atleast one transition can fire
    // that means introducing new variables for every t and adding them to a clause
    // then add clauses fire^t -> fireable(t)
    // we initialize with UNSOLVABLE, because not having any transition would leave us with an
    // unsovable deadlock,W
    translation_result_t noDeadlockResult = FORMULA_RESULT_UNSOLVABLE;
    for (arrayindex_t i = 0; i < TransitionCount; i++)
    {
        arrayindex_t t = TRANSITION_ORD(i);
        translation_result_t fireableResult;
        arrayindex_t renamedFireable = 0;
        if (net->CardArcs[TR][PRE][t])
        {
            // only need to rename for more than one pre-place
            // current implementations only need one variable to describe that the tokencount of a
            // single pre-place is greater than the multiplicity of the arc to t
            if (net->CardArcs[TR][PRE][t] > 1)
            {
                renamedFireable = S->newVar();
            }
            fireableResult = create_fireable(t, lits, renamedFireable);
        }
        else
        {
            fireableResult = FORMULA_RESULT_SATISFIED;
        }
        // UNSOLVABLE not problematic, since only one transition needs to work
        // SATISFIED means one transition is always activated, meaning no deadlocks possible
        switch (fireableResult)
        {
        case FORMULA_RESULT_UNSOLVABLE:
            if (renamedFireable)
            {
                S->addClause(~mkLit(VAR_EXTRA(renamedFireable)));
            }
            continue;
        case FORMULA_RESULT_SATISFIED:
            noDeadlockResult = FORMULA_RESULT_SATISFIED;
            break;
        case FORMULA_RESULT_INDETERMINATE:
            if (renamedFireable)
            {
                lits.push(mkLit(VAR_EXTRA(renamedFireable)));
            }
            noDeadlockResult = FORMULA_RESULT_INDETERMINATE;
            continue;
        default:
            // at this point noDeadlockResult is either UNSOLVABLE (=-2), INDETERMINATE (=0) or >0
            // if we already found the nodeadlock to maybe be solvable (INDETERMINATE), then we
            // don't care if this fireable is currently not solvable, so we don't update then
            if (noDeadlockResult != FORMULA_RESULT_INDETERMINATE && fireableResult > 0)
            {
                noDeadlockResult = noDeadlockResult < 0 || fireableResult < noDeadlockResult
                    ? fireableResult
                    : noDeadlockResult;
            }
            continue;
        }
        break;
    }
    return noDeadlockResult;
}

translation_result_t Reachability::create_and(AtomicBooleanPredicate* formula, arrayindex_t extra)
{
    // so this may be wrong, empty and only happens if the top-level formula is empty
    // there are some examples, where this means UNSOLVABLE, some where this means SATISFIED
    // therefore I can only be wrong here ¯\_(ツ)_/¯
    if (!(formula->cardSub))
        return FORMULA_RESULT_SATISFIED;
    // The stuff below is basically create_spFormula() but just for a list of predicates
    translation_result_t andResult = FORMULA_RESULT_SATISFIED;
    for (arrayindex_t child = 0; child < formula->cardSub; child++)
    {
        translation_result_t childResult;
        vec<Lit> lits;
        bool addLits = false;
        if (formula->sub[child]->isatomic)
        {
            addLits = true;
            childResult = create_atomic((AtomicStatePredicate*)(formula->sub[child]), lits);
        }
        else if (formula->sub[child]->isfireable)
        {
            FireablePredicate* f = (FireablePredicate*)(formula->sub[child]);
            if (TRANSITION_IS_ORDERED(f->t))
            {
                if (net->CardArcs[TR][PRE][f->t])
                {
                    addLits = net->CardArcs[TR][PRE][f->t] == 1;
                    if (f->sign)
                    {
                        childResult = create_fireable(f->t, lits, extra);
                    }
                    else
                    {
                        addLits = true;
                        childResult = create_notfireable(f->t, lits);
                    }
                }
                else
                {
                    childResult = FORMULA_RESULT_UNSOLVABLE + f->sign;
                }
            }
            else
            {
                childResult = FORMULA_RESULT_SATISFIED - f->sign;
            }
        }
        else if (formula->sub[child]->magicnumber == MAGIC_NUMBER_DEADLOCK)
        {
            childResult = create_deadlock(extra);
        }
        else if (formula->sub[child]->magicnumber == MAGIC_NUMBER_NODEADLOCK)
        {
            addLits = true;
            childResult = create_nodeadlock(lits);
        }
        else
        {
            AtomicBooleanPredicate* b = (AtomicBooleanPredicate*)formula->sub[child];
            if (b->isAnd)
            {
                childResult = create_and(b, extra);
            }
            else
            {
                addLits = true;
                childResult = create_or(b, lits);
            }
        }
        switch (childResult)
        {
        case FORMULA_RESULT_SATISFIED:
            continue;
        case FORMULA_RESULT_UNSOLVABLE:
            andResult = FORMULA_RESULT_UNSOLVABLE;
            if (extra)
            {
                S->addClause(~mkLit(VAR_EXTRA(extra)));
            }
            break;
        case FORMULA_RESULT_INDETERMINATE:
            if (addLits)
            {
                if (extra)
                {
                    lits.push(~mkLit(VAR_EXTRA(extra)));
                }
                S->addClause(lits);
            }
            if (andResult < 0)
                andResult = FORMULA_RESULT_INDETERMINATE;
            continue;
        default:
            andResult = childResult > andResult ? childResult : andResult;
            continue;
        }
        break;
    }
    return andResult;
}

translation_result_t Reachability::create_or(AtomicBooleanPredicate* b, vec<Lit>& lits)
{
    // similar to and, this is sometimes wrong, sometimes right ¯\_(ツ)_/¯
    if (!(b->cardSub))
        return FORMULA_RESULT_UNSOLVABLE;
    translation_result_t orResult = FORMULA_RESULT_UNSOLVABLE;
    // add literal for every sub
    for (arrayindex_t subChild = 0; subChild < b->cardSub; subChild++)
    {
        translation_result_t childResult;
        arrayindex_t renamedSub = 0;
        if (b->sub[subChild]->isatomic)
        {
            childResult = create_atomic((AtomicStatePredicate*)(b->sub[subChild]), lits);
        }
        else if (b->sub[subChild]->isfireable)
        {
            FireablePredicate* f = (FireablePredicate*)(b->sub[subChild]);
            // similar spiel as before, but for a parent or we always want to add a literal
            // representing the (not)fireable, so no need to set that
            if (TRANSITION_IS_ORDERED(f->t))
            {
                if (net->CardArcs[TR][PRE][f->t])
                {
                    if (f->sign)
                    {
                        if (net->CardArcs[TR][PRE][f->t] > 1)
                            renamedSub = S->newVar();
                        childResult = create_fireable(f->t, lits, renamedSub);
                    }
                    else
                    {
                        childResult = create_notfireable(f->t, lits);
                    }
                }
                else
                {
                    childResult = FORMULA_RESULT_UNSOLVABLE + f->sign;
                }
            }
            else
            {
                childResult = FORMULA_RESULT_SATISFIED - f->sign;
            }
        }
        else if (b->sub[subChild]->magicnumber == MAGIC_NUMBER_DEADLOCK)
        {
            // special case, since a net with one transition has a deadlock if that transition is
            // not fireable
            // since notfireable is also a clause, we can just add that clause to the parent or
            // and save on renaming
            if (net->Card[TR] == 1)
            {
                if (TRANSITION_IS_ORDERED(0))
                {
                    childResult = create_notfireable(0, lits);
                }
                else
                {
                    childResult = FORMULA_RESULT_SATISFIED;
                }
            }
            else
            {
                // since we're at or level and want to add a set of clauses, we need to rename
                renamedSub = S->newVar();
                childResult = create_deadlock(renamedSub);
            }
        }
        else if (b->sub[subChild]->magicnumber == MAGIC_NUMBER_NODEADLOCK)
        {
            childResult = create_nodeadlock(lits);
        }
        else
        {
            AtomicBooleanPredicate* bb = (AtomicBooleanPredicate*)b->sub[subChild];
            if (bb->isAnd)
            {
                // and with parent or needs renaming for CNF
                renamedSub = S->newVar();
                childResult = create_and(bb, renamedSub);
            }
            else
            {
                childResult = create_or(bb, lits);
            }
        }
        // UNSOLVABLE isn't problematic, since only one child needs to work
        // SATISFIED already satisfies the OR
        // INDETERMINATE and >0 use similar arguments as create_nodeadlock, since that
        // is essentially also an OR
        switch (childResult)
        {
        case FORMULA_RESULT_UNSOLVABLE:
            if (renamedSub)
            {
                S->addClause(~mkLit(VAR_EXTRA(renamedSub)));
            }
            continue;
        case FORMULA_RESULT_SATISFIED:
            orResult = FORMULA_RESULT_SATISFIED;
            break;
        case FORMULA_RESULT_INDETERMINATE:
            if (renamedSub)
            {
                lits.push(mkLit(VAR_EXTRA(renamedSub)));
            }
            orResult = FORMULA_RESULT_INDETERMINATE;
            continue;
        default:
            if (orResult != FORMULA_RESULT_INDETERMINATE && childResult > 0)
            {
                orResult = orResult < 0 || childResult < orResult ? childResult : orResult;
            }
            continue;
        }
        break;
    }
    return orResult;
}

reachability_result_t Reachability::lola2minisat(int round)
{
    try
    {
        setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may "
                     "be either in plain or gzipped DIMACS.\n");
        setX86FPUPrecision();

        // Get Value for PathLength
        PathLength = round;

        // create Solver
        S = new SimpSolver();
        double initial_time = cpuTime();

        if (!pre)
            S->eliminate(true);

        S->verbosity = verb;

        // Set limit on CPU-time:
        if (cpu_lim != std::numeric_limits<int>::max())
        {
            limitTime(cpu_lim);
        }

        // Set limit on virtual memory:
        if (mem_lim != std::numeric_limits<int>::max())
        {
            limitMemory(mem_lim);
        }

        double pretime = cpuTime();
        // record start and translation time for heuristics
        auto start_time = high_resolution_clock::now();
        translation_result_t preliminary_result = create_whole_formula();
        auto translation_time = high_resolution_clock::now();
        RT::log(
            "STP: formula for # {} with {} variables and {} clauses shipped to Minisat",
            portfolio_id, S->nVars(), S->nClauses()
        );

        std::ostringstream stats;
        if (dimacs)
        {
            S->toDimacs((const char*)dimacs);
            if (S->verbosity > 1)
            {
                stats << "==============================[ Writing DIMACS "
                         "]===============================\n"
                      << std::flush;
                const std::string result = stats.str();
                RT::log(result);
                S->printStats();
            }
            delete S;
            return REACHABILITY_PROPERTY_INDETERMINATE;
        }
        // only need to ask minisat if preliminary result is INDETERMINATE
        switch (preliminary_result)
        {
        case FORMULA_RESULT_INDETERMINATE:
            break;
        case FORMULA_RESULT_SATISFIED:
            delete S;
            return REACHABILITY_PROPERTY_TRUE;
        case FORMULA_RESULT_UNSOLVABLE:
            delete S;
            return REACHABILITY_PROPERTY_FALSE;
        case FORMULA_RESULT_UNKNOWN:
        default:
            delete S;
            return REACHABILITY_PROPERTY_INDETERMINATE;
        }

        // simplification
        double parsed_time = cpuTime();
        S->eliminate(true);
        double simplified_time = cpuTime();
        if (S->verbosity > 1)
        {
            stats << "\n============================[ Problem Statistics "
                     "]=============================\n"
                  << "|                                                                            "
                     " |\n"
                  << "|  Number of variables:  " << std::setw(12) << S->nVars()
                  << "                                         |\n"
                  << "|  Number of clauses:    " << std::setw(12) << S->nClauses()
                  << "                                         |\n"
                  << "|  Parse time:           " << std::setw(12) << std::setprecision(2)
                  << parsed_time - initial_time << " s                                       |\n"
                  << "|  Simplification time:  " << std::setw(12) << std::setprecision(2)
                  << simplified_time - parsed_time << " s                                       |\n"
                  << "|                                                                            "
                     " |\n";
        }

        if (!S->okay())
        {
            if (S->verbosity > 1)
            {
                stats << "========================================================================="
                         "======\n"
                      << "Solved by simplification" << std::flush;
                const std::string result = stats.str();
                RT::log(result);
                S->printStats();
            }

            delete S;
            return REACHABILITY_PROPERTY_INDETERMINATE;
        }

        vec<Lit> dummy;
        // solve through minisat
        lbool ret = S->solveLimited(dummy);
        auto solving_time = high_resolution_clock::now();

        stats << std::flush;
        const std::string prelim = stats.str();
        RT::log(prelim);
        stats.str("");
        stats.clear();
        if (S->verbosity > 1)
        {
            S->printStats();
        }

        // only if not found we can maybe skip some rounds
        if (ret != l_True)
        {
            if (RT::args.satskip_given)
            {
                duration<double, std::micro> last_trans_time = translation_time - start_time;
                duration<double, std::micro> last_solve_time = solving_time - translation_time;
                setRoundSkip(last_trans_time.count(), last_solve_time.count());
            }
            delete S;
            return REACHABILITY_PROPERTY_INDETERMINATE;
        }
        assert(ret == l_True);

        if (RT::args.satoutput_given)
            printAssignments(stats);
        const std::string result = stats.str();
        RT::log(result);

        delete S;
        return REACHABILITY_PROPERTY_TRUE;
        // #ifdef NDEBUG
        //         exit(ret == l_True ? 10 : ret == l_False ? 20 : 0);     // (faster than "return",
        //         which will invoke the destructor for 'Solver')
        // #else
        //         return (ret == l_True ? 10 : ret == l_False ? 20 : 0);
        // #endif
    }
    catch (OutOfMemoryException&)
    {
        RT::log("===============================================================================");
        RT::log("INDETERMINATE");
        exit(0);
    }
}

void Reachability::setRoundSkip(const double t, const double s)
{
    // if the solve time was basically 0, then we don't need to worry about that and can easily
    // solve another round with probably no real difference, therefore skipping rounds could result
    // in longer calculation
    if (t != 0 && s != 0)
    {
        double ratio = t / s;
        // if ratio is greater than ~1, translation takes longer than solving a non-solution round,
        // therefore we would rather solve than skip rounds to account for solution round being
        // close to current round
        if (ratio < 1 && ratio > 0)
        {
            ratio = ratio > 0.1 ? ratio : 0.1;
            ratio = floorf(ratio * 10) / 10;
            // solving now takes longer than translating rounds, therefore we could just translate
            // more rounds and hope to avoid having to solve more non-solution rounds the amount of
            // rounds to skip depends on the ratio and the current round, if the current round is
            // high and the ratio small, we can skip more rounds than if the round is small and
            // ratio large or close to 1 \todo exact addition may be adjusted with constants, needs
            // testing
            arrayindex_t nextround = ceil(RT::args.satratio_arg * PathLength / sqrt(ratio))
                - skippedRounds;
            skippedRounds += nextround - PathLength;
            if (RT::args.satrounds_arg != 0 && nextround >= RT::args.satrounds_arg)
            {
                minRounds = RT::args.satrounds_arg;
            }
            else
            {
                minRounds = nextround;
            }
            RT::log(
                "round skipping for {}:\nlast translation time: {}\nlast solving time: {}\nratio: "
                "{}\nskipping from round {} to round {}",
                portfolio_id, t, s, ratio, PathLength, minRounds
            );
        }
    }
}
