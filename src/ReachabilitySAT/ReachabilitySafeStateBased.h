/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a save Petri net to a boolean formula where AtomicStatePredicates
are calculated based on the final encoded marking.
*/

#pragma once

#include <ReachabilitySAT/Reachability.h>

/**
 * General info on how variables in Minisat are managed:
 * -Variables are addressed through positive (incl 0) integers
 * -Using them in clauses requires the conversion to a literal (in theory also represented through
 * ints) -Creating the literal requires the address
 *
 * How are addresses calculated here?
 * -In some cases the clause can be instantly created after creating the variable. Creating a
 * variable returns its address -In other cases the address needs to be calculated -Addresses are
 * organized as follows: -The first PathLength * TransitionCount addresses are reserved for firing
 * transitions -The variables for steps start with those for atomics (if calculated along the path),
 * with atomics in reserved order and values in reversed order, meaning the value for
 * Atomic[AtomicCount-1]->max (or w/e) has the lowest address -After atomics follow places in the
 * same manner, reversed order for places and their values (values may simply be one variable for
 * marked/unmarked for safe nets) -In safe nets, the atomics calculated based on the final marking
 * follow after all steps, again same reversed order -Finally, any variables arising from renaming
 * subformulas while translating spFormula follow then
 *
 * Why organize addresses in reverse order?
 * -Minisat already keeps track of the overall amount of variables. Therefore calculating addresses
 * based on that (or a given startpoint for the previous step) is easier and more efficient than
 * keeping track of variable counts and starting points for two steps (the last full complete and
 * the next, currently being calculated one)
 *
 */

// macros for more readable code
// gives meaning to different encodings in some fields
#define ATOM_SORTED_SUM(A, I) (AtomicChange[A * CardAtomicChange + I])
#define ATOM_NOT_CALC_YET(A) (Atomic[A]->magicnumber == 0)
#define ATOM_ALREADY_CALC(A) (Atomic[A]->magicnumber < 0)
#define ATOM_SATISFIED(A) (Atomic[A]->magicnumber == -1)
#define ATOM_UNSOLVABLE(A) (Atomic[A]->magicnumber == -2)
#define ATOM_IS_DUPL(A) (a >= AtomicCount)

// questionable macro I know
// used to fill the code to translate some "step", for example firing a transition
// or changing the value of a sum based on marked places
//
// The macro takes advantage of the fact that the code for these steps is basically
// the same barring some different values in the various fields
// Why not a function? The macro uses more than the 5 input parameters if it were
// a function and since this is called pretty deep in a loop, it would cause a lot
// of overhead
//
// Why does this work? Remember that we look at some "thing" with a value range
// (so the possible amount of tokens on a place or the possible value of a formal sum)
// and we want to calculate how this range changes through a step and how values from
// one step are mapped to the next.
//
// For this, we first assume variables prev and diff to be set beforehand. prev describes
// the address of the highest value of the range in the previous step (meaning the lowest
// address in the corresponding list of addresses for the range of values, remember
// above for address organisation). diff describes the difference between the amount
// of variables in the previous step and the amount that gets added through the step. Meaning
// if we have x different possible values now and the step add y more values in either direction,
// then diff = x - y (diff is therefore assumed to be positive if we add more values than
// previously existed while it is negative otherwise). Further, M is assumed to be the smaller
// value between x and y
//
// Then we can differentiate four types of variables in the next step, all handled in the loops
// 1. Variables which gain their value through the values of variables of the previous steps when
//    firing and which are set independently when copying
// 2. Variables which gain their value through the values of variables of the previous steps both
//    through firing and copying, just from different variables depending on firing/copying
// 3. Variables which gain their value independently and which are set to 0/1 based on
// firing/copying alone
// 4. Variables which gain their value through the values of variables of the previous steps when
//    copying and which are set independently when firing
// The variables for cases 1 and 4 are flipped in the range based on positive or negative change,
// but since firing/copying is signaled through a single variable, we can handle both cases in a
// single loop
//
// The first loop deals with case 1 (positive change) and case 4 (negative change)
// The second and third loops deal with cases 2 and 3 respectively
// The last loop deals with case 1 (negative change) and case 4 (positive change)
//
// The Variables T and B are offsets for Topcutoff and Bottomcutoff. When dealing with artificial
// limitation of variables, we cut off some variables at the top end (positive change) or bottom end
// (negative change). This also assumes a different calculation of diff whenever the cutoff effects
// variables from cases 2 and 3
//
// Finally, X describes the address of the variable indicating we fire or copy (so variable for a
// transition or for a place in changing a formal sum). So far, copying means variable is false,
// while firing means true For better understanding "mkLit(X, !isPos)" means "if we fire with
// negative change" while "mkLit(X, isPos)" means "if we fire with positive change". Copying is then
// the reversed case
#define TRANSLATE_STEP(T, B, M, X, isPos)                                                      \
    /* deal with case 1 (pos) and case 4 (neg), throughout all iterations, */                  \
    /* prev is set to lowest address (highest value) in previous step */                       \
    for (arrayindex_t __i = prev + T; __i < M + prev; __i++)                                   \
    {                                                                                          \
        /* create new variables -> starts with lowest address (lowest value) */                \
        arrayindex_t curr = S->newVar();                                                       \
        /* value of new variable is 0 if copy with pos or fire with neg change*/               \
        S->addClause(~mkLit(curr), mkLit(X, !isPos));                                          \
        /* value gets linked to previous variable if fire with pos or copy with neg change */  \
        S->addClause(mkLit(__i), ~mkLit(curr), mkLit(X, isPos));                               \
        S->addClause(~mkLit(__i), mkLit(curr), mkLit(X, isPos));                               \
    }                                                                                          \
    /* deal with case 2 */                                                                     \
    /* prev is set to lowest address (highest value) in previous step, but gets incremented */ \
    /* of cases 2 and 3 only one happens in practice */                                        \
    for (arrayindex_t __i = 0; __i < diff; __i++)                                              \
    {                                                                                          \
        arrayindex_t curr = S->newVar();                                                       \
        /* value gets linked to previous variable */                                           \
        S->addClause(mkLit(prev), ~mkLit(curr), mkLit(X, !isPos));                             \
        S->addClause(~mkLit(prev), mkLit(curr), mkLit(X, !isPos));                             \
        S->addClause(mkLit(prev + mult), ~mkLit(curr), mkLit(X, isPos));                       \
        S->addClause(~mkLit(prev + mult), mkLit(curr), mkLit(X, isPos));                       \
        prev++;                                                                                \
    }                                                                                          \
    /* deal with case 3 */                                                                     \
    for (arrayindex_t __i = 0; __i > diff; __i--)                                              \
    {                                                                                          \
        arrayindex_t curr = S->newVar();                                                       \
        S->addClause(mkLit(curr, isPos), mkLit(X));                                            \
        S->addClause(mkLit(curr, !isPos), ~mkLit(X));                                          \
    }                                                                                          \
    /* deal with case 1 (neg) and case 4 (pos) */                                              \
    /* in case of case 3 happening, prev is the same value as for the first loop, */           \
    /* while with case 2 happening, prev is adjusted accordingly through second loop */        \
    /* at the end, prev is set to the appropiate value for the next call of the macro (in some \
     * cases) */                                                                               \
    for (arrayindex_t __i = B; __i < M; __i++, prev++)                                         \
    {                                                                                          \
        arrayindex_t curr = S->newVar();                                                       \
        S->addClause(mkLit(curr), mkLit(X, isPos));                                            \
        S->addClause(mkLit(prev), ~mkLit(curr), mkLit(X, !isPos));                             \
        S->addClause(~mkLit(prev), mkLit(curr), mkLit(X, !isPos));                             \
    }

class ReachabilitySafeStateBased : public Reachability
{
public:
    ReachabilitySafeStateBased(Petrinet* n, StatePredicate* f, int p);
    virtual ~ReachabilitySafeStateBased();
    virtual void PreProcessAtomics();

protected:
    /// @brief Helpful array, saving a bool for each place. Used to determine if steps are
    /// calculated already for a given place or if we've seen it when ordering transitions
    bool* Computed;

    /// @brief Saves the beginning Address for the last completely calculated step
    // used to relate currently calculated steps to previous ones or to find addresses for places
    // when calculating atomics
    arrayindex_t AddressOffset;

    /// Helperfields for Atomics

    /// @brief Keeps track of atomic pointers, needed to deal with duplicates easier
    AtomicStatePredicate** Atomic;
    /// @brief Helper array to save how to "change" the formal sum to sort it by multiplicities
    // implemented as a 1D array, with the help of macros addressed like a 2D array to save time on
    // memory accesses size is #Atomic * max(length(Atomic->sum))
    int* AtomicChange;
    /// @brief Saves max(length(Atomic->sum)) for convenience
    arrayindex_t CardAtomicChange;
    /// Amount of unique atomic predicates, only saved to reduce redundant calculations
    arrayindex_t AtomicCount;

    virtual translation_result_t create_whole_formula();
    // creating a SAT formula here is split into creating the initial marking, creating steps and
    // creating the atomics
    virtual void create_initial();
    virtual void create_phi(arrayindex_t t);
    virtual void create_statebased_atomic(arrayindex_t a);
    // followed by creating spFormula
    virtual translation_result_t create_atomic(AtomicStatePredicate* f, vec<Lit>& lits);
    virtual translation_result_t create_fireable(
        arrayindex_t t, vec<Lit>& lits, arrayindex_t extra
    );
    virtual translation_result_t create_notfireable(arrayindex_t t, vec<Lit>& lits);
    void printAssignments(std::ostringstream& result);
    /// @brief Helperfunction to find out if formal sums of AtomicStatePredicates are equal
    /// @param a Pointer to first predicate
    /// @param b Pointer to second predicate
    /// @return True if sums are equal
    bool areFormalSumsEqual(AtomicStatePredicate* a, AtomicStatePredicate* b);
};
