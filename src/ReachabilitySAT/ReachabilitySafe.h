/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a save Petri net to a boolean formula, combining two methods to calculate
AtomicStatePredicates
*/

#pragma once

#include <ReachabilitySAT/ReachabilitySafeStateBased.h>

/**
 * Which two methods are implemented here?
 * 1) Calculating the sum based on the final marking (see StateBased)
 * 2) Calculating the initial value of the sum and looking at the change by firing transitions (see
 * PathBased)
 *
 * How do we decide which to choose?
 * - The amount of variables needed to encode in StateBased is only dependent on the sum, meaning
 * places and their constants
 * - The amount of variables needed for PathBased also depends on the PathLength and the amount of
 * transitions (or rather how they change the sum by firing)
 * --> There should be one value for PathLength for each sum, where StateBased needs fewer variables
 * (and clauses)
 *
 * How is that value estimated?
 * - Values for the sum can be interpreted like a funnel (or triangle)
 * (Starting with values [1,0] (not >= 1, >= 0), then adding addtional values over time)
 * ^
 * |             4  4  4  4  4  4
 * |       3  3  3  3  3  3  3  3
 * |    2  2  2  2  2  2  2  2  2
 * | 1  1  1  1  1  1  1  1  1  1
 * | 0  0  0  0  0  0  0  0  0  0
 * |         -1 -1 -1 -1 -1 -1- 1
 * |               -2 -2 -2 -2 -2
 * |                  -3 -3 -3 -3
 * |                     -4 -4 -4
 * +-------------------------------> "Time" (Transitions firing, places contributing)
 *
 * - The amount of variables should therefore be proportional to the area of that "triangle/funnel":
 * ^
 * |                      4  4  4
 * |                   4  3  3  3
 * |                4  3  2  2  2
 * |             4  3  2  1  1  1
 * |          3  3  2  1  0  0  0
 * |       3  2  2  1  0 -1 -1 -1
 * |    2  2  1  1  0 -1 -2 -2 -2
 * | 1  1  1  0  0 -1 -2 -3 -3 -3
 * | 0  0  0 -1 -1 -2 -3 -4 -4 -4
 * +-------------------------------> "Time" (Transitions firing, places contributing)
 *
 * - This area is always 0.5 * #"time"steps * #Values at the end
 *  1) For StateBased "time" refers to the amount of summands and #Values is max-min
 *  2) For PathBased "time" refers to PathLength * #Transitions and #Values is #Change per round *
 * PathLength
 * - Equating both and solving for PathLength gives
 *   PathLength = sqrt(len(sum) * (max - min) / (#Transitions * #Change per round))
 * - Further:
 *  1) StateBased sorts the places by multiplicity, therefore the amount of variables being added
 * over time is always growing, at most constant. Therefore the aforementioned "area" is always
 * over-approximated by the "triangle" 2) PathBased does not do any sorting by #Change per
 * transition, meaning a lot of values could be added early, resulting in the "area" being closer to
 * a rectangle, where the "triangle" always under-approximates this:
 *     ^
 *     |       4  4  4  4  4  4  4  4
 *     |       3  3  3  3  3  3  3  3
 *     |       2  2  2  2  2  2  2  2
 *     |       1  1  1  1  1  1  1  1
 *     |    4  0  0  0  0  0  0  0  0
 *     |    3 -1 -1 -1 -1 -1 -1 -1 -1
 *     |    2 -2 -2 -2 -2 -2 -2 -2 -2
 *     | 1  1 -3 -3 -3 -3 -3 -3 -3 -3
 *     | 0  0 -4 -4 -4 -4 -4 -4 -4 -4
 *     +-------------------------------> Transitions firing
 *
 * todo: add experiments to find a factor to multiply into the equation to help with this bias
 *
 */

// macros for more readable code
// gives meaning to different encodings in some fields
#define ATOMCHANGE(A, I) (AtomicChange[A * CardAtomicChange + I])
// True if A is duplicate of a sum calculated based on the final marking, False otherwise
#define ATOM_STATEBASED_DUPL(A) \
    (A >= AtomicCount && Atomic[Atomic[A]->magicnumber]->magicnumber < 0)
// Just to avoid repeated code
#define ATOM_ROW_COUNT(A) (A << 2)
// The macros below are used for sums which are calculated based on initial value and change over
// time Saves for a given sum index A the amount of variables used to encode the most recent fully
// completed step
#define ATOM_VAR_COUNT(A) (AtomicHelper[ATOM_ROW_COUNT(A)])
// Saves for a given sum index A the offset to convert a sum value into an index in a theoretical
// "value array" Name stems from the idea that this is an offset in the negative direction if all
// possible values are positive i.e. value [7,6,5] for the sum get mapped to indices [2,1,0] through
// neg_offset=5 I know the name doesn't make sense when talking about negative values, but I made
// this analogous to places, which don't have negative values and I'm too lazy to change it
// ¯\_(ツ)_/¯
#define ATOM_NEG_OFFSET(A) (AtomicHelper[ATOM_ROW_COUNT(A) + 1])
// For easier calculation we save the address where the value for A start in the most recent fully
// completed step
#define ATOM_VAR_ADDRESS(A) (AtomicHelper[ATOM_ROW_COUNT(A) + 2])
// We also save the maximum size of different values for a sum, this maximum can be reduced based on
// the theoretical maximum (calculated in preprocessing) with a commandline parameter
#define ATOM_ALLOWED_MAX(A) (AtomicHelper[ATOM_ROW_COUNT(A) + 3])
// For some calculations dealing with contradictions at translation time we save a theoretical min
// value of the sum
#define ATOM_MIN_VAL(A) (Atomic[A]->lower_bound)
// For consistency (and ease of calculation) we order the atomics based on the round we change their
// calculation For an index A this returns the sum index at that position Why save this in Atomic[A]
// if it's not A? At times where this macro is used we need Atomic anyway, so assuming good caching
// we can access them as fast as separate saving, while not wasting space by allocating a separate
// array
#define ATOM_ORDER(A) (Atomic[A]->cardUp)

class ReachabilitySafe : public ReachabilitySafeStateBased
{
public:
    ReachabilitySafe(Petrinet* n, StatePredicate* f, int p);
    virtual ~ReachabilitySafe();
    virtual void PreProcessAtomics();

protected:
    /// @brief Helper fiels for atomics, used alongside AtomicChange, but for other things needed
    /// for Pathbased calculations
    int* AtomicHelper;
    /// @brief Save bool to determine if all sums are calculated StateBased
    bool allStatebased;
    /// @brief Save index of next atomic to change calculation. Allows for easier looping
    arrayindex_t nextStatebased;
    // Overridden functions for new functionality
    virtual translation_result_t create_whole_formula();
    virtual void create_initial();
    void create_initial_atomic(arrayindex_t a);
    virtual void create_phi(arrayindex_t t);
    void create_phi_atomic(arrayindex_t a, arrayindex_t t, bool b);
    virtual translation_result_t create_atomic(AtomicStatePredicate* f, vec<Lit>& lits);
    void updateAtomics();
};
