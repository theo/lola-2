/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Lukas
\status new

\brief Translation of a general Petri net to a boolean formula.
*/

#include <ReachabilitySAT/ReachabilityGeneral.h>

// again macros for addressing/readability
#define VAR_ATOM(START, INDEX) ((START) - (INDEX))
#define VAR_PLACE(START, INDEX) ((START) - (INDEX))
#define VAR_FIRE(S) (S)
#define VAR_EXTRA(E) (E)

ReachabilityGeneral::ReachabilityGeneral(Petrinet* n, StatePredicate* f, int p)
    : ReachabilitySafe(n, f, p)
{
    // similar to the estimation for a minimum value for atomics in PreProcessAtomics, we can do the
    // same for places, now including an estimation for a maximum
    PlaceHelper = new int[net->Card[PL] * 6];
    for (arrayindex_t p = 0; p < net->Card[PL]; p++)
    {
        PLACE_MIN_VAL(p) = 0;
        PLACE_MAX_VAL(p) = 0;
    }
    // basically we iterate over the places that the transitions change and adjust their values
    for (arrayindex_t t_id = 0; t_id < TransitionCount; t_id++)
    {
        arrayindex_t t = TRANSITION_ORD(t_id);
        for (arrayindex_t i = 0; i < net->CardDeltaT[POST][t]; i++)
        {
            PLACE_MAX_VAL(net->DeltaT[POST][t][i]) += net->MultDeltaT[POST][t][i];
        }
        for (arrayindex_t i = 0; i < net->CardDeltaT[PRE][t]; i++)
        {
            PLACE_MIN_VAL(net->DeltaT[PRE][t][i]) += net->MultDeltaT[PRE][t][i];
        }
    }
}

ReachabilityGeneral::~ReachabilityGeneral() { delete[] PlaceHelper; }

void ReachabilityGeneral::PreProcessAtomics()
{
    AtomicCount = spFormula->countAtomic();
    Atomic = new AtomicStatePredicate*[AtomicCount];
    spFormula->collectAtomic(Atomic);
    // again hashing
    arrayindex_t nextUnique = 0;
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        int hash = 0;
        for (int i = 0; i < Atomic[a]->cardPos; i++)
        {
            hash += Atomic[a]->posMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->posPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
        }
        for (int i = 0; i < Atomic[a]->cardNeg; i++)
        {
            hash -= Atomic[a]->negMult[i];
            hash += hash << 10;
            hash ^= hash >> 6;
            hash += Atomic[a]->negPlaces[i];
            hash += hash << 10;
            hash ^= hash >> 6;
        }
        Atomic[a]->magicnumber = hash;
        arrayindex_t uniques;
        for (uniques = 0; uniques < nextUnique; uniques++)
        {
            if (Atomic[a]->magicnumber == Atomic[uniques]->magicnumber
                && areFormalSumsEqual(Atomic[a], Atomic[uniques]))
            {
                Atomic[a]->magicnumber = uniques;
                break;
            }
        }
        if (uniques == nextUnique)
        {
            if (a != nextUnique)
            {
                AtomicStatePredicate* temp = Atomic[a];
                Atomic[a] = Atomic[nextUnique];
                Atomic[nextUnique] = temp;
            }
            arrayindex_t length;
            if ((length = Atomic[nextUnique]->cardPos + Atomic[nextUnique]->cardNeg)
                > CardAtomicChange)
                CardAtomicChange = length;
            // don't need to save min/max anymore, no more comparisons or things needed for
            // StateBased, only PathBased now
            nextUnique++;
        }
    }
    if (CardAtomicChange < TransitionCount)
        CardAtomicChange = TransitionCount;
    AtomicCount = nextUnique;
    AtomicChange = new int[AtomicCount * CardAtomicChange];
    memset(AtomicChange, 0, sizeof(int) * AtomicCount * CardAtomicChange);
    AtomicHelper = new int[AtomicCount * 4];
    // Now again calculating the change of sums when transitions fire
    // Just no need for sorting at the end
    for (arrayindex_t a = 0; a < AtomicCount; a++)
    {
        // for ease of use the unique sums now point to themselves as originals
        Atomic[a]->magicnumber = a;
        // For everything below see ReachabilitySafe
        Atomic[a]->evaluate(*initial);
        arrayindex_t varcount = 0;
        arrayindex_t minval = 0;
        arrayindex_t min = Atomic[a]->cardPos;
        if (min > Atomic[a]->cardNeg)
            min = Atomic[a]->cardNeg;
        arrayindex_t p = 0;
        arrayindex_t t;
        arrayindex_t place;
        while (p < min)
        {
            place = Atomic[a]->posPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][PRE][place][i]
                        * Atomic[a]->posMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][POST][place][i]
                        * Atomic[a]->posMult[p];
            }
            place = Atomic[a]->negPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][PRE][place][i]
                        * Atomic[a]->negMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][POST][place][i]
                        * Atomic[a]->negMult[p];
            }
            p++;
        }
        while (p < Atomic[a]->cardPos)
        {
            place = Atomic[a]->posPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][PRE][place][i]
                        * Atomic[a]->posMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][POST][place][i]
                        * Atomic[a]->posMult[p];
            }
            p++;
        }
        while (p < Atomic[a]->cardNeg)
        {
            place = Atomic[a]->negPlaces[p];
            for (arrayindex_t i = 0; i < net->CardArcs[PL][PRE][place]; i++)
            {
                t = net->Arc[PL][PRE][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) -= net->Mult[PL][PRE][place][i]
                        * Atomic[a]->negMult[p];
            }
            for (arrayindex_t i = 0; i < net->CardArcs[PL][POST][place]; i++)
            {
                t = net->Arc[PL][POST][place][i];
                if (TRANSITION_IS_ORDERED(t))
                    ATOMCHANGE(a, TRANSITION_ORD_REV(t)) += net->Mult[PL][POST][place][i]
                        * Atomic[a]->negMult[p];
            }
            p++;
        }
        for (arrayindex_t t = 0; t < TransitionCount; t++)
        {
            varcount += abs(ATOMCHANGE(a, t));
            if (ATOMCHANGE(a, t) < 0)
            {
                minval -= ATOMCHANGE(a, t);
            }
        }
        ATOM_MIN_VAL(a) = minval;
        if (RT::args.satvariablebound_given)
            Atomic[a]->upper_bound = varcount;
    }
}

translation_result_t ReachabilityGeneral::create_whole_formula()
{
    // Reset some helper fields
    step = 0;
    // Compared to the arguments in ReachabilitySafe for atomics, we only have one helper array for
    // places, instead of multiple, so here we can access it and save this code in looping function
    // calls
    if (RT::args.satvariablebound_given)
    {
        for (arrayindex_t p = 0; p < net->Card[PL]; p++)
        {
            // If with the current value for PathLength allows us to reach 0,
            // we allow values until 0
            if ((PLACE_ALLOWED_MAX(p) = PLACE_MIN_VAL(p) * PathLength) >= net->Initial[p])
            {
                PLACE_ALLOWED_MAX(p) = net->Initial[p];
            }
            // Then see how many are added in positive direction and take percentage of that
            PLACE_ALLOWED_MAX(p) += PathLength * PLACE_MAX_VAL(p);
            PLACE_ALLOWED_MAX(p) = ceil(RT::args.satvariablebound_arg * PLACE_ALLOWED_MAX(p));
        }
    }
    // Declare variables for fire/copy decision for minisat
    for (arrayindex_t i = 0; i < PathLength * TransitionCount; i++)
        S->newVar();

    // Create initial formula which also initializes the remaining helper fields
    create_initial();
    // Create phi^t for every transition for every 'major step'
    for (arrayindex_t k = 0; k < PathLength; k++)
    {
        for (arrayindex_t t = 0; t < TransitionCount; t++)
        {
            create_phi(t);
        }
    }
    return create_spFormula();
}

void ReachabilityGeneral::create_initial()
{
    for (arrayindex_t a = AtomicCount - 1; a >= 0; a--)
    {
        create_initial_atomic(a);
    }
    for (arrayindex_t p = 0; p < net->Card[PL]; p++)
    {
        // similar to atomics
        PLACE_VAR_COUNT(p) = 2;
        PLACE_NEG_OFFSET(p) = net->Initial[p];
        arrayindex_t varindex = S->newVar();
        S->addClause(~mkLit(varindex));
        varindex = S->newVar();
        S->addClause(mkLit(varindex));
        PLACE_VAR_ADDRESS(p) = varindex;
    }
    AddressOffset = S->nVars() - 1;
}

void ReachabilityGeneral::create_phi(arrayindex_t t_id)
{
    bool willCopyAnyway = false;
    arrayindex_t t = TRANSITION_ORD(t_id);
    // General nets need check if transition can even fire
    // (safe nets always has the neccessary variables)
    for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
    {
        mult_t mult = net->Mult[TR][PRE][t][p];
        arrayindex_t place = net->Arc[TR][PRE][t][p];
        // Similar to other places, convert multiplicity of pre-arc to index and check if its valid
        if (mult - PLACE_NEG_OFFSET(place) >= 0)
        {
            if (mult < PLACE_VAR_COUNT(place) + PLACE_NEG_OFFSET(place) - 1)
            {
                // if valid, add clause stating "not enough tokens -> don't fire"
                S->addClause(
                    mkLit(VAR_PLACE(PLACE_VAR_ADDRESS(place), mult - PLACE_NEG_OFFSET(place))),
                    ~mkLit(VAR_FIRE(step))
                );
            }
            else
            {
                // otherwise simply add clause stating "don't fire"
                S->addClause(~mkLit(VAR_FIRE(step)));
                willCopyAnyway = true;
                break;
            }
        }
    }
    // create atomics first, makes for easier variable management, see ReachabilitySafeState for
    // addressing overview
    for (arrayindex_t a = AtomicCount - 1; a >= 0; a--)
    {
        create_phi_atomic(a, t_id, willCopyAnyway);
    }
    // only do stuff if we can even fire
    if (!willCopyAnyway)
    {
        // first go over places that gain tokens
        for (arrayindex_t p = 0; p < net->CardDeltaT[POST][t]; p++)
        {
            arrayindex_t place = net->DeltaT[POST][t][p];
            mult_t mult = net->MultDeltaT[POST][t][p];
            // pretty simple stuff, settings helper fields for TRANSLATE_STEP macro
            arrayindex_t prev = VAR_PLACE(PLACE_VAR_ADDRESS(place), PLACE_VAR_COUNT(place) - 1);
            arrayindex_t diff = PLACE_VAR_COUNT(place) - mult;
            arrayindex_t min = PLACE_VAR_COUNT(place);
            if (diff > 0)
                min = mult;
            // see ReachabilitySafe to see why values are adjusted this way, since its the same for
            // atomics
            if (RT::args.satvariablebound_given && (PLACE_ALLOWED_MAX(place) -= mult) < 0)
            {
                arrayindex_t offset = -1 * PLACE_ALLOWED_MAX(place);
                if (offset > min)
                {
                    if (diff > 0)
                    {
                        diff = PLACE_VAR_COUNT(place) - offset;
                    }
                    else
                    {
                        diff = offset - mult;
                    }
                }
                TRANSLATE_STEP(offset, 0, min, VAR_FIRE(step), true)
                PLACE_VAR_COUNT(place) += mult + PLACE_ALLOWED_MAX(place);
                S->addClause(~mkLit(VAR_PLACE(S->nVars() - 1, PLACE_VAR_COUNT(place) - 1)));
                PLACE_ALLOWED_MAX(place) = 0;
            }
            else
            {
                TRANSLATE_STEP(0, 0, min, VAR_FIRE(step), true)
                PLACE_VAR_COUNT(place) += mult;
            }
            Computed[place] = true;
            PLACE_VAR_ADDRESS(place) = S->nVars() - 1;
        }
        // then places that lose tokens
        for (arrayindex_t p = 0; p < net->CardDeltaT[PRE][t]; p++)
        {
            arrayindex_t place = net->DeltaT[PRE][t][p];
            mult_t mult = net->MultDeltaT[PRE][t][p];
            // again helper fields
            arrayindex_t prev = VAR_PLACE(PLACE_VAR_ADDRESS(place), PLACE_VAR_COUNT(place) - 1);
            arrayindex_t diff = PLACE_VAR_COUNT(place) - mult;
            arrayindex_t min = PLACE_VAR_COUNT(place);
            if (diff > 0)
                min = mult;
            // special sauce
            // places always have the lower bound 0, if we additionally restrict the amount of
            // variables through the commandline, we need to check which limit is broken first (or
            // stronger)
            if ((RT::args.satvariablebound_given && (PLACE_ALLOWED_MAX(place) -= mult) < 0)
                || (PLACE_NEG_OFFSET(place) -= mult) < 0)
            {
                arrayindex_t offset;
                // at this point we know one is broken, check which one
                if (RT::args.satvariablebound_given && PLACE_ALLOWED_MAX(place) < 0)
                {
                    // atleast cmdline-limit is broken, 0-value-limit not updated yet because of the
                    // way conditions are calculated
                    PLACE_NEG_OFFSET(place) -= mult;
                    offset = -1 * PLACE_ALLOWED_MAX(place);
                    // determine which limit is broken first
                    // keep in mind NEG_OFFSET and ALLOWED_MAX store the amount of variables over
                    // the limit times -1, therefore the smaller value is more strict
                    if (PLACE_NEG_OFFSET(place) < 0
                        && PLACE_NEG_OFFSET(place) < PLACE_ALLOWED_MAX(place))
                    {
                        // less variables allowed per neg_offset than per allowed_max, therefore
                        // that's the culprit for value changes
                        offset = -1 * PLACE_NEG_OFFSET(place);
                        // readjust for allowed_max, since not all variables for mult are added
                        PLACE_ALLOWED_MAX(place) -= PLACE_NEG_OFFSET(place);
                        PLACE_VAR_COUNT(place) += mult + PLACE_NEG_OFFSET(place);
                        PLACE_NEG_OFFSET(place) = 0;
                    }
                    else
                    {
                        // less variables allowed per allowed_max than per neg_offset, therefore
                        // that's the culprit for value changes readjust for neg_offset, since not
                        // all variables for mult are added
                        PLACE_NEG_OFFSET(place) -= PLACE_ALLOWED_MAX(place);
                        PLACE_VAR_COUNT(place) += mult + PLACE_ALLOWED_MAX(place);
                        PLACE_ALLOWED_MAX(place) = 0;
                    }
                }
                else
                {
                    // here only 0-value-limit is broken or cmdline-limit is not set
                    // therefore 0-value-limit is culprit and causes the adjustments as seen in
                    // ReachabilitySafe
                    offset = -1 * PLACE_NEG_OFFSET(place);
                    // if cmdline-limit is set, its not broken here, therefore readjust by how many
                    // variables we go over the 0-value-limit, since those are still allowed in
                    // positive direction
                    if (RT::args.satvariablebound_given)
                        PLACE_ALLOWED_MAX(place) -= PLACE_NEG_OFFSET(place);
                    PLACE_VAR_COUNT(place) += mult + PLACE_NEG_OFFSET(place);
                    // value 0 is represented, therefore no offset value<->index is needed
                    PLACE_NEG_OFFSET(place) = 0;
                }
                // also adjust other fields
                if (offset > min)
                {
                    if (diff > 0)
                    {
                        diff = PLACE_VAR_COUNT(place) - offset;
                    }
                    else
                    {
                        diff = offset - mult;
                    }
                }
                TRANSLATE_STEP(0, offset, min, VAR_FIRE(step), false)
                S->addClause(mkLit(VAR_PLACE(S->nVars() - 1, 0)));
            }
            else
            {
                TRANSLATE_STEP(0, 0, min, VAR_FIRE(step), false)
                PLACE_VAR_COUNT(place) += mult;
            }
            Computed[place] = true;
            PLACE_VAR_ADDRESS(place) = S->nVars() - 1;
        }
    }
    // now the remaining places just need to be copied and can be identified via the Computed array
    for (arrayindex_t p = 0; p < net->Card[PL]; p++)
    {
        // only copy those that weren't computed before
        if (willCopyAnyway || !Computed[p])
        {
            arrayindex_t prev = VAR_PLACE(PLACE_VAR_ADDRESS(p), PLACE_VAR_COUNT(p) - 1);
            for (arrayindex_t i = PLACE_VAR_COUNT(p) - 1; i >= 0; i--, prev++)
            {
                arrayindex_t curr = S->newVar();
                S->addClause(mkLit(prev), ~mkLit(curr));
                S->addClause(~mkLit(prev), mkLit(curr));
            }
            PLACE_VAR_ADDRESS(p) = S->nVars() - 1;
        }
        else
        {
            // we can reset the Computed value of the ones computed before for the next step
            Computed[p] = false;
        }
    }
    step++;
    AddressOffset = S->nVars() - 1;
}

translation_result_t ReachabilityGeneral::create_atomic(AtomicStatePredicate* f, vec<Lit>& lits)
{
    translation_result_t result = FORMULA_RESULT_INDETERMINATE;
    arrayindex_t a = -1;
    // need to find the atomic that is the current child so we can use its variable
    while (f != Atomic[++a])
        ;
    // now we only have PathBased, removing some cases
    if (ATOM_IS_DUPL(a))
        a = Atomic[a]->magicnumber;
    // otherwise same as in ReachabilitySafe
    if (f->threshold - ATOM_NEG_OFFSET(a) >= 0)
    {
        if (f->threshold < ATOM_VAR_COUNT(a) + ATOM_NEG_OFFSET(a) - 1)
        {
            lits.push(~mkLit(VAR_ATOM(ATOM_VAR_ADDRESS(a), f->threshold - ATOM_NEG_OFFSET(a) + 1)));
        }
        else
        {
            result = FORMULA_RESULT_SATISFIED;
        }
    }
    else
    {
        result = ATOM_MIN_VAL(a) <= 0
            ? FORMULA_RESULT_UNSOLVABLE
            : (Atomic[a]->sum - Atomic[a]->threshold) / ATOM_MIN_VAL(a) + 1;
    }
    return result;
}

translation_result_t ReachabilityGeneral::create_fireable(
    arrayindex_t t, vec<Lit>& lits, arrayindex_t extra
)
{
    // We change places, so fireable and notfireable change aswell
    // Overall the code to determine firable is the same as the check in create_phi, since thats
    // also "fireable(t) -> can fire"
    translation_result_t fireableResult = FORMULA_RESULT_SATISFIED;
    // first save some code if only one pre-place
    if (net->CardArcs[TR][PRE][t] == 1)
    {
        mult_t mult = net->Mult[TR][PRE][t][0];
        arrayindex_t place = net->Arc[TR][PRE][t][0];
        if (mult - PLACE_NEG_OFFSET(place) >= 0)
        {
            if (mult < PLACE_VAR_COUNT(place) + PLACE_NEG_OFFSET(place) - 1)
            {
                fireableResult = FORMULA_RESULT_INDETERMINATE;
                lits.push(mkLit(VAR_PLACE(PLACE_VAR_ADDRESS(place), mult - PLACE_NEG_OFFSET(place)))
                );
            }
            else
            {
                // if mult is greater than maximum, find round estimate where mult converts to a
                // valid index
                fireableResult = GetPlaceMinRoundsGreater(place, mult);
            }
        }
    }
    else
    {
        // basically the same as above, just for all pre-places with some extra checks
        for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
        {
            mult_t mult = net->Mult[TR][PRE][t][p];
            arrayindex_t place = net->Arc[TR][PRE][t][p];
            if (mult - PLACE_NEG_OFFSET(place) >= 0)
            {
                if (mult < PLACE_VAR_COUNT(place) + PLACE_NEG_OFFSET(place) - 1)
                {
                    arrayindex_t pIndex = VAR_PLACE(
                        PLACE_VAR_ADDRESS(place), mult - PLACE_NEG_OFFSET(place)
                    );
                    // fireable needs all pre-places valid, if any before isn't valid (result >
                    // INDETERMINATE or result == UNSOLVABLE), we don't need to add further stuff we
                    // still check in case of > INDETERMINATE to over-approximate the round
                    // UNSOLVABLE would result in preemptively returning, so never the case here
                    if (fireableResult <= FORMULA_RESULT_INDETERMINATE)
                    {
                        fireableResult = FORMULA_RESULT_INDETERMINATE;
                        if (extra)
                        {
                            S->addClause(mkLit(pIndex), ~mkLit(VAR_EXTRA(extra)));
                        }
                        else
                        {
                            S->addClause(mkLit(pIndex));
                        }
                    }
                }
                else
                {
                    // again check for estimate
                    int round = GetPlaceMinRoundsGreater(place, mult);
                    if (round == FORMULA_RESULT_UNSOLVABLE)
                    {
                        fireableResult = FORMULA_RESULT_UNSOLVABLE;
                        break;
                    }
                    // accumulate for largest estimate, since all pre-places need be valid
                    fireableResult = round > fireableResult ? round : fireableResult;
                }
            }
        }
    }
    return fireableResult;
}

translation_result_t ReachabilityGeneral::create_notfireable(arrayindex_t t, vec<Lit>& lits)
{
    translation_result_t fireableResult = FORMULA_RESULT_UNSOLVABLE;
    // again some saving when only one pre-place
    if (net->CardArcs[TR][PRE][t] == 1)
    {
        mult_t mult = net->Mult[TR][PRE][t][0];
        arrayindex_t place = net->Arc[TR][PRE][t][0];
        if (mult - PLACE_NEG_OFFSET(place) >= 0)
        {
            if (mult < PLACE_VAR_COUNT(place) + PLACE_NEG_OFFSET(place) - 1)
            {
                fireableResult = FORMULA_RESULT_INDETERMINATE;
                lits.push(~mkLit(VAR_PLACE(PLACE_VAR_ADDRESS(place), mult - PLACE_NEG_OFFSET(place))
                ));
            }
            else
            {
                fireableResult = FORMULA_RESULT_SATISFIED;
            }
        }
        else
        {
            // if mult is smaller than minimum, we want an estimate of when mult has a valid index
            // remember "notfireable, but mult is smaller than all values" means its always fireable
            fireableResult = GetPlaceMinRoundsLower(place, mult);
        }
    }
    else
    {
        // repeat above for all pre-places
        for (arrayindex_t p = 0; p < net->CardArcs[TR][PRE][t]; p++)
        {
            mult_t mult = net->Mult[TR][PRE][t][p];
            arrayindex_t place = net->Arc[TR][PRE][t][p];
            if (mult - PLACE_NEG_OFFSET(place) >= 0)
            {
                if (mult < PLACE_VAR_COUNT(place) + PLACE_NEG_OFFSET(place) - 1)
                {
                    fireableResult = FORMULA_RESULT_INDETERMINATE;
                    lits.push(
                        ~mkLit(VAR_PLACE(PLACE_VAR_ADDRESS(place), mult - PLACE_NEG_OFFSET(place)))
                    );
                }
                else
                {
                    fireableResult = FORMULA_RESULT_SATISFIED;
                    break;
                }
            }
            else
            {
                // only update estimate if we haven't found things to maybe be possible
                // remember for notfireable only one pre-place with not enough tokens is sufficient
                // if result is set to INDETERMINATE for any pre-place, we don't care if another is
                // not possible for notfireable
                if (fireableResult != FORMULA_RESULT_INDETERMINATE)
                {
                    int round = GetPlaceMinRoundsLower(place, mult);
                    if (round > 0)
                    {
                        // don't care if round < 0 (only UNSOLVABLE), since thats the value
                        // fireableResult would have anyway at this point SATISFIED returns early,
                        // INDETERMINATE is never the case, > 0 gets checked below only want
                        // smallest estimate, since one pre-place is enough for notfireable and we
                        // don't want to skip more rounds than neccessary
                        fireableResult = fireableResult < 0 || round < fireableResult
                            ? round
                            : fireableResult;
                    }
                }
            }
        }
    }
    return fireableResult;
}

// basically like the code for atomics and their minimum value

inline arrayindex_t ReachabilityGeneral::GetPlaceMinRoundsGreater(arrayindex_t p, mult_t m)
{
    return PLACE_MAX_VAL(p) <= 0 ? FORMULA_RESULT_UNSOLVABLE
                                 : int(m - net->Initial[p]) / PLACE_MAX_VAL(p) + 1;
}

inline arrayindex_t ReachabilityGeneral::GetPlaceMinRoundsLower(arrayindex_t p, mult_t m)
{
    return PLACE_MIN_VAL(p) <= 0 ? FORMULA_RESULT_UNSOLVABLE
                                 : int(net->Initial[p] - m) / PLACE_MIN_VAL(p) + 1;
}
