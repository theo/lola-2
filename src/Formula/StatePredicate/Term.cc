/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status new

\brief temporal list based container for terms in atomic propositions

*/

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <CoverGraph/CoverGraph.h>
#include <Formula/StatePredicate/Term.h>

Term::Term()
{
    card = 1;
    next = NULL;
    place = 0;
    mult = 0;
    sy = NULL;
}

Term::~Term() { delete next; }

void Term::multiply(int f)
{
    if (f == 1)
    {
        // do nothing
        return;
    }
    if (mult == OMEGA)
    {
        if (f <= -1)
        {
            mult = -OMEGA;
        }
        else if (f >= 1)
        {
            mult = OMEGA;
        }
        else  // f == 0
        {
            RT::log<err>("multiplication 0 * oo");
            RT::abort(ERROR::SYNTAX);
        }
    }
    else if (mult == -OMEGA)
    {
        if (f <= -1)
        {
            mult = OMEGA;
        }
        else if (f >= 1)
        {
            mult = -OMEGA;
        }
        else  // f == 0
        {
            RT::log<err>("multiplication 0 * oo");
            RT::abort(ERROR::SYNTAX);
        }
    }
    else
    {
        mult *= f;
    }
    if (next)
    {
        next->multiply(f);
    }
}

void Term::append(Term* other)
{
    if (next)
    {
        next->append(other);
    }
    else
    {
        next = other;
    }
    card += other->card;
}

Term* Term::copy()
{
    Term* T = new Term();
    T->place = place;
    T->mult = mult;
    T->sy = sy;
    if (next)
    {
        T->next = next->copy();
    }
    return T;
}

namespace parsing
{
void Term::multiply(int factor)
{
    if (factor == 1)
    {
        return;
    }
    if (mult == OMEGA)
    {
        if (factor < 0)
        {
            mult = -OMEGA;
        }
        else if (factor == 0)
        {
            RT::log<err>("multiplication 0 * oo");
            RT::abort(ERROR::SYNTAX);
        }
    }
    else if (mult == -OMEGA)
    {
        if (factor < 0)
        {
            mult = OMEGA;
        }
        else if (factor == 0)
        {
            RT::log<err>("multiplication 0 * -oo");
            RT::abort(ERROR::SYNTAX);
        }
    }
    else
    {
        mult *= factor;
    }
}

void Term::divide(int divisor)
{
    LOLA_LOG(
        trace,
        [&]()
        {
            if (mult % divisor != 0)
            {
                RT::log<warn>("Division of {} by {} does not yield integer result", mult, divisor);
            }
        }
    );
    if (divisor == 1)
    {
        return;
    }
    if (divisor == 0)
    {
        RT::log<err>("division by 0");
        RT::abort(ERROR::SYNTAX);
    }
    if (divisor == OMEGA || divisor == -OMEGA)
    {
        RT::log<err>("division by (-)omega");
        RT::abort(ERROR::SYNTAX);
    }
    if (mult == OMEGA)
    {
        if (divisor < 0)
        {
            mult = -OMEGA;
        }
    }
    else if (mult == -OMEGA)
    {
        if (divisor < 0)
        {
            mult = OMEGA;
        }
    }
    else
    {
        mult /= divisor;
    }
}

void Term::add(int summand)
{
    if (summand == 0)
    {
        return;
    }
    if (mult == OMEGA && summand == -OMEGA)
    {
        RT::log<err>("addition -oo + oo");
        RT::abort(ERROR::SYNTAX);
    }
    if (mult == -OMEGA && summand == OMEGA)
    {
        RT::log<err>("addition -oo + oo");
        RT::abort(ERROR::SYNTAX);
    }
    if (summand == OMEGA || summand == -OMEGA)
    {
        mult = summand;
    }
    if (mult != OMEGA && mult != -OMEGA)
    {
        mult += summand;
    }
}
}  // namespace parsing
