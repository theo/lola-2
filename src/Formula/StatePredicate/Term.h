/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef SRC_FORMULA_STATEPREDICATE_TERM_H
#define SRC_FORMULA_STATEPREDICATE_TERM_H

/*!
\file
\author Karsten
\status new

\brief temporal list based container for terms in atomic propositions

*/

#include <ranges>
#include <string_view>
#include <unordered_map>

#include <rapidxml/rapidxml.hpp>

#include "Core/Dimensions.h"
#include "Frontend/ModularParser/ParserMNet.h"
#include "Frontend/ModularSymbols/AliasSymbol.h"
#include "Frontend/ModularSymbols/InstanceSymbol.h"
#include "Frontend/SymbolTable/PlaceSymbol.h"
#include "Frontend/SymbolTable/Symbol.h"

class Term
{
public:
    Term();
    ~Term();
    arrayindex_t place;  // index of place. If set to Net::Card[PL]:
                         // numerical constant
    Symbol* sy;          // alternative representation of place (if net not yet defined)
                         // "place" valid only if sy == NULL
    int mult;
    Term* next;
    arrayindex_t card;

    void multiply(int);
    void append(Term*);
    Term* copy();
};

namespace parsing
{

class Term
{
public:
    explicit Term(int mult) : mult{mult} {}

    void multiply(int factor);
    void multiply(Term factor) { multiply(factor.get_mult()); }
    void divide(int divisor);
    void divide(Term divisor) { divide(divisor.get_mult()); }
    void add(int summand);
    void add(Term summand) { add(summand.get_mult()); }
    auto get_mult() const -> int { return mult; }

private:
    int mult;
};

class FormalSum
{
private:
    friend class FormulaParser;
    friend void parse_xml_term(
        rapidxml::xml_node<>* term, const ModularStructure& ms, FormalSum& current_ap,
        const Term current_factor
    );

public:
    using term_list = std::unordered_map<Alias, Term>;

    FormalSum() = default;
    explicit FormalSum(std::size_t num_global_places) : terms{num_global_places} {}

    auto get_terms() const -> const term_list& { return terms; }
    auto get_constant() const -> const Term& { return constant; }

private:
    void reset()
    {
        terms.clear();
        constant = Term{0};
    }
    void invert()
    {
        std::ranges::for_each(terms, [](auto& term) { term.second.multiply(-1); });
        constant.multiply(-1);
    }
    auto add_term(
        SymbolTable<Instance>::const_reference instance, SymbolTable<Place>::const_reference place,
        Term mult
    ) -> term_list::iterator
    {
        auto [iter, inserted] = terms.emplace(
            std::piecewise_construct,
            std::forward_as_tuple(std::addressof(instance), std::addressof(place)),
            std::forward_as_tuple(mult)
        );
        if (!inserted)
        {
            iter->second.add(mult);
        }
        return iter;
    }

    term_list terms{1};
    Term constant{0};
};
}  // namespace parsing

#endif /* SRC_FORMULA_STATEPREDICATE_TERM_H */
