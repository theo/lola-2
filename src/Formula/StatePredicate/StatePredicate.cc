/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Protonet.h>
#include <Formula/StatePredicate/StatePredicate.h>

bool StatePredicate::isOrNode() { return false; }

// LCOV_EXCL_START
arrayindex_t StatePredicate::countUnsatisfied() { return 0; }
// LCOV_EXCL_STOP

bool StatePredicate::solve_lp(
    ProtoNet* nnn, operations_research::MPSolver* solver, operations_research::MPVariable** pvars,
    operations_research::MPVariable** tvars, operations_research::MPObjective* objectiv
)
{
    const double infinity = solver->infinity();
    // return false = INFEASIBLE, true = FEASIBLE or not successful

    int attempts_to_go = RT::args.trapcheckdepth_arg;
    while (true)
    {
        operations_research::MPSolver::ResultStatus ret = solver->Solve();
        if (ret == operations_research::MPSolver::INFEASIBLE)
        {
            // provably no solution exists
            return false;
        }
        if (ret != operations_research::MPSolver::OPTIMAL
            && ret != operations_research::MPSolver::FEASIBLE)
        {
            // no useful solution obtained
            return true;
        }
        // here: have a feasible solution
        // bypass trap check
        if (attempts_to_go-- == 0)
            return true;
        // check for unmarked trap
        bool* trap = new bool[nnn->cardPL];
        bool* initial = new bool[nnn->cardPL];
        int cardinitial = 0;
        int cardtrap = 0;
        for (ProtoPlace* ppp = nnn->firstplace; ppp; ppp = ppp->next)
        {
            if (pvars[ppp->index]->solution_value() < 0.5)
            {
                trap[ppp->index] = true;
                cardtrap++;
                if (ppp->marking)
                {
                    initial[ppp->index] = true;
                    cardinitial++;
                }
                else
                    initial[ppp->index] = false;
            }
            else
                trap[ppp->index] = false;
        }
        if (!cardtrap || !cardinitial)
        {
            // there is no empty trap in target marking
            // or empty trap is empty already in initial marking
            // -> cannot add constraint
            // -> cannot prove stability
            delete[] trap;
            delete[] initial;
            return true;
        }
        bool somethingchanged;
        do
        {
            somethingchanged = false;
            for (ProtoTransition* ttt = nnn->firsttransition; ttt; ttt = ttt->next)
            {
                Arc* aaa;
                for (aaa = ttt->firstArcPost; aaa; aaa = aaa->next_tr)
                {
                    if (trap[aaa->pl->index])
                        break;
                }
                if (aaa)
                    continue;  // t has post-place in trap -> trap property holds for t
                for (aaa = ttt->firstArcPre; aaa; aaa = aaa->next_tr)
                {
                    if (trap[aaa->pl->index])
                    {
                        somethingchanged = true;
                        trap[aaa->pl->index] = false;
                        cardtrap--;
                        if (initial[aaa->pl->index])
                        {
                            cardinitial--;
                        }
                    }
                    if (!cardtrap || !cardinitial)
                    {
                        // there is no empty trap in target marking
                        // or empty trap is empty already in initial marking
                        // -> cannot add constraint
                        // -> cannot prove stability
                        delete[] trap;
                        delete[] initial;
                        return true;
                    }
                }
            }
        } while (somethingchanged);
        // if not returned yet -> remaining empty trap was initially marked
        // -> add new constraint and continue;
        operations_research::MPConstraint* T = solver->MakeRowConstraint(
            1.0, infinity, "avoid empty trap"
        );
        for (int i = 0; i < nnn->cardPL; i++)
        {
            if (trap[i])
            {
                T->SetCoefficient(pvars[i], 1.0);
            }
        }
        // set_row_name(lp,get_Nrows(lp),deconst("TRAP"));
        delete[] trap;
        delete[] initial;
    }
    return true;
}
