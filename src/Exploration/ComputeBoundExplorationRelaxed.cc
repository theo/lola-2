/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status new

\brief compute reachability using relaxed stubborn sets
*/

#include <Core/Dimensions.h>
#include <Exploration/ComputeBoundExplorationRelaxed.h>
#include <Exploration/StatePredicateProperty.h>
#include <Formula/StatePredicate/StatePredicate.h>
#include <Exploration/ChooseTransition.h>
#include <Exploration/FirelistStubbornCloseGiven.h>
#include <Frontend/Parser/ParserPnml.h>
#include <Core/Runtime.h>

extern pthread_mutex_t nestedmutex;
extern nestedunit* startunit;

#include "ortools/linear_solver/linear_solver.h"

#include <Formula/StatePredicate/AtomicStatePredicate.h>

int ComputeBoundExplorationRelaxed::depth_first(
    SimpleProperty& property, NetState& ns, Store<statenumber_t>& myStore, Firelist& myFirelist, int
)
{
    StructuralBound = lp(property, ns, myStore, myFirelist, 1);
    StatePredicateProperty* p = reinterpret_cast<StatePredicateProperty*>(&property);
    AtomicStatePredicate* a = reinterpret_cast<AtomicStatePredicate*>(p->predicate);
    StructuralBound -= a->threshold;
    LOLA_LOG(info, "Structural Bound: {}", StructuralBound);
    // RT::log("Structural Rel Bound for {}: {}", a->toString(),StructuralBound);

    FirelistStubbornCloseGiven* flclose = new FirelistStubbornCloseGiven(net);
    int* lastfired = new int[net->Card[TR]];
    memset(lastfired, 0, net->Card[TR] * sizeof(arrayindex_t));

    statenumber_t* payload;
    myStore.searchAndInsert(ns, &payload, 0);

    property.value = property.initProperty(ns);
    result = a->sum - a->threshold;
    if (result == StructuralBound)
        return result;

    bool* inUpset = new bool[net->Card[TR]];
    memset(inUpset, 0, sizeof(bool) * net->Card[TR]);
    arrayindex_t* Upset = new arrayindex_t[net->Card[TR]];
    arrayindex_t cardUpset = 0;

    bool* inDownSet = new bool[net->Card[TR]];
    memset(inDownSet, 0, sizeof(bool) * net->Card[TR]);

    for (arrayindex_t i = 0; i < a->cardPos; i++)
    {
        arrayindex_t p = a->posPlaces[i];
        for (arrayindex_t j = 0; j < net->PlCardIncreasing[p]; j++)
        {
            arrayindex_t t = net->PlIncreasing[p][j];
            if (inUpset[t])
                continue;
            inUpset[t] = true;
            Upset[cardUpset++] = t;
        }
        for (arrayindex_t j = 0; j < net->PlCardDecreasing[p]; j++)
        {
            inDownSet[net->PlDecreasing[p][j]] = true;
        }
    }
    for (arrayindex_t i = 0; i < a->cardNeg; i++)
    {
        arrayindex_t p = a->negPlaces[i];
        for (arrayindex_t j = 0; j < net->PlCardDecreasing[p]; j++)
        {
            arrayindex_t t = net->PlDecreasing[p][j];
            if (inUpset[t])
                continue;
            inUpset[t] = true;
            Upset[cardUpset++] = t;
        }
        for (arrayindex_t j = 0; j < net->PlCardIncreasing[p]; j++)
        {
            inDownSet[net->PlIncreasing[p][j]] = true;
        }
    }

    // get initial firelist
    arrayindex_t* currentFirelist;
    arrayindex_t currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
    bool containsdown = false;
    for (arrayindex_t i = 0; i < currentEntry; i++)
    {
        if (inDownSet[currentFirelist[i]])
        {
            containsdown = true;
            break;
        }
    }
    if (containsdown)
    {
        delete[] currentFirelist;
        arrayindex_t* stubbornstack = new arrayindex_t[net->Card[TR]];
        memcpy(stubbornstack, Upset, cardUpset * sizeof(arrayindex_t));
        bool* onstack = new bool[net->Card[TR]];
        memcpy(onstack, inUpset, net->Card[TR] * sizeof(bool));
        currentEntry = flclose->getFirelist(
            ns, &currentFirelist, stubbornstack, onstack, cardUpset, &containsdown
        );
        delete[] stubbornstack;
        delete[] onstack;
    }

    // initialise dfsnumber,lowlink and highest_lowlink
    statenumber_t currentDFSNumber = 1;
    statenumber_t highest_lowlink = 1;

    // set initial dfs to one
    setDFS(payload, ++currentDFSNumber);

    statenumber_t currentLowlink = currentDFSNumber;

    while (true)
    {
        if (currentEntry-- > 0)
        {
            // fire the next enabled transition
            arrayindex_t t = currentFirelist[currentEntry];
            if (getDFS(payload) > lastfired[t])
                lastfired[t] = getDFS(payload);
            net->fire(ns, t);

            // already in store eg visited?
            statenumber_t* newPayload;

            // search and insert the current netstate
            if (myStore.searchAndInsert(ns, &newPayload, 0))
            {
                // already visited
                // get the dfs number from the found payload
                statenumber_t newDFS = getDFS(newPayload);
                // backfire the transition because already known
                net->backfire(ns, currentFirelist[currentEntry]);

                // set the lowlink of the top element on the stack to this
                // because we have reached it from this state from the
                // topelement... (only if its smaller)
                if (dfsstack.StackPointer != 0)
                {
                    if (newDFS < currentLowlink)
                    {
                        currentLowlink = newDFS;
                    }
                }
            }
            else
            {
                // not yet visited
                // set the dfs number
                new (dfsstack.push())
                    DFSStackEntry(currentFirelist, currentEntry, payload, currentLowlink);
                setDFS(newPayload, ++currentDFSNumber);
                // set the currentlowlink
                currentLowlink = currentDFSNumber;
                payload = newPayload;

                // put an entry on the searchstack
                // with the current dfs number and the current lowlink

                // check the given property and
                // update enabled transitions
                net->updateEnabled(ns, currentFirelist[currentEntry]);
                property.value = property.checkProperty(ns, currentFirelist[currentEntry]);
                if (a->sum - a->threshold > result)
                {
                    result = a->sum - a->threshold;
                    if (result == StructuralBound)
                        return result;
                }

                // update the firelist
                currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
                for (arrayindex_t i = 0; i < currentEntry; i++)
                {
                    if (inDownSet[currentFirelist[i]])
                    {
                        containsdown = true;
                        break;
                    }
                }
                if (containsdown)
                {
                    delete[] currentFirelist;
                    arrayindex_t* stubbornstack = new arrayindex_t[net->Card[TR]];
                    memcpy(stubbornstack, Upset, cardUpset * sizeof(arrayindex_t));
                    bool* onstack = new bool[net->Card[TR]];
                    memcpy(onstack, inUpset, net->Card[TR] * sizeof(bool));
                    currentEntry = flclose->getFirelist(
                        ns, &currentFirelist, stubbornstack, onstack, cardUpset, &containsdown
                    );
                    delete[] stubbornstack;
                    delete[] onstack;
                }
            }
        }
        else
        {
            // check if this is the root of a tscc
            if (getDFS(payload) == currentLowlink && highest_lowlink < currentLowlink)
            {
                // this is root of tscc
                // --> check for ignored transitions in an up-set of this marking
                bool needEnabled = false;

                arrayindex_t* ups = new arrayindex_t[net->Card[TR]];
                memcpy(ups, Upset, cardUpset * sizeof(arrayindex_t));
                bool* inups = new bool[net->Card[TR]];
                memcpy(inups, inUpset, net->Card[TR] * sizeof(bool));
                arrayindex_t cardups = cardUpset;
                for (arrayindex_t i = 0; i < cardups; i++)
                {
                    arrayindex_t t = ups[i];
                    if (lastfired[t] >= getDFS(payload))
                    {
                        // this transition is not ignored
                        inups[t] = false;
                        ups[i--] = ups[--cardups];
                    }
                    else
                    {
                        lastfired[t] = getDFS(payload);
                    }
                }
                if (cardups)
                {
                    // there are indeed ignored transitions

                    // compute new stubborn set around ignored transitions
                    delete[] currentFirelist;
                    currentEntry = flclose->getFirelist(
                        ns, &currentFirelist, ups, inups, cardups, &needEnabled
                    );
                    delete[] ups;
                    delete[] inups;
                    if (currentEntry)
                        continue;
                }
                // this tscc is finally closed

                // update the highest lowlink to the current
                highest_lowlink = currentLowlink;
            }
            // getting v'
            delete[] currentFirelist;
            statenumber_t tmpLowlink = currentLowlink;
            if (dfsstack.StackPointer == 0)
            {
                // set the property value
                return result;
            }
            DFSStackEntry& stackentry = dfsstack.top();
            currentLowlink = stackentry.lowlink;
            currentEntry = stackentry.flIndex;
            currentFirelist = stackentry.fl;
            payload = reinterpret_cast<statenumber_t*>(stackentry.payload);
            stackentry.fl = NULL;
            dfsstack.pop();

            if (currentLowlink > tmpLowlink)
            {
                currentLowlink = tmpLowlink;
            }
            assert(currentEntry < net->Card[TR]);
            // backfire and revert the enabledness
            net->backfire(ns, currentFirelist[currentEntry]);
            net->revertEnabled(ns, currentFirelist[currentEntry]);

            // update the property
            property.updateProperty(ns, currentFirelist[currentEntry]);
        }
    }
}

int ComputeBoundExplorationRelaxed::lp(
    SimpleProperty& property, NetState& ns, Store<statenumber_t>& myStore, Firelist& myFirelist, int
)
{
    StatePredicateProperty* sp = reinterpret_cast<StatePredicateProperty*>(&property);
    sp->createDownSets(sp->predicate);
    AtomicStatePredicate* a = reinterpret_cast<AtomicStatePredicate*>(sp->predicate);
    // Create the mip solver with the SCIP backend.
    operations_research::MPSolver* solver(operations_research::MPSolver::CreateSolver("SCIP"));
    if (!solver)
    {
        return MAX_CAPACITY;
    }
    // declare variables for all places and transitions
    const double infinity = solver->infinity();
    operations_research::MPVariable** pvars = new operations_research::MPVariable*[net->Card[PL]];
    operations_research::MPVariable** tvars = new operations_research::MPVariable*[net->Card[TR]];
    for (int i = 0; i < net->Card[PL]; i++)
    {
        pvars[i] = solver->MakeIntVar(0.0, RT::args.safe_given ? 1.0 : infinity, net->Name[PL][i]);
    }
    for (int i = 0; i < net->Card[TR]; i++)
    {
        tvars[i] = solver->MakeIntVar(0.0, infinity, net->Name[TR][i]);
    }
    // add constraints from state equation (one equation per place)
    operations_research::MPConstraint** C = new operations_research::MPConstraint*[net->Card[PL]];
    for (int i = 0; i < net->Card[PL]; i++)
    {
        C[i] = solver->MakeRowConstraint(net->Initial[i], net->Initial[i], net->Name[PL][i]);
        C[i]->SetCoefficient(pvars[i], 1);
    }
    for (int j = 0; j < net->Card[TR]; j++)
    {
        for (int k = 0; k < net->CardDeltaT[PRE][j]; k++)
        {
            C[net->DeltaT[PRE][j][k]]->SetCoefficient(tvars[j], 1.0 * net->MultDeltaT[PRE][j][k]);
        }
        for (int k = 0; k < net->CardDeltaT[POST][j]; k++)
        {
            C[net->DeltaT[POST][j][k]]->SetCoefficient(tvars[j], -net->MultDeltaT[POST][j][k]);
        }
    }
    /*
        // nested unit info
        pthread_mutex_lock(&nestedmutex);
        for (nestedunit* uuu = startunit; uuu; uuu = uuu->nextunit)
        {
            if (!uuu->leafunit)
                continue;
            for (nestedunit* nnn = uuu; nnn; nnn = nnn->nextinunit)
            {
                nnn->marked = true;
            }
            operations_research::MPConstraint* Nest = solver->MakeRowConstraint(
                -infinity, 1.0, "nested unit"
            );
            for (int i = 0; i < net->Card[PL]; i++)
            {
                if (!(((PlaceSymbol*)(net->thesymbol[PL][i]))->nested->marked))
                    continue;
                Nest->SetCoefficient(pvars[i], 1.0);
            }
            for (nestedunit* nnn = uuu; nnn; nnn = nnn->nextinunit)
            {
                nnn->marked = false;
            }
        }
        pthread_mutex_unlock(&nestedmutex);
    */
    // set objective function
    operations_research::MPObjective* const objective = solver->MutableObjective();
    for (int i = 0; i < a->cardPos; i++)
    {
        objective->SetCoefficient(pvars[a->posPlaces[i]], 1.0 * a->posMult[i]);
    }
    for (int i = 0; i < a->cardNeg; i++)
    {
        objective->SetCoefficient(pvars[a->negPlaces[i]], -1.0 * a->negMult[i]);
    }
    objective->SetMaximization();

    bool* trap = new bool[net->Card[PL]];
    while (true)
    {
        // run the solver
        const operations_research::MPSolver::ResultStatus result_status = solver->Solve();
        // Check that the problem has an optimal solution .

        if (result_status != operations_research::MPSolver::OPTIMAL)
        {
            a->upper_bound = MAX_CAPACITY;
            delete solver;
            return MAX_CAPACITY;
        }
        // use traps to check whether solution is spurious

        int marked = 0;
        int cardtrap = 0;
        for (int i = 0; i < net->Card[PL]; i++)
        {
            if (pvars[i]->solution_value() < 0.4)
            {
                trap[i] = true;
                cardtrap++;
                if (net->Initial[i])
                {
                    marked++;
                }
            }
            else
            {
                trap[i] = false;
            }
        }
        bool somethingchanged = true;
        while (somethingchanged)
        {
            somethingchanged = false;
            for (int i = 0; i < net->Card[TR]; i++)
            {
                bool havepostplace = false;
                for (int j = 0; j < net->CardArcs[TR][POST][i]; j++)
                {
                    if (trap[net->Arc[TR][POST][i][j]])
                    {
                        havepostplace = true;
                        break;
                    }
                }
                if (havepostplace)
                    continue;

                // here, we have transition that does not produce into trap

                for (int j = 0; j < net->CardArcs[TR][PRE][i]; j++)
                {
                    int p = net->Arc[TR][PRE][i][j];
                    if (trap[p])
                    {
                        // p needs to be removed from trap
                        trap[p] = false;
                        cardtrap--;
                        if (net->Initial[p])
                        {
                            marked--;
                        }
                        somethingchanged = true;
                        if (cardtrap == 0)
                            break;
                    }
                }
                if (cardtrap == 0)
                    break;
            }
            if (cardtrap == 0)
                break;
        }
        // here: have a trap
        if (cardtrap == 0 || marked == 0)
        {
            // have
            int result = static_cast<int>(objective->Value());
            a->upper_bound = result;
            delete solver;
            return result;
        }

        // have detected spurious solution -> add constraint to exclude empty trap
        operations_research::MPConstraint* Trap = solver->MakeRowConstraint(1.0, infinity, "trap");
        for (int i = 0; i < net->Card[PL]; i++)
        {
            Trap->SetCoefficient(pvars[i], 1.0);
        }
        LOLA_LOG(info, "Excluded spurious solution");
    }
}
