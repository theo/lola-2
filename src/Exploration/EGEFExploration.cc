/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status approved 23.05.2012, changed

\brief Evaluates EGEF property
*/

#include <Core/Dimensions.h>
#include <CoverGraph/CoverGraph.h>
#include <Exploration/EGEFExploration.h>
#include <Exploration/StatePredicateProperty.h>
#include <Exploration/Firelist.h>
#include <Core/Runtime.h>

bool EGEFExploration::depth_first(
    StatePredicateProperty& property, NetState& ns, Store<int>& myStore, Firelist& myFirelist, int
)
{
    // net->print();
    //  main original ideas for this type of search:
    //  1. store remembers:
    //  - on stack (bit 0)
    //  - some successor satisfies phi (bit 1)
    //  2. remember earliest back egde (--> "cycle")
    //      satisfies if between cycle and phi-satisfying state
    //  remember dfs (bits 2....)

    // prepare property
    int earliestcycle = 0;  // no cycle known yet
    property.value = property.initProperty(ns);

    int* stateinfo;
    myStore.searchAndInsert(ns, &stateinfo, 0);

    arrayindex_t* currentFirelist;
    arrayindex_t currentEntry;
    int dfsnum = 1;
    int maxdfsnum = 1;
    currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
    if (property.value)
    {
        LOLA_LOG(debug, "PROP HOLDS IN INIT");
        *stateinfo = 7;  // state 1 + on stack + has phi-successor
    }
    else
    {
        LOLA_LOG(debug, "PROP DOES NOT HOLD IN INIT");
        *stateinfo = 5;  // state 1 + on stack + has no phi-successor;
    }

    while (true)  // exit when trying to pop from empty stack
    {
        LOLA_LOG(
            debug,
            [&]()
            {
                RT::log<debug>(
                    "considering... dfs: {} earliest: {} onstack: {}  phisucc: {}", dfsnum,
                    earliestcycle, *stateinfo & 1, *stateinfo & 2
                );
                for (arrayindex_t i = 0; i < net->Card[PL]; i++)
                {
                    RT::log<debug>("{}: {}", net->Name[PL][i], ns.Current[i]);
                }
            }
        );

        if (currentEntry-- > 0)
        {
            // there is a next transition that needs to be explored in current marking

            // fire this transition to produce new arking::Current
            LOLA_LOG(
                debug, "firing {} at list pos {}", net->Name[TR][currentFirelist[currentEntry]],
                currentEntry
            );
            net->fire(ns, currentFirelist[currentEntry]);

            int* newstateinfo;
            if (myStore.searchAndInsert(ns, &newstateinfo, 0))
            {
                LOLA_LOG(
                    debug, "succ exists. dfs: {} onstack: {} efphi: {}", *newstateinfo / 4,
                    *newstateinfo & 1, *newstateinfo & 2
                );
                // State exists!

                // 1. check back edge:
                if ((*newstateinfo) & 1)
                {
                    int otherdfs = *newstateinfo / 4;
                    if ((otherdfs < earliestcycle) || earliestcycle == 0)
                        earliestcycle = otherdfs;
                }

                // 2. check phi-succ
                if ((*newstateinfo) & 2)
                {
                    *stateinfo |= 2;
                }

                // 3. check EGEF prop
                if (((*stateinfo) & 2) && earliestcycle)
                {
                    LOLA_LOG(debug, "return true by fresh cycle");
                    return true;
                }
                LOLA_LOG(
                    debug, "backtracking to dfs:  {} onstack {} efphi {}", *stateinfo / 4,
                    *stateinfo & 1, *stateinfo & 2
                );
                // 4. backtrack
                net->backfire(ns, currentFirelist[currentEntry]);
            }
            else
            {
                LOLA_LOG(debug, "state is new.");
                // State does not exist!
                new (stack.push()) EGEFStackFrame(currentFirelist, currentEntry, dfsnum, stateinfo);
                net->updateEnabled(ns, currentFirelist[currentEntry]);
                property.value = property.checkProperty(ns, currentFirelist[currentEntry]);
                dfsnum = ++maxdfsnum;
                stateinfo = newstateinfo;
                *stateinfo = 4 * dfsnum + 1;  // state + onstack
                if (property.value)
                {
                    LOLA_LOG(debug, "new state sat phi.");
                    *stateinfo |= 2;
                    // check EGEF prop
                    if (earliestcycle)
                    {
                        LOLA_LOG(debug, "return true by fresh phi");
                        return true;
                    }
                    if (ns.CardEnabled == 0)
                    {
                        LOLA_LOG(debug, "return true by deadlock phi");
                        return true;  // special case: deadlock sat phi -> EGEF phi
                    }
                }
                currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
                continue;
            }  // end else branch for "if state exists"
        }
        else
        {
            LOLA_LOG(debug, "close state dfs {} SP {}", dfsnum, stack.StackPointer);
            // firing list completed -->close state and return to previous state
            delete[] currentFirelist;
            int efphi = (*stateinfo) & 2;
            *stateinfo ^= 1;  // toggle "on stack" bit
            if (stack.StackPointer == 0)
            {
                // have completely processed initial marking --> state not found
                return false;
            }
            EGEFStackFrame& frame = stack.top();
            currentEntry = frame.current;
            currentFirelist = frame.fl;
            dfsnum = frame.dfs;
            stateinfo = frame.stinfo;
            stack.pop();
            net->backfire(ns, currentFirelist[currentEntry]);
            net->revertEnabled(ns, currentFirelist[currentEntry]);
            property.value = property.updateProperty(ns, currentFirelist[currentEntry]);
            if (efphi)
            {
                *stateinfo |= 2;
                if (earliestcycle)
                {
                    return true;
                }
            }
            if (dfsnum < earliestcycle)
                earliestcycle = 0;
        }
    }
}

Path EGEFExploration::path() const { return _p; }
