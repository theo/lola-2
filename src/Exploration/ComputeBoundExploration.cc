/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Karsten
\status approved 23.05.2012, changed

\brief Evaluates simple property (only SET of states needs to be computed).
Actual property is a parameter of the constructor
*/

#include <Core/Dimensions.h>
#include <CoverGraph/CoverGraph.h>
#include <Planning/StoreCreator.h>
#include <Exploration/ComputeBoundExploration.h>
#include <Exploration/SimpleProperty.h>
#include <Exploration/StatePredicateProperty.h>
#include <Exploration/ChooseTransition.h>
#include <Net/LinearAlgebra.h>
#include <SweepLine/Sweep.h>
#include <Formula/StatePredicate/AtomicStatePredicate.h>
#include <Exploration/ChooseTransition.h>
#include <Exploration/ChooseTransitionHashDriven.h>
#include <Exploration/ChooseTransitionRandomly.h>
#include <Frontend/Parser/ParserPnml.h>

#include "ortools/linear_solver/linear_solver.h"

extern pthread_mutex_t nestedmutex;
extern nestedunit* startunit;

/*!
The result will be the maximum (over all reachable markings) of the given formal sum of places.

\param property  contains the expression to be checked as atomic proposition
\param ns  The initial state of the net has to be given as a net state object.
\param myStore  the store to be used. The selection of the store may greatly influence the
performance of the program
\param myFirelist  the firelists to use in this search. The firelist _must_ be
applicable to the given property, else the result of this function may be
wrong. It is not guaranteed that the given firelist will actually be used. In
the parallel work-mode the given list will just be used as a base list form
which all other lists will be generated
\param threadNumber  will be ignored by the standard seach. In the parallel
execution mode this number indicates the number of threads to be used for the
search
*/
int ComputeBoundExploration::depth_first_num(
    SimpleProperty& property, NetState& ns, Store<void>& myStore, Firelist& myFirelist, int
)
{
    StatePredicateProperty* p = reinterpret_cast<StatePredicateProperty*>(&property);
    AtomicStatePredicate* a = reinterpret_cast<AtomicStatePredicate*>(p->predicate);
    StructuralBound = a->upper_bound;
    StructuralBound -= a->threshold;

    StatePredicateProperty* sp = reinterpret_cast<StatePredicateProperty*>(&property);
    LOLA_LOG(debug, "SEARCH START ON {}", sp->predicate->toString());
    sp->createDownSets(sp->predicate);
    LOLA_LOG(info, "Structural Bound: {}", StructuralBound);

    // prepare property
    property.value = property.initProperty(ns);
    result = a->sum - a->threshold;
    if (result == StructuralBound)
        return result;

    // add initial marking to store
    // we do not care about return value since we know that store is empty

    myStore.searchAndInsert(ns, 0, 0);

    // get first firelist
    arrayindex_t* currentFirelist;
    arrayindex_t currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
    while (true)
    {  // exit when trying to pop from empty stack
        if (currentEntry-- > 0)
        {
            // there is a next transition that needs to be explored in current marking

            // fire this transition to produce new Marking::Current
            net->fire(ns, currentFirelist[currentEntry]);

            if (myStore.searchAndInsert(ns, 0, 0))
            {
                // State exists! -->backtracking to previous state
                net->backfire(ns, currentFirelist[currentEntry]);
            }
            else
            {
                // State does not exist!
                net->updateEnabled(ns, currentFirelist[currentEntry]);
                // check current marking for property
                property.value = property.checkProperty(ns, currentFirelist[currentEntry]);
                if (a->sum - a->threshold > result)
                {
                    result = a->sum - a->threshold;
                    if (result == StructuralBound)
                        return result;
                }

                SimpleStackEntry* stack = property.stack.push();
                stack = new (stack) SimpleStackEntry(currentFirelist, currentEntry);
                currentEntry = myFirelist.getFirelist(ns, &currentFirelist);
            }  // end else branch for "if state exists"
        }
        else
        {
            // firing list completed -->close state and return to previous state
            delete[] currentFirelist;
            if (property.stack.StackPointer == 0)
            {
                // have completely processed initial marking --> state not found
                return result;
            }
            SimpleStackEntry& stack = property.stack.top();
            currentEntry = stack.current;
            currentFirelist = stack.fl;
            stack.fl = NULL;
            property.stack.pop();
            assert(currentEntry < net->Card[TR]);
            net->backfire(ns, currentFirelist[currentEntry]);
            net->revertEnabled(ns, currentFirelist[currentEntry]);
            property.value = property.updateProperty(ns, currentFirelist[currentEntry]);
        }
    }
    return false;
}
void printsolver(
    operations_research::MPSolver* solver, operations_research::MPVariable** orplace, int,
    operations_research::MPVariable** ortransition, int
);

int ComputeBoundExploration::lp(Petrinet* net, AtomicStatePredicate* a)
{
    // Create the mip solver with the SCIP backend.
    operations_research::MPSolver* solver(operations_research::MPSolver::CreateSolver("SCIP"));
    if (!solver)
    {
        a->upper_bound = MAX_CAPACITY;
        return MAX_CAPACITY;
    }
    // declare variables for all places and transitions
    const double infinity = solver->infinity();
    operations_research::MPVariable** pvars = new operations_research::MPVariable*[net->Card[PL]];
    operations_research::MPVariable** tvars = new operations_research::MPVariable*[net->Card[TR]];
    for (int i = 0; i < net->Card[PL]; i++)
    {
        pvars[i] = solver->MakeIntVar(0.0, RT::args.safe_given ? 1.0 : infinity, net->Name[PL][i]);
    }
    for (int i = 0; i < net->Card[TR]; i++)
    {
        tvars[i] = solver->MakeIntVar(0.0, infinity, net->Name[TR][i]);
    }
    // add constraints from state equation (one equation per place)
    operations_research::MPConstraint** C = new operations_research::MPConstraint*[net->Card[PL]];
    for (int i = 0; i < net->Card[PL]; i++)
    {
        C[i] = solver->MakeRowConstraint(net->Initial[i], net->Initial[i], net->Name[PL][i]);
        C[i]->SetCoefficient(pvars[i], 1.0);
    }
    for (int j = 0; j < net->Card[TR]; j++)
    {
        for (int k = 0; k < net->CardDeltaT[PRE][j]; k++)
        {
            C[net->DeltaT[PRE][j][k]]->SetCoefficient(tvars[j], 1.0 * net->MultDeltaT[PRE][j][k]);
        }
        for (int k = 0; k < net->CardDeltaT[POST][j]; k++)
        {
            C[net->DeltaT[POST][j][k]]->SetCoefficient(
                tvars[j], -1.0 * net->MultDeltaT[POST][j][k]
            );
        }
    }
    /*
        // nested unit info
        pthread_mutex_lock(&nestedmutex);
        for (nestedunit* uuu = startunit; uuu; uuu = uuu->nextunit)
        {
            if (!uuu->leafunit)
                continue;
            for (nestedunit* nnn = uuu; nnn; nnn = nnn->nextinunit)
            {
                nnn->marked = true;
            }
            operations_research::MPConstraint* Nest = solver->MakeRowConstraint(
                -infinity, 1.0, "nested unit"
            );
            for (int i = 0; i < net->Card[PL]; i++)
            {
                if (!(((PlaceSymbol*)(net->thesymbol[PL][i]))->nested->marked))
                    continue;
                Nest->SetCoefficient(pvars[i], 1.0);
            }
            for (nestedunit* nnn = uuu; nnn; nnn = nnn->nextinunit)
            {
                nnn->marked = false;
            }
        }
        pthread_mutex_unlock(&nestedmutex);
    */
    // set objective function
    operations_research::MPObjective* const objective = solver->MutableObjective();
    for (int i = 0; i < a->cardPos; i++)
    {
        objective->SetCoefficient(pvars[a->posPlaces[i]], 1.0 * a->posMult[i]);
    }
    for (int i = 0; i < a->cardNeg; i++)
    {
        objective->SetCoefficient(pvars[a->negPlaces[i]], -1.0 * a->negMult[i]);
    }
    objective->SetMaximization();

    bool* trap = new bool[net->Card[PL]];
    int attempts_to_go = RT::args.trapcheckdepth_arg;
    while (true)
    {
        // printsolver(solver,pvars, net->Card[PL],tvars,net->Card[TR]);
        //  run the solver
        const operations_research::MPSolver::ResultStatus result_status = solver->Solve();
        // Check that the problem has an optimal solution .

        if (result_status != operations_research::MPSolver::OPTIMAL || attempts_to_go-- == 0)
        {
            a->upper_bound = MAX_CAPACITY;
            delete solver;
            return MAX_CAPACITY;
        }

        // use traps to check whether solution is spurious

        int marked = 0;
        int cardtrap = 0;
        for (int i = 0; i < net->Card[PL]; i++)
        {
            if (pvars[i]->solution_value() < 0.4)
            {
                trap[i] = true;
                cardtrap++;
                if (net->Initial[i])
                {
                    marked++;
                }
            }
            else
            {
                trap[i] = false;
            }
        }
        bool somethingchanged = true;
        while (somethingchanged)
        {
            somethingchanged = false;
            for (int i = 0; i < net->Card[TR]; i++)
            {
                bool havepostplace = false;
                for (int j = 0; j < net->CardArcs[TR][POST][i]; j++)
                {
                    if (trap[net->Arc[TR][POST][i][j]])
                    {
                        havepostplace = true;
                        break;
                    }
                }
                if (havepostplace)
                    continue;

                // here, we have transition that does not produce into trap

                for (int j = 0; j < net->CardArcs[TR][PRE][i]; j++)
                {
                    int p = net->Arc[TR][PRE][i][j];
                    if (trap[p])
                    {
                        // p needs to be removed from trap
                        trap[p] = false;
                        cardtrap--;
                        if (net->Initial[p])
                        {
                            marked--;
                        }
                        somethingchanged = true;
                        if (cardtrap == 0)
                            break;
                    }
                }
                if (cardtrap == 0)
                    break;
            }
            if (cardtrap == 0)
                break;
        }
        // here: have a trap
        if (cardtrap == 0 || marked == 0)
        {
            // have
            int result = static_cast<int>(objective->Value());
            a->upper_bound = result;
            delete solver;
            return result;
        }

        // have detected spurious solution -> add constraint to exclude empty trap
        operations_research::MPConstraint* Trap = solver->MakeRowConstraint(1.0, infinity, "trap");
        for (int i = 0; i < net->Card[PL]; i++)
        {
            Trap->SetCoefficient(pvars[i], 1.0);
        }
        LOLA_LOG(info, "Excluded spurious solution");
    }
}

void* ComputeBoundExploration::zerooneinvariantthread(void* vvv)
{
    Petrinet* nnn = Petrinet::InitialNet;
    operations_research::MPSolver* solver(operations_research::MPSolver::CreateSolver("SCIP"));
    if (!solver)
    {
        return NULL;
    }
    /* set all variables to integer and bound 1 */
    operations_research::MPVariable** vars = new operations_research::MPVariable*[nnn->Card[PL]];
    for (int i = 0; i < nnn->Card[PL]; i++)
    {
        vars[i] = solver->MakeIntVar(0.0, 0.1, nnn->Name[PL][i]);
    }

    LOLA_LOG(debug, "SOLVER CREATED");
    // fill invariant equations
    for (int i = 0; i < nnn->Card[TR]; i++)
    {
        operations_research::MPConstraint* C = solver->MakeRowConstraint(
            0.0, 0.0, nnn->Name[TR][i]
        );
        for (int j = 0; j < nnn->CardDeltaT[PRE][i]; j++)
        {
            C->SetCoefficient(vars[nnn->DeltaT[PRE][i][j]], -1.0 * nnn->MultDeltaT[PRE][i][j]);
        }
        for (int j = 0; j < nnn->CardDeltaT[POST][i]; j++)
        {
            C->SetCoefficient(vars[nnn->DeltaT[POST][i][j]], 1.0 * nnn->MultDeltaT[POST][i][j]);
        }
    }
    LOLA_LOG(debug, "INCIDENCE MATRIX LOADED");
    // create constraint for having only one marked place (m[p] == 1) in support
    operations_research::MPConstraint* S = solver->MakeRowConstraint(1.0, 1.0, "one place marked");
    for (int i = 0; i < nnn->Card[PL]; i++)
    {
        if (nnn->Initial[i])
        {
            S->SetCoefficient(vars[i], 1.0);
        }
    }

    // nested unit info
    /*
        pthread_mutex_lock(&nestedmutex);
        for (nestedunit* uuu = startunit; uuu; uuu = uuu->nextunit)
        {
            if (!uuu->leafunit)
                continue;
            for (nestedunit* nnn = uuu; nnn; nnn = nnn->nextinunit)
            {
                nnn->marked = true;
            }
            operations_research::MPConstraint* Nest = solver->MakeRowConstraint(
                0.0, 1.0, "nested unit"
            );
            for (int i = 0; i < nnn->Card[PL]; i++)
            {
                if (!(((PlaceSymbol*)(nnn->thesymbol[PL][i]))->nested->marked))
                    continue;
                Nest->SetCoefficient(vars[i], 1.0);
            }
            for (nestedunit* nn = uuu; nn; nn = nn->nextinunit)
            {
                nn->marked = false;
            }
        }
        pthread_mutex_unlock(&nestedmutex);
    */
    LOLA_LOG(debug, "REMAINING CONSTRAINTS");

    // prepare objective function
    operations_research::MPObjective* objective = solver->MutableObjective();
    objective->SetMaximization();

    ternary_t* resultvector = new ternary_t[nnn->Card[PL]];
    for (arrayindex_t i = 0; i < nnn->Card[PL]; i++)
    {
        resultvector[i] = TERNARY_VOID;
        if (nnn->Initial[i] > 1)
        {
            resultvector[i] = TERNARY_FALSE;
            portfoliomanager::synchroniseplacevector(resultvector, GLOBAL_PREPROCESSING);
            goto finish;
        }
    }
    while (true)
    {
        /* set the objective function */

        for (int i = 0; i < nnn->Card[PL]; i++)
        {
            objective->SetCoefficient(vars[i], TERNARY_VOID ? 1.0 : 0.0);
        }

        LOLA_LOG(debug, "LAUNCH SOLVER");
        const operations_research::MPSolver::ResultStatus result_status = solver->Solve();
        if (result_status != operations_research::MPSolver::OPTIMAL)
        {
            goto finish;
        }
        LOLA_LOG(debug, "SOLVER RETURNED");

        if (objective->Value() <= 0.5)
        {
            // no new variable in support
            goto finish;
        }

        // evaluate solution
        int cardnewsolved = 0;
        for (int i = 0; i < nnn->Card[PL]; i++)
        {
            if (vars[i]->solution_value() > 0.5)  // in support of invariant
            {
                if (resultvector[i] == TERNARY_VOID)
                {
                    LOLA_LOG(debug, "HURRA");
                    cardnewsolved++;
                }
                resultvector[i] = TERNARY_TRUE;
            }
        }
        portfoliomanager::synchroniseplacevector(resultvector, GLOBAL_ZEROONE);
        if (!portfoliomanager::cardtodo)
        {
            goto finish;
        }
    }
finish:  // report to be ready
    delete solver;
    return NULL;
}

// LCOV_EXCL_START
Path ComputeBoundExploration::path() const { return *(new Path(net)); }
// LCOV_EXCL_STOP

void ComputeBoundExploration::check_safety(
    SimpleProperty& property, NetState& ns, Store<void>& myStore,
    FirelistStubbornComputeBound& myFirelist, int
)
{
    arrayindex_t p = portfoliomanager::gettargetplace();
    // add initial marking to store
    // we do not care about return value since we know that store is empty

    Petrinet* net = Petrinet::InitialNet;
    myStore.searchAndInsert(ns, 0, 0);

    // get first firelist
    arrayindex_t* currentFirelist;
    arrayindex_t currentEntry = myFirelist.getFirelist(p, ns, &currentFirelist);
    while (true)
    {  // exit when trying to pop from empty stack
        if (currentEntry-- > 0)
        {
            arrayindex_t ttt = currentFirelist[currentEntry];
            // there is a next transition that needs to be explored in current marking

            // fire this transition to produce new Marking::Current
            net->fire(ns, ttt);

            if (myStore.searchAndInsert(ns, 0, 0))
            {
                // State exists! -->backtracking to previous state
                net->backfire(ns, ttt);
            }
            else
            {
                // State does not exist!
                net->updateEnabled(ns, ttt);
                // check current marking for property
                for (arrayindex_t i = 0; i < net->CardArcs[TR][POST][ttt]; i++)
                {
                    if (ns.Current[net->Arc[TR][POST][ttt][i]] > 1)
                    {
                        portfoliomanager::synchroniseplace(p, TERNARY_FALSE, GLOBAL_SEARCH);
                        p = -1;
                        return;
                    }
                }

                SimpleStackEntry* stack = property.stack.push();
                stack = new (stack) SimpleStackEntry(currentFirelist, currentEntry);
                currentEntry = myFirelist.getFirelist(p, ns, &currentFirelist);
            }
        }
        else
        {
            // firing list completed -->close state and return to previous state
            delete[] currentFirelist;
            if (property.stack.StackPointer == 0)
            {
                // have completely processed initial marking --> state not found
                portfoliomanager::synchroniseplace(p, TERNARY_TRUE, GLOBAL_SEARCH);
                return;
            }
            SimpleStackEntry& stack = property.stack.top();
            currentEntry = stack.current;
            currentFirelist = stack.fl;
            stack.fl = NULL;
            property.stack.pop();
            assert(currentEntry < net->Card[TR]);
            net->backfire(ns, currentFirelist[currentEntry]);
            net->revertEnabled(ns, currentFirelist[currentEntry]);
        }
    }
}

void* ComputeBoundExploration::Safety(void*)
{
    // do whole planning stuff
    // create Mara object

    Petrinet* net = Petrinet::InitialNet;
    Mara* memory = new Mara();
    memory->taskid = 3;
    portfoliomanager::taskjson[3] = JSON();
    NetState* ns;

    // init target place

    while (true)
    {
        Store<void>* store = StoreCreator::createStore<void>(net, memory, 1);
        ns = NetState::createNetStateFromInitial(net);
        SimpleProperty* property = new SimpleProperty(net);
        // run for target place
        check_safety(*property, *ns, *store, *new FirelistStubbornComputeBound(net, NULL), 1);

        // reset Mara memory
        memory->reset();
        delete property;
    }
}

void* ComputeBoundExploration::findpathsafetythread(void*)
{
    // initialise and get target
    Petrinet* net = Petrinet::InitialNet;
    FirelistStubbornComputeBound* myFirelist = new FirelistStubbornComputeBound(net, NULL);
    NetState ns = *NetState::createNetStateFromInitial(net);

    arrayindex_t targetplace = portfoliomanager::gettargetplace();
    while (true)
    {
        unsigned int attempts = RT::args.retrylimit_arg;
        if (attempts == 0)
            attempts = 1000;
        unsigned int maxdepth = RT::args.depthlimit_arg;
        unsigned int currentattempt = 0;

        // loop #attempts times
        while (currentattempt++ < attempts)
        {
            // copy initial marking into current marking
            memcpy(ns.Current, net->Initial, net->Card[PL] * sizeof(arrayindex_t));
            ns.HashCurrent = net->HashInitial;

            // reset enabledness information
            ns.CardEnabled = net->Card[TR];
            for (arrayindex_t t = 0; t < net->Card[TR]; ++t)
            {
                ns.Enabled[t] = true;
            }

            for (arrayindex_t t = 0; t < net->Card[TR]; ++t)
            {
                net->checkEnabled(ns, t);
            }

            // check property
            for (arrayindex_t i = 0; i < net->Card[PL]; i++)
            {
                if (ns.Current[i] > 1)
                {
                    portfoliomanager::synchroniseplace(i, TERNARY_FALSE, GLOBAL_FINDPATH);
                    return NULL;
                }
            }

            // generate a firing sequence until given depth or deadlock is reached
            for (arrayindex_t depth = 0; depth < maxdepth; ++depth)
            {
                // get firelist
                arrayindex_t* currentFirelist;
                arrayindex_t cardFirelist = myFirelist->getFirelist(
                    targetplace, ns, &currentFirelist
                );
                if (cardFirelist == 0)
                {
                    // deadlock or empty up set (i.e. property not reachable)
                    break;  // go to next attempt
                }

                arrayindex_t chosen = rand() % ((cardFirelist * (cardFirelist + 1)) / 2);
                int i;
                for (i = 0; i < cardFirelist; i++)
                {
                    if (chosen > ((cardFirelist - i - 1) * (cardFirelist - i)) / 2)
                    {
                        chosen = currentFirelist[i];
                        break;
                    }
                }
                if (i >= cardFirelist)
                    chosen = currentFirelist[0];
                if (i >= cardFirelist)  // safety belt, condition should be always false
                {
                    chosen = currentFirelist[0];
                }
                delete[] currentFirelist;

                net->fire(ns, chosen);
                net->updateEnabled(ns, chosen);

                // check property
                for (int i = 0; i < net->CardOnlyPost[TR][chosen]; i++)
                {
                    if (ns.Current[net->OnlyPost[TR][chosen][i]] > 1)
                    {
                        portfoliomanager::synchroniseplace(
                            net->OnlyPost[TR][chosen][i], TERNARY_FALSE, GLOBAL_FINDPATH
                        );
                        return NULL;
                    }
                }
                for (int i = 0; i < net->CardGreaterPost[TR][chosen]; i++)
                {
                    if (ns.Current[net->GreaterPost[TR][chosen][i]] > 1)
                    {
                        portfoliomanager::synchroniseplace(
                            net->GreaterPost[TR][chosen][i], TERNARY_FALSE, GLOBAL_FINDPATH
                        );
                        return NULL;
                    }
                }
            }
        }
        // we did not find a state after #attempts
        // select new target place
        portfoliomanager::synchroniseplace(targetplace, TERNARY_VOID, GLOBAL_FINDPATH);
        targetplace = portfoliomanager::gettargetplace();
    }
}

bool ComputeBoundExploration::find_path(
    SimpleProperty& property, NetState& ns, Firelist& myFirelist, EmptyStore<void>& s,
    ChooseTransition& c
)
{
    // this table counts hits for various hash buckets. This is used for
    // steering search into less frequently entered areas of the state space.

    attempts = RT::args.retrylimit_arg;
    maxdepth = RT::args.depthlimit_arg;
    StatePredicateProperty* spp = (StatePredicateProperty*)&property;

    AtomicStatePredicate* a = (AtomicStatePredicate*)spp->predicate;
    if (a->upper_bound == MAX_CAPACITY)
    {
        return false;
    }
    spp->createDownSets(a);
    unsigned int currentattempt = 0;
    capacity_t result;

    // get memory for witness path info
    arrayindex_t* witness = new arrayindex_t[maxdepth];

    // loop #attempts times
    while (attempts == 0 || currentattempt++ < attempts)
    {
        // register this try
        s.tries++;

        // copy initial marking into current marking
        memcpy(ns.Current, net->Initial, net->Card[PL] * sizeof(arrayindex_t));
        ns.HashCurrent = net->HashInitial;

        // reset enabledness information
        ns.CardEnabled = net->Card[TR];
        for (arrayindex_t t = 0; t < net->Card[TR]; ++t)
        {
            ns.Enabled[t] = true;
        }

        for (arrayindex_t t = 0; t < net->Card[TR]; ++t)
        {
            net->checkEnabled(ns, t);
        }

        // prepare property
        property.value = property.initProperty(ns);
        result = a->sum - a->threshold;
        StructuralBound = a->upper_bound;
        StructuralBound -= a->threshold;
        RT::log(
            "Structural@Findpath: {} (upper {} threshold {}", StructuralBound, a->upper_bound,
            a->threshold
        );
        if (result == StructuralBound)
            return true;

        // generate a firing sequence until given depth or deadlock is reached
        for (arrayindex_t depth = 0; depth < maxdepth; ++depth)
        {
            // register this transition's firing
            s.searchAndInsert(ns, 0, 0);

            // get firelist
            arrayindex_t* currentFirelist;
            arrayindex_t cardFirelist = myFirelist.getFirelist(ns, &currentFirelist);
            if (cardFirelist == 0)
            {
                // deadlock or empty up set (i.e. property not reachable)
                break;  // go to next attempt
            }

            arrayindex_t chosen = c.choose(ns, cardFirelist, currentFirelist);
            delete[] currentFirelist;

            net->fire(ns, chosen);
            net->updateEnabled(ns, chosen);

            property.value = property.checkProperty(ns, chosen);
            result = a->sum - a->threshold;
            if (result == StructuralBound)
                return true;
        }
    }

    // we did not find a state after #attempts
    return false;
}
