#include <sstream>

#include "Invariants/tinvariants.h"
#include "ortools/linear_solver/linear_solver.h"
#include <Core/Runtime.h>

void compute_tinvariants(Petrinet* net)
{
    bool* support = new bool[net->Card[TR]];

    // launch OR-TOOLS LP solver
    operations_research::MPSolver* solver = operations_research::MPSolver::CreateSolver("SCIP");
    const double infinity = solver->infinity();
    solver->SuppressOutput();
    if (!solver)
        return;

    // define variables for LP problem
    operations_research::MPVariable** tvars = new operations_research::MPVariable*[net->Card[TR]];
    for (int i = 0; i < net->Card[TR]; i++)
    {
        tvars[i] = solver->MakeIntVar(0.0, infinity, net->Name[TR][i]);
    }

    // define objective function of LP problem
    operations_research::MPObjective* objective = solver->MutableObjective();
    for (int i = 0; i < net->Card[TR]; i++)
    {
        objective->SetCoefficient(tvars[i], 1.0);
    }
    objective->SetMinimization();

    // Exclude trivial solution
    operations_research::MPConstraint* C = solver->MakeRowConstraint(
        1.0, infinity, "exclude trivial solution"
    );
    for (int i = 0; i < net->Card[TR]; i++)
    {
        C->SetCoefficient(tvars[i], 1.0);
    }

    // add incidence matrix

    for (int j = 0; j < net->Card[PL]; j++)
    {
        // add equation for place j

        operations_research::MPConstraint* C = solver->MakeRowConstraint(
            0.0, 0.0, net->Name[PL][j]
        );
        for (int k = 0; k < net->CardArcs[PL][PRE][j]; k++)
        {
            C->SetCoefficient(tvars[net->Arc[PL][PRE][j][k]], -1.0 * net->Mult[PL][PRE][j][k]);
        }
        for (int k = 0; k < net->CardArcs[PL][POST][j]; k++)
        {
            C->SetCoefficient(
                tvars[net->Arc[PL][POST][j][k]],
                C->GetCoefficient(tvars[net->Arc[PL][POST][j][k]]) + 1.0 * net->Mult[PL][POST][j][k]
            );
        }
    }

    // run LP solver repeatedly

    while (true)
    {
        // get solution
        operations_research::MPSolver::ResultStatus ret = solver->Solve();
        if (ret == operations_research::MPSolver::INFEASIBLE)
        {
            RT::print<VERB::AUX>("NO MORE INVARIANTS");
            RT::shutdown(0);
        }
        if (ret != operations_research::MPSolver::OPTIMAL)
        {
            RT::log<critical>("Solver returned strange result, giving up.");
        }

        // record solution
        std::ostringstream out;
        out << "INVARIANT ";
        int sum = 0;
        for (int i = 0; i < net->Card[TR]; i++)
        {
            int iii = round(tvars[i]->solution_value());
            if (iii != 0)
            {
                fmt::print(out, "{}: {} ", net->Name[TR][i], iii);
                support[i] = true;
                sum += iii;
            }
            else
            {
                support[i] = false;
            }
        }
        RT::print(out.str());

        // prepare next solution

        switch (RT::args.overlapping_arg)
        {
        case overlapping_arg_yes:
        {
            // we want all minimal invariants, including overlapping

            // add constraint that rules out invariants bigger than this one

            operations_research::MPConstraint* C = solver->MakeRowConstraint(
                0.0, 1.0 * (sum - 1), "exlude existing"
            );
            for (int i = 0; i < net->Card[TR]; i++)
            {
                if (support[i])
                {
                    C->SetCoefficient(tvars[i], 1.0);
                }
            }
            break;
        }
        case overlapping_arg_simple:
        {
            // we want disjoint invariants
            for (int i = 0; i < net->Card[TR]; i++)
            {
                if (support[i])
                {
                    operations_research::MPConstraint* C = solver->MakeRowConstraint(
                        0.0, 0.0, "exlude existing"
                    );
                    C->SetCoefficient(tvars[i], 1.0);
                }
            }
            break;
        }
        case overlapping_arg_hyper:
        {
            // we want invariants that do not even share place in their environment
            for (int i = 0; i < net->Card[TR]; i++)
            {
                if (support[i])
                {
                    operations_research::MPConstraint* C = solver->MakeRowConstraint(
                        0.0, 0.0, "exlude existing"
                    );
                    C->SetCoefficient(tvars[i], 1.0);
                    for (int a = 0; a < net->CardArcs[TR][PRE][i]; a++)
                    {
                        int p = net->Arc[TR][PRE][i][a];
                        for (int b = 0; b < net->CardArcs[PL][PRE][p]; b++)
                        {
                            int tt = net->Arc[PL][PRE][p][b];
                            operations_research::MPConstraint* C = solver->MakeRowConstraint(
                                0.0, 0.0, "exlude existing"
                            );
                            C->SetCoefficient(tvars[tt], 1.0);
                        }
                        for (int b = 0; b < net->CardArcs[PL][POST][p]; b++)
                        {
                            int tt = net->Arc[PL][POST][p][b];
                            operations_research::MPConstraint* C = solver->MakeRowConstraint(
                                0.0, 0.0, "exlude existing"
                            );
                            C->SetCoefficient(tvars[tt], 1.0);
                        }
                    }
                    for (int a = 0; a < net->CardArcs[TR][POST][i]; a++)
                    {
                        int p = net->Arc[TR][POST][i][a];
                        for (int b = 0; b < net->CardArcs[PL][PRE][p]; b++)
                        {
                            int tt = net->Arc[PL][PRE][p][b];
                            operations_research::MPConstraint* C = solver->MakeRowConstraint(
                                0.0, 0.0, "exlude existing"
                            );
                            C->SetCoefficient(tvars[tt], 1.0);
                        }
                        for (int b = 0; b < net->CardArcs[PL][POST][p]; b++)
                        {
                            int tt = net->Arc[PL][POST][p][b];
                            operations_research::MPConstraint* C = solver->MakeRowConstraint(
                                0.0, 0.0, "exlude existing"
                            );
                            C->SetCoefficient(tvars[tt], 1.0);
                        }
                    }
                }
            }
            break;
        }
        default:
            break;
        }
    }
}
