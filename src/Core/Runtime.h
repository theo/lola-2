/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\status new

\brief Definition of runtime specific functionality.
*/

#pragma once

#include <cmdline.h>
#include <InputOutput/InputOutput.h>
#include <InputOutput/JSON.h>

namespace RT
{
// the command line parameters
inline gengetopt_args_info args;

// key/value store for structured information
inline JSON data;

// PID of a potential sara process; 0 if no sara process is there
inline int saraPID = 0;

// PID of a potential child process; 0 if no child is there
inline int childpid = 0;

/// collects all formula ids
inline char** formula_id = nullptr;

/// Shall we copy log message to JSON?
inline bool log_to_json = false;

/// the current file to read a Petri net from
inline Input* currentInputFile = nullptr;

/// the current file name to read a formula from
inline std::string inputFormulaFileName = "";

/// a thread that runs the reporter_thread function
inline pthread_t reporter_thread;

/// string containing the result to be put if LoLA is aborted early
inline std::string interim_result = "";
inline int preliminary_length = 0;
inline std::string* preliminary_result = nullptr;

/// indicating if a localtimelimit for a single task is needed
inline bool needLocalTimeLimit = true;

/// int containing the compound number for unique sara task files
inline int compoundNumber = 0;

/// int containing the number of compound tasks
inline int numberOfCompoundTasks = 1;

/// containing the start time of LoLA used for '--localtimelimit=0'
inline time_t startTime = 0;

/// containing the dynamic localtimelimit value
inline int localTimeLimitDynamic = 0;

/// If true abortion is in progress.
inline bool aborted = false;

/**
 * @brief Abort execution and print information on why
 *
 * @param e Enum value for the error type
 */
void abort [[noreturn]] (enum ERROR e);

// a function that implements the timeout for `--timelimit'
void* reporter_internal(void*);

// initialize the runtime structure
void initialize(int argc, char** argv);

// send the used configuration to a logging server
void callHome();

// a function that implements the localtimeout for `--localtimelimit'
void* local_reporter_internal(void*);

}  // namespace RT
