#include <Core/MainUtils/None.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Planning/EmptyTask.h>

void None::process(ParserPTNet* symbolTables)
{
    // if net is HL net: skeleton is obsolete -> unfold to actual PT net
    if (HighLevelNet)
    {
        symbolTables = unfoldHLNet();
    }
    // now, the symbol table structure definitely contains a PT net.
    // Next step is to transform this into the internal representation

    symbolTables->symboltable2net();

    // have initialised enough data structures for outputting the net
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet::InitialNet->preprocess0();
    Petrinet::InitialNet->preprocess1();

    // prepare for launching the portfolio manager
    RT::data["portfolio"] = JSON();
    RT::data["portfolio"]["task"] = JSON();
    int myid = portfoliomanager::addFormula(NULL, true);
    portfoliomanager::name[myid] = deconst("empty task");
    portfoliomanager::cardvisible = 1;
    EmptyTask::buildTask(Petrinet::InitialNet, -1, NULL, myid);
    portfoliomanager::pn[myid] = Petrinet::InitialNet;
    Petrinet::InitialNet->preprocessingfinished = true;
    // environment(Petrinet::InitialNet);
}
