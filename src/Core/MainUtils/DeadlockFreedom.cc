#include <Core/MainUtils/DeadlockFreedom.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Exploration/DFSExploration.h>
#include <Planning/SiphonTrapTask.h>

void DeadlockFreedom::process(ParserPTNet* symbolTables)
{
    portfoliomanager::nodetype = PL;
    if (HighLevelNet)
    {
        symbolTables = unfoldHLNet();
        ParserPTNet::currentsymbols = symbolTables;
    }
    symbolTables->setIndices();
    symbolTables->symboltables2protonet();
    if (RT::args.netreduction_arg != netreduction_arg_off)
        ProtoNet::currentnet->reduce(false, true, FORMULA_DEADLOCK);
    Petrinet::InitialNet = ParserPTNet::protonet2net(ProtoNet::currentnet);
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();
    net->preprocess1();
    net->preprocess2();
    net->preprocess3();

    // launch deadlock search
    pthread_t searchthread;
    pthread_create(&searchthread, NULL, DFSExploration::deadlockthread, NULL);

    // launch deadlock findpath
    pthread_t findpaththread;
    pthread_create(&findpaththread, NULL, DFSExploration::findpathdeadlocklivenessthread, NULL);

    // launch deadlock siphon trap
    pthread_t siphontrapthread;
    pthread_create(&siphontrapthread, NULL, SiphonTrapTask::siphontrapthread, NULL);
}
