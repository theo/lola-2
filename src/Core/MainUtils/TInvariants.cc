#include <Core/MainUtils/TInvariants.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Invariants/tinvariants.h>

void TInvariants::process(ParserPTNet* symbolTables)
{
    if (HighLevelNet)
    {
        symbolTables = unfoldHLNet();
        ParserPTNet::currentsymbols = symbolTables;
    }
    symbolTables->setIndices();
    symbolTables->symboltables2protonet();
    Petrinet::InitialNet = ParserPTNet::protonet2net(ProtoNet::currentnet);
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();
    net->preprocess1();
    net->preprocess2();
    net->preprocess3();
    compute_tinvariants(net);
}
