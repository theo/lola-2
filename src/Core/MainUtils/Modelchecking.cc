#include <Core/MainUtils/Modelchecking.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>
#include <Frontend/Parser/ValueAutomaton.h>

#include <Planning/LTLTask.h>
#include <Formula/StatePredicate/MagicNumber.h>

extern ParserPTNet* LLsymbolTables;
extern ParserPTNet* HLsymbolTables;

// launch portfolio manager in its own thread
void* Modelchecking::portfolio_thread(void*)
{
    portfoliomanager::run();
    return NULL;
}

void Modelchecking::process(ParserPTNet* symbolTables)
{
    // In this mode, LoLA expects a property. The property can be given either as
    // a Buchi automaton (then the property is inevitably a single LTL formula) or
    // as a (collection of) CTL or LTL formulas.

    // check that either --formula or --buechi is specified in cmdline

    if ((not RT::args.formula_given) and (not RT::args.buechi_given))
    {
        RT::log<err>(
            "{} given without {} or {}", RT::markup(MARKUP::PARAMETER, "--check=modelchecking"),
            RT::markup(MARKUP::PARAMETER, "--formula"), RT::markup(MARKUP::PARAMETER, "--buechi")
        );
        RT::abort(ERROR::COMMANDLINE);
    }
    if (RT::args.formula_given and RT::args.buechi_given)
    {
        RT::log<err>(
            "both {} and {} given", RT::markup(MARKUP::PARAMETER, "--formula"),
            RT::markup(MARKUP::PARAMETER, "--buechi")
        );
        RT::abort(ERROR::COMMANDLINE);
    }
    if (RT::args.buechi_given)
    {
        // workflow for property given as Büchi automaton

        // Currently, LoLA does not support HL annotation in Büchi automata.
        // Hence, we cannot exploit an a priori generated HL net skeleton and thus
        // transform it immediately into a PT net

        if (HighLevelNet)
        {
            symbolTables = unfoldHLNet();
        }
        // now, the symbol table structure definitely contains a PT net.

        // parse the given Buchi automaton

        MagicNumber::init();
        ParserPTNet::currentsymbols = symbolTables;
        LTLTask::ParseBuechi();
        pthread_mutex_lock(&Task::compound_mutex);
        ProtoNet::currentvisibility++;
        symbolTables->markVisibleBuchiNodes();

        // net reduction: reduction requires the transformation of the net
        // into a "reduction-friendly" structure: the ProtoNet.

        symbolTables->symboltables2protonet();
        ProtoNet::currentnet->isSkeleton = false;
        time_t startreduction = time(NULL);
        ProtoNet::currentnet->markedvisibility = ProtoNet::currentvisibility;
        pthread_mutex_unlock(&Task::compound_mutex);
        ProtoNet::currentnet->applyEmptySiphon();
        if (RT::args.netreduction_arg != netreduction_arg_off)
        {
            RT::data["globalreduction"] = JSON();
            ProtoNet::currentnet->data = &RT::data["globalreduction"];
            ProtoNet::currentnet->reduce(true, false, FORMULA_LTL);
        }

        // transform reduced proto-net into "search friendly" Petrinet data
        // structure

        Petrinet* petri = ParserPTNet::protonet2net(ProtoNet::currentnet);
        petri->name = deconst("Buchi Automaton");
        petri->preprocess0();

        // feed and launch portfolio manager
        RT::data["portfolio"] = JSON();
        RT::data["portfolio"]["task"] = JSON();

        int myid = portfoliomanager::addFormula(NULL, true);
        portfoliomanager::name[myid] = deconst("buchi automaton");
        portfoliomanager::cardvisible = 1;
        RT::data["formula"]["buchi automaton"] = JSON();
        petri->preprocess1();
        petri->preprocess2();
        petri->preprocess3();
        petri->preprocessingfinished = true;
        LTLTask::buildTask(petri, -1, NULL, myid);

        RT::log("launching portfolio manager");
        portfoliomanager::run();

        RT::shutdown(EXIT_NORMAL);
    }
    // Remaining case: property is given as formula
    // net: single (original input) as symboltable; formula: not yet
    MagicNumber::init();
    Task::bignetsemaphore = new mysemaphore(4);
    if (HighLevelNet)
    {
        Task::havehlnet = true;
        // net: SymbolTable Global HighLevel Unreduced    formula: -
        // transform symboltables into protonet

        symbolTables->setIndices();
        symbolTables->symboltables2protonet();
        ProtoNet::currentnet->isSkeleton = true;
        symbolTables->symboltable2net();  // for unfolding
        Petrinet::InitialNet->preprocess0();
        if (RT::args.xmlformula_given)
        {
            Task::havehlformula = true;
            // We have a HL net and expect a HL formula. That is, we may use the a
            // priori skeleton approach where the skeleton is derived from the HL
            // net and formula structures.

            // We start with checking whether the HL net may  have deadlocks that
            // are not visible in its skeleton We mark the respective transitions

            ValueAutomaton::init();

            RT::log(
                "Reading HL formula in XML format ({})",
                RT::markup(MARKUP::PARAMETER, "--xmlformula")
            );
            ReadHLPnmlFormula();

            // net: SymbolTable Global HighLevel Unreduced    formula: HighLevel
            // Local but all attached to global HL net symboltable

            // net: SymbolTable+ProtoNet+Net Global HighLevel Unreduced formula:
            // HighLevel Local but all attached to global HL net symboltable

            // Since HL net and HL formula are already given, we prepare and
            // launch the skeleton tasks as soon as possible. This way, we may get
            // results even if net unfolding is prohibitively expensive.

            // 	add skeleton tasks

            Task::GlobalHLNet = Petrinet::InitialNet;
            Task::symboltables = symbolTables;
            HLsymbolTables = symbolTables;
            portfoliomanager::cardvisible = 0;
            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                if (portfoliomanager::visible[i])
                    portfoliomanager::cardvisible++;
                // run through all top level skeleton formulas
                if (!portfoliomanager::topskeleton[i])
                    continue;
                portfoliomanager::formula[i]->portfolio_id = i;
                portfoliomanager::proto[i] = new ProtoNet(*ProtoNet::currentnet);
                // net: SymbolTable+ProtoNet+Net Local HighLevel Unreduced
                // formula: HighLevel Local but all attached to global HL net
                // symboltable
            }
            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                if (!portfoliomanager::topskeleton[i])
                    continue;
                pthread_t ttt;
                pthread_create(
                    &ttt, NULL, Task::process_skeleton_subformula_havehlnet,
                    &portfoliomanager::formula[i]
                );
            }
            // 	start portfolio manager &

            pthread_t portfoliothreadid;
            pthread_create(&portfoliothreadid, NULL, portfolio_thread, NULL);

            // unfold net

            LLsymbolTables = unfoldHLNet();
            LLsymbolTables->setIndices();

            // net: SymbolTable Global LowLevel Unreduced formula: HighLevel Local
            // but all attached to global HL net symboltable

            // empty siphon

            LLsymbolTables->symboltables2protonet();
            ProtoNet::currentnet->isSkeleton = false;
            ProtoNet::currentnet->createLP();
            ProtoNet::currentnet->detectEmptySiphon();
            LLsymbolTables->symboltable2net();
            Task::GlobalLLNet = Petrinet::InitialNet;
            Task::GlobalLLNet->preprocess0();
            Task::GlobalLLNet->preprocess1();

            // unfold formulas

            pthread_mutex_lock(&Task::compound_mutex);
            ProtoNet::currentvisibility++;
            ProtoNet::currentnet->markedvisibility = ProtoNet::currentvisibility;
            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                if (!portfoliomanager::top[i])
                    continue;
                tFormula f = portfoliomanager::formula[i];
                pthread_mutex_lock(&kimwitu_mutex);
                f = f->rewrite(kc::tautology);
                f->unparse(myprinter, kc::containstemp);
                f = f->rewrite(kc::emptyquantifiers);
                portfoliomanager::formula[i] = f;
                f->unparse(myprinter, kc::hlunfold);
                f->unparse(myprinter, kc::checkstableatomic);
                f = f->rewrite(kc::tautology);
                f->unparse(myprinter, kc::containstemp);
                f->unparse(myprinter, kc::temporal);
                f->unparse(myprinter, kc::reduction);
                portfoliomanager::formula[i] = f;
                pthread_mutex_unlock(&kimwitu_mutex);
            }
            Petrinet* TheLLNetForInitial = NULL;
            bool containsDL = false;
            bool containsNX = false;
            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                tFormula f = portfoliomanager::formula[i];
                if (!portfoliomanager::top[i])
                    continue;

                if (f->type == FORMULA_INITIAL)
                {
                    portfoliomanager::pn[i] = Task::GlobalLLNet;
                    portfoliomanager::formula[i] = f;
                    f->portfolio_id = i;
                    Task::process_initial(&f);
                }
                else
                {
                    LLsymbolTables->markVisibleNodes(portfoliomanager::formula[i]);
                }

                if (portfoliomanager::formula[i]->containsNext)
                    containsNX = true;
                if (portfoliomanager::formula[i]->containsDeadlock)
                    containsDL = true;
            }
            pthread_mutex_unlock(&Task::compound_mutex);

            // global net reduction

            ProtoNet::currentnet->recordVisibleNodes();
            ProtoNet::currentnet->applyEmptySiphon();
            if (RT::args.netreduction_arg == netreduction_arg_both
                || RT::args.netreduction_arg == netreduction_arg_global)
            {
                RT::data["hlglobalreduction"] = RT::data["globalreduction"];
                RT::data["globalreduction"] = JSON();
                ProtoNet::currentnet->data = &RT::data["globalreduction"];
                time_t startreduction = time(NULL);
                ProtoNet::currentnet->reduce(containsNX, containsDL, FORMULA_MODELCHECKING);
            }

            // local reduction & task building

            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                if (!portfoliomanager::top[i])
                    continue;
                if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
                    continue;  // already planned
                portfoliomanager::formula[i]->portfolio_id = i;
                portfoliomanager::proto[i] = new ProtoNet(*ProtoNet::currentnet);
            }
            for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
            {
                if (!portfoliomanager::top[i])
                    continue;
                if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
                    continue;  // already planned
                pthread_t ttt;
                pthread_create(&ttt, NULL, Task::process_subformula, &portfoliomanager::formula[i]);
            }

            // let portfolio (in its own thread) do its work
            while (true)
                sleep(10000);
        }
        else
        {
            Task::havellformula = true;
            // We have a HL net and expect a LL net formula (since formula in LoLA
            // format are always LL). Hence, we unfold the HL net to a LL net and
            // then continue according to the LL net + LL formula approach

            // unfold net

            HLsymbolTables = symbolTables;
            Petrinet* HLNet = Petrinet::InitialNet;
            symbolTables = unfoldHLNet();

            // continue at "Have LL net and expect LL formula"
        }
    }
    Task::havellnet = true;
    Task::havellformula = true;
    // Here, we have a LL net and expect a LL formula
    // net: SymbolTable Global LowLevel Unreduced formula: -

    // Before parsing the formula, we compute the largest empty siphon of the net.
    // This way, places in the siphon (that will never get any token anyway) can
    // be removed from the formula already upon parsing the formula. For computing
    // the siphon, net is transformed from the symbol table structure into a net
    // reduction friendly format, the ProtoNet.

    LLsymbolTables = symbolTables;
    symbolTables->symboltables2protonet();
    ProtoNet::currentnet->isSkeleton = false;
    symbolTables->setIndices();
    ProtoNet* TheLLNet = ProtoNet::currentnet;
    TheLLNet->createLP();  // prepare for stable atomic analysis
    TheLLNet->data = new JSON();
    TheLLNet->detectEmptySiphon();
    // net: SymbolTable+ProtoNet Global LowLevel Unreduced formula: -

    // now we parse the actual LL formula. It may be given in XML or in LoLA
    // format.

    Petrinet::InitialNet = ParserPTNet::protonet2net(TheLLNet);
    RT::log("Reading formula.");
    ParserPTNet::currentsymbols = symbolTables;
    if (RT::args.xmlformula_given)
    {
        RT::log("Using XML format ({})", RT::markup(MARKUP::PARAMETER, "--xmlformula"));
        ReadPnmlFormula();
    }
    else
    {
        Task::parseFormula();
    }
    delete TheLLNet->solver;
    TheLLNet->solver = NULL;

    // net: SymbolTable+ProtoNet Global LowLevel Unreduced formula: -

    // launch skeleton folding and add skeleton tasks

    // symbolTables -> symboltable2net(); // folding LL to HL net requires a net
    // in final data structure Petrinet::InitialNet -> preprocess0();

    portfoliomanager::cardvisible = 0;
    Task::foldllnetsemaphore = new mysemaphore(4);
    ProtoNet* ProtoNetToBeFolded = new ProtoNet(*TheLLNet);
    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (portfoliomanager::visible[i])
            portfoliomanager::cardvisible++;
        // run through all top level skeleton formulas
        if (!portfoliomanager::topskeleton[i])
            continue;
        if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
            continue;
        portfoliomanager::formula[i]->portfolio_id = i;
        portfoliomanager::proto[i] = ProtoNetToBeFolded;
    }
    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (!portfoliomanager::topskeleton[i])
            continue;
        if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
            continue;
        pthread_t ttt;
        pthread_create(
            &ttt, NULL, Task::process_skeleton_subformula_havellnet, &portfoliomanager::formula[i]
        );
    }

    // start portfolio manager

    pthread_t portfoliothreadid;
    pthread_create(&portfoliothreadid, NULL, portfolio_thread, NULL);  // ??????????????????

    // launch initial tasks
    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (!portfoliomanager::top[i])
            continue;
        if (portfoliomanager::formula[i]->type != FORMULA_INITIAL)
            continue;
        portfoliomanager::formula[i]->portfolio_id = i;  // 11111111111111111111111
        portfoliomanager::proto[i] = new ProtoNet(*TheLLNet);
    }

    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (!portfoliomanager::top[i])
            continue;
        if (portfoliomanager::formula[i]->type != FORMULA_INITIAL)
            continue;
        pthread_t ttt;
        pthread_create(&ttt, NULL, Task::process_subformula, &portfoliomanager::formula[i]);
    }

    // global reduction
    if (RT::args.netreduction_arg == netreduction_arg_both
        || RT::args.netreduction_arg == netreduction_arg_global)
    {
        time_t startreduction = time(NULL);
        bool ctdeadlock = false;
        bool ctnext = false;
        bool cttemporal = false;

        pthread_mutex_lock(&Task::compound_mutex);
        ProtoNet::currentvisibility++;
        TheLLNet->markedvisibility = ProtoNet::currentvisibility;
        TheLLNet->applyEmptySiphon();
        for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
        {
            if (!portfoliomanager::top[i])
                continue;
            symbolTables->markVisibleNodes(portfoliomanager::formula[i]);
            if (portfoliomanager::formula[i]->containsDeadlock)
                ctdeadlock = true;
            if (portfoliomanager::formula[i]->containsNext)
                ctnext = true;
            if (portfoliomanager::formula[i]->containsTemporal)
                cttemporal = true;
        }
        if (cttemporal)
        {
            TheLLNet->recordVisibleNodes();
            TheLLNet->reduce(ctnext, ctdeadlock, FORMULA_CTL);
        }
        pthread_mutex_unlock(&Task::compound_mutex);
    }

    // for all formulas:
    //             * local reduction
    //	       * task building

    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (!portfoliomanager::top[i])
            continue;
        if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
            continue;
        portfoliomanager::formula[i]->portfolio_id = i;
        portfoliomanager::proto[i] = new ProtoNet(*TheLLNet);
    }

    for (int i = 0; i < portfoliomanager::nr_of_formulas; i++)
    {
        if (!portfoliomanager::top[i])
            continue;
        if (portfoliomanager::formula[i]->type == FORMULA_INITIAL)
            continue;
        pthread_t ttt;
        pthread_create(&ttt, NULL, Task::process_subformula, &portfoliomanager::formula[i]);
    }
}
