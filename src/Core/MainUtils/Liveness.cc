#include <Core/MainUtils/Liveness.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Exploration/DFSExploration.h>
#include <Exploration/TSCCExploration.h>

#include <Highlevel/hltransition.h>

void Liveness::process(ParserPTNet* symbolTables)
{
    portfoliomanager::nodetype = TR;
    Task::havehlnet = false;
    if (HighLevelNet)
    {
        Task::havehlnet = true;
        symbolTables = unfoldHLNet();
        ParserPTNet::currentsymbols = symbolTables;
        portfoliomanager::hlcardtodo = hltransition::card;
        portfoliomanager::hltrue = new int[hltransition::card];
        portfoliomanager::hlunknown = new int[hltransition::card];
    }
    if (RT::args.netreduction_arg == netreduction_arg_off)
    {
        symbolTables->setIndices();
        symbolTables->symboltable2net();
    }
    else
    {
        symbolTables->symboltables2protonet();
        ProtoNet::currentnet->reduce(false, false, FORMULA_REACHABLE);
        if (ProtoNet::currentnet->cardTR == 0 || ProtoNet::currentnet->cardPL == 0)
        {
            RT::print(RT::markup(MARKUP::GOOD, "The net is live"));
            portfoliomanager::compareresult(true);
            if (RT::args.mcc_given)
            {
                portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_TRUE);
            }
            RT::shutdown(0);
        }
        Petrinet::InitialNet = ParserPTNet::protonet2net(ProtoNet::currentnet);
    }
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();

    portfoliomanager::symmetrymap[PL] = new int[net->Card[PL]];
    portfoliomanager::symmetrymap[TR] = new int[net->Card[TR]];
    portfoliomanager::globalresult = new ternary_t[net->Card[TR]];
    portfoliomanager::globalproducer = new globalproducer_t[net->Card[TR]];

    for (arrayindex_t i = 0; i < net->Card[TR]; i++)
    {
        portfoliomanager::symmetrymap[TR][i] = -1;
        portfoliomanager::globalresult[i] = TERNARY_VOID;
        portfoliomanager::globalproducer[i] = GLOBAL_VOID;
    }
    for (arrayindex_t i = 0; i < net->Card[PL]; i++)
    {
        portfoliomanager::symmetrymap[PL][i] = -1;
    }
    portfoliomanager::cardtodo = net->Card[TR];

    // launching reporter
    pthread_t globalrep;
    pthread_create(&globalrep, NULL, portfoliomanager::globalreporter, NULL);

    net->preprocess1();

    net->preprocess2();
    net->preprocess3();
    // launch symmetry task
    pthread_t symmthread;
    pthread_create(&symmthread, NULL, portfoliomanager::launchsymmetrythread, NULL);
    pthread_t searchthread;
    pthread_create(
        &searchthread, NULL,
        Task::havehlnet ? TSCCExplorationAGEF::HLLiveness : TSCCExplorationAGEF::Liveness, NULL
    );
    pthread_t livenessthread;
    pthread_create(
        &livenessthread, NULL,
        Task::havehlnet ? TSCCExplorationAGEFRelaxed::GlobalHLliveness
                        : TSCCExplorationAGEFRelaxed::Globalliveness,
        NULL
    );
    pthread_t dlthread;
    pthread_create(&dlthread, NULL, DFSExploration::deadlockthread, NULL);
    // launch findpath
    pthread_t findpaththread;
    pthread_create(&findpaththread, NULL, DFSExploration::findpathdeadlocklivenessthread, NULL);
}
