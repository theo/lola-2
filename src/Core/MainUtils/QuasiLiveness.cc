#include <Core/MainUtils/QuasiLiveness.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Exploration/DFSExploration.h>
#include <Planning/StateEquationTask.h>

#include <Highlevel/hltransition.h>

void QuasiLiveness::process(ParserPTNet* symbolTables)
{
    portfoliomanager::nodetype = TR;
    Task::havehlnet = false;
    if (HighLevelNet)
    {
        Task::havehlnet = true;
        symbolTables = unfoldHLNet();
        ParserPTNet::currentsymbols = symbolTables;
        portfoliomanager::hltrue = new int[hltransition::card];
        portfoliomanager::hlunknown = new int[hltransition::card];
        portfoliomanager::hlcardtodo = hltransition::card;
    }
    if (RT::args.netreduction_arg == netreduction_arg_off)
    {
        symbolTables->setIndices();
        symbolTables->symboltable2net();
    }
    else
    {
        symbolTables->symboltables2protonet();
        ProtoNet::currentnet->reduce(false, false, FORMULA_REACHABLE);
        if (ProtoNet::currentnet->cardTR == 0 || ProtoNet::currentnet->cardPL == 0)
        {
            RT::print(RT::markup(MARKUP::GOOD, "The net is quasilive"));
            portfoliomanager::compareresult(true);
            if (RT::args.mcc_given)
            {
                portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_TRUE);
            }
            RT::shutdown(0);
        }
        Petrinet::InitialNet = ParserPTNet::protonet2net(ProtoNet::currentnet);
    }
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();

    portfoliomanager::symmetrymap[PL] = new int[net->Card[PL]];
    portfoliomanager::symmetrymap[TR] = new int[net->Card[TR]];
    portfoliomanager::globalresult = new ternary_t[net->Card[TR]];
    portfoliomanager::globalproducer = new globalproducer_t[net->Card[TR]];

    for (arrayindex_t i = 0; i < net->Card[TR]; i++)
    {
        portfoliomanager::symmetrymap[TR][i] = -1;
        portfoliomanager::globalresult[i] = TERNARY_VOID;
        portfoliomanager::globalproducer[i] = GLOBAL_VOID;
    }
    for (arrayindex_t i = 0; i < net->Card[PL]; i++)
    {
        portfoliomanager::symmetrymap[PL][i] = -1;
    }
    portfoliomanager::cardtodo = net->Card[TR];

    // launching reporter
    pthread_t globalrep;
    pthread_create(&globalrep, NULL, portfoliomanager::globalreporter, NULL);

    net->preprocess1();

    StateEquationTask* sssttt = new StateEquationTask(Petrinet::InitialNet, -1, NULL, 9);
    pthread_t stateequationthread;
    pthread_create(&stateequationthread, NULL, sssttt->quasilivenessstateequationthread, NULL);

    net->preprocess2();
    net->preprocess3();

    // launch symmetry task
    pthread_t symmthread;
    pthread_create(&symmthread, NULL, portfoliomanager::launchsymmetrythread, NULL);
    pthread_t searchthread;
    Mara* memory = new Mara();
    DFSExploration* quasilivenessexploration = new DFSExploration(net, memory);
    pthread_create(&searchthread, NULL, quasilivenessexploration->Quasiliveness, NULL);
    // launch findpath
    pthread_t findpaththread;
    pthread_create(&findpaththread, NULL, DFSExploration::findpathquasilivenessthread, NULL);
}
