#pragma once

#include <Frontend/Parser/ParserPTNet.h>

namespace Modelchecking
{
void process(ParserPTNet*);

void* portfolio_thread(void*);
}  // namespace Modelchecking
