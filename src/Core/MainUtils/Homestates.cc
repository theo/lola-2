#include <Core/MainUtils/Homestates.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Exploration/TSCCExploration.h>

void Homestates::process(ParserPTNet* symbolTables)
{
    portfoliomanager::nodetype = PL;
    if (HighLevelNet)
    {
        symbolTables = unfoldHLNet();
        ParserPTNet::currentsymbols = symbolTables;
    }
    symbolTables->setIndices();
    symbolTables->symboltable2net();
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();
    net->preprocess1();
    net->preprocess2();
    net->preprocess3();
    pthread_t searchthread;
    pthread_create(&searchthread, NULL, TSCCExplorationAGEFRelaxed::GlobalHomestates, NULL);
}
