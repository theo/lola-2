#include <Core/MainUtils/Full.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Planning/FullTask.h>

void Full::process(ParserPTNet* symbolTables)
{
    // if net is HL net: skeleton is obsolete -> unfold to actual PT net
    if (HighLevelNet)
    {
        symbolTables = unfoldHLNet();
    }
    // now, the symbol table structure definitely contains a PT net.
    // Next step is to transform this into the internal representation

    symbolTables->setIndices();
    symbolTables->symboltable2net();
    // have initialised enough data structures for outputting the net
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet::InitialNet->preprocess0();
    Petrinet::InitialNet->preprocess1();
    Petrinet::InitialNet->preprocess2();
    Petrinet::InitialNet->preprocess3();

    // prepare for launching the portfolio manager
    RT::data["portfolio"] = JSON();
    RT::data["portfolio"]["task"] = JSON();
    int myid = portfoliomanager::addFormula(NULL, true);
    portfoliomanager::pn[myid] = Petrinet::InitialNet;
    Petrinet::InitialNet->preprocessingfinished = true;
    portfoliomanager::name[myid] = deconst("full task");
    portfoliomanager::cardvisible = 1;
    FullTask::buildTask(Petrinet::InitialNet, -1, NULL, myid);
}
