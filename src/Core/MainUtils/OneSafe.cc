#include <Core/MainUtils/OneSafe.h>

#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Net/Petrinet.h>
#include <Net/Protonet.h>
#include <Portfolio/portfoliomanager.h>

#include <Exploration/ComputeBoundExploration.h>
#include <Planning/StateEquationTask.h>

void OneSafe::process(ParserPTNet* symbolTables)
{
    if (HighLevelNet)
    {
        RT::log<warn>(RT::markup(MARKUP::WARNING, "The net may or may not be safe"));
        RT::shutdown(0);
    }
    // launch timer
    portfoliomanager::nodetype = PL;
    symbolTables->setIndices();
    if (RT::args.netreduction_arg == netreduction_arg_off)
    {
        symbolTables->setIndices();
        symbolTables->symboltable2net();
    }
    else
    {
        symbolTables->symboltables2protonet();
        ProtoNet::currentnet->reduce(false, false, FORMULA_REACHABLE);
        if (ProtoNet::currentnet->cardPL == 0)
        {
            RT::print(RT::markup(MARKUP::GOOD, "1 The net is safe"));
            portfoliomanager::compareresult(true);
            if (RT::args.mcc_given)
            {
                portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_TRUE);
            }
            RT::shutdown(0);
        }
        if (ProtoNet::currentnet->cardTR == 0)
        {
            ProtoPlace* ppp;
            for (ppp = ProtoNet::currentnet->firstplace; ppp; ppp = ppp->next)
            {
                if (ppp->marking > 1)
                {
                    break;
                }
            }
            if (ppp)
            {
                RT::print(RT::markup(MARKUP::BAD, "The net is not safe"));
                portfoliomanager::compareresult(false);
                if (RT::args.mcc_given)
                {
                    portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_FALSE);
                }
            }
            else
            {
                RT::print(RT::markup(MARKUP::GOOD, "2 The net is safe"));
                portfoliomanager::compareresult(true);
                if (RT::args.mcc_given)
                {
                    portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_TRUE);
                }
            }
            RT::shutdown(0);
        }
        Petrinet::InitialNet = ParserPTNet::protonet2net(ProtoNet::currentnet);
    }
    if (RT::args.printNet_given)
    {
        RT::log("Print net ({})", RT::markup(MARKUP::PARAMETER, "--printNet"));
        Petrinet::InitialNet->print();
    }
    Petrinet* net = Petrinet::InitialNet;
    net->preprocess0();

    portfoliomanager::symmetrymap[PL] = new int[net->Card[PL]];
    portfoliomanager::symmetrymap[TR] = new int[net->Card[TR]];
    portfoliomanager::globalresult = new ternary_t[net->Card[PL]];
    portfoliomanager::globalproducer = new globalproducer_t[net->Card[PL]];

    for (arrayindex_t i = 0; i < net->Card[PL]; i++)
    {
        portfoliomanager::symmetrymap[PL][i] = -1;
        portfoliomanager::globalresult[i] = TERNARY_VOID;
        portfoliomanager::globalproducer[i] = GLOBAL_VOID;
    }
    for (arrayindex_t i = 0; i < net->Card[TR]; i++)
    {
        portfoliomanager::symmetrymap[TR][i] = -1;
    }
    portfoliomanager::cardtodo = net->Card[PL];

    for (int i = 0; i < net->Card[PL]; i++)
    {
        if (net->Capacity[i] <= 1)
        {
            // safe by definition, e.g. nested unit
            portfoliomanager::globalresult[i] = TERNARY_TRUE;
            portfoliomanager::globalproducer[i] = GLOBAL_PREPROCESSING;
            portfoliomanager::cardtodo--;
        }
    }
    if (!portfoliomanager::cardtodo)
    {
        RT::print(RT::markup(MARKUP::GOOD, "3 The net is safe"));
        portfoliomanager::compareresult(true);
        if (RT::args.mcc_given)
        {
            portfoliomanager::mcc_boolean(ReadPnmlFormulaId(), TERNARY_TRUE);
        }
        RT::shutdown(0);
        portfoliomanager::globalplacereport();
        RT::shutdown(0);
    }
    net->preprocess1();
    net->preprocess2();
    net->preprocess3();

    // launch 0/1 invariant task
    ComputeBoundExploration* invariantexploration = new ComputeBoundExploration(net);
    pthread_t invariantthread;
    pthread_create(&invariantthread, NULL, invariantexploration->zerooneinvariantthread, NULL);

    // launching reporter
    pthread_t globalrep;
    pthread_create(&globalrep, NULL, portfoliomanager::globalreporter, NULL);

    // launch state equation
    StateEquationTask* sssttt = new StateEquationTask(Petrinet::InitialNet, -1, NULL, 9);
    pthread_t stateequationthread;
    pthread_create(&stateequationthread, NULL, sssttt->safetystateequationthread, NULL);

    // launch symmetry task
    pthread_t symmthread;
    pthread_create(&symmthread, NULL, portfoliomanager::launchsymmetrythread, NULL);
    pthread_t searchthread;
    pthread_create(&searchthread, NULL, invariantexploration->Safety, NULL);

    // launch findpath
    pthread_t findpaththread;
    pthread_create(&findpaththread, NULL, invariantexploration->findpathsafetythread, NULL);
}
