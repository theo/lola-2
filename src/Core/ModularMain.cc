/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/*!
\file
\author Judith Overath
\status wip 21.03.2023

\brief main method for modular petrinets
 */

#include "Core/ModularMain.h"

#include <algorithm>
#include <future>
#include <memory>
#include <ranges>
#include <thread>

#include <bs_thread_pool/pool.hpp>
#include <fmt/core.h>

#include "Core/Runtime.h"
#include "Formula/StatePredicate/MagicNumber.h"
#include "Frontend/ModularParser/Parser.h"
#include "Frontend/ModularParser/ParserMNet.h"
#include "Memory/Util.h"
#include "Net/ModularStructure.h"
#include "Portfolio/portfoliomanager.h"

namespace
{
fmt::memory_buffer buf{};
}

void modular_main(std::unique_ptr<parsing::ModularStructure> modular_symbols)
{
    auto ms = modular_symbols->symboltable2modularstructure();
    Petrinet::InitialNet = std::addressof((*ms)[lola::MOD][0]);
    Petrinet::InitialNet->preprocessingfinished = true;
    // modular_symbols->debug_print();

    pthread_t timer_id;

    switch (RT::args.check_arg)
    {
    case check_arg_full:
    {
        RT::log("Full state space of modular net.");
        // prepare for launching the portfolio manager
        RT::data["portfolio"] = JSON();
        RT::data["portfolio"]["task"] = JSON();
        int myid = portfoliomanager::addFormula(NULL, true);
        portfoliomanager::pn[myid] = Petrinet::InitialNet;
        Petrinet::InitialNet->preprocessingfinished = true;
        portfoliomanager::name[myid] = deconst("full task");
        portfoliomanager::cardvisible = 1;

        // FullModularTask::buildTask(modularStructure, -1, NULL, myid);

        portfoliomanager::run();
        RT::shutdown(EXIT_NORMAL);
    }
    case check_arg_modelchecking:
    {
        MagicNumber::init();
        Task::havellnet = Task::havellformula = true;
        RT::log("Reading formula(s)");
        if (!RT::args.formula_given)
        {
            RT::log<err>(
                "{} given without {}", RT::markup(MARKUP::PARAMETER, "--check=modelchecking"),
                RT::markup(MARKUP::PARAMETER, "--formula")
            );
            RT::abort(ERROR::COMMANDLINE);
        }
        if (RT::args.xmlformula_given)
        {
            parsing::parse_xml_formulas({"formula", RT::args.formula_arg}, *modular_symbols);
        }
        else
        {
            parsing::parse_formula({"formula", RT::args.formula_arg}, *modular_symbols);
        }

        std::promise<BS::thread_pool&> promise;
        auto future = promise.get_future();
        std::thread portfolio_thread{[p = std::addressof(promise), ms = ms.get()]
                                     { portfoliomanager::run(p, ms); }};
        auto& threads = future.get();

        std::ranges::for_each(
            lola::counter(portfoliomanager::nr_of_formulas),
            [&threads, &ms](const auto i)
            {
                portfoliomanager::formula[i]->portfolio_id = i;
                if (portfoliomanager::topskeleton[i])
                {
                    auto _ = threads.submit_task(
                        [i] { Task::process_skeleton_subformula(portfoliomanager::formula[i]); }
                    );
                }
                if (portfoliomanager::top[i])
                {
                    // we don't wait on the task to finish
                    auto _ = threads.submit_task(
                        [i, &ms] { Task::process_subformula(portfoliomanager::formula[i], *ms); }
                    );
                }
            }
        );

        portfolio_thread.join();
    }
    case check_arg_modularize:
    {
        RT::log("Reading formula(s)");
        auto formulas = parsing::parse_and_modularize_xml_formula(
            {"formula", RT::args.formula_arg}, *modular_symbols
        );

        auto formula_string = formulas
            | std::views::transform(
                                  [](const auto& formula)
                                  {
                                      formula->unparse(
                                          [](const std::string& s, const kc::UView&)
                                          { fmt::format_to(std::back_inserter(buf), "{}", s); },
                                          kc::out
                                      );
                                      auto s = fmt::to_string(buf);
                                      buf.clear();
                                      return s;
                                  }
            );

        if (RT::args.formulafile_given)
        {
            Output formulafile{"Formulas", std::string{RT::args.formulafile_arg}};
            formulafile.print("{}", fmt::join(formula_string, "\n:\n"));
        }
        else
        {
            RT::print<VERB::OUTPUT>("{}", fmt::join(formula_string, "\n:\n"));
        }

        RT::shutdown(EXIT_SUCCESS);
    }
    default:
        RT::shutdown(EXIT_ERROR);
    }
}
