/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/*!
\file
\author Niels
\status approved 25.01.2012

\brief Main entry point for LoLA.
 */

#include <Core/Dimensions.h>
#include <Core/MainUtils/DeadlockFreedom.h>
#include <Core/MainUtils/Full.h>
#include <Core/MainUtils/Homestates.h>
#include <Core/MainUtils/Liveness.h>
#include <Core/MainUtils/Modelchecking.h>
#include <Core/MainUtils/None.h>
#include <Core/MainUtils/OneSafe.h>
#include <Core/MainUtils/QuasiLiveness.h>
#include <Core/MainUtils/StableMarking.h>
#include <Core/MainUtils/TInvariants.h>
#include <Core/ModularMain.h>
#include <Core/Runtime.h>
#include <Frontend/ModularParser/Parser.h>
#include <Frontend/ModularParser/ParserMlola.h>
#include <Frontend/ModularSymbols/FusionSetSymbol.h>
#include <Frontend/Parser/ParserPTNet.h>
#include <Frontend/Parser/ParserPnml.h>
#include <Frontend/SymbolTable/SymbolTable.h>
#include <Memory/Mara.h>
#include <Net/Petrinet.h>
#include <Witness/Path.h>
#include <lolaconf.h>

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <memory>
#include <streambuf>
#include <string>

void environment(Petrinet*);
pthread_mutex_t kimwitu_mutex = PTHREAD_MUTEX_INITIALIZER;
/*!
\brief symbol tables for the parsed net
\ingroup g_globals
 */
tFormula TheHLFormula;

/*!
\brief symbol table for a given Büchi automaton
\ingroup g_globals
 */
SymbolTable* buechiStateTable = NULL;

pthread_mutex_t sat_mutex = PTHREAD_MUTEX_INITIALIZER;

/*!
\brief the main workflow of LoLA
 */

void* globalproperty_timer(void*)
{
    if (RT::args.timelimit_given)
    {
        sleep(RT::args.timelimit_arg);
        RT::shutdown(1);
    }
    return NULL;
}

#define LAUNCH_TIMER pthread_create(&timer_id, NULL, globalproperty_timer, NULL);

#define WAIT_WHILE_THREADS_WORK \
    while (true)                \
        sleep(10'000);

extern int* compoundformulaid;
extern tFormula* compoundformula;
extern tFormula* skeletonformula;

ProtoNet* skeletonnet;

// Possible status of net evolution:
// 1st dimension: shape
// * SY: as symboltables for places and transitions with arc lists at transitions
// * PR: as protonet (objects for places, transitions, arcs. places and arcs are linked to
// a symbol table
// * PN: as petrinet (index structures for everything)
//
// 2nd dimension: distribution
// * GL: one for all formulas
// * LC: individual per formula
//
// 3rd dimension: net class
// * HL: (original CP net)
// * LL: (original PT net)
// * UF: (unfolded to PT net)
// * SK: (folded to skeleton)
//
// 4th dimension: progress
// * CR: as freshly created
// * RG: after global reduction
// * RL: after local reduction
// * Px: after preprocessing step x

// Possible status of formula evolution:
// 1st dimension: net class
// 2nd dimension: attached net

int main(int argc, char** argv)
{
    ParserPTNet* symbolTables;
    std::unique_ptr<parsing::ModularStructure> modularSymbolTables;

    // This routine is responsible for the workflow of LoLA between
    // launching the tool and task generation / portfolio management.
    // The workflow has several branches depending on
    // whether net is given as HL net or LL net
    // what verfication option is chosen (full, none, modelchecking
    // whether property is given as formula or as Buchi automaton
    // whether formula is in HL or LL format.

    //=================
    // (1) set up LoLA
    //=================

    portfoliomanager::globalstarttime = time(NULL);  // record start time (for portfolio
                                                     // managemment)
    std::srand(42);                      // initialise random number generator (for repeatable hash
                                         // parameters)
    RT::initialize(argc, argv);          // initialize the runtime (includes parsing of cmdline
                                         // options)
    for (int i = 0; i < MAX_TASKS; i++)  // initialise parent array in portfolio manager
    {                                    // (crucial for termination)
        portfoliomanager::parent[i] = -1;
    }
    threadid_t number_of_threads = static_cast<threadid_t>(RT::args.threads_arg);

    portfoliomanager::init();
    RT::args.threads_arg = 1;

    // inspect verdict file, if given, and modify cmdline options

    if (RT::args.verdictfile_given)
    {
        std::ifstream verdict(RT::args.verdictfile_arg);
        if (verdict)
        {
            std::string verdictline;
            while (std::getline(verdict, verdictline))
                if (verdictline.find("DEADLOCK") != std::string::npos
                    && verdictline.find("false") != std::string::npos)
                {
                    RT::args.deadlockfree_given = true;
                }
            if (verdictline.find("SAFE") != std::string::npos
                && verdictline.find("true") != std::string::npos)
            {
                RT::args.safe_given = true;
            }
        }
    }

    //===================
    // (2) process net
    //===================

    RT::log("NET");  // prepare status info for net input
    RT::indent_by(2);

    if (RT::args.inputs_num == 0)
    {
        // read from stdin
        RT::currentInputFile = new Input("net");
    }
    else if (RT::args.inputs_num == 1)
    {
        // read from specified file
        RT::currentInputFile = new Input("net", RT::args.inputs[0]);
    }
    else
    {
        RT::log<err>("too many input files given - expecting at most one");
        RT::abort(ERROR::COMMANDLINE);
    }

    if (RT::args.mlola_given)  // input expected as a mlola file
    {
        RT::log("input: mlola file ({})", RT::markup(MARKUP::PARAMETER, "--mlola"));
        modularSymbolTables = parsing::parse_modular_symbols(std::move(*RT::currentInputFile));
    }
    else if (RT::args.pnmlnet_given)  // input expected in PNML
    {
        RT::log("input: PNML file ({})", RT::markup(MARKUP::PARAMETER, "--pnmlnet"));

        symbolTables = ReadPnmlFile();

        // LoLA survives function ReadPnmlFile only if net input is syntactically correct.
        // If PNML file/stream contains a LL net,
        // this is translated into symbol table representation as is.
        // If PNML file contains a HL net,
        // only the skeleton of the net is represented in symbol table representation.
        // The signature of the HL net is preserved in the HL net data structures for
        // later unfolding.
    }

    else  // input expected as LoLA file
    {
        // pass the opened file pointer to flex via FILE *yyin
        extern FILE* ptnetlola_in;
        ptnetlola_in = *RT::currentInputFile;

        // read the input file(s)
        extern ParserPTNet* ParserPTNetLoLA();
        symbolTables = ParserPTNetLoLA();

        // tidy parser
        extern int ptnetlola_lex_destroy();
        ptnetlola_lex_destroy();

        // ParserPTNetLoLA() is only survived for syntactiaclly correct input.
        // A file given in LoLA format is always a LL net (a native LoLA HL format is
        // still future work)
    }

    // close net file
    delete RT::currentInputFile;
    RT::currentInputFile = NULL;

    RT::log("finished parsing");

    if (RT::args.mlola_given)
    {
        modular_main(std::move(modularSymbolTables));
        RT::shutdown(0);
    }
    // assign indices to place+transition symbols

    ParserPTNet::currentsymbols = symbolTables;
    symbolTables->setIndices();

    // Parsing the net is complete. We have either a PT net or the skeleton of a HL
    // net.

    // Evaluate the cmdline arguments --check, --formula, and --buechi.
    // Values cause substantially different workflow

    pthread_t timer_id;

    //===================
    // (3) execute selected check
    //===================
    // Proceed according to the selected option of the --check commandline argument

    switch (RT::args.check_arg)
    {
    case check_arg_tinvariants:
    {
        TInvariants::process(symbolTables);
        break;
    }
    case check_arg_deadlockfreedom:
        LAUNCH_TIMER;
        DeadlockFreedom::process(symbolTables);
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_OneSafe:
        LAUNCH_TIMER;
        OneSafe::process(symbolTables);  // this can call RT::shutdown
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_homestates:
        LAUNCH_TIMER;
        Homestates::process(symbolTables);
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_QuasiLiveness:
        LAUNCH_TIMER;
        QuasiLiveness::process(symbolTables);  // this can call RT::shutdown
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_StableMarking:
        LAUNCH_TIMER;
        StableMarking::process(symbolTables);  // this can call RT::shutdown
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_Liveness:
        LAUNCH_TIMER;
        Liveness::process(symbolTables);  // this can call RT::shutdown
        WAIT_WHILE_THREADS_WORK;
        break;
    case check_arg_none:
    case check__NULL:
    {
        None::process(symbolTables);
        portfoliomanager::run();
        RT::shutdown(EXIT_NORMAL);
        break;
    }
    case check_arg_full:
    {
        Full::process(symbolTables);
        portfoliomanager::run();
        RT::shutdown(EXIT_NORMAL);
        break;
    }
    case check_arg_modelchecking:
    {
        Modelchecking::process(symbolTables);  // this can call RT::shutdown
        WAIT_WHILE_THREADS_WORK;
        break;
    }
    default:
        RT::shutdown(EXIT_ERROR);
    }
}

void environment(Petrinet* net)
{
    int minin = net->Card[TR] + 1;
    int minout = net->Card[TR] + 1;
    int minboth = net->Card[TR] + 1;
    int avgin = 0;
    int avgout = 0;
    int avgboth = 0;
    int maxin = 0;
    int maxout = 0;
    int maxboth = 0;

    for (int i = 0; i < net->Card[PL]; i++)
    {
        int in = net->CardArcs[PL][PRE][i];
        int out = net->CardArcs[PL][POST][i];
        int both = in + out;
        if (in < minin)
            minin = in;
        if (in > maxin)
            maxin = in;
        avgin += in;
        if (out < minout)
            minout = out;
        if (out > maxout)
            maxout = out;
        avgout += out;
        if (both < minboth)
            minboth = both;
        if (both > maxboth)
            maxboth = both;
        avgboth += both;
    }
    RT::log(
        "ENVIRONMENT NET {} MININ {} MAXIN {} AVGIN {} MINOUT {} MAXOUT {} AVGOUT {} "
        "MINBOTH {} MAXBOTH {} AVGBOTH {}",
        net->name, minin, maxin, avgin / net->Card[PL], minout, maxout, avgout / net->Card[PL],
        minboth, maxboth, avgboth / net->Card[PL]
    );
}
