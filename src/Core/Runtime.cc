/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Niels
\status new

\brief Definition of runtime specific functionality.
*/

#include <lolaconf.h>
#include <Core/Handlers.h>
#include <Core/Runtime.h>
#include <InputOutput/Socket.h>
#include <unistd.h>

namespace RT
{

/**
 * @brief Abort execution and print information on why
 *
 * @param e Enum value for the error type
 */
void abort [[noreturn]] (enum ERROR e)
{
    log<level::critical>(
        "{} -- aborting {}", markup(MARKUP::BAD, "{}", error_msg[static_cast<int>(e)]),
        markup(MARKUP::IMPORTANT, "{}", static_cast<int>(e))
    );

    log("see manual for a documentation of this error");

    if (errno != 0)
    {
        log("last error message: {}", markup(MARKUP::IMPORTANT, strerror(errno)));
        data["call"]["error"] = strerror(errno);
    }

    exit(EXIT_ERROR);
}

// a function that implements the timeout for `--timelimit'
void* reporter_internal(void*)
{
    sleep(args.timelimit_arg);
    data["exit"]["timelimitreached"] = true;
    log(markup(MARKUP::IMPORTANT, "time limit reached - aborting"));

    // first kill child if any
    if (childpid > 0)
    {
        kill(childpid, SIGUSR1);
    }

    // second kill sara if running
    if (saraPID > 0)
    {
        kill(saraPID, SIGKILL);
    }

    // abort LoLA by sending SIGUSR1 signal
    //    kill(getpid(), SIGUSR1);
    //
    //    static pthread_t internal_thread;
    //    return NULL;
    // \todo check destructors and use return instead
    Handlers::exitHandler();
    RT::shutdown(0);
}

// initialize the runtime structure
void initialize(int argc, char** argv)
{
    // get start time for dynamic localtimelimit calculation
    time(&startTime);
    childpid = 0;
    // install exit handler for ordered exit()
    Handlers::installExitHandler();

    // initialize JSON data
    data["build"]["package_version"] = PACKAGE_VERSION;
    // data["build"]["svn_version"] = VERSION_SVN;
    data["build"]["build_system"] = CONFIG_BUILDSYSTEM;
    data["build"]["build_hostname"] = CONFIG_HOSTNAME;
    data["build"]["architecture"] = static_cast<int>(sizeof(void*) * 8);
#ifdef NDEBUG
    data["build"]["assertions"] = false;
#else
    data["build"]["assertions"] = true;
#endif
#ifdef USE_PERFORMANCE
    data["build"]["optimizations"] = true;
#else
    data["build"]["optimizations"] = false;
#endif
    for (int i = 1; i < argc; ++i)
    {
        data["call"]["parameters"] += argv[i];
    }
    const time_t ttt = startTime;
    char* ccc = ctime(&ttt);
    ccc[strlen(ccc) - 1] = '\0';  // remove newline
    data["call"]["starttime"] = std::string(ccc);
    char hname[1024];
    gethostname(hname, 1024);
    data["call"]["exec_host"] = std::string(hname);
    data["exit"]["signal"] = JSON::null;
    data["exit"]["error"] = JSON::null;
    data["exit"]["timelimitreached"] = false;
    data["child"] = JSON();

    // parse the command line parameters

    // strip path information from the tool name to have nicer error messages
    argv[0] = basename(argv[0]);

    // initialize the parameters structure
    struct cmdline_parser_params* params = cmdline_parser_params_create();

    // call the cmdline parser
    if (cmdline_parser(argc, argv, &args) != 0) [[unlikely]]
    {
        fmt::print(
            "[{0}] {1}\n", fmt::format(fmt::fg(fmt::terminal_color::magenta), "lola"),
            "invalid command-line parameters"
        );
        exit(EXIT_ERROR);
    }

    params->initialize = 0;
    params->override = 0;

    // call the config file parser
    if (args.conf_given && cmdline_parser_config_file(args.conf_arg, &args, params) != 0)
        [[unlikely]]
    {
        fmt::print(
            "[{0}] {1}\n", fmt::format(fmt::fg(fmt::terminal_color::magenta), "lola"),
            "invalid command-line parameters"
        );
        exit(EXIT_ERROR);
    }

    free(params);

    detail::Internal::get_instance().setup_loggers(args);

    // check for json include for logging
    for (unsigned int i = 0; i < args.jsoninclude_given; ++i)
    {
        if (args.jsoninclude_arg[i] == jsoninclude_arg_log)
        {
            log_to_json = true;
            data["log"] = JSON();
        }
    }

    // install termination handler for ordered premature termination
    Handlers::installTerminationHandlers();

    // install new handler
    std::set_new_handler(Handlers::newHandler);

    if (args.timelimit_given)
    {
        data["call"]["timelimit"] = args.timelimit_arg;
        log("LoLA will run for {0:d} seconds at most ({1})", args.timelimit_arg,
            markup(MARKUP::PARAMETER, "--timelimit"));
        const int ret = pthread_create(&reporter_thread, NULL, reporter_internal, nullptr);
        // LCOV_EXCL_START
        if (ret != 0) [[unlikely]]
        {
            log<level::critical>("thread could not be created");
            abort(ERROR::THREADING);
        }
        // LCOV_EXCL_STOP
    }
    else
    {
        data["call"]["timelimit"] = -1;
    }
}

// send the used configuration to a logging server
void callHome()
{
    // call home unless option --nolog is used
    if (not args.nolog_given)
    {
        Socket et(
            5000, RT::detail::Internal::get_instance().get_status(), "stats.service-technology.org",
            false
        );
        et.send(data.toString().c_str());
    }
}

// a function that implements the localtimeout for `--localtimelimit'
void* local_reporter_internal(void*)
{
    data["exit"]["localtimelimitreached"] = false;
    int last_type, last_state;
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &last_type);
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &last_state);
    if (localTimeLimitDynamic > 0)
    {
        // use dynamic localtimelimit
        data["call"]["localtimelimit"] = localTimeLimitDynamic;
        sleep(localTimeLimitDynamic);
    }
    else
    {
        // use for localtimelimit the passed argument

        data["call"]["localtimelimit"] = args.localtimelimit_arg;
        sleep(args.localtimelimit_arg);
    }
    data["exit"]["localtimelimitreached"] = true;
    log<level::critical>("{}", markup(MARKUP::IMPORTANT, "local time limit reached - aborting"));

    // first kill child if any
    if (childpid > 0)
    {
        kill(childpid, SIGUSR1);
    }

    // second kill sara if running
    if (saraPID > 0)
    {
        kill(saraPID, SIGKILL);
    }

    // abort LoLA child by sending SIGUSR2 signal
    // kill(getpid(), SIGUSR2);
    Handlers::exitHandler();
    RT::shutdown(0);

    // static pthread_t internal_thread;
    // return NULL;
}

}  // namespace RT
