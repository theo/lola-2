/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#ifndef SRC_PLANNING_MODULARREACHABILITYTASK_H
#define SRC_PLANNING_MODULARREACHABILITYTASK_H

#include "Core/Dimensions.h"
#include "Frontend/Parser/ast-system-k.h"
#include "Net/ModularStructure.h"
#include "Planning/Task.h"

class ModularReachabilityTask : public Task
{
public:
    ternary_t getResult() { return TERNARY_UNKNOWN; }
    char* interpreteResult(ternary_t result) { return nullptr; }
    Path getWitnessPath() { return {nullptr}; }
    capacity_t* getMarking() { return nullptr; }
    statusrecord* getStatistics() { return nullptr; }
    statusrecord* getStatus() { return nullptr; }
    void derail() {}
    Task* copy() { return nullptr; }
    static void buildTask(const lola::ModularStructure& ms, tFormula f, int parent, int fid);

private:
};

class ModularReachabilityPrintTask : public Task
{
public:
    ModularReachabilityPrintTask(const lola::ModularStructure& ms, tFormula f)
        : ms{std::addressof(ms)}
    {
        preprocessingfinished = false;
        net = Petrinet::InitialNet;
        formula = f;
        formula_id = f->portfolio_id;
        memory = new Mara();
        taskname = "modular print";
    }

    ternary_t getResult();
    char* interpreteResult(ternary_t result) { return nullptr; }
    Path getWitnessPath() { return {nullptr}; }
    capacity_t* getMarking() { return nullptr; }
    statusrecord* getStatistics() { return nullptr; }
    statusrecord* getStatus() { return nullptr; }
    void derail() {}
    Task* copy() { return nullptr; }
    static void buildTask(const lola::ModularStructure& ms, tFormula f, int parent, int fid)
    {
        auto* task = new ModularReachabilityPrintTask(ms, f);
        task->portfolio_id = portfoliomanager::addTask(
            Petrinet::InitialNet, task, parent, fid, FORMULA_REACHABLE, CONST_TASK, task->memory
        );
        task->preprocessingfinished = true;
    }

private:
    const lola::ModularStructure* ms;
};

#endif /* SRC_PLANNING_MODULARREACHABILITYTASK_H */
