/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include "ModularReachabilityTask.h"

#include "Portfolio/portfoliomanager.h"

void ModularReachabilityTask::buildTask(
    const lola::ModularStructure& ms, tFormula f, int parent, int fid
)
{
    int agg = portfoliomanager::addTask(
        Petrinet::InitialNet, nullptr, parent, fid, FORMULA_REACHABLE, AGGREGATE_TASK, nullptr
    );
    auto copy = portfoliomanager::copyFormula(f);
    copy->id = nullptr;
    auto copyid = portfoliomanager::addFormula(copy);
    ModularReachabilityPrintTask::buildTask(ms, copy, agg, copyid);
}

auto ModularReachabilityPrintTask::getResult() -> ternary_t
{
    pthread_mutex_lock(&kimwitu_mutex);
    formula =
        formula->rewrite(kc::singletemporal)->rewrite(kc::simpleneg)->rewrite(kc::booleanlists);
    formula->unparse(myprinter, kc::internal);
    StatePredicate* spFormula = formula->formula->copy(nullptr);
    pthread_mutex_unlock(&kimwitu_mutex);
    RT::print<VERB::OUTPUT>(spFormula->to_string(ms));
    portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
    return TERNARY_UNKNOWN;
}
