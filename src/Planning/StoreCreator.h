/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#pragma once

#include <Core/Dimensions.h>
#include <Net/Petrinet.h>
#include <Core/Runtime.h>
#include <Stores/CompareStore.h>
#include <Stores/StoreWrapper/CycleStore.h>
#include <Stores/NetStateEncoder/BitEncoder.h>
#include <Stores/NetStateEncoder/CopyEncoder.h>
#include <Stores/NetStateEncoder/FullCopyEncoder.h>
#include <Stores/NetStateEncoder/SimpleCompressedEncoder.h>
#include <Stores/StoreWrapper/PluginStore.h>
#include <Stores/StoreWrapper/EnlargingStore.h>
#include <Stores/Store.h>
#include <Stores/StoreWrapper/SymmetryStore.h>
#include <Stores/StoreWrapper/HashingWrapperStore.h>
#include <Stores/VectorStores/PrefixTreeStore.h>
#include <Stores/VectorStores/VBloomStore.h>
#include <Stores/VectorStores/VSTLStore.h>
#include <Symmetry/Constraints.h>
#include <Witness/Path.h>
#include <Memory/Mara.h>
#include <ReachabilitySAT/Reachability.h>

// forward declarations
class AutomataTree;
class BuechiAutomata;
class LTLExploration;
struct CTLFormula;
class ChooseTransition;
class DFSExploration;
class TSCCExploration;
class Firelist;
class NetState;
class SimpleProperty;
class StatePredicate;
class CTLExploration;
struct CoverPayload;
class SymmetryCalculator;

/// generic store creator, basically a templatarized namespace for static creation
/// methods. Resolves the code duplication problem with using different templates in
/// Task::setStore
namespace StoreCreator
{

/// auxiliary method that raises an error. Will be called by the different specializations
/// of createSpecializedStore.
inline void storeCreationError()
{
    RT::log<err>(RT::markup(MARKUP::BAD, "specified store does not fit the given task"));
    RT::abort(ERROR::COMMANDLINE);
}

// create bindings for a resulting store by binding function pointers to existing
// functions from another store
template <typename T, typename UnderlyingStore>
void createBindings(Store<T>* store, UnderlyingStore* ustore)
{
    store->_searchAndInsert =
        [ustore](NetState& ns, T** payload, threadid_t thread, bool noinsert) -> bool
    { return ustore->searchAndInsert(ns, payload, thread, noinsert); };

    store->_popState = [ustore](NetState& ns, threadid_t thread) -> bool
    { return ustore->popState(ns, thread); };

    store->_deleter = [ustore]() -> void { delete ustore; };
}

// finish creation of a store by wrapping it in a Cycle- and/or SymmetryStore and creating
// bindings for a result, basically nesting and redudant code got too much for my taste,
// so this was delegated, this as well as createBindings above should be inline-able
template <typename T, typename UnderlyingStore>
void finishStore(Store<T>* store, UnderlyingStore* ustore, Petrinet* net)
{
    if (RT::args.cycle_given)
    {
        auto cyclestore = new CycleStore(net, ustore, RT::args.cycleheuristic_arg);
        if (RT::args.symmetry_given && SymmetryCalculator::G
            && SymmetryCalculator::G->knownGenerators != 0)
        {
            auto symstore = new SymmetryStore(net, cyclestore, SymmetryCalculator::G);
            createBindings(store, symstore);
            return;
        }
        createBindings(store, cyclestore);
    }
    else if (RT::args.symmetry_given && SymmetryCalculator::G && SymmetryCalculator::G->knownGenerators != 0)
    {
        auto symstore = new SymmetryStore(net, ustore, SymmetryCalculator::G);
        createBindings(store, symstore);
        return;
    }
    createBindings(store, ustore);
}

/// creates a new store based on the specified template and the command line arguments
template <typename T>
Store<T>* createStore(Petrinet* net, Mara* mem, threadid_t number_of_threads = 1)
{
    // create an encode according --encoder
    NetStateEncoder* enc = nullptr;
    if (net->cardbuchistates < 1)  // we are not in the LTL business
    {
        switch (RT::args.encoder_arg)
        {
        case encoder_arg_bit:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "bit-perfect";
            if (net->isSkeleton)
            {
                enc = new SimpleCompressedEncoder(net->CardSignificant, number_of_threads);
            }
            else
            {
                enc = new BitEncoder(net, number_of_threads);
            }
            break;
        }

        case encoder_arg_copy:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "copy";
            enc = new CopyEncoder(net->CardSignificant, number_of_threads);
            break;
        }

        case encoder_arg_simplecompressed:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "simple compression";
            enc = new SimpleCompressedEncoder(net->CardSignificant, number_of_threads);
            break;
        }

        case encoder_arg_fullcopy:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] = "full
            // copy";
            enc = new FullCopyEncoder(net->Card[PL], number_of_threads);
            break;
        }

        case encoder__NULL:
        {
            // cannot happen
            assert(false);
        }
        }
    }
    else
    {
        switch (RT::args.encoder_arg)
        {
        case encoder_arg_bit:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "bit-perfect";
            if (net->isSkeleton)
            {
                enc = new LTLSimpleCompressedEncoder(net->CardSignificant, number_of_threads);
            }
            else
            {
                enc = new LTLBitEncoder(net, number_of_threads);
            }
            break;
        }

        case encoder_arg_copy:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "copy";
            enc = new LTLCopyEncoder(net->CardSignificant, number_of_threads);
            break;
        }

        case encoder_arg_simplecompressed:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] =
            // "simple compression";
            enc = new LTLSimpleCompressedEncoder(net->CardSignificant, number_of_threads);
            break;
        }

        case encoder_arg_fullcopy:
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["encoder"] = "full
            // copy";
            enc = new LTLFullCopyEncoder(net->Card[PL], number_of_threads);
            break;
        }

        case encoder__NULL:
        {
            // cannot happen
            assert(false);
        }
        }
    }

    // create a store according to --store
    Store<T>* st = new Store<T>(number_of_threads);

    switch (RT::args.store_arg)
    {
    case store_arg_comp:
    {
        if (RT::args.largeprefix_given)
        {
            RT::log<debug>("Using enlarging store!");
            auto underlying_store = new CompareStore<
                T, NSPluginStore<T, PrefixTreeStore<T>, arrayindex_t>,
                NSEnlargingStore<T, int*, PrefixTreeStore<T, int*>, arrayindex_t>>(
                new NSPluginStore<T, PrefixTreeStore<T>, arrayindex_t>(
                    enc, mem, net->Card[PL], number_of_threads
                ),
                new NSEnlargingStore<T, int*, PrefixTreeStore<T, int*>, arrayindex_t>(
                    enc, mem, net->Card[PL], number_of_threads, net
                )
            );
            finishStore(st, underlying_store, net);
        }
        else
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["store"] =
            // "compare";
            auto underlying_store = new CompareStore<
                T, NSPluginStore<T, PrefixTreeStore<T>, arrayindex_t>,
                NSPluginStore<T, VSTLStore<T>>>(
                new NSPluginStore<T, PrefixTreeStore<T>, arrayindex_t>(
                    enc, mem, net->Card[PL], number_of_threads
                ),
                new NSPluginStore<T, VSTLStore<T>>(enc, mem, number_of_threads)
            );
            finishStore(st, underlying_store, net);
        }

        break;
    }

    case store_arg_prefix:
    {
        // portfoliomanager::taskjson[mem->taskid]["configuration"]["store"] = "prefix";
        if (RT::args.bucketing_given)
        {
            auto underlying_store = new NSHashingWrapperStore<T, PrefixTreeStore<T>, arrayindex_t>(
                enc, mem, net->Card[PL], RT::args.bucketing_arg, number_of_threads
            );
            finishStore(st, underlying_store, net);
        }
        else
        {
            auto underlying_store = new NSPluginStore<T, PrefixTreeStore<T>, arrayindex_t>(
                enc, mem, net->Card[PL], number_of_threads
            );
            finishStore(st, underlying_store, net);
        }
        break;
    }

    case store_arg_stl:
    {
        // portfoliomanager::taskjson[mem->taskid]["configuration"]["store"] = "stl";
        if (RT::args.bucketing_given)
        {
            auto underlying_store = new NSHashingWrapperStore<T, VSTLStore<T>>(
                enc, mem, RT::args.bucketing_arg, number_of_threads
            );
            finishStore(st, underlying_store, net);
        }
        else
        {
            auto underlying_store = new NSPluginStore<T, VSTLStore<T>>(enc, mem, number_of_threads);
            finishStore(st, underlying_store, net);
        }
        break;
    }

    case store_arg_bloom:
    {
        // only allow bloomstore for void payloads
        if constexpr (std::is_same_v<T, void>)
        {
            // portfoliomanager::taskjson[mem->taskid]["configuration"]["store"] =
            // "bloom";
            if (RT::args.bucketing_given)
            {
                auto underlying_store = new NSHashingWrapperStore<
                    void, VBloomStore<(unsigned long)BLOOM_FILTER_SIZE / SIZEOF_MARKINGTABLE + 1>,
                    size_t>(
                    enc, mem, RT::args.hashfunctions_arg, RT::args.bucketing_arg, number_of_threads
                );
                finishStore(st, underlying_store, net);
            }
            else
            {
                auto underlying_store =
                    new NSPluginStore<void, VBloomStore<BLOOM_FILTER_SIZE>, size_t>(
                        enc, mem, RT::args.hashfunctions_arg, number_of_threads
                    );
                finishStore(st, underlying_store, net);
            }
        }
        else
        {
            storeCreationError();
        }
        break;
    }

    case store__NULL:
    {
        // cannot happen
        assert(false);
    }
    }
    return st;
}
};  // namespace StoreCreator
