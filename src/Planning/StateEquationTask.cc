/****************************************************************************
This file is part of LoLA.

LoLA is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
more details.

You should have received a copy of the GNU Affero General Public License
along with LoLA. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#include <Core/Runtime.h>
#include <Planning/Task.h>
#include <Planning/StateEquationTask.h>
#include <Frontend/Parser/ast-system-k.h>
#include <Frontend/Parser/ast-system-rk.h>
#include <Frontend/Parser/ast-system-unpk.h>
#include <Formula/StatePredicate/AtomicBooleanPredicate.h>
#include <Formula/StatePredicate/AtomicStatePredicate.h>
#include <Formula/StatePredicate/MagicNumber.h>
#include <Exploration/DFSExploration.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

/////////////////////
// HELPER FUNCTION //
/////////////////////

#ifndef __cplusplus11
inline std::string int_to_string(int i)
{
    std::stringstream* s = new std::stringstream;
    (*s) << i;
    return (*s).str();
}
#endif

void printsolver(
    operations_research::MPSolver* solver, operations_research::MPVariable** orplace, int,
    operations_research::MPVariable** ortransition, int
);
// pthread_mutex_t StateEquationTask::stateequation_mutex = PTHREAD_MUTEX_INITIALIZER;

StateEquationTask::StateEquationTask(Petrinet* n, int par, tFormula f, int fid)
{
    preprocessingfinished = false;
    net = n;
    parent = par;
    formula = f;
    formula_id = fid;
    result = TERNARY_UNKNOWN;
    taskname = deconst("state equation");
    portfolio_id = portfoliomanager::addTask(n, this, par, fid, FORMULA_REACHABLE, EQUATION_TASK);
    memory = new Mara();
    goStatus = false;
}

StateEquationTask::~StateEquationTask() {}

/// check whether parikh vector can be realized at marking in net n

typedef struct se
{
    int currententry;
    int* firelist;
    struct se* next;
} stackentry;

int StateEquationTask::steqfirelist(
    Petrinet* n, int* parikh, NetState* ns, int* stubstack, int* stubtarjan, bool* onstubtarjan,
    bool* visited, int** mustbeincluded, int* currentindex, int* dfs, int* lowlink, int** result
)
{
    // compute a fire list for the realizability check of parikh
    // we compute a regular stubborn set but pretend that only the
    // transitions in the support of parikh exist.

    memset(onstubtarjan, false, n->Card[TR] * sizeof(bool));
    memset(visited, false, n->Card[TR] * sizeof(bool));
    int cardstubstack = 0;
    int cardstubtarjan = 0;

    // find activated transition
    int i;
    for (i = 0; i < n->Card[TR]; i++)
    {
        if (!parikh[i])
            continue;
        if (ns->Enabled[i])
            break;
    }
    if (i >= n->Card[TR])
    {
        *result = NULL;
        return 0;
    }
    stubstack[0] = stubtarjan[0] = i;
    onstubtarjan[i] = visited[i] = true;
    dfs[i] = lowlink[i] = 0;
    mustbeincluded[0] = n->TrDecreased[i];  // t_i is activated
    currentindex[0] = n->TrCardDecreasing[i] + (n->TrDecreasing[i] - n->TrDecreased[i]);

    int currenttransition = i;
    int nextdfs = 1;
    int lastenabled = 0;
    while (true)
    {
        // explore currenttransition
        int t = stubstack[cardstubstack];
        if (currentindex[cardstubstack] > 0)
        {
            // t has another successor to be explored

            int newt = mustbeincluded[cardstubstack][--(currentindex[cardstubstack])];
            if (!parikh[newt])  // newt to be ignored since it does not occur in vector
            {
                continue;
            }
            if (visited[newt])
            {
                // newt already seen

                // update lowlink

                if (onstubtarjan[newt] && lowlink[t] > dfs[newt])
                {
                    lowlink[t] = dfs[newt];
                }

                // proceed with t
            }
            else
            {
                // newt not yet seen
                visited[newt] = true;
                dfs[newt] = lowlink[newt] = nextdfs++;
                stubstack[++cardstubstack] = newt;
                stubtarjan[++cardstubtarjan] = newt;
                if (ns->Enabled[newt])
                {
                    // mustbeincluded = conflicting
                    lastenabled = dfs[newt];
                    mustbeincluded[cardstubstack] = n->TrDecreased[newt];
                    currentindex[newt] = n->TrCardDecreasing[newt]
                        + (n->TrDecreasing[newt] - n->TrDecreased[newt]);
                }
                else
                {
                    // mustbeincluded = pre of scapegoat

                    // find scapegoat
                    int i;
                    for (i = 0; i < n->CardArcs[TR][PRE][newt]; i++)
                    {
                        if (n->Mult[TR][PRE][newt][i] > ns->Current[n->Arc[TR][PRE][newt][i]])
                        {
                            break;
                        }
                    }
                    int scapegoat = n->Arc[TR][PRE][newt][i];

                    // include pre of scapegoat

                    mustbeincluded[cardstubstack] = n->PlIncreasing[scapegoat];
                    currentindex[cardstubstack] = n->PlCardIncreasing[scapegoat];
                }
            }
        }
        else
        {
            // exploration of t is completed

            // check for completed scc

            if (dfs[t] == lowlink[t])
            {
                // scc closed

                if (lastenabled >= dfs[t])
                {
                    // scc contains enabled transition
                    // build firelist and return
                    int cardresult = 0;
                    for (int tt = cardstubtarjan; stubtarjan[tt] != t; tt--)
                    {
                        if (ns->Enabled[stubtarjan[tt]])
                        {
                            cardresult++;
                        }
                    }
                    if (ns->Enabled[t])
                        cardresult++;
                    int* firelist = new int[cardresult];
                    cardresult = 0;
                    for (int tt = cardstubtarjan; stubtarjan[tt] != t; tt--)
                    {
                        if (ns->Enabled[stubtarjan[tt]])
                        {
                            firelist[cardresult++] = stubtarjan[tt];
                        }
                    }
                    if (ns->Enabled[t])
                    {
                        firelist[cardresult++] = t;
                    }
                    *result = firelist;
                    return cardresult;
                }

                // scc closed but does not contain enabled transitions

                // remove scc from tarjan stack
                while (stubtarjan[cardstubtarjan] != t)
                {
                    int popped = stubtarjan[cardstubtarjan--];
                    onstubtarjan[popped] = false;
                }
                cardstubtarjan--;
                onstubtarjan[t] = false;

                // backtrack from t
                cardstubstack--;
            }
            else
            {
                // scc not close --> simple backtrack from t

                cardstubstack--;
                if (lowlink[t] < lowlink[stubstack[cardstubstack]])
                {
                    lowlink[stubstack[cardstubstack]] = lowlink[t];
                }
            }
        }
    }
}

bool StateEquationTask::realizable(Petrinet* n, int* parikh)
{
    // current marking as net state
    NetState* ns = NetState::createNetStateFromInitial(n);

    // stack and tarjan stack for stubborn set calculation
    // allocated once, to be recycled in whole dfs
    int* stubstack = new int[n->Card[TR]];
    int* stubtarjan = new int[n->Card[TR]];
    bool* onstubtarjan = new bool[n->Card[TR]];
    bool* visited = new bool[n->Card[TR]];
    int** mustbeincluded = new int*[n->Card[TR]];
    int* currentindex = new int[n->Card[TR]];
    int* dfs = new int[n->Card[TR]];
    int* lowlink = new int[n->Card[TR]];

    // callstack
    stackentry* stack = new stackentry;
    stack->currententry = steqfirelist(
        n, parikh, ns, stubstack, stubtarjan, onstubtarjan, visited, mustbeincluded, currentindex,
        dfs, lowlink, &(stack->firelist)
    );
    stack->next = NULL;
    // for(int i = 0; i < stack->currententry;i++)
    //{
    // RT::log("initial fl: {}", n->Name[TR][stack->firelist[i]]);
    //}

    // depth is the number of remaining transitions in parikh vector
    int depth = 0;
    for (int i = 0; i < n->Card[TR]; i++)
    {
        depth += parikh[i];
    }
    // RT::log("original depth is {}", depth);
    // if(!depth) return true;
    // for(int i = 0; i < n->Card[TR]; i++)
    //{
    // if(parikh[i]) RT::log("parikh of {} is {}", n->Name[TR][i], parikh[i]);
    //}

    while (true)
    {
        // RT::log("NEW ROUND");
        // for(int i = 0; i < n->Card[TR]; i++)
        //{
        // if(parikh[i]) RT::log("remaining parikh of {} is {}", n->Name[TR][i], parikh[i]);
        //}
        // consider situation at top of stack
        if (stack->currententry > 0)
        {
            // there is a transition to be tried
            int currenttransition = stack->firelist[--(stack->currententry)];
            // RT::log("try {} at depth {}", n->Name[TR][currenttransition],depth);
            depth--;
            if (!depth)
            {
                // we have realized the whole vector
                delete ns;
                delete[] stubstack;
                delete[] stubtarjan;
                delete[] onstubtarjan;
                delete[] visited;
                delete[] mustbeincluded;
                delete[] currentindex;
                delete[] dfs;
                delete[] lowlink;
                while (stack)
                {
                    stackentry* garbage = stack;
                    stack = stack->next;
                    delete[] garbage->firelist;
                    delete garbage;
                }
                return true;
            }
            // we have not yet realized the whole vector
            parikh[currenttransition]--;
            // RT::log("FIRE {}", n->Name[TR][currenttransition]);
            n->fire(*ns, currenttransition);
            n->updateEnabled(*ns, currenttransition);
            stackentry* nextstack = new stackentry;
            nextstack->next = stack;
            nextstack->currententry = steqfirelist(
                n, parikh, ns, stubstack, stubtarjan, onstubtarjan, visited, mustbeincluded,
                currentindex, dfs, lowlink, &(nextstack->firelist)
            );
            stack = nextstack;
            // continue with next transition
        }
        else
        {
            // there is no more transition to be tried
            // --> backtrack
            stackentry* garbage = stack;
            stack = stack->next;
            delete[] garbage->firelist;
            delete garbage;
            depth++;
            if (!stack)
            {
                // search exhaustivey completed
                delete ns;
                delete[] stubstack;
                delete[] stubtarjan;
                delete[] onstubtarjan;
                delete[] visited;
                delete[] mustbeincluded;
                delete[] currentindex;
                delete[] dfs;
                delete[] lowlink;
                return false;
            }
            parikh[stack->firelist[stack->currententry]]++;
            // RT::log("BACKTRACK: backfire {}", n->Name[TR][stack->firelist[stack->currententry]]);
            n->backfire(*ns, stack->firelist[stack->currententry]);
            n->revertEnabled(*ns, stack->firelist[stack->currententry]);
        }
    }
}

operations_research::MPSolver* getStateEquation(
    Petrinet* net, operations_research::MPVariable** orplace,
    operations_research::MPVariable** ortransition
)
{
    // Create an LP solver filled with constraints for the state equation of the given net.
    // Also fill in the necessary variables into pre-allocated arrays
    // orplace and ortransition:
    // transition variable represents the firing count ot the firing sequence
    // place variable represents the final marking.
    // Initial marking is the initial marking of the net.
    // variable lists are returned to permit the insertion of further constraints
    // optimization criterion is set to minimal length of the firing vector.

    // get solver instance and fill with net

    operations_research::MPSolver* solver = operations_research::MPSolver::CreateSolver("SCIP");
    double infinity = solver->infinity();

    //    create place variables
    for (int i = 0; i < net->Card[PL]; i++)
    {
        orplace[i] = solver->MakeIntVar(0, RT::args.safe_given ? 1.0 : infinity, net->Name[PL][i]);
    }

    //    create transition variables
    for (int i = 0; i < net->Card[TR]; i++)
    {
        ortransition[i] = solver->MakeIntVar(0, infinity, net->Name[TR][i]);
    }

    // add objective function: short sequence length

    operations_research::MPObjective* objective = solver->MutableObjective();
    for (int i = 0; i < net->Card[TR]; i++)
    {
        objective->SetCoefficient(ortransition[i], 1.0);
    }
    objective->SetMinimization();

    // create equations from state equation: Cx = m - m0
    // --> Cx - m = - m0
    // one equation per place

    for (int i = 0; i < net->Card[PL]; i++)
    {
        operations_research::MPConstraint* ccc = solver->MakeRowConstraint(
            -1.0 * net->Initial[i], -1.0 * net->Initial[i]
        );

        ccc->SetCoefficient(orplace[i], -1.0);
        for (int j = 0; j < net->CardArcs[PL][PRE][i]; j++)
        {
            ccc->SetCoefficient(
                ortransition[net->Arc[PL][PRE][i][j]], 1.0 * net->Mult[PL][PRE][i][j]
            );
        }
        for (int j = 0; j < net->CardArcs[PL][POST][i]; j++)
        {
            ccc->SetCoefficient(
                ortransition[net->Arc[PL][POST][i][j]],
                ccc->GetCoefficient(ortransition[net->Arc[PL][POST][i][j]])
                    - 1.0 * net->Mult[PL][POST][i][j]
            );
        }
    }
    return solver;
}

ternary_t StateEquationTask::solveLP(
    Petrinet* net, operations_research::MPSolver* solver, operations_research::MPVariable** orplace,
    operations_research::MPVariable** ortransition
)
{
    // solve LP problem, exclude spurious solutions using traps, and try to realize
    // solution.
    // return values:
    // TERNARY_FALSE: problem infeasible
    // TERNARY_TRUE: problem feasible, solution corresponds to executable firing sequence
    // TERNARY_UNKNOWN: problem feasible, solution not realizeable

    double infinity = solver->infinity();
    ternary_t result;
    // solve problem iteratively until solution is found that is not spurious wrt traps.
    while (true)
    {
        operations_research::MPSolver::ResultStatus rs = solver->Solve();
        switch (rs)
        {
        case operations_research::MPSolver::OPTIMAL:
        case operations_research::MPSolver::FEASIBLE:
            result = TERNARY_TRUE;
            break;
        case operations_research::MPSolver::INFEASIBLE:
            result = TERNARY_FALSE;
            break;
        default:
            return TERNARY_UNKNOWN;
        }
        if (result == TERNARY_FALSE)
        {
            // proceed with next conjunction
            return TERNARY_FALSE;
        }

        // here: we have a solution. Next task: look at traps to check whether solution is spurious

        int cardtrap = 0;
        int* trap = new int[net->Card[PL]];      // lists all unmarked places
        bool* intrap = new bool[net->Card[PL]];  // initially true for unmarked places
        int cardinitiallymarked = 0;             // number of initially marked places in trap
        for (int i = 0; i < net->Card[PL]; i++)
        {
            intrap[i] = (orplace[i]->solution_value() < 0.5);
            if (intrap[i])
            {
                trap[cardtrap++] = i;
                if (net->Initial[i])
                    cardinitiallymarked++;
            }
        }

        // iteratively remove places from trap that violate trap property
        // violation: p has post-transition t and t has no post-places in trap
        // we can abort trap calculation as soon as it no longer contains an
        // initially marked place: if the trap contains only unmarked places
        // it does not indicate a spurious solution

        bool somethingchanged = true;
        while (somethingchanged && cardinitiallymarked)
        {
            somethingchanged = false;
            for (int i = 0; i < cardtrap;)
            {
                int p = trap[i];  // check trap property for place p
                bool violationfound = false;
                for (int j = 0; j < net->CardArcs[PL][POST][p]; j++)
                {
                    int t = net->Arc[PL][POST][p][j];  // t is post of p
                    int k;
                    for (k = 0; k < net->CardArcs[TR][POST][t]; k++)
                    {
                        int q = net->Arc[TR][POST][t][k];  // q is post of t
                        if (intrap[q])
                            break;  // trap ok for t
                    }
                    if (k >= net->CardArcs[TR][POST][t])
                    {
                        // t does not put tokens into trap
                        violationfound = true;
                        break;
                    }
                }
                if (violationfound)
                {
                    // remove p from trap
                    somethingchanged = true;
                    intrap[p] = false;
                    trap[i] = trap[--cardtrap];
                    if (net->Initial[p])
                        cardinitiallymarked--;
                    if (!cardinitiallymarked)
                    {
                        break;
                    }
                    // do not increment i since new place has been
                    // shifted to position i
                }
                else
                {
                    // check next place in trap
                    i++;
                }
            }
        }
        if (cardinitiallymarked)
        {
            // have found trap --> exclude it with new constraint and find new solution
            // constraint is: sum of all places in trap is greater or equal to 1
            operations_research::MPConstraint* ccc = solver->MakeRowConstraint(1.0, infinity);
            for (int i = 0; i < cardtrap; i++)
            {
                ccc->SetCoefficient(orplace[trap[i]], 1.0);
            }
            delete[] trap;
            delete[] intrap;
            continue;
        }

        // trap check passed. Now: try to realize parikh vector computed by solver
        // if solution can be realized, whole problem results in true
        // if solution cannot be realized: either continue trying, or mark "unknown"
        delete[] trap;
        delete[] intrap;
        int* parikh = new int[net->Card[TR]];  // carries the Parikh vector of solution
        for (int i = 0; i < net->Card[TR]; i++)
        {
            parikh[i] = static_cast<int>(ortransition[i]->solution_value() + 0.5);
        }
        if (StateEquationTask::realizable(net, parikh))
        {
            delete[] parikh;
            return TERNARY_TRUE;
        }
        else
        {
            delete[] parikh;
            return TERNARY_UNKNOWN;
        }
    }
}

ternary_t StateEquationTask::getResult()
{
    // reset
    saraIsRunning = 0;  // not yet started
    goStatus = true;
    // extract state predicate from formula
    assert(formula);
    // copy formula for additional dnf rewrite
    kc::tFormula TheFormulaDNF = formula;

    pthread_mutex_lock(&kimwitu_mutex);
    TheFormulaDNF = TheFormulaDNF->rewrite(kc::singletemporal);
    TheFormulaDNF = TheFormulaDNF->rewrite(kc::simpleneg);

    // get the assumed length of DNF formula and the assumed number of ORs
    unparsed.clear();
    TheFormulaDNF->unparse(myprinter, kc::internal);
    pthread_mutex_unlock(&kimwitu_mutex);
    StatePredicate* TheStatePredicate = TheFormulaDNF->formula;
    // RT::rep->status("STATE EQUATION: %d %s", portfolio_id, TheStatePredicate -> toString());
    // RT::log("DNF {}", TheStatePredicate -> toString());

    // unparse the content of the problem file for sara
    AtomicBooleanPredicate* dnf = TheStatePredicate->DNF();

    // check pathological cases in dnf
    pthread_mutex_lock(&portfoliomanager::portfolio_mutex);
    if (dnf)
    {
        portfoliomanager::taskjson[portfolio_id]["stateequation literals"] = static_cast<int>(
            dnf->literals
        );
        portfoliomanager::taskjson[portfolio_id]["stateequation problems"] = static_cast<int>(
            dnf->cardSub
        );
    }
    else
    {
        portfoliomanager::taskjson[portfolio_id]["stateequation literals"] = JSON::null;
        portfoliomanager::taskjson[portfolio_id]["stateequation problems"] = JSON::null;
    }
    pthread_mutex_unlock(&portfoliomanager::portfolio_mutex);

    if (!dnf)
    {
        portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
        return TERNARY_UNKNOWN;
    }
    if (dnf->magicnumber == MAGIC_NUMBER_TRUE)
    {
        portfoliomanager::report(portfolio_id, TERNARY_TRUE);
        return TERNARY_TRUE;
    }
    if (dnf->magicnumber == MAGIC_NUMBER_FALSE)
    {
        portfoliomanager::report(portfolio_id, TERNARY_FALSE);
        return TERNARY_FALSE;
    }
    if ((!(dnf->isAnd)) && (dnf->cardSub == 0))
    {
        portfoliomanager::report(portfolio_id, TERNARY_FALSE);
        return TERNARY_FALSE;
    }
    if ((dnf->isAnd) && (dnf->cardSub == 0))
    {
        portfoliomanager::report(portfolio_id, TERNARY_TRUE);
        return TERNARY_TRUE;
    }

    if (dnf->isAnd)
    {
        AtomicBooleanPredicate* temp = new AtomicBooleanPredicate(net, false);
        temp->addSub(dnf);
        dnf = temp;
    }

    // RT::rep->status("dnf: %s",dnf->toString());
    // RT::rep->status("%s",dnf->toString());

    // RT::log("in get result: {}", dnf->toString());

    // We solve the state equation with less optimisations than sara.
    // We work on dnf and assume that its top operator is "or"

    assert(!dnf->isAnd);

    // dnf is checked by iterating the contained conjunctions. If any conjunction
    // turns out to be satisfiable, so is whole dnf, and we may return immediately.
    // If all conjunctions are false, so is the final result.
    // If any conjunction remains unknown, and no conjunction is true, result is unknown.

    bool haveUnknownConjunction = false;

    // prepare solver

    operations_research::MPVariable** orplace = new operations_research::MPVariable*[net->Card[PL]];
    operations_research::MPVariable** ortransition =
        new operations_research::MPVariable*[net->Card[TR]];
    operations_research::MPSolver* solver = getStateEquation(net, orplace, ortransition);
    ConstraintManager* cm = new ConstraintManager(solver);
    double infinity = solver->infinity();

    // try to individually solve for the conjunctions of the dnf

    for (int currentindex = 0; currentindex < dnf->cardSub; currentindex++)
    {
        AtomicBooleanPredicate* currentconj = (AtomicBooleanPredicate*)(dnf->sub[currentindex]);

        // create inequations from conjunction, one inequation for each atom

        cm->reset(orplace, net->Card[PL]);  // reset solver to pure state equation
        for (int i = 0; i < currentconj->cardSub; i++)
        {
            AtomicStatePredicate* atomic = (AtomicStatePredicate*)currentconj->sub[i];
            operations_research::MPConstraint* ccc = cm->newTmpConstraint(
                -infinity, 1.0 * atomic->threshold
            );

            for (int j = 0; j < atomic->cardPos; j++)
            {
                ccc->SetCoefficient(orplace[atomic->posPlaces[j]], 1.0 * atomic->posMult[j]);
            }
            for (int j = 0; j < atomic->cardNeg; j++)
            {
                ccc->SetCoefficient(orplace[atomic->negPlaces[j]], -1.0 * atomic->negMult[j]);
            }
        }

        // here: LP problem for state equation and conjunction currentconj is created.
        // now: solve LP problem.
        ternary_t res = StateEquationTask::solveLP(net, solver, orplace, ortransition);
        switch (res)
        {
        case TERNARY_TRUE:
            portfoliomanager::report(portfolio_id, TERNARY_TRUE);
            return TERNARY_TRUE;
        case TERNARY_UNKNOWN:
            haveUnknownConjunction = true;
            continue;
        default:
            continue;
        }
    }
    if (haveUnknownConjunction)
    {
        portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
        delete[] orplace;
        delete[] ortransition;
        // delete solver;
        delete cm;
        return TERNARY_UNKNOWN;
    }
    else
    {
        portfoliomanager::report(portfolio_id, TERNARY_FALSE);
        delete[] orplace;
        delete[] ortransition;
        // delete solver;
        delete cm;
        return TERNARY_FALSE;
    }
}

ternary_t StateEquationTask::getSafetyResult(
    Petrinet* net, ConstraintManager* cm, operations_research::MPVariable** orplace,
    operations_research::MPVariable** ortransition, arrayindex_t ppp
)
{
    // add constraint 2 <= ppp <= oo
    operations_research::MPConstraint* safetyconstraint = cm->newTmpConstraint(
        2.0, cm->solver->infinity()
    );
    safetyconstraint->SetCoefficient(orplace[ppp], 1.0);
    ternary_t result = StateEquationTask::solveLP(net, cm->solver, orplace, ortransition);
    // smart reset: only one place needs to be nulled
    cm->reset(orplace + ppp, 1);
    // cm -> reset(orplace,net->Card[PL]);
    // cm -> solver -> Reset();
    if (result == TERNARY_FALSE)
        return TERNARY_TRUE;
    if (result == TERNARY_TRUE)
        return TERNARY_FALSE;
    return TERNARY_UNKNOWN;
}

void StateEquationTask::getQuasilivenessResult(
    Petrinet* net, ConstraintManager* cm, operations_research::MPVariable** orplace,
    operations_research::MPVariable** ortransition
)
{
    // add constraint t- >= m
    arrayindex_t targettransition = portfoliomanager::gettargettransition();
    for (int i = 0; i < net->CardArcs[TR][PRE][targettransition]; i++)
    {
        operations_research::MPConstraint* fireconstraint = cm->newTmpConstraint(
            1.0 * net->Mult[TR][PRE][targettransition][i], cm->solver->infinity()
        );
        fireconstraint->SetCoefficient(orplace[net->Arc[TR][PRE][targettransition][i]], 1.0);
    }
    ternary_t result = StateEquationTask::solveLP(net, cm->solver, orplace, ortransition);
    portfoliomanager::synchronisetransition(targettransition, result, GLOBAL_STATEEQUATION);
    cm->reset(orplace, net->Card[PL]);
    return;
}

void StateEquationTask::getConstantPlaceResult(
    Petrinet* net, ConstraintManager* cm, operations_research::MPVariable** orplace,
    operations_research::MPVariable** ortransition
)
{
    arrayindex_t targetplace = portfoliomanager::gettargetplace();
    // phase one: check whether some sequence can increase the place

    // add constraint p >= m0(p)+1
    operations_research::MPConstraint* constantconstraint = cm->newTmpConstraint(
        1.0 + net->Initial[targetplace], cm->solver->infinity()
    );
    constantconstraint->SetCoefficient(orplace[targetplace], 1.0);
    ternary_t result1 = StateEquationTask::solveLP(net, cm->solver, orplace, ortransition);
    // smart reset: only one place needs to be nulled
    cm->reset(orplace + targetplace, 1);
    if (result1 == TERNARY_TRUE)
    {
        portfoliomanager::synchroniseplace(targetplace, TERNARY_TRUE, GLOBAL_STATEEQUATION);
    }
    // phase two: check whether some sequence can decrease the place

    // add constraint p <= m0(p)-1
    constantconstraint = cm->newTmpConstraint(
        -cm->solver->infinity(), net->Initial[targetplace] - 1.0
    );
    constantconstraint->SetCoefficient(orplace[targetplace], 1.0);
    ternary_t result2 = StateEquationTask::solveLP(net, cm->solver, orplace, ortransition);
    // smart reset: only one place needs to be nulled
    cm->reset(orplace + targetplace, 1);
    if (result2 == TERNARY_TRUE)
    {
        portfoliomanager::synchroniseplace(targetplace, TERNARY_TRUE, GLOBAL_STATEEQUATION);
    }
    if (result1 == TERNARY_FALSE && result2 == TERNARY_FALSE)
    {
        // we can rely on the infeasibility results
        portfoliomanager::synchroniseplace(targetplace, TERNARY_FALSE, GLOBAL_STATEEQUATION);
    }
    portfoliomanager::synchroniseplace(targetplace, TERNARY_UNKNOWN, GLOBAL_STATEEQUATION);
}

char* StateEquationTask::interpreteResult(ternary_t result)
{
    switch (result)
    {
    case TERNARY_TRUE:
        return deconst("The predicate is reachable.");
    case TERNARY_FALSE:
        return deconst("The predicate is unreachable.");
    case TERNARY_UNKNOWN:
        return deconst("The predicate may or may not be reachable.");
    default:
        assert(false);
    }
    return NULL;
}

Path StateEquationTask::getWitnessPath()
{
    // \todo add witness path
    Path* p = new Path(net);

    return *p;
}

capacity_t* StateEquationTask::getMarking() { return NULL; }

statusrecord* StateEquationTask::getStatistics() { return NULL; }

statusrecord* StateEquationTask::getStatus()
{
    statusrecord* result = new statusrecord();
    switch (saraIsRunning)
    {
    case 0:
        result->text = deconst("sara not yet started (preprocessing)");
        break;
    case 1:
        result->text = deconst("sara is running");
        break;
    case 2:
        result->text = deconst("could not launch sara");
        break;
    default:
        result->text = deconst("unknwon sara status");
    }
    return result;
}

void StateEquationTask::buildTask(Petrinet* n, int par, tFormula f, int fid)
{
    StateEquationTask* S = new StateEquationTask(n, par, f, fid);
    S->preprocessingfinished = true;
}

void* StateEquationTask::safetystateequationthread(void*)
{
    Petrinet* net = Petrinet::InitialNet;
    arrayindex_t targetplace = portfoliomanager::gettargetplace();
    bool* tried = new bool[net->Card[PL]];
    memset(tried, 0, net->Card[PL] * sizeof(bool));

    // prepare solver

    operations_research::MPVariable** orplace = new operations_research::MPVariable*[net->Card[PL]];
    operations_research::MPVariable** ortransition =
        new operations_research::MPVariable*[net->Card[TR]];
    operations_research::MPSolver* solver = getStateEquation(net, orplace, ortransition);
    ConstraintManager* cm = new ConstraintManager(solver);

    while (true)
    {
        ternary_t result = getSafetyResult(
            Petrinet::InitialNet, cm, orplace, ortransition, targetplace
        );
        tried[targetplace] = true;
        switch (result)
        {
        case TERNARY_TRUE:
        case TERNARY_FALSE:

            portfoliomanager::synchroniseplace(targetplace, result, GLOBAL_STATEEQUATION);
            break;

        default:;
        }
        for (targetplace = 0; targetplace < net->Card[PL]; targetplace++)
        {
            if (portfoliomanager::globalresult[targetplace] == TERNARY_VOID && !tried[targetplace])
                break;
        }
        if (targetplace == net->Card[PL])
            return NULL;
    }
    return NULL;
}

void* StateEquationTask::constantplacestateequationthread(void*)
{
    // prepare solver

    operations_research::MPVariable** orplace =
        new operations_research::MPVariable*[Petrinet::InitialNet->Card[PL]];
    operations_research::MPVariable** ortransition =
        new operations_research::MPVariable*[Petrinet::InitialNet->Card[TR]];
    operations_research::MPSolver* solver = getStateEquation(
        Petrinet::InitialNet, orplace, ortransition
    );
    ConstraintManager* cm = new ConstraintManager(solver);

    while (true)
    {
        getConstantPlaceResult(Petrinet::InitialNet, cm, orplace, ortransition);
    }
    return NULL;
}

void* StateEquationTask::quasilivenessstateequationthread(void*)
{
    // prepare solver

    operations_research::MPVariable** orplace =
        new operations_research::MPVariable*[Petrinet::InitialNet->Card[PL]];
    operations_research::MPVariable** ortransition =
        new operations_research::MPVariable*[Petrinet::InitialNet->Card[TR]];
    operations_research::MPSolver* solver = getStateEquation(
        Petrinet::InitialNet, orplace, ortransition
    );
    ConstraintManager* cm = new ConstraintManager(solver);

    while (true)
    {
        getQuasilivenessResult(Petrinet::InitialNet, cm, orplace, ortransition);
    }
    return NULL;
}

void StateEquationTask::derail()
{
    if (portfoliomanager::childpid[portfolio_id] > 0)
        kill(portfoliomanager::childpid[portfolio_id], SIGKILL);
}

Task* StateEquationTask::copy()
{
    return new StateEquationTask(net, parent, portfoliomanager::copyFormula(formula), formula_id);
}

ConstraintManager::ConstraintManager(operations_research::MPSolver* s)
{
    solver = s;
    active = total = 0;
}

void ConstraintManager::reset(operations_research::MPVariable** var, int card)
{
    for (int i = 0; i < active; i++)
    {
        for (int j = 0; j < card; j++)
        {
            constraints[i]->SetCoefficient(var[j], 0.0);
        }
        constraints[i]->SetBounds(-1.0, 1.0);
    }
    active = 0;
}

operations_research::MPConstraint* ConstraintManager::newTmpConstraint(double lb, double ub)
{
    if (active < total)
    {
        operations_research::MPConstraint* ccc = constraints[active++];
        ccc->SetBounds(lb, ub);
        return ccc;
    }
    operations_research::MPConstraint* c = solver->MakeRowConstraint(lb, ub);
    constraints.push_back(c);
    total++;
    active++;
    return c;
}

void printsolver(
    operations_research::MPSolver* solver, operations_research::MPVariable** orplace, int cardp,
    operations_research::MPVariable** ortransition, int cardt
)
{
    RT::log("LP PROBLEM");
    RT::log("==========");
    RT::log("OBJECTIVE");
    const operations_research::MPObjective& objective = solver->Objective();
    if (objective.minimization())
    {
        RT::log("MINIMIZE ");
    }
    else
    {
        RT::log("MAXIMIZE");
    }
    for (int i = 0; i < cardp; i++)
    {
        double k = objective.GetCoefficient(orplace[i]);
        if (k != 0.0)
        {
            RT::log("+ {} * {} ", k, orplace[i]->name());
        }
    }
    for (int i = 0; i < cardt; i++)
    {
        double k = objective.GetCoefficient(ortransition[i]);
        if (k != 0.0)
        {
            RT::log("+ {} * {} ", k, ortransition[i]->name());
        }
    }
    int card = solver->NumConstraints();
    for (int a = 0; a < card; a++)
    {
        RT::log("CONSTRAINT");
        operations_research::MPConstraint* ccc = solver->constraint(a);
        RT::log("{} <= ", ccc->lb());
        for (int i = 0; i < cardp; i++)
        {
            double k = ccc->GetCoefficient(orplace[i]);
            if (k != 0.0)
            {
                RT::log("+ {} * {}", k, orplace[i]->name());
            }
        }
        for (int i = 0; i < cardt; i++)
        {
            double k = ccc->GetCoefficient(ortransition[i]);
            if (k != 0.0)
            {
                RT::log("+ {} * {}", k, ortransition[i]->name());
            }
        }
        RT::log(" <= {} ", ccc->ub());
    }
}
