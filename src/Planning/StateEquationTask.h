/****************************************************************************
This file is part of LoLA.

LoLA is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
more details.

You should have received a copy of the GNU Affero General Public License
along with LoLA. If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

#pragma once
#include <Core/Dimensions.h>
#include <Witness/Path.h>
#include <Planning/Task.h>
#include <Planning/Task.h>
#include <Frontend/Parser/ast-system-k.h>
#include <Frontend/Parser/ast-system-rk.h>
#include <Frontend/Parser/ast-system-unpk.h>

#define EQ 1
#define LE 2
#define GE 3


class ConstraintManager;

class StateEquationTask : public Task
{
public:
    static int id;
    int myid;
    kc::tFormula myFormula;
    bool finished;
    ternary_t result;
    StateEquationTask(Petrinet* n, int par, tFormula f, int fid);
    ~StateEquationTask();

    /// get result from Sara
    virtual ternary_t getResult();

    /// interprete and display the result
    virtual char* interpreteResult(ternary_t r);

    /// return the witness path
    virtual Path getWitnessPath();

    /// return the target marking
    virtual capacity_t* getMarking();

    /// return the statistics of
    virtual statusrecord* getStatistics();

    /// return the status of the analysis every 5 seconds
    statusrecord* getStatus();

    static void buildTask(Petrinet*, int, tFormula, int);
    static pthread_mutex_t stateequation_mutex;
    static void* safetystateequationthread(void*);
    static void* quasilivenessstateequationthread(void*);
    static void* constantplacestateequationthread(void*);
    static ternary_t getSafetyResult(Petrinet *, ConstraintManager *, operations_research::MPVariable **, operations_research::MPVariable **, arrayindex_t);
    static void getQuasilivenessResult(Petrinet *, ConstraintManager *, operations_research::MPVariable **, operations_research::MPVariable **);
    static void getConstantPlaceResult(Petrinet *, ConstraintManager *, operations_research::MPVariable **, operations_research::MPVariable **);
    void derail();
    Task* copy();

static int steqfirelist(Petrinet * n, int * parikh, NetState * ns, int * stubstack, int * stubtarjan, bool * onstubtarjan, bool * visited, int ** mustbeincluded, int * currentindex, int * dfs, int * lowlink, int ** result); // produces firelist for realizability check
static bool realizable(Petrinet * n, int * parikh); // checks if parikh vector is realizable
static ternary_t solveLP(Petrinet * net, operations_research::MPSolver * solver, operations_research::MPVariable ** orplace, operations_research::MPVariable ** ortransition);

private:
    int saraIsRunning;
};

// A ConstraintManager is created relative to an OR solver.
// Its task is to manage constraints that are temporarily added
// to the OR problem. A typical example:
// If we check whether a state predicate in DNF can be realized
// using the state equation, we add the constraints of the 
// state equation permanently, and the constraints for the
// conjunctions in the DNF temporarily. This way, we do not need
// to fill in the state equation again and again.

class ConstraintManager
{
public:
	ConstraintManager(operations_research::MPSolver *); // constructor
	operations_research::MPSolver * solver; // the solver to be handled
	std::vector<operations_research::MPConstraint *> constraints; // the temporary constraints
	void reset(operations_research::MPVariable ** var, int card); // void all temporary constraints: var: array of variables to be voided, card: size of array var
	operations_research::MPConstraint * newTmpConstraint(double lb, double ub); // deliver fresh constraint

	int active; // number of existing constraints
	int total; // number of managed constraints
};


