/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

#include <Frontend/Parser/ast-system-k.h>
#include <Frontend/Parser/ast-system-rk.h>
#include <Frontend/Parser/ast-system-unpk.h>
#include <CoverGraph/CoverPayload.h>
#include <Symmetry/Constraints.h>
#include <SweepLine/Sweep.h>
#include <Core/Dimensions.h>
#include <Core/Runtime.h>
#include <Core/Handlers.h>
#include <Witness/Path.h>
#include <Planning/Task.h>
#include <Planning/StoreCreator.h>
#include <Planning/ReachabilitySearchTask.h>
#include <Exploration/StatePredicateProperty.h>
#include <Exploration/FirelistStubbornStatePredicate.h>
#include <Exploration/DFSExploration.h>
#include <Exploration/ReachabilityExplorationRelaxed.h>
#include <Exploration/FirelistStubbornReachabilityRelaxed.h>
#include <Exploration/ParallelExploration.h>
#include <Formula/StatePredicate/MagicNumber.h>
#include <Formula/StatePredicate/AtomicBooleanPredicate.h>
#include <Formula/StatePredicate/AtomicStatePredicate.h>

/*!
\brief the verification task

This class wraps the reachability check by statespace exploration

*/

ReachabilitySearchTask::ReachabilitySearchTask(Petrinet* n, int par, tFormula f, int fid)
{
    preprocessingfinished = false;
    tandem = false;
    net = n;
    parent = par;
    formula = f;
    formula_id = fid;
    result = TERNARY_UNKNOWN;
    memory = new Mara();
    taskname = deconst("state space");
    portfolio_id = portfoliomanager::addTask(
        n, this, par, fid, FORMULA_REACHABLE, EXCL_MEM_TASK, memory
    );
    tokenthreshold = 0;
    goStatus = false;
    // extract state predicate from formula
    // check is also used
    kc::tFormula TheFormulaRS = formula;

    pthread_mutex_lock(&kimwitu_mutex);
    TheFormulaRS = TheFormulaRS->rewrite(kc::singletemporal);
    TheFormulaRS = TheFormulaRS->rewrite(kc::simpleneg);
    TheFormulaRS = TheFormulaRS->rewrite(kc::booleanlists);

    Petrinet::InitialNet = net;
    unparsed.clear();
    TheFormulaRS->unparse(myprinter, kc::internal);
    spFormula = TheFormulaRS->formula->copy(NULL);
    pthread_mutex_unlock(&kimwitu_mutex);
}
ReachabilitySearchTask::ReachabilitySearchTask(Petrinet* n, int par, tFormula f, int fid, bool fi)
{
    preprocessingfinished = false;
    tandem = true;
    forceinsertion = fi;
    net = n;
    parent = par;
    formula = f;
    formula_id = fid;
    result = TERNARY_UNKNOWN;
    memory = new Mara();
    tokenthreshold = 0;
    if (forceinsertion)
    {
        taskname = deconst("tandem / insertion");
        portfolio_id = portfoliomanager::addTask(
            n, this, par, fid, FORMULA_REACHABLE, SEARCH_TASK, memory
        );
        relaxed = false;
        portfoliomanager::taskjson[portfolio_id]["taskanme"] = "tandem / insertion";
    }
    else
    {
        taskname = deconst("tandem  / relaxed");
        portfolio_id = portfoliomanager::addTask(
            n, this, par, fid, FORMULA_REACHABLE, EXCL_MEM_TASK, memory
        );
        relaxed = true;
        portfoliomanager::taskjson[portfolio_id]["taskanme"] = "tandem / relaxed";
    }
    goStatus = false;
    // extract state predicate from formula
    // copy formula for additional rewrite to avoid conflict if stateequation
    // check is also used
    kc::tFormula TheFormulaRS;
    pthread_mutex_lock(&kimwitu_mutex);
    TheFormulaRS = formula;
    TheFormulaRS = TheFormulaRS->rewrite(kc::singletemporal);
    TheFormulaRS = TheFormulaRS->rewrite(kc::simpleneg);
    TheFormulaRS = TheFormulaRS->rewrite(kc::booleanlists);

    Petrinet::InitialNet = net;
    unparsed.clear();
    TheFormulaRS->unparse(myprinter, kc::internal);
    spFormula = TheFormulaRS->formula->copy(NULL);
    pthread_mutex_unlock(&kimwitu_mutex);
}

ReachabilitySearchTask::~ReachabilitySearchTask()
{
#ifndef USE_PERFORMANCE
    delete ns;
    delete store;
    delete covStore;
    delete p;
    delete sweep_store;
    // delete spFormula;
#endif
}

ternary_t ReachabilitySearchTask::getResult()
{
    // compute symmetries
    if (RT::args.symmetry_given && RT::args.search_arg != search_arg_sweepline && !tokenthreshold)
    {
        SymmetryCalculator* SC = new SymmetryCalculator(net, spFormula);
        assert(SC);
        SC->ComputeSymmetries(portfolio_id);
        delete SC;
    }
    number_of_threads = 1;
    if (tandem)
    {
        if (relaxed)
        {
            // choose a store
            store1 = StoreCreator::createStore<statenumber_t>(net, memory, number_of_threads);
            store = nullptr;
            covStore = nullptr;
            sweep_store = nullptr;
        }
        else
        {
            switch (RT::args.search_arg)
            {
            case search_arg_sweepline:
            {
                assert(false);  // not supported in tandem search
                break;
            }
            case search_arg_covergraph:
            {
                assert(false);  // not supported in tandem search
                break;
            }
            case search_arg_depth:
            {
                // choose a store
                store = StoreCreator::createStore<void>(net, memory, number_of_threads);
                covStore = nullptr;
                store1 = nullptr;
                sweep_store = nullptr;
                break;
            }
            case search__NULL:
                assert(false);
            default:;
            }
        }

        // choose a simple property
        p = new StatePredicateProperty(net, spFormula);
        if (forceinsertion)
        {
            portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn"] =
                "reachability preserving/insertion";
            fl = new FirelistStubbornStatePredicate(net, spFormula);
        }
        else
        {
            portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn"] =
                "relaxed reachability preserving/deletion";
            bool* visible = new bool[net->Card[TR]];
            memset(visible, 0, net->Card[TR] * sizeof(bool));
            p->predicate->setNondestroying(visible);
            int vis = 0;
            for (arrayindex_t i = 0; i < net->Card[TR]; i++)
            {
                if (visible[i])
                    vis++;
            }
            portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn visible"] = vis;
            fl = new FirelistStubbornReachabilityRelaxed(net, spFormula);
            fl->visible = visible;
        }

        // set the correct exploration algorithm
        if (relaxed)
        {
            exploration1 = new ReachabilityExplorationRelaxed(net);
        }
        else
        {
            exploration = new DFSExploration(net, memory);
        }
    }
    else
    {
        if (RT::args.relaxed_given)
        {
            portfoliomanager::taskjson[portfolio_id]["taskanme"] = "state space";
            relaxed = true;

            // choose a store
            store1 = StoreCreator::createStore<statenumber_t>(net, memory, number_of_threads);
            store = nullptr;
            covStore = nullptr;
            sweep_store = nullptr;
        }
        else
        {
            relaxed = false;
            switch (RT::args.search_arg)
            {
            case search_arg_sweepline:
            {
                // dummy store for the sweepline method, only counts markings and calls
                portfoliomanager::taskjson[portfolio_id]["configuration"]["method"] = "sweepline";
                sweep_store = new SweepEmptyStore();
                covStore = nullptr;
                store1 = nullptr;
                store = nullptr;
                break;
            }
            case search_arg_covergraph:
            {
                portfoliomanager::taskjson[portfolio_id]["configuration"]["method"] = "cover";

                if (RT::args.encoder_arg != encoder_arg_fullcopy)
                {
                    RT::log<warn>("state space: encoder does not fully support coverability graphs"
                    );
                }
                covStore = StoreCreator::createStore<CoverPayload>(net, memory, number_of_threads);
                store = nullptr;
                store1 = nullptr;
                sweep_store = nullptr;
                break;
            }
            case search_arg_depth:
            {
                portfoliomanager::taskjson[portfolio_id]["configuration"]["method"] = "dfs";

                // choose a store
                store = StoreCreator::createStore<void>(net, memory, number_of_threads);
                covStore = nullptr;
                store1 = nullptr;
                sweep_store = nullptr;
                break;
            }
            case search__NULL:
                assert(false);
            default:;
            }
        }

        // choose a simple property
        p = new StatePredicateProperty(net, spFormula);

        // prepare stubborn set method
        if (RT::args.stubborn_arg != stubborn_arg_off)
        {
            if (RT::args.relaxed_given)
            {
                bool* visible = new bool[net->Card[TR]];
                memset(visible, 0, net->Card[TR] * sizeof(bool));
                p->predicate->setNondestroying(visible);
                int vis = 0;
                for (arrayindex_t i = 0; i < net->Card[TR]; i++)
                {
                    if (visible[i])
                        vis++;
                }
                portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn visible"] = vis;

                if (RT::args.stubborn_arg != stubborn_arg_tarjan)
                {
                    portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn"] =
                        "relaxed reachability preserving/insertion";
                    fl = new FirelistStubbornReachabilityRelaxed(net, spFormula);
                }
                else
                {
                    portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn"] =
                        "relaxed reachability preserving/deletion";
                    fl = new FirelistStubbornReachabilityRelaxed(net, spFormula);
                }
                fl->visible = visible;
            }
            else
            {
                portfoliomanager::taskjson[portfolio_id]["configuration"]["stubborn"] =
                    "reachability preserving/insertion";
                fl = new FirelistStubbornStatePredicate(net, spFormula);
            }
        }
        else
        {
            portfoliomanager::taskjson[portfolio_id]["cwconfiguration"]["stubborn"] = "no";
            fl = new Firelist(net);
        }

        // set the correct exploration algorithm
        portfoliomanager::taskjson[portfolio_id]["configuration"]["threads"] = number_of_threads;
        if (RT::args.relaxed_given)
        {
            exploration1 = new ReachabilityExplorationRelaxed(net);
        }
        else
        {
            if (number_of_threads == 1)
            {
                exploration = new DFSExploration(net, memory);
            }
            else
            {
                exploration = new ParallelExploration(net, memory);
            }
        }
    }
    ns = NetState::createNetStateFromInitial(net);
    try
    {
        // Apply specific modification in case we are a tokenquickcheck

        if (RT::args.tokenquickcheck_arg != tokenquickcheck_arg_off && tokenthreshold != 0)
        {
            if (RT::args.tokenquickcheck_arg == tokenquickcheck_arg_mono)
            {
                if (!spFormula->monotonous())
                {
                    portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
                    return TERNARY_UNKNOWN;
                }
            }
            int* newm0 = new int[net->Card[PL]];
            bool change_made = false;
            switch (RT::args.tokenthresholdbase_arg)
            {
            case tokenthresholdbase_arg_one:
                for (int i = 0; i < net->Card[PL]; i++)
                {
                    if (ns->Current[i])
                    {
                        newm0[i] = RT::args.tokenthresholdfactor_arg / 100
                            + RT::args.tokenthresholdsummand_arg;
                        if (newm0[i] <= 0)
                            newm0[i] = 1;
                        if (newm0[i] > ns->Current[i])
                            newm0[i] = ns->Current[i];
                    }
                    else
                    {
                        newm0[i] = 0;
                    }
                    if (ns->Current[i] != newm0[i])
                        change_made = true;
                }
                break;
            case tokenthresholdbase_arg_all:
                for (int i = 0; i < net->Card[PL]; i++)
                {
                    if (ns->Current[i])
                    {
                        newm0[i] = ns->Current[i] * RT::args.tokenthresholdfactor_arg / 100
                            + RT::args.tokenthresholdsummand_arg;
                        if (newm0[i] <= 0)
                            newm0[i] = 1;
                        if (newm0[i] > ns->Current[i])
                            newm0[i] = ns->Current[i];
                    }
                    else
                    {
                        newm0[i] = 0;
                    }
                    if (ns->Current[i] != newm0[i])
                        change_made = true;
                }
                break;
            case tokenthresholdbase_arg_largestconstant:
            {
                int cutoff = spFormula->getLargestConstant();
                for (arrayindex_t p = 0; p < net->Card[PL]; p++)
                {
                    for (int i = 0; i < net->CardArcs[PL][PRE][p]; i++)
                    {
                        int result1 = net->Mult[PL][PRE][p][i];
                        if (result1 > cutoff)
                            cutoff = result1;
                    }
                    for (int i = 0; i < net->CardArcs[PL][POST][p]; i++)
                    {
                        int result1 = net->Mult[PL][POST][p][i];
                        if (result1 > cutoff)
                            cutoff = result1;
                    }
                }
                for (int i = 0; i < net->Card[PL]; i++)
                {
                    if (ns->Current[i])
                    {
                        newm0[i] = cutoff * RT::args.tokenthresholdfactor_arg / 100
                            + RT::args.tokenthresholdsummand_arg;
                        if (newm0[i] <= 0)
                            newm0[i] = 1;
                        if (newm0[i] > ns->Current[i])
                            newm0[i] = ns->Current[i];
                    }
                    else
                    {
                        newm0[i] = 0;
                    }
                    if (ns->Current[i] != newm0[i])
                        change_made = true;
                }
                break;
            }
            default:
                break;
            }
            if (!change_made)
            {
                portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
                return TERNARY_UNKNOWN;
            }
            if (RT::args.tokenquickcheck_arg == tokenquickcheck_arg_shift)
            {
                if (!spFormula->shiftable())
                {
                    portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
                    return TERNARY_UNKNOWN;
                }
                spFormula->exec_shift(newm0, ns->Current);
            }
            // adjust netstate
            ns->HashCurrent = 0;
            for (int i = 0; i < net->Card[PL]; i++)
            {
                if (ns->Current[i] > newm0[i])
                {
                    ns->Current[i] = newm0[i];
                }
                ns->HashCurrent += ns->Current[i] * net->Hash[i];
                ns->HashCurrent %= SIZEOF_MARKINGTABLE;
            }
            for (int i = 0; i < net->Card[TR]; i++)
            {
                net->checkEnabled(*ns, i);
            }
            spFormula->evaluate(*ns);
        }

        bool bool_result(false);
        ternary_t result(TERNARY_FALSE);

        goStatus = true;  // start reporting progress

        if (relaxed)
        {
            bool_result = exploration1->depth_first(*p, *ns, *store1, *fl, number_of_threads);
        }
        else
        {
            switch (RT::args.search_arg)
            {
            case search_arg_depth:
                bool_result = exploration->depth_first(*p, *ns, *store, *fl, number_of_threads);
                break;

            case search_arg_sweepline:
                // no choice of stores for sweepline method here
                bool_result = exploration->sweepline(
                    *p, *ns, *sweep_store, *fl, RT::args.sweepfronts_arg, number_of_threads, memory
                );
                break;

            case search_arg_covergraph:
                result = exploration->cover_breadth_first(
                    *p, *ns, *covStore, *fl, number_of_threads, FORMULA_REACHABLE
                );
                break;

            default:
                assert(false);
            }
        }

        // temporary result transfer, as long as the variable bool_result is needed
        if (bool_result)
        {
            result = TERNARY_TRUE;
        }
        if (tokenthreshold && (result == TERNARY_FALSE))
            result = TERNARY_UNKNOWN;
        if (RT::args.store_arg == store_arg_bloom)
        {
            if (result == TERNARY_FALSE)
            {
                result = TERNARY_UNKNOWN;
            }
        }
        portfoliomanager::report(portfolio_id, result);
        return result;
    }
    catch (quickcheckexception)
    {
        portfoliomanager::report(portfolio_id, TERNARY_UNKNOWN);
        return TERNARY_UNKNOWN;
    }
}

char* ReachabilitySearchTask::interpreteResult(ternary_t result)
{
    switch (result)
    {
    case TERNARY_TRUE:
        return deconst("The predicate is reachable.");
    case TERNARY_FALSE:
        return deconst("The predicate is unreachable.");
    case TERNARY_UNKNOWN:
        return deconst("The predicate may or may not be reachable.");
    default:
        assert(false);
    }
    return NULL;
}

Path ReachabilitySearchTask::getWitnessPath()
{
    if (relaxed)
        return *(new Path(net));
    if (RT::args.search_arg == search_arg_covergraph)
    {
        // cover graph
        return exploration->path();
    }
    else
    {
        // simple property
        return p->path();
    }
}

capacity_t* ReachabilitySearchTask::getMarking()
{
    // we only provide witness states for simple properties where we found
    // a result
    if (p and p->value)
    {
        return ns->Current;
    }
    else
    {
        return NULL;
    }
}

statusrecord* ReachabilitySearchTask::getStatistics()
{
    statusrecord* result = new statusrecord();
    if (store)
    {
        result->markings = store->get_number_of_markings();
    }
    else if (store1)
    {
        result->markings = store1->get_number_of_markings();
    }
    else if (covStore)
    {
        result->markings = covStore->get_number_of_markings();
    }
    else if (sweep_store)
    {
        result->markings = sweep_store->get_number_of_markings();
    }
    else
    {
        assert(false);
    }
    if (store)
    {
        result->calls = store->get_number_of_calls();
    }
    else if (store1)
    {
        result->calls = store1->get_number_of_calls();
    }
    else if (covStore)
    {
        result->calls = covStore->get_number_of_calls();
    }
    else if (sweep_store)
    {
        result->calls = sweep_store->get_number_of_calls();
    }
    else
    {
        assert(false);
    }
    return result;
}

void ReachabilitySearchTask::buildTask(Petrinet* n, int par, tFormula f, int fid)
{
    Task* result;
    if (RT::args.stubborn_arg != stubborn_arg_both
        && RT::args.tokenquickcheck_arg == tokenquickcheck_arg_off)
    {
        ReachabilitySearchTask* R = new ReachabilitySearchTask(n, par, f, fid);
        R->preprocessingfinished = true;
        return;
    }
    f->id = NULL;
    tFormula ff = portfoliomanager::copyFormula(f);
    tFormula fff = portfoliomanager::copyFormula(f);
    if (RT::args.stubborn_arg == stubborn_arg_both)
    {
        int agg = portfoliomanager::addTask(n, NULL, par, fid, FORMULA_REACHABLE, AGGREGATE_TASK);
        f->id = NULL;
        tFormula ff = portfoliomanager::copyFormula(f);
        tFormula fff = portfoliomanager::copyFormula(f);
        int f1 = portfoliomanager::addFormula(f);
        int f2 = portfoliomanager::addFormula(ff);
        ReachabilitySearchTask* R1 = new ReachabilitySearchTask(n, agg, f, f1, true);
        ReachabilitySearchTask* R2 = new ReachabilitySearchTask(n, agg, ff, f2, false);
        R1->preprocessingfinished = true;
        R2->preprocessingfinished = true;
    }
    else
    {
        ReachabilitySearchTask* R = new ReachabilitySearchTask(n, par, f, fid);
        R->preprocessingfinished = true;
    }
    if (RT::args.tokenquickcheck_arg != tokenquickcheck_arg_off)
    {
        int f3 = portfoliomanager::addFormula(fff);
        ReachabilitySearchTask* tqc = new ReachabilitySearchTask(n, par, fff, f3, true);
        tqc->tokenthreshold = 1;
        tqc->taskname = deconst("search / frozen tokens");
        tqc->preprocessingfinished = true;
    }
}

statusrecord* ReachabilitySearchTask::getStatus()
{
    if (!goStatus)
    {
        return NULL;  // do not report anything since symmetry calculation is still under way
    }
    statusrecord* result = new statusrecord();
    switch (RT::args.search_arg)
    {
    case search_arg_covergraph:
    {
        result->markings = covStore->get_number_of_markings();
        result->calls = covStore->get_number_of_calls();
        break;
    }
    case search_arg_sweepline:
        exploration->sweepline_get_status(result);
        break;
    default:
    {
        if (relaxed)
        {
            result->markings = store1->get_number_of_markings();
            result->calls = store1->get_number_of_calls();
        }
        else
        {
            result->markings = store->get_number_of_markings();
            result->calls = store->get_number_of_calls();
        }
    }
    }
    return result;
}

void ReachabilitySearchTask::derail() { memory->stop = true; }

Task* ReachabilitySearchTask::copy()
{
    return new ReachabilitySearchTask(
        net, parent, portfoliomanager::copyFormula(formula), formula_id
    );
}
