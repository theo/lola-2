/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Harro
\status new

\brief Store for transient markings

Each front in the SweepLine method needs one array of stores for transient markings.
Since markings need to be retrieved from this array, the only possible encoding
for this array is the FullCopyEncoder. Multithreading is disregarded as at most
one thread can access one store at any time.
*/

#pragma once

#include <Core/Dimensions.h>
#include <Stores/StoreWrapper/HashingWrapperStore.h>
#include <Stores/VectorStores/PrefixTreeStore.h>
#include <Stores/VectorStores/VSTLStore.h>
#include <Stores/NetStateEncoder/FullCopyEncoder.h>
#include <SweepLine/SweepListStore.h>
#include <Memory/Mara.h>
#include <Net/Petrinet.h>

/*!
\brief Store for transient markings of the SweepLine method

Each front in the SweepLine method needs one array of stores for transient markings.
Since markings need to be retrieved from this array, the only possible encoding
for this array is the FullCopyEncoder.
*/
template <class Payload, class UnderlyingStore, class... StoreArgs>
class SweepRingStore
{
public:
    /// constructor with size of the store and maximal positive progress
    SweepRingStore(
        arrayindex_t _size, arrayindex_t _front_offset, arrayindex_t _transient_offset,
        FullCopyEncoder* fce, NetStateEncoder* nse, StoreArgs... args
    )
        : size(_size), front_offset(_front_offset), active(size - front_offset - 1),
          transient_offset(_transient_offset), fullencoder(fce), sigpencoder(nse)
    {
        assert(size > front_offset);
        // obtain space for counters
        count = new int[size]();
        // obtain store ring for transient states
        store = new UnderlyingStore*[size];
        for (arrayindex_t i = 0; i < size; ++i)
        {
            store[i] = new UnderlyingStore(args...);
        }
        // obtain swap space for current progress measure
        samevalue = new UnderlyingStore(args...);
        // obtain space for pointers to old and new persistent states
        newpersistent = new SweepListStore<Payload, UnderlyingStore, StoreArgs...>*[size]();
        oldpersistent = new SweepListStore<Payload, UnderlyingStore, StoreArgs...>*[size]();
        deleted_store_size = 0;
        store_arguments = std::forward_as_tuple(std::forward<StoreArgs>(args)...);
        // this array has nr_of_threads elements. This parameter is passed through the
        // parameter pack for convenience. There, it's always the last element, so we
        // access it through that
        inserted_persistent =
            new bool[std::get<std::tuple_size_v<std::tuple<StoreArgs...>> - 1>(store_arguments)];
    };
    /// destructor
    ~SweepRingStore()
    {
        for (arrayindex_t i = 0; i < size; ++i)
        {
            delete store[i];
        }
        delete[] store;
        delete[] count;
        delete samevalue;
        delete[] newpersistent;
        delete[] oldpersistent;
        delete[] inserted_persistent;
    };

    /// check if a state is in the store at progress offset, insert it if not
    bool searchAndInsert(NetState& ns, int offset, Payload** payload, arrayindex_t thread);
    /// get a state at active progress and relocate it to swap or permanent storage
    bool popState(NetState& ns, arrayindex_t thread);
    /// advance the active progress by one
    bool advanceProgress();
    /// initialise the store with the persistent states
    void init(
        SweepListStore<Payload, UnderlyingStore, StoreArgs...>* oldpstates,
        SweepListStore<Payload, UnderlyingStore, StoreArgs...>* newpstates
    );
    /// delete all transient states and check if there aren't new persistent ones at all
    bool clear();
    /*!
     * \brief check if there are new persistent states that have minimal progress measure
     * with respect to the ring store Minimal progress on the ring means that this store
     * of new persistent states will be unlinked from the ring store by the next
     * advanceProgress().
     * \return If a new persistent state was found in the minimal progress bucket of the
     * ring store
     */
    bool checkNewPersistent()
    {
        return !(newpersistent[(active + front_offset + 1) % size]->store->empty());
    };
    /// check for a new persistent state in buckets with the lowest progress values
    int checkNewPersistent(int);
    /*!
     * \brief check if the last inserted state has been new persistent. Only valid
     * directly after a successfull insert by searchAndInsert()
     * \return True if it is new persistent state
     */
    bool insertedIsNewPersistent(arrayindex_t thread) { return inserted_persistent[thread]; };
    /*!
     * \brief check if the last inserted state has been new persistent. Only valid
     * directly after a successfull insert by searchAndInsert()
     * \return True if it is new persistent state
     */
    int getNumberOfDeletedStates() { return deleted_store_size; };

private:
    /// size of the store ring for transient states
    arrayindex_t size;
    /// maximal progress of a single transition
    arrayindex_t front_offset;
    /// store element for the current progress value
    arrayindex_t active;
    /// progress offset at which transient state are forgotten
    arrayindex_t transient_offset;
    /// size of the last deleted store bucket
    int deleted_store_size;
    /// encoder for all transient and new persistent states
    FullCopyEncoder* fullencoder;
    /// encoder for old persistent states
    NetStateEncoder* sigpencoder;
    /// state counter for transient store
    int* count;
    /// ring of stores for transient states
    UnderlyingStore** store;
    /// swap space for states with computed successors
    UnderlyingStore* samevalue;
    /// connectors from the store ring to the stores of old persistent states with the
    /// same progress values
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>** oldpersistent;
    /// connectors from the store ring to the stores of new persistent states with the
    /// same progress values
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>** newpersistent;
    /// flag indicating if the list of new persistent states is empty
    bool new_persistent_empty;
    /// flag indicating whether the last inserted state is persistent or transient
    bool* inserted_persistent;

    std::tuple<StoreArgs...> store_arguments;
};

#include <SweepLine/SweepRingStore.inc>
