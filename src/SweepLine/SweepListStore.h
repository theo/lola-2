/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\author Harro
\status new

\brief List of Stores for persistent markings in the SweepLine method

This class realizes one store in a list of stores, sorted by progress
measure and containing known persistent markings, either old or new ones.
*/

#pragma once

#include <Net/Petrinet.h>

#include <tuple>
#include <utility>

/*!
\brief List of Stores for persistent markings in the SweepLine method

This class realizes one store in a list of stores, sorted by progress
measure and containing known persistent markings, either old or new ones.
*/
template <typename Payload, typename UnderlyingStore, typename... StoreArgs>
class SweepListStore
{
public:
    /// constructor for a single element of a list
    SweepListStore(StoreArgs... args) : next(nullptr)
    {
        store = new UnderlyingStore(args...);
        store_arguments = std::forward_as_tuple(std::forward<StoreArgs>(args)...);
    };

    /// constructor for a list of stores of given length
    SweepListStore(arrayindex_t size, StoreArgs... args)
        : next(size > 0 ? new SweepListStore(size - 1, args...) : nullptr)
    {
        store = new UnderlyingStore(args...);
        store_arguments = std::forward_as_tuple(std::forward<StoreArgs>(args)...);
    };

    /// destructor
    ~SweepListStore()
    {
        delete store;
        delete next;
    };

    /// check if there is a next store in the list and return it (or NULL)
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>* checkNext() { return next; };

    /// check for and possibly create a next store in the list
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>* getNext()
    {
        if (!next)
        {
            next = std::apply(
                [](StoreArgs... args)
                {
                    return new SweepListStore<Payload, UnderlyingStore, StoreArgs...>(
                        std::forward<StoreArgs>(args)...
                    );
                },
                this->store_arguments
            );
        }
        return next;
    };

    /// set the next store in the list
    void setNext(SweepListStore<Payload, UnderlyingStore, StoreArgs...>* sls) { next = sls; };
    /// the actual store for this list element
    UnderlyingStore* store;

private:
    /// the next store in the list
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>* next;

    std::tuple<StoreArgs...> store_arguments;
};
