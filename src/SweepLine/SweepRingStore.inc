/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*
\file
\author Harro
\status new

\brief Stores for transient markings (analogue to PluginStore)

This store needs additional Lists of stores for new and old persistent
markings. The lists must have an initial size of at least the size of the store
itself
*/

#include <SweepLine/SweepRingStore.h>

/*!
 * \brief Search for a given state with a given progress measure, insert it if not stored yet
 * \param ns The state to search for
 * \param offset The progress offset of the state ns in relation to the current progress measure in
 * this store \param payload Dummy at this time; additional information for this state could be
 * stored \param thread The active thread number \return If the state was found in the store (and
 * thus not inserted)
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
bool SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::searchAndInsert(
    NetState& ns, int offset, Payload** payload, arrayindex_t thread
)
{
    // calculate the index on our store ring where the state could be
    // cout << "size=" << size << " active=" << active << endl;
    arrayindex_t pos((active + size + offset) % size);
    // check old persistent markings first, use encoder selected by command line
    bitarrayindex_t bitlen;
    unsigned int* data(sigpencoder->encodeState(ns, bitlen, thread));
    // basically check if the UnderlyingStore is a hashing store
    if constexpr (handles_hash<UnderlyingStore, Payload>)
    {
        // preemptively choose the bucket, this is cleaner than using both
        // signatures for searchAndInsert (hashing vs. non-hashing)
        // TODO make this function a friend of HashingWrapperStore<...>, so that buckets
        // can be private again

        // since we know that UnderlyingStore is of hashing type, we find that nr_of_buckets
        // is the second to last parameter in StoreArgs. Therefore we determine the size of
        // the tuple at compiletime (-2 for second to last) and use that to access the tuple
        hash_t hash = ns.HashCurrent
            % std::get<std::tuple_size_v<decltype(this->store_arguments)> - 2>(this->store_arguments
            );
        if (oldpersistent[pos]->store->searchAndInsert(data, bitlen, hash, payload, thread, true))
        {
            return true;
        }
        // now check other stores with FullCopyEncoder
        data = fullencoder->encodeState(ns, bitlen, thread);
        bool ret(true);
        if (offset > 0)
        {
            // future states are made transient, we will see them shortly
            // so check first if they are persistent already
            if (newpersistent[pos]->store->searchAndInsert(
                    data, bitlen, hash, payload, thread, true
                ))
            {
                return true;
            }
            // then possibly insert them into a transient store
            if (!(ret = store[pos]->searchAndInsert(data, bitlen, hash, payload, thread)))
            {
                inserted_persistent[thread] = false;
                ++(count[pos]);
            }
        }
        else if (offset < 0)
        {
            // past states must be made persistent, the next front will visit them
            // but check first if we have already seen them ourselves
            if (store[pos]->searchAndInsert(data, bitlen, hash, payload, thread, true))
            {
                return true;
            }
            // and only if not, make them new persistent
            if (!(ret = newpersistent[pos]->store->searchAndInsert(
                      data, bitlen, hash, payload, thread
                  )))
            {
                new_persistent_empty = false;
                inserted_persistent[thread] = true;
            }
        }
        else
        {
            // successor states with the same progress value must additionally check
            // the swap space before being inserted as transient states like
            // in the future case
            if (newpersistent[pos]->store->searchAndInsert(
                    data, bitlen, hash, payload, thread, true
                ))
            {
                return true;
            }
            if (samevalue->searchAndInsert(data, bitlen, hash, payload, thread, true))
            {
                return true;
            }
            if (!(ret = store[pos]->searchAndInsert(data, bitlen, hash, payload, thread)))
            {
                inserted_persistent[thread] = false;
                ++(count[pos]);
            }
        }
        return ret;
    }
    else
    {
        if (oldpersistent[pos]->store->searchAndInsert(data, bitlen, payload, thread, true))
        {
            return true;
        }
        // now check other stores with FullCopyEncoder
        data = fullencoder->encodeState(ns, bitlen, thread);
        bool ret(true);
        if (offset > 0)
        {
            // future states are made transient, we will see them shortly
            // so check first if they are persistent already
            if (newpersistent[pos]->store->searchAndInsert(data, bitlen, payload, thread, true))
            {
                return true;
            }
            // then possibly insert them into a transient store
            if (!(ret = store[pos]->searchAndInsert(data, bitlen, payload, thread)))
            {
                inserted_persistent[thread] = false;
                ++(count[pos]);
            }
        }
        else if (offset < 0)
        {
            // past states must be made persistent, the next front will visit them
            // but check first if we have already seen them ourselves
            if (store[pos]->searchAndInsert(data, bitlen, payload, thread, true))
            {
                return true;
            }
            // and only if not, make them new persistent
            if (!(ret = newpersistent[pos]->store->searchAndInsert(data, bitlen, payload, thread)))
            {
                new_persistent_empty = false;
                inserted_persistent[thread] = true;
            }
        }
        else
        {
            // successor states with the same progress value must additionally check
            // the swap space before being inserted as transient states like
            // in the future case
            if (newpersistent[pos]->store->searchAndInsert(data, bitlen, payload, thread, true))
            {
                return true;
            }
            if (samevalue->searchAndInsert(data, bitlen, payload, thread, true))
            {
                return true;
            }
            if (!(ret = store[pos]->searchAndInsert(data, bitlen, payload, thread)))
            {
                inserted_persistent[thread] = false;
                ++(count[pos]);
            }
        }
        return ret;
    }
    // cout << "inserting at offset " << offset << ", pos " << pos << ": ";
}

/*!
 * \brief Get a state with the current progress value.
 *	The state will get relocated from transient storage to the swap space or from
 *	new to old persistent storage during this operation.
 * \param ns Reserved memory for the state to be put in
 * \return If a state with current progress value has been found at all
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
bool SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::popState(
    NetState& ns, arrayindex_t thread
)
{
    // get either a transient or new persistent state
    // and transfer it to the samevalue resp. old persistent store
    bitarrayindex_t bitlen;
    unsigned int* data = nullptr;
    // check for a transient state first
    if (store[active]->popState(data, thread))
    {
        // decode, reencode, and insert into swap space
        // cout << "transient popped " << data << " " << &ns << endl;
        fullencoder->decodeState(ns, data);

        data = fullencoder->encodeState(ns, bitlen, thread);
        if constexpr (handles_hash<UnderlyingStore, Payload>)
        {
            ns.HashCurrent = store[active]->getLastHash(thread);
            samevalue->searchAndInsert(data, bitlen, ns.HashCurrent, nullptr, thread);  // need to
                                                                                        // get
                                                                                        // payload
                                                                                        // from
                                                                                        // store!!!
        }
        else
        {
            samevalue->searchAndInsert(data, bitlen, nullptr, thread);
        }
        return true;
    }

    // if no transient state is found, check for the persistent ones
    if (newpersistent[active]->store->popState(data, thread))
    {
        // decode, reencode, and insert into old persistent store
        // cout << "persistent popped " << data << endl;
        fullencoder->decodeState(ns, data);

        data = sigpencoder->encodeState(ns, bitlen, thread);
        if constexpr (handles_hash<UnderlyingStore, Payload>)
        {
            ns.HashCurrent = newpersistent[active]->store->getLastHash(thread);
            oldpersistent[active]->store->searchAndInsert(
                data, bitlen, ns.HashCurrent, nullptr,
                thread
            );  // need to get payload from store!!!
        }
        else
        {
            oldpersistent[active]->store->searchAndInsert(
                data, bitlen, nullptr, thread
            );  // need to get payload from store!!!
        }
        return true;
    }

    // no state with current progress measure found
    return false;
}

/*!
 * \brief Advance the current progress measure by one
 * \return If the progress measure could be advanced (i.e. false = this front can be terminated)
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
bool SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::advanceProgress()
{
    // find our foremost and rearmost store
    arrayindex_t front(active + front_offset);
    if (front >= size)
    {
        front -= size;
    }

    // check if all transient stores in front of the active are empty
    arrayindex_t pos(active);
    bool store_empty(true);
    while (pos != front)
    {
        if (++pos == size)
        {
            pos = 0;
        }
        // cout << "advance at front=" << front << " pos=" << pos << endl;
        if (!store[pos]->empty())
        {
            store_empty = false;
            break;
        }
    }
    // if additionally we are at the end of the list of persistent markings then we are through
    if (store_empty && !oldpersistent[front]->checkNext())
    {
        return false;
    }

    // delete the transients at the back and swap in the transients at the active position
    arrayindex_t back(
        transient_offset > active ? active + size - transient_offset : active - transient_offset
    );
    if (transient_offset > 0)
    {
        delete store[back];
        store[back] = store[active];  // empty store at this point
        store[active] = samevalue;
    }
    else
    {
        delete samevalue;
    }
    deleted_store_size = count[back];
    count[back] = 0;
    samevalue = std::apply(
        [](StoreArgs... args) { return new UnderlyingStore(std::forward<StoreArgs>(args)...); },
        this->store_arguments
    );

    // link in next persistent stores at the back (= the new front)
    arrayindex_t newfront(front + 1 < size ? front + 1 : 0);
    newpersistent[newfront] = newpersistent[front]->getNext();
    oldpersistent[newfront] = oldpersistent[front]->getNext();

    // advance on the ring
    if (++active >= size)
    {
        active = 0;
    }

    return true;
}

/*!
 * \brief Initialise the ring store with pointers to new and old persistent states and set the
 * current progress measure \param oldp Pointer to the minimum progress measure store of old
 * persistent states \param newp Pointer to the minimum progress measure store of new persistent
 * states
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
void SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::init(
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>* oldp,
    SweepListStore<Payload, UnderlyingStore, StoreArgs...>* newp
)
{
    // Link in the old and new persistent stores with the ring store
    for (arrayindex_t i = 0; i < size; ++i)
    {
        if (i)
        {
            oldp = oldp->getNext();
            newp = newp->getNext();
        }
        oldpersistent[i] = oldp;
        newpersistent[i] = newp;
    }
    // set the current progress measure, i.e. select the correct store on the ring
    active = size - front_offset - 1;
    new_persistent_empty = true;
}

/*!
 * \brief Clear the ring store of transient states
 * \return If the list of new persistent states is also empty
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
bool SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::clear()
{
    auto createStore = [](StoreArgs... args)
    { return new UnderlyingStore(std::forward<StoreArgs>(args)...); };
    // clear all non-empty stores on the ring
    for (arrayindex_t i = 0; i < size; ++i)
    {
        if (!store[i]->empty())
        {
            delete store[i];
            store[i] = std::apply(createStore, this->store_arguments);
        }
    }

    // clear the swap space (usually necessary)
    if (!samevalue->empty())
    {
        delete samevalue;
        samevalue = std::apply(createStore, this->store_arguments);
    }

    return new_persistent_empty;
}

/*!
 * \brief check if there are new persistent states in the bp oldest stores with respect to the ring
 * store \return The relative "age" of the store with a new persistent state (0 if none)
 */
template <class Payload, class UnderlyingStore, class... StoreArgs>
int SweepRingStore<Payload, UnderlyingStore, StoreArgs...>::checkNewPersistent(int bp)
{
    // the oldest store has already been checked, thus "i+2" in the following formula
    // test the second oldest one first, and then stores with increasing progress
    assert(bp >= 0);
    for (int i = 0; i < bp; ++i)
    {
        if (!(newpersistent[(active + front_offset + i + 2) % size]->store->empty()))
        {
            return bp - i;
        }
    }
    return 0;
}
