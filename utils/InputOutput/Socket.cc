/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\file
\brief implementation of class Socket
\author Niels
\status approved 25.01.2012
\ingroup g_reporting
*/

#include "Socket.h"

#ifdef WIN32
#include <winsock.h>
#include <windows.h>
#else
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif

#include <cassert>
#include <unistd.h>

#include <fmt/core.h>
#include <fmt/color.h>
#include <spdlog/spdlog.h>

namespace
{
template <typename S, typename... Args>
auto markup(const S& fmt, Args&&... fargs) -> std::string
{
    return fmt::format(
        fmt::fg(fmt::terminal_color::blue) | fmt::emphasis::underline, fmt,
        std::forward<Args>(fargs)...
    );
}

enum : uint16_t
{
    UDP_BUFFER_SIZE = 1024
};

inline socklen_t addressLength = sizeof(sockaddr_in);
}  // namespace

/*!
\param[in] port  The port to be used. Note that port numbers <1000 need root
           access.
\param[in] hostname  If the socket should be used to send messages (client)
           then this parameter should hold an a hostname or an IPv4 address in
           the standard format "xxx.xxx.xxx.xxx". It defaults to NULL (server).
\param[in] failonerror  Whether errors lead to program abortion (true).

\post The member #listening is set according to whether we are sending
      (client) or receiving messages (server).
\post The socket #sock is created. If we are receiving (server), the it is
      also bound.
*/
Socket::Socket(
    u_short port, std::shared_ptr<spdlog::logger> logger, const char* hostname, bool failonerror
)
    : sock(socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)), listening((hostname == nullptr)),
      failonerror(failonerror), logger{std::move(logger)}
{
    // LCOV_EXCL_START
    // socket fails with -1
    if (sock == -1) [[unlikely]]
    {
        logger->critical("could not initialize socket at port {}", markup("{}", port));
        if (failonerror)
        {
            panic();
        }
        else
        {
            return;
        }
    }
    // LCOV_EXCL_STOP

    // specify the address
    memset(&address, 0, addressLength);
    address.sin_family = AF_INET;
    address.sin_port = htons(port);

    if (listening)
    {
        address.sin_addr.s_addr = INADDR_ANY;

        // bind the socket sock to the address specified in address
        // LCOV_EXCL_START
        if (bind(sock, (struct sockaddr*)&address, addressLength) == -1) [[unlikely]]
        {
            close(sock);
            logger->critical("could not bind socket at port {}", markup("{}", port));
            if (failonerror)
            {
                panic();
            }
            else
            {
                return;
            }
        }
        // LCOV_EXCL_STOP
    }
    else
    {
        // resolve the hostname
        const hostent* record = gethostbyname(hostname);

        // LCOV_EXCL_START
        if (record == nullptr) [[unlikely]]
        {
            if (failonerror)
            {
                logger->critical("host '{}' is not available", markup("{}", hostname));
                panic();
            }
            else
            {
                return;
            }
        }
        // LCOV_EXCL_STOP

        const in_addr* resolved_address = reinterpret_cast<in_addr*>(record->h_addr);
        address.sin_addr.s_addr = inet_addr(inet_ntoa(*resolved_address));
    }
}

/*!
\post The socket #sock is closed.
*/
Socket::~Socket() { close(sock); }

void Socket::panic [[noreturn]] () const
{
    logger->critical(
        "{} -- aborting",
        fmt::format(fmt::fg(fmt::terminal_color::red) | fmt::emphasis::bold, "network error")
    );
    exit(EXIT_FAILURE);
}

/*!
\post No postcondition: this function never terminates.
\todo The reporter could be a parameter.
*/
void Socket::receive() const
{
    assert(listening);

    // a buffer for incoming messages
    char buffer[UDP_BUFFER_SIZE];

    while (true)
    {
        // receive data from the socket sock, stores it into buffer with
        // length UDP_BUFFER_SIZE, sets no flags, receives from address
        // specified in sa with length fromlen
        const auto recsize = recvfrom(
            sock, reinterpret_cast<void*>(buffer), UDP_BUFFER_SIZE, 0,
            const_cast<struct sockaddr*>(reinterpret_cast<const struct sockaddr*>(&address)),
            &addressLength
        );

        // LCOV_EXCL_START
        if (recsize < 0) [[unlikely]]
        {
            logger->error("could not receive message");
            if (failonerror)
            {
                panic();
            }
            else
            {
                return;
            }
        }
        // LCOV_EXCL_STOP

        // get sender IP
        char display[INET_ADDRSTRLEN] = {0};
        inet_ntop(AF_INET, &address.sin_addr.s_addr, display, INET_ADDRSTRLEN);

        logger->info("{}: {}", display, buffer);
    }  // LCOV_EXCL_LINE
}

/*!
\param[in] message  The message to be sent. We do not care about its memeory
           management; that is, we do not release its memory.

\post The given message is sent to the socket #sock. As we are using UDP
      datagrams it is not guaranteed whether the message is actually received.
*/
void Socket::send(const char* message) const
{
    const auto bytes_sent = sendto(
        sock, message, strlen(message), 0, reinterpret_cast<const struct sockaddr*>(&address),
        sizeof(sockaddr_in)
    );

    // LCOV_EXCL_START
    if (bytes_sent < 0) [[unlikely]]
    {
        logger->critical("could not send message '{}'", message);
        if (failonerror)
        {
            panic();
        }
        else
        {
            return;
        }
    }
    // LCOV_EXCL_STOP
}

/*!
\note This function is best called inside a thread.
\param[in] message  the target message to wait for
\return return the sender IP if a message is received that matches the target
        message
\post Memory for return value needs to be freed by caller.
*/
char* Socket::waitFor(const char* message) const
{
    assert(listening);

    // a buffer for incoming messages
    char buffer[UDP_BUFFER_SIZE];

    while (true)  // LCOV_EXCL_LINE
    {
        // receive data from the socket sock, stores it into buffer with
        // length UDP_BUFFER_SIZE, sets no flags, receives from address
        // specified in sa with length fromlen
        const auto recsize = recvfrom(
            sock, reinterpret_cast<void*>(buffer), UDP_BUFFER_SIZE, 0,
            const_cast<struct sockaddr*>(reinterpret_cast<const struct sockaddr*>(&address)),
            &addressLength
        );

        // LCOV_EXCL_START
        if (recsize < 0) [[unlikely]]
        {
            logger->critical("could not receive message");
            if (failonerror)
            {
                panic();
            }
            else
            {
                return nullptr;
            }
        }
        // LCOV_EXCL_STOP

        char* received = nullptr;
        const int bytes = asprintf(&received, "%.*s", static_cast<int>(recsize), buffer);
        assert(bytes != -1);
        assert(received);

        // the received message matches the target message
        if (std::string_view{received} == std::string_view{message})
        {
            // get sender IP
            char* senderaddress = new char[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &address.sin_addr.s_addr, senderaddress, INET_ADDRSTRLEN);
            return senderaddress;
        }
    }  // LCOV_EXCL_LINE
}
