/****************************************************************************
  This file is part of LoLA.

  LoLA is free software: you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  LoLA is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for
  more details.

  You should have received a copy of the GNU Affero General Public License
  along with LoLA. If not, see <http://www.gnu.org/licenses/>.
****************************************************************************/

/*!
\status approved 25.01.2012
\todo comment me
*/

#include <cstdlib>
#include <csignal>

#include <fmt/format.h>
#include <fmt/color.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "InputOutput/Socket.h"

void signal_callback_handler [[noreturn]] (int signum)
{
    fmt::print(
        "[{}] caught signal: '{}'\n", fmt::format(fmt::fg(fmt::terminal_color::magenta), "lola"),
        strsignal(signum)
    );
    exit(EXIT_SUCCESS);
}

int main(int argc, char** argv)
{
    if (argc == 2 && std::string_view{argv[1]} == "--help") [[unlikely]]
    {
        fmt::print("No help\n");
        return EXIT_SUCCESS;
    }

    if (argc == 2 && std::string_view{argv[1]} == "--version") [[unlikely]]
    {
        fmt::print("No version\n");
        return EXIT_SUCCESS;
    }

    const int port = 5555;
    std::signal(SIGTERM, signal_callback_handler);

    Socket s(
        port,
        []
        {
            auto logger = spdlog::stdout_color_mt("console");
            logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e][%^%L%$] %v");
            return logger;
        }()
    );
    fmt::print(
        "[{}] listening on port {}", fmt::format(fmt::fg(fmt::terminal_color::magenta), "lola"),
        fmt::format(fmt::fg(fmt::terminal_color::blue) | fmt::emphasis::underline, "{}", port)
    );
    s.receive();

    return EXIT_SUCCESS;  // LCOV_EXCL_LINE
}
