                           _          _        _
                          | |    ___ | |      / \
                          | |   / _ \| |     / _ \
                          | |__| (_) | |___ / ___ \
                          |_____\___/|_____/_/   \_\
                             a low-level analyzer
                      http://service-technology.org/lola

## Prerequisites

LoLA dependencies can be divided into three categories currently:

- Mandatory (need to be present beforehand)
- Required, but not mandatory (needed for LoLA, can be built alongside)
- Optional

The following tools are mandatory to compile LoLA from scratch:

- A C and C++23 compatible compiler (atleast GCC12 or Clang16)
- CMake (and a generator like ```make``` or ```ninja```)
- GNU Gengetopt
- Flex and GNU Bison (for MacOS user: commandline-tools install is too old, homebrew or self-compiled version needed)

The following tools are required, but can be built alongside LoLA:

- Google OR-Tools
- fmt
- spdlog
- yeet

Finally, when yeet does not want to be used, kimwitu++ can optionally be used instead. This causes the following optional dependencies:

- kimwitu++
- GNU sed (for MacOS user: default sed may cause problems, GNU sed needed)

## Build

Simply invoking
```
$ pwd
~/lola-2
$ cmake -Bbuild [-S.]
...
$ cmake --build build [--jobs=#]
...
$ cmake --install build [--prefix=/path/to/dir]
```

should suffice to configure and build `lola` in the folder `build` over `#` jobs and install it in `/path/to/dir/bin` (and addtional stuff from dependencies in familiar directories).

### Important things to note

- The parameter `-S` is optional, since `cmake` would infer the source directory from the `cwd`. Invoking `cmake` in another directory requires you to set `-S` to the directory of this README
- The invocations above build and install the missing dependencies in a folder `_deps` adjacent to this README and also installs them with `lola`. See [Customization](#customization) on how to influence this behaviour


### Customization

The missing, but required dependencies can be delivered to `lola` in three ways

- Using a pre-compiled version (C++)
- Downloading, building and installing before LoLA
- Downloading and building with LoLA

The first method assumes a system install or a custom install location specified through the commandline-parameter ```-DCMAKE_PREFIX_PATH=$PATHS``` or environment variable ```$DEP_ROOT```. Here, ```PATHS``` describes a semi-colon separated, double quotation mark encapsuled list of directories. A parent directory including multiple directories to search for is also supported. A single directory does not require double quotation marks or a trailing semi-colon and relative paths are supported aswell. The environment variable should point to a single directory

The second option uses the cmake subproject in ```cmake/deps```. By invoking
```
$ cmake -Bbuild -S/path/to/cmake/deps [-DBUILD_$dep=ON]...
```
the activated dependencies can be built beforehand in a separate project. By installing them to ```_deps```, the LoLA configuration will find that installation automatically.

Per default, `lola` will search for libraries first before building them when they're not found. This behaviour can be overridden by passing `-DLOLA_BUILD_DEPS=ON` (for all libraries) or `-DBUILD_$dep=ON` at configure time.

### Example configuration calls

- Dependencies downloaded to directory `dep`:
```
cmake -Bbuild [-S.] -DCMAKE_PREFIX_PATH=dep
```
or
```
cmake -Bbuild [-S.] -DCMAKE_PREFIX_PATH="dep/ortools;dep/fmt;dep/spdlog"
```
- Enforcing self-build threads:
```
cmake -Bbuild [-S.] -DBUILD_DEPS=ON
```

## Developing LoLA

If you wish to develop LoLA, there are currently some things to keep in mind.

### Contributing to remote

- To allow arbitrary ```build``` folders for CMake (for the souls that don't like calling it ```build```), the ```.gitignore``` is ignoring any file that is not explicitely allowed to be in the repo. Exceptions are files in the ```cmake```,`ext`,`gen`, ```src``` and ```utils``` folders. Adding a new folder and files (at project source) to the repository therefore requires explicitely allowing them in the ```.gitignore``` if you want to keep the repo intact. You can find the patterns to do that in the ```.gitignore```
- Contributing to LoLA STRONGLY recommends the use of ```pre-commit```. The use cannot be enforced, but I hate you if you don't.

### Contributing code

- Adding files to the ```.gitignore``` is not the only thing needed to be done though for new files, they also need to be included in the build process. For that, one must add them to the sources in the ```CMakeLists.txt``` in the ```src``` folder, similar to the existing files.
- Adding more steps to the build process, like generating files through other programs or including other self-compiling libraries is also done in the ```CMakeLists.txt``` in the `ext`,`gen` or ```src``` folders or in scripts in ```cmake/```. Examples for existing things can be found there for inspiration.

### IDE configuration

- The configuration step for LoLA always generates a ```compile_commands.json``` which can be included in your IDE configuration
- For folks working with VSCode, ```cmake/vscode.cmake``` generates a configuration ```lola``` in ```.vscode/c_cpp_properties.json``` reflecting the relevant settings for VSCode. This can be disabled through the configure option ```-DVSCODE_SUPPORT=OFF```. The configuration can be edited afterwards, only the following settings will be overridden
    - ```compilerPath```
    - ```cppStandard```
    - ```cStandard```
    - ```defines```
    - ```compile_commands```

### Debugging

- Debugging requires the commandline parameter ```-DCMAKE_BUILD_TYPE=Debug``` (where the default value would be ```Release``` instead of ```Debug```) when configuring. Building LoLA afterwards allows the use of assertions and debuggers like ```gdb``` or ```lldb```. Integrating this into VSCode also works by writing a ```launch.json``` like below for ```gdb```.
```
{
    "configurations": [
        {
            "name": "Debug (no cmake)",
            "type": "cppdbg",
            "request": "launch",
            "program": "${workspaceFolder}/$BUILD_FOLDER/lola",
            "args": ["--check=full", "--pnmlnet", ...],
            "MIMode": "gdb",
            "cwd": "${workspaceFolder}",
            "setupCommands": [
                {
                    "description": "Enable pretty-printing for gdb",
                    "text": "-enable-pretty-printing",
                    "ignoreFailures": true
                },
                {
                    "description": "Set Disassembly Flavor to Intel",
                    "text": "-gdb-set disassembly-flavor intel",
                    "ignoreFailures": true
                }
            ]
        }
    ]
}
```

### Logging

- LoLA imports ```spdlog``` for logging. For this, function templates ```log``` and ```print``` are defined in ```InputOutput/Logger.h``` in the ```RT``` namespace. Per default, ```log``` writes to ```stderr``` while ```print``` writes to ```stdout```. The non-deduced template parameter for ```log``` is the log level (one of ```trace```, ```debug```, ```info```, ```warning```, ```error``` and ```critical```, default ```info```) and for ```print``` is the verbosity (one of ```VERB::OUTPUT```, ```VERB::AUX``` and ```VERB::FULL```, default ```VERB::OUTPUT```).

- ```RT::log``` can also execute arbitrary code for more complex logging, by passing arguments where ```std::is_invocable``` is true

- Further, several functions are implemented in the ```RT``` namespace for convenience.
    - ```RT::markup``` takes an enum value (like ```MARKUP::WARNING```) and a format string to format arguments into and colors the text accordingly. If colored output is disabled (see below), this function does not do anything besides formatting the arguments into the format string
    - ```RT::abort``` takes an enum value (like ```ERROR::SYNTAX```) to output a highlighted message and abort the process
    - ```RT::indent_to``` and ```RT::indent_by``` take an integer value to set or change the indentation level of messages
    - ```RT::newline``` can be used like ```std::endl``` to insert a LoLA-formatted newline into a stringstream without flushing. Passing the template parameter ```true``` handles the newline for ```log``` calls.

- Things like file output or log-/verbosity-levels can be configured through different commandline parameters (see ```--full-help``` for details)

- For performance, logging can also be done through the ```LOLA_LOG``` macro that resolves to nothing for release builds. The loglevel is passed as the first argument and the remainder are passed to ```RT::log```. The compile-time loglevel that decides which messages resolve to nothing is controlled through the parameter ```-DLOLA_ACTIVE_LOGLEVEL``` and takes the same values like ```RT::log```.

- Finally, messages are formatted using [```libfmt```](https://github.com/fmtlib/fmt).

### Docker

We provide definitions to develop LoLA using a docker container. `.devcontainer/` holds a `Dockerfile` to build an image that includes the needed dependencies and a bunch of useful tools per default. The image is supposed to be available at `registry.git.informatik.uni-rostock.de/theo/lola-2/lola:latest`, but that registry is still misconfigured, which is why that does not work. You can simply create the image by calling

```
$ docker build -t registry.git.informatik.uni-rostock.de/theo/lola-2/lola:latest .devcontainer
```

Inside the image, the user `lola-dev` with UID:GID `1000:1000` is created. Any container with the image can be customized through the following environment variables (passed to the container):
- `USER_UID`: Update the UID of `lola-dev` to a user provided one. This is useful to match the UID and GID between the host user and the docker user on Linux. Passing `0` causes the container to be started as `root` instead of `lola-dev` MacOS and Windows should not need this.
- `USER_GID`: Update the GID of `lola-dev` to a user provided one. Assumes the value of `USER_UID` if not set.
- `USER_PASSWORD[_FILE]`: Set the password for the `lola-dev` user. The `USER_PASSWORD_FILE` variable can be used together with docker secrets to read the password from a file. `lola-dev` is part of the `sudo` group, where a password would be needed.
- `USER_SHELL`: Set the default shell for `lola-dev`. Sets the shell to the first matching entry of the value in `/etc/shells`.
- `USER_PATHS`: Comma-separated list of paths that will be `chown`'ed by `lola-dev`. This is useful when mounting volumes outside of `home/lola-dev`.

We also provide docker compose files to work with the image. `.devcontainer/docker-compose.base.yaml` includes a basic service definition for mounting the current working directory at `home/lola-dev/lola`. Also, a `.devcontainer/user.env` file can be used to set the aforementioned environment variables. Using the extension functionality of docker compose, you can write a `docker-compose.yaml` with user specific mounts and other settings. For example

```
services:
  lola:
    extends:
      file: docker-compose.base.yaml
      service: lola
    volumes:
      - lola-builds:/builds
      - lola-home:/home/lola-dev
    environment:
      - USER_PATHS=/builds

volumes:
  lola-builds:
  lola-home:
```

creates two more volumes for the container. The first mounts the home directory to preverse any settings for the shell and similar, while the other mounts a `/builds` folder for builds inside the container. The latter is useful for Docker Desktop, since bind mounts are slower there.

We also provide a rudimentary `.devcontainer` file for VSCode Dev Containers. You should provide a docker compose file for that. Any user specific changes should be kept to your `docker-compose.yaml` for proper version control.
