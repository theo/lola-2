# Some functions useful in other scripts

# convert json array in variable input_list to cmake list and write to out_var
function(json_array_to_list input_list out_var)
    string(JSON len LENGTH ${${input_list}})

    if(len GREATER 0)
        math(EXPR max_elem ${len}-1)

        foreach(i RANGE ${max_elem})
            string(JSON elem GET ${${input_list}} ${i})
            list(APPEND tmp ${elem})
        endforeach()

        set(${out_var} ${tmp} PARENT_SCOPE)
    else()
        set(${out_var} "" PARENT_SCOPE)
    endif()
endfunction(json_array_to_list)

# convert list given by var input_list to json array and write to out_var
function(list_to_json_array input_list out_var)
    list(LENGTH ${input_list} len)

    if(len GREATER 0)
        list(JOIN ${input_list} "\", \"" tmp)
        string(PREPEND tmp "[ \"")
        string(APPEND tmp "\" ]")
        set(${out_var} ${tmp} PARENT_SCOPE)
    else()
        set(${out_var} "[]" PARENT_SCOPE)
    endif()
endfunction(list_to_json_array)

# merge json array given in arr with cmake list given in list by appending list to arr,
# removing duplicates and writing to out_var
function(merge_json_array_list arr list out_var)
    json_array_to_list(${arr} arrlist)
    list(APPEND arrlist ${${list}})
    list(REMOVE_DUPLICATES arrlist)
    list_to_json_array(arrlist result)
    set(${out_var} ${result} PARENT_SCOPE)
endfunction(merge_json_array_list)

# merge json arrays given in arr1 and arr2 by appending arr2 to arr1,
# removing duplicates and writing to out_var
function(merge_json_arrays arr1 arr2 out_var)
    json_array_to_list(${arr2} list2)
    merge_json_array_list(${arr1} list2 tmp)
    set(${out_var} ${tmp} PARENT_SCOPE)
endfunction(merge_json_arrays)

# for a given list of directories (given per varname, not directly), collect all
# subdirectories and append them to the list
macro(collect_subdirs dir_list)
    foreach(dir IN LISTS ${dir_list})
        file(GLOB CHILDREN LIST_DIRECTORIES true "${dir}/*/")
        set(SUBDIRS "")

        foreach(child ${CHILDREN})
            if(IS_DIRECTORY ${child})
                list(APPEND SUBDIRS ${child})
            endif()
        endforeach(child ${CHILDREN})

        list(APPEND ${dir_list} ${SUBDIRS})
    endforeach()
endmacro()

# Search for named dependency
# signature: search_dep(<package_name> [TARGET <target_name>] [VERSION <version_number>])
function(search_dep package)
    set(args TARGET VERSION)
    cmake_parse_arguments(target "" "${args}" "" ${ARGN})

    if(target_KEYWORDS_MISSING_VALUES)
        message(WARNING "Argument(s) ${target_KEYWORDS_MISSING_VALUES} given without values")
    endif()

    if(NOT BUILD_${package})
        message(STATUS "Checking ${package}.")

        find_package(${package} ${target_VERSION} QUIET)

        if(NOT ${package}_FOUND)
            message(WARNING "Could not find ${package}. Installing from source")
            set(BUILD_${package} ON CACHE BOOL "Whether to build ${package} or not" FORCE)
        else()
            message(STATUS "Using ${package} from ${${package}_DIR}")
        endif()
    endif()
endfunction()
