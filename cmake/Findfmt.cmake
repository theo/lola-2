#[=======================================================================[.rst:
Findfmt
--------

This module determines the fmt library of the system.

IMPORTED Targets
^^^^^^^^^^^^^^^^

This module defines :prop_tgt:`IMPORTED` target ``fmt::fmt``, if
fmt has been found.

Result Variables
^^^^^^^^^^^^^^^^

This module defines the following variables:

::

fmt_FOUND          - True if fmt found.

Hints
^^^^^

A user may set ``fmt_ROOT`` to an fmt installation root to tell this
module where to look.
#]=======================================================================]

include(FindPackageHandleStandardArgs)

# first search for cmake based install
find_package(fmt QUIET CONFIG)

# if found, we're done
if(fmt_FOUND)
    find_package_handle_standard_args(fmt CONFIG_MODE)
    return()
endif()

set(fmt_FOUND FALSE)

if(NOT fmt_ROOT)
    set(fmt_ROOT $ENV{fmt_ROOT})
endif()

message(STATUS "fmt_ROOT: ${fmt_ROOT}")

if(fmt_ROOT)
    set(fmt_FOUND TRUE)
endif()

include(GNUInstallDirs)

if(fmt_FOUND AND NOT TARGET fmt::fmt)
    if(EXISTS "${fmt_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}fmt${CMAKE_SHARED_LIBRARY_SUFFIX}")
        add_library(fmt::fmt SHARED IMPORTED)

        set_target_properties(fmt::fmt PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${fmt_ROOT}/include"
            IMPORTED_LOCATION "${fmt_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}fmt${CMAKE_SHARED_LIBRARY_SUFFIX}"
            INTERFACE_COMPILE_DEFINITIONS "FMT_SHARED"
        )
    elseif(EXISTS "${fmt_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}fmt${CMAKE_STATIC_LIBRARY_SUFFIX}")
        add_library(fmt::fmt STATIC IMPORTED)

        set_target_properties(fmt::fmt PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${fmt_ROOT}/include"
            IMPORTED_LOCATION "${fmt_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}fmt${CMAKE_STATIC_LIBRARY_SUFFIX}"
        )
    else()
        message(FATAL_ERROR "fmt_ROOT: fmt library file not found")
    endif()
endif()
