#[=======================================================================[.rst:
Findyeet
--------

This module determines the yeet executable of the system.

IMPORTED Targets
^^^^^^^^^^^^^^^^

This module defines :prop_tgt:`IMPORTED` target ``yeet::yeet``, if
yeet has been found.

Result Variables
^^^^^^^^^^^^^^^^

This module defines the following variables:

::

yeet_FOUND          - True if yeet found.

Hints
^^^^^

A user may set ``yeet_ROOT`` to an yeet installation root to tell this
module where to look.
#]=======================================================================]

include(FindPackageHandleStandardArgs)

# first search for cmake based install
find_package(yeet QUIET CONFIG)

# if found, we're done
if(yeet_FOUND)
    find_package_handle_standard_args(yeet CONFIG_MODE)
    return()
endif()

set(yeet_FOUND FALSE)

if(NOT yeet_ROOT)
    set(yeet_ROOT $ENV{yeet_ROOT})
endif()

message(STATUS "yeet_ROOT: ${yeet_ROOT}")

if(yeet_ROOT)
    set(yeet_FOUND TRUE)
endif()

include(GNUInstallDirs)

if(yeet_FOUND AND NOT TARGET yeet::yeet)
    add_executable(yeet::yeet IMPORTED)

    if(EXISTS "${yeet_ROOT}/${CMAKE_INSTALL_BINDIR}/yeet")
        set_target_properties(yeet::yeet PROPERTIES IMPORTED_LOCATION "${yeet_ROOT}/${CMAKE_INSTALL_BINDIR}/yeet")
    elseif(EXISTS "${yeet_ROOT}/${CMAKE_INSTALL_BINDIR}/yeetd")
        set_target_properties(yeet::yeet PROPERTIES IMPORTED_LOCATION "${yeet_ROOT}/${CMAKE_INSTALL_BINDIR}/yeetd")
    else()
        message(FATAL_ERROR "yeet_ROOT: yeet binary file not found in ${yeet_ROOT}/${CMAKE_INSTALL_BINDIR}")
    endif()
endif()
