#[=======================================================================[.rst:
Findortools
--------

This module determines the ortools library of the system.

IMPORTED Targets
^^^^^^^^^^^^^^^^

This module defines :prop_tgt:`IMPORTED` target ``ortools::ortools``, if
ortools has been found.

Result Variables
^^^^^^^^^^^^^^^^

This module defines the following variables:

::

ortools_FOUND          - True if ortools found.

Hints
^^^^^

A user may set ``ortools_ROOT`` to an ortools installation root to tell this
module where to look.
#]=======================================================================]

include(FindPackageHandleStandardArgs)

# first search for cmake based install
find_package(ortools QUIET CONFIG)

# if found, we're done
if(ortools_FOUND)
    find_package_handle_standard_args(ortools CONFIG_MODE)
    return()
endif()

set(ortools_FOUND FALSE)

if(NOT ortools_ROOT)
    set(ortools_ROOT $ENV{ortools_ROOT})
endif()

message(STATUS "ortools_ROOT: ${ortools_ROOT}")

if(ortools_ROOT)
    set(ortools_FOUND TRUE)
endif()

include(GNUInstallDirs)

if(ortools_FOUND AND NOT TARGET ortools::ortools)
    if(EXISTS "${ortools_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ortools${CMAKE_SHARED_LIBRARY_SUFFIX}")
        add_library(ortools::ortools SHARED IMPORTED)

        set_target_properties(ortools::ortools PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${ortools_ROOT}/include"
            IMPORTED_LOCATION "${ortools_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ortools${CMAKE_SHARED_LIBRARY_SUFFIX}"
            INTERFACE_COMPILE_DEFINITIONS "OR_TOOLS_AS_DYNAMIC_LIB"
        )
    elseif(EXISTS "${ortools_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}ortools${CMAKE_STATIC_LIBRARY_SUFFIX}")
        add_library(ortools::ortools STATIC IMPORTED)

        set_target_properties(ortools::ortools PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${ortools_ROOT}/include"
            IMPORTED_LOCATION "${ortools_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}ortools${CMAKE_STATIC_LIBRARY_SUFFIX}"
        )
    else()
        message(FATAL_ERROR "ortools_ROOT: ortools library file not found")
    endif()
endif()
