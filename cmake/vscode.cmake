# #############################
#
# Patch things for better VSCode usage
#
# Basically optionally patch a template workspace file with proper include paths and definitions
#
# #############################

cmake_minimum_required(VERSION 3.19)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

include(utils)

function(patch_config_file target filepath)
    if(TARGET ${target})
        get_target_property(target_LINKS ${target} INTERFACE_LINK_LIBRARIES)
        get_target_property(target_C_STANDARD ${target} C_STANDARD)
        get_target_property(target_CXX_STANDARD ${target} CXX_STANDARD)

        if(target_LINKS STREQUAL "target_LINKS-NOTFOUND")
            set(target_LINKS "")
        endif()

        foreach(subtarget IN LISTS ${target} target_LINKS)
            if(TARGET ${subtarget})
                get_target_property(subtarget_DEFS ${subtarget} INTERFACE_COMPILE_DEFINITIONS)

                if(subtarget_DEFS STREQUAL "subtarget_DEFS-NOTFOUND")
                    set(subtarget_DEFS "")
                endif()

                list(APPEND PROJECT_DEFINES ${subtarget_DEFS})
            endif()
        endforeach()

        list(REMOVE_DUPLICATES PROJECT_INCLUDES)
    else()
        set(target_CXX_STANDARD 20)
        set(target_C_STANDARD 11)
        set(PROJECT_DEFINES "")
    endif()

    list_to_json_array(PROJECT_DEFINES PROJECT_DEFINES)

    if(EXISTS ${filepath})
        message(STATUS "Updating existing c_cpp_properties file")

        # Read existing file
        file(READ ${filepath} CONFIG_FILE)

        string(JSON CONFIGURATIONS_EXIST ERROR_VARIABLE MSG GET ${CONFIG_FILE} configurations)

        if(CONFIGURATIONS_EXIST STREQUAL "configurations-NOTFOUND")
            # create settings key first if it does not exist yet
            string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations "[]")
            set(CONFIGURATIONS_EXIST "[]")
        endif()

        json_array_to_list(CONFIGURATIONS_EXIST configs)

        list(LENGTH configs len)
        set(curr_index 0)

        while(curr_index LESS len)
            list(GET configs ${curr_index} elem)
            string(JSON name GET ${elem} name)

            if(name STREQUAL "lola")
                break()
            endif()

            math(EXPR curr_index ${curr_index}+1)
        endwhile()

        if(curr_index EQUAL len)
            # entry does not exist yet
            string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} "{}")
            string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} name "\"lola\"")
        endif()

        string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} cStandard "\"c${target_C_STANDARD}\"")
        string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} includePath "[\"\${workspaceFolder}/**\"]")
        string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} compileCommands "\"${CMAKE_BINARY_DIR}/compile_commands.json\"")

        if(NOT CMAKE_HOST_APPLE)
            # on MacOS, cmake per default finds the xcode/cmdline tools c/cxx compiler
            # no idea what magic cmake does to make things work, but autotools and vscode don't like that one
            # therefore we only save the compiler for other platforms and let vscode query for MacOS
            # since we also don't overwrite, the user can simply provide their own if things don't work
            string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} compilerPath "\"${CMAKE_CXX_COMPILER}\"")
        endif()

        string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} cppStandard "\"c++${target_CXX_STANDARD}\"")
        string(JSON CONFIG_FILE SET ${CONFIG_FILE} configurations ${curr_index} defines ${PROJECT_DEFINES})

        # Write changed file
        file(WRITE ${filepath} ${CONFIG_FILE})

    else()
        message(STATUS "Generating c_cpp_properties file")

        if(CMAKE_HOST_APPLE)
            set(COMPILER_PATH "")
        else()
            set(COMPILER_PATH "\"compilerPath\": \"${CMAKE_CXX_COMPILER}\",")
        endif()

        configure_file(
            ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/editor/c_cpp_properties.json.in
            ${filepath}
            @ONLY
        )
    endif()
endfunction()

function(patch_default_config_file)
    set(CMAKE_BINARY_DIR ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/../build)
    cmake_path(ABSOLUTE_PATH CMAKE_BINARY_DIR NORMALIZE)

    # Set compilerpath to default
    set(CMAKE_CXX_COMPILER "")
    patch_config_file("" ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/../.vscode/c_cpp_properties.json)
endfunction()

# if we're in script mode, we cannot inherit variables from a build system, so we use defaults
if(CMAKE_SCRIPT_MODE_FILE)
    # this is dispatched to a function to use CMAKE_CURRENT_FUNCTION_LIST_DIR to get the exact path to this script file
    # since even in script mode this file could be included, CMAKE_CURRENT_LIST_DIR may not be set to the correct folder
    patch_default_config_file()
endif()
