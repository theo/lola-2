# #############################
#
# Enable external libraries
#
# Basically we search for pre-installed libraries, if they're not found
# we build them ourselves at configure time to isolate LoLA build
# (since dependencies don't need to be checked again after building once)
#
# #############################

include(CMakeDependentOption)
include(utils)

# #############################
#
# Threads library
#
# #############################
set(CMAKE_THREAD_PREFER_PTHREAD ON)
set(THREAD_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

if(NOT CMAKE_USE_PTHREADS_INIT)
    message(FATAL_ERROR "LoLA uses pthread as a thread library, but pthread was not found.")
endif()

# #############################
#
# Prefix Path Settings
#
# #############################

# at this point, this is only defined (as a cache variable) if it's given via -D -> don't add _deps
if(NOT DEFINED CACHE{CMAKE_PREFIX_PATH})
    set(CMAKE_PREFIX_PATH_GIVEN FALSE)
else()
    set(CMAKE_PREFIX_PATH_GIVEN TRUE)
endif()

# Fix to allow relative paths in prefix path
# According to cmake docs its empty per default and setting a value
# on the commandline overrides this anyway, so it should be fine?
set(CMAKE_PREFIX_PATH "" CACHE PATH "CMake prefix path to search for packages")

if(NOT CMAKE_PREFIX_PATH_GIVEN)
    set(DEPENDENCIES_INSTALL_DIR ${CMAKE_CURRENT_LIST_DIR}/../_deps)
    cmake_path(NORMAL_PATH DEPENDENCIES_INSTALL_DIR)

    # Prepend install path to prefix path so that we can find already installed versions
    # we prepend so that locally installed versions have precedence over system installed versions
    list(PREPEND CMAKE_PREFIX_PATH ${DEPENDENCIES_INSTALL_DIR})
endif()

# To allow passing a parent directory containing all dependencies, we iterate
# through the CMAKE_PREFIX_PATH and append all subdirs of each entry
# into CMAKE_PREFIX_PATH
collect_subdirs(CMAKE_PREFIX_PATH)

# #############################
#
# Search for libraries and programs
#
# #############################

# Options
option(LOLA_BUILD_DEPS "Whether to build dependency libraries or not" OFF)
option(USE_YEET "Use yeet instead of kimwitu++ for term processing" ON)

cmake_dependent_option(BUILD_ortools "Whether to build ortools or not" OFF "NOT LOLA_BUILD_DEPS" ON)
cmake_dependent_option(BUILD_fmt "Whether to build fmt or not" OFF "NOT LOLA_BUILD_DEPS" ON)
cmake_dependent_option(BUILD_spdlog "Whether to build spdlog or not" OFF "NOT LOLA_BUILD_DEPS" ON)
cmake_dependent_option(BUILD_yeet "Whether to build yeet or not" OFF "NOT (USE_YEET AND LOLA_BUILD_DEPS)" ON)

search_dep(ortools VERSION 9.4)
search_dep(fmt VERSION 9.0.0)
search_dep(spdlog VERSION 1.11.0)

if(USE_YEET)
    search_dep(yeet VERSION 0.3.0)
endif()

# #############################
#
# Build necessary libraries and programs
#
# #############################
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/deps)
