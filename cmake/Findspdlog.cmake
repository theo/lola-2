#[=======================================================================[.rst:
Findspdlog
--------

This module determines the spdlog library of the system.

IMPORTED Targets
^^^^^^^^^^^^^^^^

This module defines :prop_tgt:`IMPORTED` target ``spdlog::spdlog``, if
spdlog has been found.

Result Variables
^^^^^^^^^^^^^^^^

This module defines the following variables:

::

spdlog_FOUND          - True if spdlog found.

Hints
^^^^^

A user may set ``spdlog_ROOT`` to an spdlog installation root to tell this
module where to look.
#]=======================================================================]

include(FindPackageHandleStandardArgs)

# first search for cmake based install
find_package(spdlog QUIET CONFIG)

# if found, we're done
if(spdlog_FOUND)
    find_package_handle_standard_args(spdlog CONFIG_MODE)
    return()
endif()

set(spdlog_FOUND FALSE)

if(NOT spdlog_ROOT)
    set(spdlog_ROOT $ENV{spdlog_ROOT})
endif()

message(STATUS "spdlog_ROOT: ${spdlog_ROOT}")

if(spdlog_ROOT)
    set(spdlog_FOUND TRUE)
endif()

include(GNUInstallDirs)

if(spdlog_FOUND AND NOT TARGET spdlog::spdlog)
    if(EXISTS "${spdlog_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}spdlog${CMAKE_SHARED_LIBRARY_SUFFIX}")
        add_library(spdlog::spdlog SHARED IMPORTED)

        set_target_properties(spdlog::spdlog PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${spdlog_ROOT}/include"
            IMPORTED_LOCATION "${spdlog_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}spdlog${CMAKE_SHARED_LIBRARY_SUFFIX}"
            INTERFACE_COMPILE_DEFINITIONS "SPDLOG_SHARED_LIB;SPDLOG_COMPILED_LIB"
        )
    elseif(EXISTS "${spdlog_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}spdlog${CMAKE_STATIC_LIBRARY_SUFFIX}")
        add_library(spdlog::spdlog STATIC IMPORTED)

        set_target_properties(spdlog::spdlog PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${spdlog_ROOT}/include"
            IMPORTED_LOCATION "${spdlog_ROOT}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_STATIC_LIBRARY_PREFIX}spdlog${CMAKE_STATIC_LIBRARY_SUFFIX}"
            INTERFACE_COMPILE_DEFINITIONS "SPDLOG_COMPILED_LIB"
        )
    else()
        message(FATAL_ERROR "spdlog_ROOT: spdlog library file not found")
    endif()
endif()
