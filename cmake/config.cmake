# #############################
#
# Configuration
# Simulate custom checks from old configure.ac
#
# #############################
message(STATUS "Starting configuration.")

cmake_host_system_information(RESULT CONFIG_HOSTNAME QUERY HOSTNAME)
cmake_host_system_information(RESULT CONFIG_BUILDSYSTEM QUERY OS_NAME)
set(PACKAGE "lola")
set(PACKAGE_VERSION ${PROJECT_VERSION})

option(LOLA_BENCHMARK "whether to enable benchmarking things in lola" OFF)

# Search for awk and grep for their macros, used by lola
# AC_PROG_AWK checks for gawk, mawk, nawk and awk and prefers that order
find_program(AWK awk)
find_program(NAWK nawk)
find_program(MAWK mawk)
find_program(GAWK gawk)

if(GAWK)
    set(TOOL_AWK ${GAWK})
elseif(MAWK)
    set(TOOL_AWK ${MAWK})
elseif(NAWK)
    set(TOOL_AWK ${NAWK})
elseif(AWK)
    set(TOOL_AWK ${AWK})
else()
    message(WARNING "LoLA may require awk (or any of the derivatives gawk, mawk, nawk), which was not found.")
endif()

# AC_PROG_GREP in theory does more than just check existence
# It also checks for support of multiple '-e' options and which
# program supports the longest lines.
# LoLA doesn't use multiple '-e' and I don't think line length is
# important for us, so I didn't put in the work to support that.
# If one wants to support that, an outline of /usr/share/autoconf/autoconf/programs.m4 is below
find_program(GREP grep)
find_program(GGREP ggrep)

if(GGREP)
    set(TOOL_GREP ${GGREP})
elseif(GREP)
    set(TOOL_GREP ${GREP})
else()
    message(WARNING "LoLA may require grep (or the derivative ggrep), which was not found.")
endif()

# AC_PROG_GREP checks multiple '-e' by using grep and ggrep with arguments "-e 'GREP$' -e '-(cannot match)-'"
# on an input file. The maximum length is checked by creating a temp file, first with 10 characters ('0123456789'),
# which expands by printing the file twice and piping that back into the file, effectively doubling the line length.
# 'GREP' (without quotes) and a newline is then appended to the end of the file. If the call with that file succeeds,
# the length x (for 10*2^x) is saved, before trying the next size. The max size is ~10000 characters (10*2^10).
# The preferred tool by autotools is ggrep
message(STATUS "Finished configuration.")

# Copy config into build, thereby replacing the strings with values set above
configure_file(
    ${CMAKE_CURRENT_LIST_DIR}/config.h.in
    ${CMAKE_CURRENT_BINARY_DIR}/lolaconf.h
)
