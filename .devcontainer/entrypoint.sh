#!/bin/sh

set -e

# setup optional password for lola-dev user
# taken from entrypoint script of nextcloud docker image
# https://github.com/nextcloud/docker/blob/master/docker-entrypoint.sh
pw_value=$(env | grep -E "^USER_PASSWORD=" | sed -E -e "s/^USER_PASSWORD=//")
pw_file_value=$(env | grep -E "^USER_PASSWORD_FILE=" | sed -E -e "s/^USER_PASSWORD_FILE=//")
if [ -n "${pw_value}" ] && [ -n "${pw_file_value}" ]; then
    echo >&2 "error: both password and password file set, but they're exclusive"
    exit 1
fi
if [ -n "${pw_file_value}" ]; then
    USER_PASSWORD="$(cat "${pw_file_value}")"
fi
# change password if set
if [ -n "${USER_PASSWORD}" ]; then
    echo "lola-dev:${USER_PASSWORD}" | chpasswd
fi

# change user shell if desired
if [ -n "${USER_SHELL}" ]; then
    USER_SHELL_PATH=$(grep -E "${USER_SHELL}" /etc/shells | head -n1)
    if [ -n "${USER_SHELL_PATH}" ]; then
        chsh -s "${USER_SHELL_PATH}" lola-dev
        # just change for root aswell, who cares
        chsh -s "${USER_SHELL_PATH}"
    else
        echo "Shell ${USER_SHELL} not installed, using default"
    fi
fi

# assume USER_UID as USER_GID if the latter is unset
if [ -z "${USER_GID}" ]; then
    USER_GID="${USER_UID}"
fi
# change GID of lola-dev group
if [ "${USER_GID:-0}" != "0" ]; then
    USER_GID_STRING="-g ${USER_GID}"
    groupmod "${USER_GID_STRING}" lola-dev
fi

# don't set uid if it's root
if [ "${USER_UID:-0}" != "0" ]; then
    USER_UID_STRING="${USER_UID}"
fi

if [ -n "${USER_UID_STRING}" ]; then
    USER_UID_STRING="-u ${USER_UID_STRING}"
fi

if [ -n "${USER_UID_STRING}" ] || [ -n "${USER_GID_STRING}" ]; then
    usermod "${USER_UID_STRING}" "${USER_GID_STRING}" lola-dev
fi

USER_UID=$(id -u lola-dev)
USER_GID=$(id -g lola-dev)

IFS=","
for path in ${USER_PATHS}; do
    chown --preserve-root "${USER_UID}:${USER_GID}" "${path}"
done

# change to user if requested user is not root
# either pass the CMD from docker as entry or use shell session read from passwd
if [ "${USER_UID}" != "0" ]; then
    export HOME=/home/lola-dev
    cd $HOME
    exec gosu lola-dev "${@:-$(getent passwd lola-dev | awk -F: '{print $NF}')}"
else
    exec "${@:-$(getent passwd lola-dev | awk -F: '{print $NF}')}"
fi
