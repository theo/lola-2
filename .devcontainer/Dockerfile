# syntax=docker/dockerfile:1
FROM gcc:14 AS build-deps
ENV DEBIAN_FRONTEND=noninteractive
ADD https://git.informatik.uni-rostock.de/theo/lola-2.git#docker-v0.2 /lola-2
WORKDIR /lola-2
RUN <<EOI
apt-get --yes update
apt-get --yes install git cmake gengetopt flex bison ninja-build
rm -rf /var/lib/apt/lists/*
EOI
RUN <<EOI
cmake -Bbuild -S. -G "Ninja Multi-Config" -DLOLA_BUILD_DEPS=ON
EOI
RUN <<EOI
cmake --build build --config Release
EOI
RUN <<EOI
cmake --install build --prefix /app --config Release
rm -rf build
EOI

FROM gcc:14 AS lola-dev
COPY --from=build-deps /app /usr/local
COPY entrypoint.sh /usr/local/bin/
RUN <<EOI
export DEBIAN_FRONTEND=noninteractive
apt-get --yes update
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
apt-get --yes install autoconf automake bat bison cmake clang-format-16 clang-tidy-16 dos2unix flex gdb gengetopt git git-lfs gosu htop libtool linux-perf nano ninja-build python3 python3-venv pipx sudo valgrind vim zsh
rm -rf /var/lib/apt/lists/*
curl -OL https://github.com/pypa/pipx/releases/download/1.7.1/pipx.pyz
python3 pipx.pyz install --global pre-commit
rm pipx.pyz
ln -s /usr/bin/clang-format-16 /usr/bin/clang-format
ln -s /usr/bin/clang-tidy-16 /usr/bin/clang-tidy
groupadd --gid 1000 lola-dev
useradd --uid 1000 --gid 1000 --groups sudo --create-home lola-dev
dos2unix /usr/local/bin/entrypoint.sh
EOI
ENTRYPOINT [ "entrypoint.sh" ]
CMD ["sleep", "infinity"]
